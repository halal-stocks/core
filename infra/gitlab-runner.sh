helm repo add gitlab https://charts.gitlab.io
helm repo update
if ! kubectl get namespace gitlab-runner; then
  kubectl create namespace gitlab-runner
fi
sleep 5
# For Helm 3
helm upgrade -i gitlab-runner gitlab/gitlab-runner --namespace gitlab-runner -f gitlab-runner-conf.yaml 
