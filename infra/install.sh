helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-rbac.yaml
helm upgrade -i traefik stable/traefik --set rbac.enabled=true --set=ssl.enabled=true --namespace kube-system --version=1.86.1

kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/v0.13.0/deploy/manifests/00-crds.yaml
if ! kubectl get namespace cert-manager; then
  kubectl create namespace cert-manager
fi

helm upgrade -i cert-manager --namespace cert-manager --version v0.13.0 jetstack/cert-manager
helm upgrade -i traefik-issuer ./cluster-issuer --namespace cert-manager