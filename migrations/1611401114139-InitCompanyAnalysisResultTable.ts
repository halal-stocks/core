import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitCompanyAnalysisResultTable1611401114139 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'company_analysis_result',
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    isPrimary: true,
                    isUnique: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                { name: 'current_assets_minus_liabilities', type: 'numeric(20, 2)', isNullable: true },
                { name: 'free_cash_flow', type: 'numeric(20, 2)', isNullable: true },
                { name: 'net_profit_margin', type: 'numeric(20, 2)', isNullable: true },
                { name: 'beta', type: 'numeric(15, 5)', isNullable: true },
                { name: 'peg_ratio', type: 'numeric(15, 5)', isNullable: true },
                { name: 'pb_ratio', type: 'numeric(15, 5)', isNullable: true },
                { name: 'ps_ratio', type: 'numeric(15, 5)', isNullable: true },
                { name: 'graham_ratio', type: 'numeric(15, 5)', isNullable: true },
                { name: 'payout_ratio', type: 'numeric(15, 5)', isNullable: true },
                { name: 'dividend_yield', type: 'numeric(15, 5)', isNullable: true },
                { name: 'roa', type: 'numeric(15, 5)', isNullable: true },
                { name: 'roe', type: 'numeric(15, 5)', isNullable: true },
                { name: 'pe_ratio', type: 'numeric(15, 5)', isNullable: true },
                { name: 'eps', type: 'numeric(15, 5)', isNullable: true },
                { name: 'reported_at', type: 'timestamp with time zone' },
                { name: 'created_at', type: 'timestamp with time zone', default: 'now()' },
                { name: 'updated_at', type: 'timestamp with time zone', isNullable: true },
                { name: 'deleted_at', type: 'timestamp with time zone', isNullable: true },
                { name: 'company_id', type: 'uuid' },
            ],
            foreignKeys: [
                {
                    columnNames: ['company_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'company',
                    onDelete: 'CASCADE',
                    onUpdate: 'CASCADE',
                },
            ],
            indices: [
                { columnNames: ['current_assets_minus_liabilities'] },
                { columnNames: ['beta'] },
                { columnNames: ['peg_ratio'] },
                { columnNames: ['pb_ratio'] },
                { columnNames: ['ps_ratio'] },
                { columnNames: ['graham_ratio'] },
                { columnNames: ['free_cash_flow'] },
                { columnNames: ['payout_ratio'] },
                { columnNames: ['dividend_yield'] },
                { columnNames: ['net_profit_margin'] },
                { columnNames: ['roa'] },
                { columnNames: ['roe'] },
                { columnNames: ['pe_ratio'] },
                { columnNames: ['eps'] },
                { columnNames: ['company_id'] },
            ],
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable(
          'company_analysis_result',
          true,
          true,
          true,
        )
    }

}
