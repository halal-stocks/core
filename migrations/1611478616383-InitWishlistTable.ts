import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitUserWishlist1611478616383 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'wishlist',
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    isPrimary: true,
                    isUnique: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                { name: 'company_id', type: 'uuid' },
                { name: 'user_id', type: 'uuid' },
            ],
            foreignKeys: [
                {
                    columnNames: ['company_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'company',
                },
                {
                    columnNames: ['user_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'user',
                    onDelete: 'CASCADE',
                    onUpdate: 'CASCADE',
                },
            ],
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable(
          'user_watch_list',
          true,
          true,
          true,
        )
    }

}
