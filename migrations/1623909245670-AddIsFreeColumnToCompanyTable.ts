import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm'

export class AddIsFreeColumnToCompanyTable1623909245670 implements MigrationInterface {
    private tableName = 'company'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(this.tableName, new TableColumn({
            name: 'is_free',
            type: 'boolean',
            default: false,
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn(this.tableName, 'is_free')
    }

}
