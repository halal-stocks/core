import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class AddLogsTable1640242113554 implements MigrationInterface {
  private tableName = 'logs'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: this.tableName,
      columns: [
        { name: 'id', type: 'uuid', isPrimary: true, isUnique: true, isGenerated: true, generationStrategy: 'uuid' },
        { name: 'user_firebase_id', type: 'varchar', isNullable: true },
        { name: 'status', type: 'smallint', isNullable: true },
        { name: 'request', type: 'json', isNullable: true },
        { name: 'response', type: 'json', isNullable: true },
        { name: 'error', type: 'json', isNullable: true },
        { name: 'created_at', type: 'timestamp with time zone', default: 'now()' },
      ],
      foreignKeys: [
        {
          columnNames: ['user_firebase_id'],
          referencedColumnNames: ['firebase_id'],
          referencedTableName: 'user',
          onDelete: 'CASCADE',
        },
      ],
      indices: [
        { columnNames: ['user_firebase_id'] },
        { columnNames: ['status'] },
      ],
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.tableName, true, true, true)
  }

}
