import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm'

export class UpdateUserTable1626612097787 implements MigrationInterface {
  private tableName = 'user'

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.addColumns(this.tableName, [
      new TableColumn({
        name: 'paid_till',
        type: 'timestamp with time zone',
        isNullable: true,
      }),
    ])
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropColumn(this.tableName, 'paid_till')
  }

}
