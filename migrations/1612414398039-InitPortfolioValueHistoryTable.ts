import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitPortfolioValueHistoryTable1612414398039 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'portfolio_value_history',
      columns: [
        {
          name: 'id',
          type: 'uuid',
          isPrimary: true,
          isUnique: true,
          isGenerated: true,
          generationStrategy: 'uuid',
        },
        { name: 'value', type: 'numeric(15,2)' },
        { name: 'snapshotted_at', type: 'date', default: 'now()' },
        { name: 'user_id', type: 'uuid' },
      ],
      foreignKeys: [
        {
          columnNames: ['user_id'],
          referencedColumnNames: ['id'],
          referencedTableName: 'user',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      ],
      indices: [
        { columnNames: ['snapshotted_at'] },
      ],
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('portfolio_value_history', true, true, true)
  }

}
