import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitUserPortfolioTable1611161207913 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'user_portfolio',
      columns: [
        {
          name: 'id',
          type: 'uuid',
          isPrimary: true,
          isUnique: true,
          isGenerated: true,
          generationStrategy: 'uuid',
        },
        { name: 'bought_stock_price', type: 'numeric(15, 2)' },
        { name: 'quantity', type: 'int' },
        { name: 'created_at', type: 'timestamp with time zone', default: 'now()' },
        { name: 'updated_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'deleted_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'user_id', type: 'uuid' },
        { name: 'company_id', type: 'uuid' },
      ],
      foreignKeys: [
        {
          columnNames: ['user_id'],
          referencedColumnNames: ['id'],
          referencedTableName: 'user',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
        {
          columnNames: ['company_id'],
          referencedColumnNames: ['id'],
          referencedTableName: 'company',
        },
      ],
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable(
      'user_portfolio',
      true,
      true,
      true,
    )
  }

}
