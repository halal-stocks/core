import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitCompanyIndicatorsTable1611160254071 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'company_indicators',
      columns: [
        {
          name: 'id',
          type: 'uuid',
          isPrimary: true,
          isUnique: true,
          isGenerated: true,
          generationStrategy: 'uuid',
        },
        { name: 'actual_stock_price', type: 'numeric(15, 5)' },
        { name: 'actual_market_cap', type: 'numeric(20, 2)' },
        { name: 'previous_day_stock_price', type: 'numeric(15, 5)' },
        { name: 'updated_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'company_id', type: 'uuid' },
      ],
      foreignKeys: [
        {
          columnNames: ['company_id'],
          referencedColumnNames: ['id'],
          referencedTableName: 'company',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      ],
      indices: [
        { columnNames: ['actual_stock_price'] },
        { columnNames: ['actual_market_cap'] },
        { columnNames: ['previous_day_stock_price'] },
        { columnNames: ['company_id'] },
      ],
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable(
      'company_indicators',
      true,
      true,
      true,
    )
  }

}
