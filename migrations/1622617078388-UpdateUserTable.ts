import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm'

export class UpdateUserTable1622617078388 implements MigrationInterface {
  private tableName = 'user'

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropColumn(this.tableName, 'first_name')
    await queryRunner.dropColumn(this.tableName, 'last_name')
    await queryRunner.dropColumn(this.tableName, 'password')
    await queryRunner.dropColumn(this.tableName, 'email_verified_at')
    await queryRunner.dropColumn(this.tableName, 'last_login')

    await queryRunner.addColumns(this.tableName, [
      new TableColumn({ name: 'firebase_id', type: 'varchar', isUnique: true, isNullable: true }),
    ])
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.addColumns(this.tableName, [
      new TableColumn({ name: 'first_name', type: 'varchar', length: '255' }),
      new TableColumn({ name: 'last_name', type: 'varchar', length: '255' }),
      new TableColumn({ name: 'password', length: '255', type: 'varchar', isNullable: true }),
      new TableColumn({ name: 'email_verified_at', type: 'timestamp with time zone', isNullable: true }),
      new TableColumn({ name: 'last_login', type: 'timestamp with time zone', isNullable: true }),
    ])

    await queryRunner.dropColumn(this.tableName, 'firebase_id')
  }

}
