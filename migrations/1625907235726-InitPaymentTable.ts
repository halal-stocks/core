import { MigrationInterface, QueryRunner, Table } from 'typeorm'
import { Currency } from '../src/consts/currency.enum'
import { PaymentStatus } from '../src/domains/payment/payment.interfaces'

export class InitPaymentTable1625907235726 implements MigrationInterface {
  private tableName = 'payment'

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: this.tableName,
      columns: [
        { name: 'id', type: 'uuid', isPrimary: true, isUnique: true, isGenerated: true, generationStrategy: 'uuid' },
        { name: 'invoice_id', type: 'int', isPrimary: true, isUnique: true, isGenerated: true, generationStrategy: 'increment' },
        { name: 'user_id', type: 'uuid' },
        { name: 'status', type: 'enum', enum: Object.values(PaymentStatus), default: `'${PaymentStatus.PENDING}'` },
        { name: 'payment_url', type: 'varchar' },
        { name: 'is_active_recurrent_parent', type: 'boolean', default: false },
        { name: 'parent_payment_id', type: 'uuid', isNullable: true },
        { name: 'price_id', type: 'uuid' },
        { name: 'currency', type: 'enum', enum: Object.values(Currency) },
        { name: 'price', type: 'numeric(10, 2)' },
        { name: 'created_at', type: 'timestamp with time zone', default: 'now()' },
        { name: 'updated_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'deleted_at', type: 'timestamp with time zone', isNullable: true },
      ],
      foreignKeys: [
        {
          columnNames: ['user_id'],
          referencedColumnNames: ['id'],
          referencedTableName: 'user',
          onDelete: 'CASCADE',
        },
        {
          columnNames: ['parent_payment_id'],
          referencedColumnNames: ['id'],
          referencedTableName: this.tableName,
          onDelete: 'CASCADE',
        },
      ],
      indices: [
        { columnNames: ['user_id'] },
      ],
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable(this.tableName, true, true, true)
  }

}
