import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitCompanyTable1611025478039 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'company',
      columns: [
        {
          name: 'id',
          type: 'uuid',
          isPrimary: true,
          isUnique: true,
          isGenerated: true,
          generationStrategy: 'uuid',
        },
        { name: 'name', type: 'varchar' },
        { name: 'ticker', type: 'varchar' },
        { name: 'exchange', type: 'varchar' },
        { name: 'sector', type: 'varchar', isNullable: true },
        { name: 'is_halal', type: 'boolean', isNullable: true },
        { name: 'created_at', type: 'timestamp with time zone', default: 'now()' },
        { name: 'updated_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'deleted_at', type: 'timestamp with time zone', isNullable: true },
      ],

    }), true)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
  }

}
