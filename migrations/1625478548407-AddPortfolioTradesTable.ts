import { MigrationInterface, QueryRunner, Table, TableColumn } from 'typeorm'
import { TableUnique } from 'typeorm/schema-builder/table/TableUnique'

export class AddPortfolioTradesTable1625478548407 implements MigrationInterface {
  private portfolioTableName = 'user_portfolio'
  private tradeTableName = 'portfolio_trade'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(this.portfolioTableName, 'bought_stock_price')
    await queryRunner.dropColumn(this.portfolioTableName, 'quantity')
    await queryRunner.dropColumn(this.portfolioTableName, 'deleted_at')

    await queryRunner.createUniqueConstraint(this.portfolioTableName, new TableUnique({
      columnNames: ['company_id', 'user_id'],
    }))

    await queryRunner.createTable(new Table({
      name: this.tradeTableName,
      columns: [
        { name: 'id', type: 'uuid', isPrimary: true, isUnique: true, isGenerated: true, generationStrategy: 'uuid' },
        { name: 'entry_stock_price', type: 'numeric(15, 2)' },
        { name: 'quantity', type: 'numeric(20, 4)' },
        { name: 'traded_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'portfolio_id', type: 'uuid' },
      ],
      foreignKeys: [
        {
          columnNames: ['portfolio_id'],
          referencedColumnNames: ['id'],
          referencedTableName: this.portfolioTableName,
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      ],
      indices: [
        { columnNames: ['portfolio_id'] },
      ],
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns(this.portfolioTableName, [
      new TableColumn({ name: 'bought_stock_price', type: 'numeric(15, 2)', isNullable: true }),
      new TableColumn({ name: 'quantity', type: 'int', isNullable: true }),
      new TableColumn({ name: 'deleted_at', type: 'timestamp with time zone', isNullable: true }),
    ])

    await queryRunner.dropUniqueConstraint(this.portfolioTableName, new TableUnique({
      columnNames: ['company_id', 'user_id'],
    }))

    await queryRunner.dropTable(this.tradeTableName, true, true, true)
  }

}
