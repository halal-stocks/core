import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm'

export class UpdateCompanyTable1619532038537 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.addColumns('company', [
      new TableColumn({ name: 'description', type: 'varchar', isNullable: true }),
      new TableColumn({ name: 'full_time_employees', type: 'int', isNullable: true }),
      new TableColumn({ name: 'industry', type: 'varchar', isNullable: true }),
    ])
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropColumn('company', 'description')
    await queryRunner.dropColumn('company', 'full_time_employees')
    await queryRunner.dropColumn('company', 'industry')
  }

}
