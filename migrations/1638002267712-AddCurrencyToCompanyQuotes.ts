import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm'

export class AddCurrencyToCompanyQuotes1638002267712 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TYPE currency_type as ENUM ('RUB', 'USD', 'EUR')`)
    await queryRunner.addColumn('company_analysis_result', new TableColumn({
      name: 'currency',
      type: 'currency_type',
      default: `'USD'::currency_type`,
      isNullable: false,
    }))
    await queryRunner.addColumn('company_indicators', new TableColumn({
      name: 'currency',
      type: 'currency_type',
      default: `'USD'::currency_type`,
      isNullable: false,
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('company_analysis_result', 'currency')
    await queryRunner.dropColumn('company_indicators', 'currency')
    await queryRunner.query('DROP TYPE currency_type')
  }

}
