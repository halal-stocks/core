import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitStockMarketIndexTable1611120553744 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'stock_market_index',
      columns: [
        {
          name: 'id',
          type: 'uuid',
          isPrimary: true,
          isUnique: true,
          isGenerated: true,
          generationStrategy: 'uuid',
        },
        { name: 'name', type: 'varchar' },
        { name: 'code', type: 'varchar', isUnique: true },
      ],
    }), true)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('stock_market_index', true)
  }

}
