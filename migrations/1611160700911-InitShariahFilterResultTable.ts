import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitShariahFilterResultTable1611160700911 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'shariah_filter_result',
      columns: [
        {
          name: 'id',
          type: 'uuid',
          isPrimary: true,
          isUnique: true,
          isGenerated: true,
          generationStrategy: 'uuid',
        },
        { name: 'is_charter_valid', type: 'boolean', isNullable: true },
        { name: 'link_to_charter', type: 'varchar', isNullable: true },
        { name: 'charter_not_valid_reason', type: 'varchar', isNullable: true },
        { name: 'deposit_to_market_cap', type: 'numeric(15, 5)', isNullable: true },
        { name: 'dept_to_market_cap', type: 'numeric(15, 5)', isNullable: true },
        { name: 'forbidden_income', type: 'numeric(15, 5)', isNullable: true },
        { name: 'forbidden_income_per_share', type: 'numeric(15, 5)', isNullable: true },

        { name: 'other_current_assets', type: 'numeric(20, 2)', isNullable: true },
        { name: 'cash_and_short_term_investments', type: 'numeric(20, 2)', isNullable: true },
        { name: 'long_term_investments', type: 'numeric(20, 2)', isNullable: true },
        { name: 'market_cap_average', type: 'numeric(20, 2)', isNullable: true },
        { name: 'short_term_debt', type: 'numeric(20, 2)', isNullable: true },
        { name: 'other_current_liabilities', type: 'numeric(20, 2)', isNullable: true },
        { name: 'long_term_debt', type: 'numeric(20, 5)', isNullable: true },
        { name: 'shares_outstanding', type: 'numeric(20, 2)', isNullable: true },
        { name: 'interest_income', type: 'numeric(20, 2)', isNullable: true },
        { name: 'revenue', type: 'numeric(20, 2)', isNullable: true },

        { name: 'reported_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'checked_at', type: 'timestamp with time zone', default: 'now()' },
        { name: 'updated_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'deleted_at', type: 'timestamp with time zone', isNullable: true },
        { name: 'company_id', type: 'uuid' },
      ],
      foreignKeys: [
        {
          columnNames: ['company_id'],
          referencedColumnNames: ['id'],
          referencedTableName: 'company',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      ],
      indices: [
        { columnNames: ['company_id'] },
      ],
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
  }

}
