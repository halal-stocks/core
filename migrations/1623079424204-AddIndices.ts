import { MigrationInterface, QueryRunner, TableIndex } from 'typeorm'
import { Table } from 'typeorm/schema-builder/table/Table'

export class AddIndices1623079424204 implements MigrationInterface {
  private companyTableName = 'company'
  private companyStockMarketIndexesTableName = 'company_stock_market_indexes'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createIndices(this.companyTableName, [
      new TableIndex({ columnNames: ['ticker'] }),
      new TableIndex({ columnNames: ['exchange'] }),
      new TableIndex({ columnNames: ['sector'] }),
      new TableIndex({ columnNames: ['is_halal'] }),
    ])

    await queryRunner.createIndices(this.companyStockMarketIndexesTableName, [
      new TableIndex({ columnNames: ['company_id'] }),
    ])
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const companyTable = await queryRunner.getTable(this.companyTableName)
    if (companyTable) {
      await this.dropIndex(companyTable, 'ticker', queryRunner)
      await this.dropIndex(companyTable, 'exchange', queryRunner)
      await this.dropIndex(companyTable, 'sector', queryRunner)
      await this.dropIndex(companyTable, 'is_halal', queryRunner)
    }

    const companyStockMarketIndexesTable = await queryRunner.getTable(this.companyStockMarketIndexesTableName)
    if (companyStockMarketIndexesTable) {
      await this.dropIndex(companyStockMarketIndexesTable, 'company_id', queryRunner)
    }
  }

  private async dropIndex(table: Table, column: string, queryRunner: QueryRunner): Promise<void> {
    const index = table.indices.find(fk => (fk.columnNames.indexOf(column) !== -1))
    index && await queryRunner.dropIndex(table.name, index)
  }

}
