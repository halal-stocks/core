import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class InitCompanyStockMarketIndexesJoinTable1611121603342 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'company_stock_market_indexes',
      columns: [
        { name: 'company_id', type: 'uuid' },
        { name: 'stock_market_index_id', type: 'uuid' },
      ],
      foreignKeys: [
        {
          columnNames: ['company_id'],
          referencedColumnNames: ['id'],
          referencedTableName: 'company',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
        {
          columnNames: ['stock_market_index_id'],
          referencedColumnNames: ['id'],
          referencedTableName: 'stock_market_index',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      ],
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable(
      'company_stock_market_indexes',
      true,
      true,
      true,
    )
  }

}
