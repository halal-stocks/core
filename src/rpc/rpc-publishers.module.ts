import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq'
import { Module } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { CompanyPublisher } from './company/company.publisher'
import { PaymentPublisher } from './payment/payment.publisher'
import { UserPublisher } from './user/user.publisher'

@Module({
  imports: [
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      useFactory: async (configService: ConfigService) => ({
        exchanges: [
          {
            name: 'company',
            type: 'topic',
          },
          {
            name: 'payment',
            type: 'topic',
          },
          {
            name: 'user',
            type: 'topic',
          },
        ],
        prefetchCount: 1,
        uri: configService.get('RABBITMQ_URI')!,
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [
    CompanyPublisher,
    PaymentPublisher,
    UserPublisher,
  ],
  exports: [
    RpcPublishersModule,
    CompanyPublisher,
    PaymentPublisher,
    UserPublisher,
  ],
})
export class RpcPublishersModule {}
