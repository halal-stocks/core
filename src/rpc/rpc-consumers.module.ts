import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { CompanyDomainModule } from '../domains/company/company.domain-module'
import { PaymentDomainModule } from '../domains/payment/payment.domain-module'
import { UserDomainModule } from '../domains/user/user.domain-module'
import { CompanyConsumer } from './company/company.consumer'
import { CompanyScheduler } from './company/company.scheduler'
import { PaymentConsumer } from './payment/payment.consumer'
import { PaymentScheduler } from './payment/payment.scheduler'
import { UserConsumer } from './user/user.consumer'

@Module({
  imports: [
    CompanyDomainModule,
    ScheduleModule.forRoot(),
    UserDomainModule,
    PaymentDomainModule,
  ],
  providers: [
    CompanyConsumer,
    CompanyScheduler,
    UserConsumer,
    PaymentConsumer,
    PaymentScheduler,
  ],
  exports: [],
})
export class RpcConsumersModule {}
