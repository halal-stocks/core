import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Cron, CronExpression, SchedulerRegistry } from '@nestjs/schedule'
import { NodeEnv } from '../../consts/system.env'
import { PaymentDomain } from '../../domains/payment/payment.domain'

@Injectable()
export class PaymentScheduler {
  constructor(
    private configService: ConfigService,
    private schedulerRegistry: SchedulerRegistry,
    private paymentDomain: PaymentDomain,
  ) {}

  async onApplicationBootstrap() {
    const nodeEnv = this.configService.get('NODE_ENV')
    if (nodeEnv !== NodeEnv.production) {
      this.schedulerRegistry.getCronJob('checkUpcomingWithdraws')
        .stop()
    }
  }

  @Cron(CronExpression.EVERY_DAY_AT_10AM, { name: 'checkUpcomingWithdraws' })
  async checkUpcomingWithdraws(): Promise<void> {
    await this.paymentDomain.scheduleUpcomingWithdraw()
      .catch(e => console.error(`[CompanyScheduler.checkUpcomingWithdraws] => ${e}`))
  }

}
