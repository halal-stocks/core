import { RabbitRPC } from '@golevelup/nestjs-rabbitmq'
import { Injectable } from '@nestjs/common'
import { PaymentDomain } from '../../domains/payment/payment.domain'

@Injectable()
export class PaymentConsumer {
  constructor(
    private paymentDomain: PaymentDomain,
  ) {}

  @RabbitRPC({
    exchange: 'payment',
    routingKey: ['withdraw-needed'],
    queue: 'payment-withdraw-needed',
  })
  async withdrawSubscription(paymentId: string) {
    await this.paymentDomain.withdrawSubscription(paymentId)
      .catch(e => console.error(`[PaymentConsumer.withdrawSubscription] PaymentId: ${paymentId}  => ${e}`))

    return 42
  }

  @RabbitRPC({
    exchange: 'payment',
    routingKey: ['succeeded'],
    queue: 'cancel-other-subscription-starters',
  })
  async cancelOtherSubscriptionStarters(paymentId: string) {
    await this.paymentDomain.cancelOtherSubscriptionStarters(paymentId)
      .catch(e => console.error(`[PaymentConsumer.cancelOtherSubscriptionStarters] PaymentId: ${paymentId}  => ${e}`))

    return 42
  }
}
