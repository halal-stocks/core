import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { Injectable } from '@nestjs/common'

@Injectable()
export class PaymentPublisher {
  constructor(
    private readonly amqpConnection: AmqpConnection,
  ) {}

  async publishPaymentSucceededMessage(paymentId: string): Promise<void> {
    await this.amqpConnection.publish(
      'payment',
      'succeeded',
      paymentId,
    )
  }

  async publishWithdrawNeededMessage(paymentId: string): Promise<void> {
    await this.amqpConnection.publish(
      'payment',
      'withdraw-needed',
      paymentId,
    )
  }
}
