import { TickerType } from '../../consts/report.enum'

export class CompanyUpdateIndicatorsMessage {
  ticker: TickerType | TickerType[]
}

export class CompanyToCreateMessage {
  ticker: TickerType
  stockMarketIndexId?: string
}
