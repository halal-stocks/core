export const mockedCompanyPublisher = {
  publishCompanyCreatedMessage: () => {},
  publishCompanyToCreateMessage: () => {},
  publishNotFoundAnyCompanyByTickerMessage: () => {},
  publishUpdateFullAnalysisMessage: () => {},
  publishUpdateIndicatorsMessage: () => {},
}
