import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Cron, SchedulerRegistry } from '@nestjs/schedule'
import { CronExpression } from '@nestjs/schedule/dist/enums/cron-expression.enum'
import { NodeEnv } from '../../consts/system.env'
import { CompanyIndicatorsDomain } from '../../domains/company/indicators/indicators.domain'
import { StockMarketIndexDomain } from '../../domains/company/stock-market-index/stock-market-index.domain'

@Injectable()
export class CompanyScheduler {
  constructor(
    private configService: ConfigService,
    private companyIndicatorsDomain: CompanyIndicatorsDomain,
    private schedulerRegistry: SchedulerRegistry,
    private stockMarketIndexDomain: StockMarketIndexDomain,
  ) {}

  async onApplicationBootstrap() {
    const nodeEnv = this.configService.get('NODE_ENV')
    if (nodeEnv !== NodeEnv.production) {
      const updateIndicatorsToAllCompaniesCronJob = this.schedulerRegistry.getCronJob('updateIndicatorsToAllCompanies')
      updateIndicatorsToAllCompaniesCronJob.stop()

      const updateAllIndexesCronJob = this.schedulerRegistry.getCronJob('updateAllIndexes')
      updateAllIndexesCronJob.stop()
    }
  }

  @Cron('0 */10 * * * 1-5', { name: 'updateIndicatorsToAllCompanies' })
  async scheduleUpdateIndicatorsToAllCompanies(): Promise<void> {
    await this.companyIndicatorsDomain.updateIndicatorsToAllCompanies()
      .catch(e => console.error(`[CompanyScheduler.scheduleUpdateIndicatorsToAllCompanies] => ${e}`))
  }

  @Cron(CronExpression.EVERY_DAY_AT_4AM, { name: 'updateAllIndexes' })
  async updateAllIndexes(): Promise<void> {
    await this.stockMarketIndexDomain.updateAllIndexes()
      .catch(e => console.error(`[CompanyScheduler.updateAllIndexes] => ${e}`))
  }
}
