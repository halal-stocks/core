import { RabbitRPC } from '@golevelup/nestjs-rabbitmq'
import { Injectable } from '@nestjs/common'
import { TickerType } from '../../consts/report.enum'
import { CompanyIndicatorsDomain } from '../../domains/company/indicators/indicators.domain'
import { CompanyAnalyzeDomain } from '../../domains/company/subdomains/analyze/company-analyze.domain'
import { CompanyMutationDomain } from '../../domains/company/subdomains/mutation/company-mutation.domain'
import { CompanyToCreateMessage } from './company.messages'

@Injectable()
export class CompanyConsumer {
  constructor(
    private companyMutationDomain: CompanyMutationDomain,
    private companyAnalyzeDomain: CompanyAnalyzeDomain,
    private companyIndicatorsDomain: CompanyIndicatorsDomain,
  ) {}

  @RabbitRPC({
    exchange: 'company',
    routingKey: ['company-created', 'company-update-full-analysis'],
    queue: 'company-update-full-analysis',
  })
  async updateFullAnalysis(ticker: TickerType) {
    console.log('updateFullAnalysis', ticker)
    try {
      await this.companyAnalyzeDomain.updateFullAnalysis(ticker)
    } catch (e) {
      console.error(`[CompanyConsumer.updateFullAnalysis] Ticker: ${ticker}  => ${e}`)
      throw Error
    }
  }

  @RabbitRPC({
    exchange: 'company',
    routingKey: ['company-created'],
    queue: 'company-add-initial-history',
  })
  async addInitialHistory(ticker: TickerType) {
    await this.companyIndicatorsDomain.addInitialIndicators(ticker)
      .catch(e => console.error(`[CompanyConsumer.addInitialHistory] Ticker: ${ticker}  => ${e}`))

    return 42
  }

  @RabbitRPC({
    exchange: 'company',
    routingKey: 'company-to-create',
    queue: 'company-to-create',
  })
  async createCompany(dto: CompanyToCreateMessage) {
    await this.companyMutationDomain.createOne(dto)
      .catch(e => console.error(`[CompanyConsumer.createCompany] Dto: ${JSON.stringify(dto, null, 2)} => ${e}`))

    return 42
  }

  @RabbitRPC({
    exchange: 'company',
    routingKey: 'company-not-found-by-ticker',
    queue: 'company-try-to-create',
  })
  async tryToCreateCompany(ticker: TickerType) {
    await this.companyMutationDomain.tryToCreateCompany(ticker)
      .catch(e => console.error(`[CompanyConsumer.tryToCreateCompany]Ticker: ${ticker} => ${e}`))

    return 42
  }

  @RabbitRPC({
    exchange: 'company',
    routingKey: 'company-update-indicators',
    queue: 'company-update-indicators',
  })
  async updateIndicators(tickers: TickerType[]) {
    await this.companyIndicatorsDomain.updateIndicators(tickers)
      .catch(e => console.error(`[CompanyConsumer.updateIndicators]Tickers: ${tickers} => ${e}`))

    return 42
  }

  @RabbitRPC({
    exchange: 'company',
    routingKey: 'company-shariah-filter-results-updated',
    queue: 'update-company-is-halal-conclusion',
  })
  async updateCompanyIsHalalConclusion(ticker: TickerType): Promise<number> {
    await this.companyAnalyzeDomain.updateCompanyIsHalalConclusion(ticker)
      .catch(e => console.error(`[CompanyConsumer.updateCompanyIsHalalConclusion]Ticker: ${ticker} => ${e}`))

    return 42
  }

  @RabbitRPC({
    exchange: 'company',
    routingKey: 'company-update-shariah-filter-result',
    queue: 'update-company-shariah-filter-result',
  })
  async updateShariahFilterResults(ticker: TickerType): Promise<number> {
    await this.companyAnalyzeDomain.updateShariahFilterResults(ticker)
      .catch(e => console.error(`[CompanyConsumer.updateShariahFilterResults]Ticker: ${ticker} => ${e}`))

    return 42
  }
}
