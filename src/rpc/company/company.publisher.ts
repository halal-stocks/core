import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { Injectable } from '@nestjs/common'
import { TickerType } from '../../consts/report.enum'
import { isArray } from '../../utils'
import { CompanyToCreateMessage } from './company.messages'

@Injectable()
export class CompanyPublisher {
  constructor(
    private readonly amqpConnection: AmqpConnection,
  ) {}

  async publishCompanyCreatedMessage(ticker: TickerType): Promise<void> {
    await this.amqpConnection.publish(
      'company',
      'company-created',
      ticker,
    )
  }

  async publishCompanyToCreateMessage(dto: CompanyToCreateMessage): Promise<void> {
    await this.amqpConnection.publish(
      'company',
      'company-to-create',
      dto,
    )
  }

  async publishNotFoundAnyCompanyByTickerMessage(ticker: TickerType): Promise<void> {
    await this.amqpConnection.publish(
      'company',
      'company-not-found-by-ticker',
      ticker,
    )
  }

  async publishUpdateFullAnalysisMessage(ticker: TickerType): Promise<void> {
    await this.amqpConnection.publish(
      'company',
      'company-update-full-analysis',
      ticker,
    )
  }

  async publishUpdateShariahFilterResultMessage(ticker: TickerType): Promise<void> {
    await this.amqpConnection.publish(
      'company',
      'company-update-shariah-filter-result',
      ticker,
    )
  }

  async publishUpdateIndicatorsMessage(ticker: TickerType | TickerType[]): Promise<void> {
    await this.amqpConnection.publish(
      'company',
      'company-update-indicators',
      isArray(ticker) ? ticker : [ticker],
    )
  }

  async publishCompanyShariahFilterResultsUpdatedMessage(ticker: TickerType): Promise<void> {
    await this.amqpConnection.publish(
      'company',
      'company-shariah-filter-results-updated',
      ticker,
    )
  }
}
