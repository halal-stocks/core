export interface UserRpcStripeCustomerCreatedMessage {
  id: string
  stripeCustomerId: string
}
