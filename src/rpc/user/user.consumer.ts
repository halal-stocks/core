import { RabbitRPC } from '@golevelup/nestjs-rabbitmq'
import { Injectable } from '@nestjs/common'
import { UserDomain } from '../../domains/user/user.domain'
import { UserRpcStripeCustomerCreatedMessage } from './user.interfaces'

@Injectable()
export class UserConsumer {
  constructor(
    private userDomain: UserDomain,
  ) {}

  @RabbitRPC({
    exchange: 'user',
    routingKey: ['stripe-customer-created'],
    queue: 'user-update',
  })
  async updateUser(dto: UserRpcStripeCustomerCreatedMessage) {
    await this.userDomain.updateOne(dto)
      .catch(e => console.error(`[UserConsumer.updateUser] Dto: ${JSON.stringify(dto)}  => ${e}`))

    return 42
  }
}
