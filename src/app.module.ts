import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import typeormConfigAsync from '../ormconfig-async'
import { ApiModule } from './api/api.module'
import { AuthModule } from './domains/auth/auth.module'
import { CompanyDomainModule } from './domains/company/company.domain-module'
import { MailchimpDomainModule } from './domains/mailchimp/mailchimp.domain-module'
import { MailerDomainModule } from './domains/mailer/mailer.domain-module'
import { PaymentDomainModule } from './domains/payment/payment.domain-module'
import { PortfolioDomainModule } from './domains/portfolio/portfolio.domain-module'
import { ScreenerDomainModule } from './domains/screener/screener.domain.module'
import { StripeDomainModule } from './domains/stripe/stripe.domain-module'
import { ThirdPartyDataSourceDomainModule } from './domains/third-party-data-source/third-party-data-source.domain-module'
import { UserDomainModule } from './domains/user/user.domain-module'
import { WishlistDomainModule } from './domains/wishlist/wishlist.domain-module'
import { LogsImplModule } from './persistence/logs/logs.impl-module'
import { PaymentImplModule } from './persistence/payment/payment.impl-module'
import { PortfolioPersistenceModule } from './persistence/portfolio/portfolio.persistence-module'
import { StockMarketIndexImplModule } from './persistence/stock-market-index/stock-market-index.impl-module'
import { UserImplModule } from './persistence/user/user.impl-module'
import { WishlistImplModule } from './persistence/wishlist/wishlist.impl-module'
import { RpcConsumersModule } from './rpc/rpc-consumers.module'
import { RpcPublishersModule } from './rpc/rpc-publishers.module'
import { validate } from './shared/config/env.validation'
import { LoggerModule } from './shared/logger/logger.module'

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, validate }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: typeormConfigAsync,
    }),
    ApiModule,
    LoggerModule,
    CompanyDomainModule,
    StockMarketIndexImplModule,
    ThirdPartyDataSourceDomainModule,
    UserDomainModule,
    UserImplModule,
    ScreenerDomainModule,
    PaymentDomainModule,
    StripeDomainModule,
    AuthModule,
    PortfolioPersistenceModule,
    PortfolioDomainModule,
    WishlistImplModule,
    PaymentImplModule,
    RpcConsumersModule,
    RpcPublishersModule,
    MailchimpDomainModule,
    MailerDomainModule,
    WishlistDomainModule,
    LogsImplModule,
  ],
})
export class AppModule {}
