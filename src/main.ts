import { INestApplication, ValidationPipe } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { Transport } from '@nestjs/microservices'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import * as cookieParser from 'cookie-parser'
import { AppModule } from './app.module'
import { LogsService } from './persistence/logs/logs.service'
import { ExceptionInterceptor } from './shared/interceptors/exception.interceptor'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  const config = app.get<ConfigService>(ConfigService)
  app.connectMicroservice({
    transport: Transport.TCP,
    options: {
      port: config.get('RABBITMQ_PORT'),
      urls: ['amqp://localhost:5672'],
    },
  }, { inheritAppConfig: true })
  const logsService = app.get(LogsService)
  app.setGlobalPrefix('api/v1')
  app.enableCors()
  app.useGlobalInterceptors(new ExceptionInterceptor(logsService))
  app.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true }))
  app.use(cookieParser())
  initSwagger(app)
  await app.startAllMicroservicesAsync()
  await app.listen(process.env.PORT || 7000)
}

const initSwagger = (app: INestApplication) => {
  const options = new DocumentBuilder()
  .setTitle('Auth API')
  .setDescription('Auth API documentation')
  .setVersion(`${process.env.npm_package_version}`)
  .addBearerAuth()
  .build()
  const document = SwaggerModule.createDocument(app, options)
  SwaggerModule.setup('api/_swagger', app, document)
}

bootstrap()
