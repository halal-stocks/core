import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { WishlistRepository } from './wishlist.repository'
import { WishlistService } from './wishlist.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([
      WishlistRepository,
    ]),
  ],
  providers: [
    WishlistService,
  ],
  exports: [
    WishlistImplModule,
    WishlistService,
  ],
})
export class WishlistImplModule {}
