import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { CompanyEntity } from '../company/company.entity'
import { UserEntity } from '../user/user.entity'

@Entity('wishlist')
export class WishlistEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @ManyToOne(() => CompanyEntity)
  @JoinColumn()
  company: CompanyEntity

  @ManyToOne(() => UserEntity)
  @JoinColumn()
  user: UserEntity
}
