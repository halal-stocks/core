import { DeleteResult, EntityManager, EntityRepository, SelectQueryBuilder } from 'typeorm'
import { TickerType } from '../../consts/report.enum'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { BaseRepository } from '../../shared/repository/base-repository'
import { PortfolioEntity } from '../portfolio/portfolio.entity'
import { WishlistEntity } from './wishlist.entity'
import { WishlistResponseModel } from './wishlist.model'

@EntityRepository(WishlistEntity)
export class WishlistRepository extends BaseRepository<WishlistEntity> {
  async findManyByUserId(userId: string): Promise<ItemsWithCount<WishlistResponseModel>> {
    const [items, count] = await this.getQueryBuilderWithJoins()
      .select('wishlist.id')
      .innerJoin('wishlist.user', 'user')
      .innerJoin('wishlist.company', 'company')
      .addSelect('company.id')
      .where('user.id = :userId', { userId })
      .getManyAndCount()

    return {
      items,
      count,
    }
  }

  deleteByIdsAndUserId(ids: string[], userId: string): Promise<DeleteResult> {
    return this.createQueryBuilder('wishlist')
      .leftJoin('wishlist.user', 'user')
      .delete()
      .from(WishlistEntity)
      .where('wishlist.id IN (:...ids)', { ids })
      .andWhere('user.id = :userId', { userId })
      .execute()
  }

  private getQueryBuilderWithJoins(entityManager?: EntityManager): SelectQueryBuilder<WishlistEntity> {
    return entityManager
      ? entityManager.createQueryBuilder(PortfolioEntity, 'wishlist')
      : this.createQueryBuilder('wishlist')
  }

  findByUserIdAndTicker(ticker: TickerType, userId: string): Promise<WishlistResponseModel | undefined> {
    return this.getQueryBuilderWithJoins()
      .select('wishlist.id')
      .leftJoin('wishlist.user', 'user')
      .leftJoin('wishlist.company', 'company')
      .addSelect('company.id')
      .where('user.id = :userId', { userId })
      .andWhere('company.ticker = :ticker', { ticker })
      .getOne()
  }
}
