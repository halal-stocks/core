import { Injectable } from '@nestjs/common'
import { DeleteResult, EntityManager } from 'typeorm'
import { TickerType } from '../../consts/report.enum'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { WishlistResponseModel } from './wishlist.model'
import { WishlistRepository } from './wishlist.repository'

@Injectable()
export class WishlistService {
  constructor(
    private repository: WishlistRepository,
  ) {}

  async addByIdAndUserId(
    companyId: string,
    userId: string,
    entityManager?: EntityManager,
  ): Promise<void> {
    const entityLike = {
      user: { id: userId },
      company: { id: companyId },
    }
    await this.repository.createOrUpdateOne(entityLike, entityManager)
  }

  findCompanyIdsByUserId(userId: string): Promise<ItemsWithCount<WishlistResponseModel>> {
    return this.repository.findManyByUserId(userId)
  }

  deleteByIdsAndUserId(ids: string[], userId: string): Promise<DeleteResult> {
    return this.repository.deleteByIdsAndUserId(ids, userId)
  }

  findByUserIdAndTicker(ticker: TickerType, userId: string): Promise<WishlistResponseModel | undefined> {
    return this.repository.findByUserIdAndTicker(ticker, userId)
  }


}
