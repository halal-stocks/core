import { CompanyResponse } from '../../domains/company/subdomains/query/company-query.interfaces'

export interface WishlistResponseModel {
  id: string
  company: Pick<CompanyResponse, 'id'>
}
