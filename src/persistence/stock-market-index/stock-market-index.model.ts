import { StockMarketIndexEntity } from './stock-market-index.entity'

export interface StockMarketIndexCreateRequestModel extends Pick<StockMarketIndexEntity, 'name' | 'code'> {}

export interface StockMarketIndexResponseModel extends Omit<StockMarketIndexEntity, 'companies'> {}
