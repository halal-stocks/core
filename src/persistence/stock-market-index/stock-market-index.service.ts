import { Injectable } from '@nestjs/common'
import { EntityManager } from 'typeorm'
import { StockMarketIndex } from '../../consts/stock-market-index'
import { toUpperCase } from '../../utils'
import { StockMarketIndexCreateRequestModel, StockMarketIndexResponseModel } from './stock-market-index.model'
import { StockMarketIndexRepository } from './stock-market-index.repository'

@Injectable()
export class StockMarketIndexService {
  constructor(
    private repository: StockMarketIndexRepository,
  ) {}

  createOne(dto: StockMarketIndexCreateRequestModel, entityManager?: EntityManager): Promise<StockMarketIndexResponseModel> {
    return this.repository.createOrUpdateOne(dto, entityManager)
  }

  findOneByCode(code: StockMarketIndex): Promise<StockMarketIndexResponseModel | undefined> {
    return this.repository.findOneByCode(toUpperCase(code))
  }
}
