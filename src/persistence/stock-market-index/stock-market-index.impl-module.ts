import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { StockMarketIndexRepository } from './stock-market-index.repository'
import { StockMarketIndexService } from './stock-market-index.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StockMarketIndexRepository,
    ]),
  ],
  providers: [StockMarketIndexService],
  exports: [StockMarketIndexImplModule, StockMarketIndexService],
})
export class StockMarketIndexImplModule {}
