import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm'
import { StockMarketIndex } from '../../consts/stock-market-index'
import { CompanyEntity } from '../company/company.entity'

@Entity('stock_market_index')
export class StockMarketIndexEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string
  
  @Column({ type: 'varchar' })
  name: string
  
  @Column({ type: 'varchar' })
  code: StockMarketIndex
  
  @ManyToMany(
    () => CompanyEntity,
    companies => companies.stockMarketIndexes,
  )
  @JoinTable({
    name: 'company_stock_market_indexes',
    joinColumn: { name: 'stock_market_index_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'company_id', referencedColumnName: 'id' },
  })
  companies: CompanyEntity[]
}
