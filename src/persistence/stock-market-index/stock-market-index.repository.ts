import { EntityRepository } from 'typeorm'
import { StockMarketIndex } from '../../consts/stock-market-index'
import { BaseRepository } from '../../shared/repository/base-repository'
import { StockMarketIndexEntity } from './stock-market-index.entity'
import { StockMarketIndexResponseModel } from './stock-market-index.model'

@EntityRepository(StockMarketIndexEntity)
export class StockMarketIndexRepository extends BaseRepository<StockMarketIndexEntity> {
  findOneByCode(code: StockMarketIndex): Promise<StockMarketIndexResponseModel | undefined> {
    return this.createQueryBuilder('index')
      .where('index.code = :code', { code })
      .getOne()
  }
}
