import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { PaymentEntity } from '../payment/payment.entity'
import { PortfolioEntity } from '../portfolio/portfolio.entity'
import { WishlistEntity } from '../wishlist/wishlist.entity'

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ default: false })
  isNotifyOnCompanyHalalnessChange: boolean

  @Column()
  firebaseId: string

  @Column({ type: 'varchar', nullable: true })
  email: string | null

  @Column({ type: 'timestamp with time zone', nullable: true })
  paidTill: Date | null

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn({ type: 'timestamp with time zone', nullable: true })
  updatedAt: Date | null

  @DeleteDateColumn({ type: 'timestamp with time zone', nullable: true })
  deletedAt: Date | null

  @OneToMany(() => PortfolioEntity, portfolio => portfolio.user)
  portfolio: PortfolioEntity[]

  @OneToMany(() => WishlistEntity, watchList => watchList.user)
  watchList: WishlistEntity[]

  @OneToMany(() => PaymentEntity, payment => payment.user)
  payments: PaymentEntity[]
}
