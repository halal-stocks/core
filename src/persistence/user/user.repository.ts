import { EntityRepository } from 'typeorm'
import { PaymentStatus } from '../../domains/payment/payment.interfaces'
import { BaseRepository } from '../../shared/repository/base-repository'
import { UserEntity } from './user.entity'
import { UserPaymentInfoResponseModel, UserResponseModel } from './user.model'

@EntityRepository(UserEntity)
export class UserRepository extends BaseRepository<UserEntity> {
  findOneByFirebaseId(firebaseId: string): Promise<UserResponseModel | undefined> {
    return this.createQueryBuilder('user')
      .where('user.firebaseId = :firebaseId', { firebaseId })
      .getOne()
  }

  findOneUserByPaymentId(paymentId: string): Promise<UserResponseModel | undefined> {
    return this.createQueryBuilder('user')
      .innerJoin(
        'user.payments',
        'payment',
        'payment.user_id = user.id AND payment.id = :paymentId',
        { paymentId },
      )
      .getOne()
  }

  findOneWithPaidPayments(id: string): Promise<UserPaymentInfoResponseModel | undefined> {
    return this.createQueryBuilder('user')
      .leftJoinAndSelect(
        'user.payments',
        'payment',
        'payment.user_id = user.id AND payment.status = :status',
        { status: PaymentStatus.SUCCEEDED },
      )
      .where('user.id = :id', { id })
      .getOne()
  }
}
