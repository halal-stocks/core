import { PaymentResponseModel } from '../payment/payment.model'
import { UserEntity } from './user.entity'

export type UserResponseModel = Omit<UserEntity, 'watchList' | 'portfolio' | 'payments'>

export interface UserCreateRequestModel extends Pick<UserEntity, 'firebaseId'> {
  email?: string | null
}


export interface UserUpdateRequestModel extends Partial<Omit<UserEntity, 'watchList' | 'portfolio' | 'createdAt' | 'updatedAt' | 'deletedAt' | 'id'>> {
  id: string
}

export interface UserPaymentInfoResponseModel {
  paidTill: UserResponseModel['paidTill']
  payments: PaymentResponseModel[]
}
