import { Injectable } from '@nestjs/common'
import { DeleteResult, EntityManager } from 'typeorm'
import {
  UserCreateRequestModel,
  UserPaymentInfoResponseModel,
  UserResponseModel,
  UserUpdateRequestModel,
} from './user.model'
import { UserRepository } from './user.repository'

@Injectable()
export class UserService {
  constructor(
    private repository: UserRepository,
  ) {}

  createOne(dto: UserCreateRequestModel, entityManager?: EntityManager): Promise<UserResponseModel> {
    return this.repository.createOrUpdateOne(dto, entityManager)
  }

  updateOne(dto: UserUpdateRequestModel, entityManager?: EntityManager): Promise<UserResponseModel> {
    return this.repository.createOrUpdateOne(dto, entityManager)
  }

  createOrUpdateMany(dtos: (UserCreateRequestModel | UserUpdateRequestModel) [], entityManager?: EntityManager): Promise<UserResponseModel[]> {
    return this.repository.createOrUpdateMany(dtos, entityManager)
  }

  createOrUpdateOne(dto: (UserCreateRequestModel | UserUpdateRequestModel), entityManager?: EntityManager): Promise<UserResponseModel> {
    return this.repository.createOrUpdateOne(dto, entityManager)
  }

  deleteMany(ids: string[]): Promise<DeleteResult> {
    return this.repository.softDelete(ids)
  }

  getAll(withDeleted = false): Promise<UserResponseModel[]> {
    return this.repository.find({ withDeleted })
  }

  findManyByIds(ids: string[]): Promise<UserResponseModel[]> {
    return this.repository.findByIds(ids)
  }

  findOneById(id: string): Promise<UserResponseModel | undefined> {
    return this.repository.findOne(id)
  }

  findOneByFirebaseId(firebaseId): Promise<UserResponseModel | undefined> {
    return this.repository.findOne({ where: { firebaseId } })
  }

  deleteOneById(id: string): Promise<DeleteResult> {
    return this.repository.softDelete(id)
  }

  findOneUserByPaymentId(paymentId: string): Promise<UserResponseModel | undefined> {
    return this.repository.findOneUserByPaymentId(paymentId)
  }

  findOneWithPaidPayments(id: string): Promise<UserPaymentInfoResponseModel | undefined> {
    return this.repository.findOneWithPaidPayments(id)
  }
}
