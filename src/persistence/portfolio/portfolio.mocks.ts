import { dummyPortfolioList } from '../../dummy/portfolio/portfolio.dummy'

export const mockedPortfolioService = {
  addOne: () => {},
  findManyByUserId: () => dummyPortfolioList,
  removeOneByIdAndUserId: () => {},
  updateOneByUserId: () => {},
}
