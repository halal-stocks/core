import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PortfolioRepository } from './portfolio.repository'
import { PortfolioService } from './portfolio.service'
import { PortfolioTradeRepository } from './trade/portfolio-trade.repository'
import { PortfolioTradeService } from './trade/portfolio-trade.service'
import { PortfolioValueHistoryRepository } from './value-history/portfolio-value-history.repository'
import { PortfolioValueHistoryService } from './value-history/portfolio-value-history.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PortfolioRepository,
      PortfolioTradeRepository,
      PortfolioValueHistoryRepository,
    ]),
  ],
  providers: [
    PortfolioService,
    PortfolioTradeService,
    PortfolioValueHistoryService,
  ],
  exports: [
    PortfolioService,
    PortfolioTradeService,
    PortfolioValueHistoryService,
    PortfolioPersistenceModule,
  ],
})
export class PortfolioPersistenceModule {}
