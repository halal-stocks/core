import {
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { CompanyEntity } from '../company/company.entity'
import { UserEntity } from '../user/user.entity'
import { PortfolioTradeEntity } from './trade/portfolio-trade.entity'

@Entity('user_portfolio')
export class PortfolioEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @ManyToOne(() => UserEntity)
  @JoinColumn()
  user: UserEntity

  @ManyToOne(() => CompanyEntity)
  @JoinColumn()
  company: CompanyEntity

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn({ type: 'timestamp with time zone', nullable: true })
  updatedAt: Date | null

  @OneToMany(() => PortfolioTradeEntity, trades => trades.portfolio)
  trades: PortfolioTradeEntity[]
}
