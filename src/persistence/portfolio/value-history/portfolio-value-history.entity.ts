import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { NumericColumnTransformer } from '../../../shared/repository/numeric-column.transformer'
import { UserEntity } from '../../user/user.entity'

@Entity('portfolio_value_history')
export class PortfolioValueHistoryEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({
    precision: 10,
    scale: 2,
    transformer: new NumericColumnTransformer(),
  })
  value: number

  @Column({ type: 'date' })
  snapshottedAt: Date

  @ManyToOne(() => UserEntity)
  user: UserEntity
}
