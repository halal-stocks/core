import { EntityRepository } from 'typeorm'
import { BaseRepository } from '../../../shared/repository/base-repository'
import { PortfolioValueHistoryEntity } from './portfolio-value-history.entity'

@EntityRepository(PortfolioValueHistoryEntity)
export class PortfolioValueHistoryRepository extends BaseRepository<PortfolioValueHistoryEntity> {
  findByUserIdAndPeriod(userId: string, dateLimit?: Date | undefined): Promise<Omit<PortfolioValueHistoryEntity, 'user'>[]> {
    const queryBuilder = this.createQueryBuilder('portfolio-value')
                             .leftJoin('portfolio-value.user', 'user')
                             .where('user.id = :userId', { userId })

    if (dateLimit) {
      queryBuilder.andWhere('portfolio-value.snapshottedAt >= :dateLimit', { dateLimit })
    }

    return queryBuilder.getMany()
  }
}
