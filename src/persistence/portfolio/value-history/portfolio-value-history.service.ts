import { Injectable } from '@nestjs/common'
import { EntityManager } from 'typeorm'
import { PortfolioValueHistoryEntity } from './portfolio-value-history.entity'
import { PortfolioValueHistoryRepository } from './portfolio-value-history.repository'

@Injectable()
export class PortfolioValueHistoryService {
  constructor(
    private repository: PortfolioValueHistoryRepository,
  ) {}

  findByUserId(userId: string, dateLimit?: Date | undefined): Promise<Omit<PortfolioValueHistoryEntity, 'user'>[]> {
    return this.repository.findByUserIdAndPeriod(userId, dateLimit)
  }

  createOne(
    userId: string,
    dto: Pick<PortfolioValueHistoryEntity, 'value' | 'snapshottedAt'>,
    entityManager?: EntityManager): Promise<PortfolioValueHistoryEntity> {
    return this.repository.createOrUpdateOne({
      user: { id: userId },
      ...dto,
    }, entityManager)
  }

  createMany(
    userId: string,
    dtos: Pick<PortfolioValueHistoryEntity, 'value' | 'snapshottedAt'>[],
    entityManager?: EntityManager): Promise<PortfolioValueHistoryEntity[]> {
    const toSave = dtos.map(it => ({
      user: { id: userId },
      ...it,
    }))
    return this.repository.createOrUpdateMany(toSave, entityManager)
  }
}
