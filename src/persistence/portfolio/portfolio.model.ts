export interface UserPortfolioCreateRequestModel {
  companyId: string
  userId: string
}

export interface UserPortfolioUpdateRequestModel extends Partial<UserPortfolioCreateRequestModel> {}
