import { DeepPartial, DeleteResult, EntityRepository } from 'typeorm'
import { TickerType } from '../../consts/report.enum'
import { BaseRepository } from '../../shared/repository/base-repository'
import { PortfolioEntity } from './portfolio.entity'

@EntityRepository(PortfolioEntity)
export class PortfolioRepository extends BaseRepository<PortfolioEntity> {
  async updateOneByUserId(
    id: string,
    entityLike: DeepPartial<PortfolioEntity>,
    userId: string,
  ): Promise<void> {
    await this.createQueryBuilder('portfolio')
      .leftJoin('portfolio.user', 'user')
      .update(PortfolioEntity)
      .set(entityLike)
      .where('portfolio.id = :id', { id })
      .andWhere('user.id = :userId', { userId })
      .execute()
  }

  findManyByUserId(userId: string): Promise<Omit<PortfolioEntity, 'user'>[]> {
    return this.createQueryBuilder('portfolio')
      .innerJoinAndSelect('portfolio.company', 'company')
      .leftJoinAndSelect('portfolio.trades', 'trades')
      .leftJoin('portfolio.user', 'user')
      .where('user.id = :userId', { userId })
      .getMany()
  }

  async removeManyByIdsAndUserId(ids: string[], userId: string): Promise<DeleteResult> {
    if (!ids.length) {
      return {
        raw: '',
        affected: 0,
      }
    }
    return this.createQueryBuilder('user_portfolio')
      .leftJoin('user_portfolio.user', 'user')
      .delete()
      .from(PortfolioEntity, 'user_portfolio')
      .where('user_portfolio.id IN (:...ids)', { ids })
      .andWhere('user.id = :userId', { userId })
      .execute()
  }

  countByIdAndUserId(id: string, userId: string): Promise<number> {
    return this.createQueryBuilder('portfolio')
      .select('portfolio.id')
      .leftJoin('portfolio.user', 'user')
      .where('portfolio.id = :id', { id })
      .andWhere('user.id = :userId', { userId })
      .getCount()
  }

  countByTickerAndUserId(ticker: TickerType, userId: string): Promise<number> {
    return this.createQueryBuilder('portfolio')
      .select('portfolio.id')
      .leftJoin('portfolio.user', 'user')
      .leftJoin('portfolio.company', 'company')
      .where('company.ticker = :ticker', { ticker })
      .andWhere('user.id = :userId', { userId })
      .getCount()
  }
}
