import { PortfolioTradeEntity } from './portfolio-trade.entity'

export interface PortfolioTradeCreateModel extends Pick<PortfolioTradeEntity, 'quantity' | 'entryStockPrice'> {
  tradedAt?: Date | null
}

export interface PortfolioTradeUpdateModel extends Omit<Partial<PortfolioTradeCreateModel>, 'portfolioId'> {
  id: string
}
