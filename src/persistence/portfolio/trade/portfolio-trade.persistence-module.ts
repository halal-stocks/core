import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PortfolioTradeRepository } from './portfolio-trade.repository'
import { PortfolioTradeService } from './portfolio-trade.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([PortfolioTradeRepository]),
  ],
  providers: [
    PortfolioTradeService,
  ],
  exports: [PortfolioTradeService, PortfolioTradePersistenceModule],
})
export class PortfolioTradePersistenceModule {}
