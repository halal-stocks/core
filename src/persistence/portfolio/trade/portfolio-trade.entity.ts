import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { StockPriceType } from '../../../consts/report.enum'
import { NumericColumnTransformer } from '../../../shared/repository/numeric-column.transformer'
import { PortfolioEntity } from '../portfolio.entity'

@Entity('portfolio_trade')
export class PortfolioTradeEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({
    precision: 15,
    scale: 2,
    transformer: new NumericColumnTransformer(),
  })
  entryStockPrice: StockPriceType

  @Column({
    precision: 20,
    scale: 4,
    transformer: new NumericColumnTransformer(),
  })
  quantity: number

  @Column({ type: 'timestamp with time zone', nullable: true })
  tradedAt: Date | null

  @ManyToOne(() => PortfolioEntity, portfolio => portfolio.trades)
  @JoinColumn()
  portfolio: PortfolioEntity
}
