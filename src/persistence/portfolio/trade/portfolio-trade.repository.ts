import { DeleteResult, EntityRepository } from 'typeorm'
import { BaseRepository } from '../../../shared/repository/base-repository'
import { PortfolioTradeEntity } from './portfolio-trade.entity'

@EntityRepository(PortfolioTradeEntity)
export class PortfolioTradeRepository extends BaseRepository<PortfolioTradeEntity> {
  async deleteManyByIdsAndPortfolioIdAndUserId(userId: string, portfolioId: string, tradeIds: string[]): Promise<DeleteResult> {
    if (!tradeIds.length) {
      return {
        raw: '',
        affected: 0,
      }
    }

    const subQuery = this.createQueryBuilder('trade')
      .select('trade.id')
      .innerJoin(
        'trade.portfolio',
        'portfolio',
      )
      .innerJoin(
        'portfolio.user',
        'user',
      )
      .where('portfolio.id = :portfolioId')
      .andWhere('user.id = :userId')
      .andWhere('trade.id IN (:...tradeIds)')
      .getQuery()


    return this.createQueryBuilder('portfolio_trade')
      .delete()
      .from(PortfolioTradeEntity, 'portfolio_trade')
      .where(`portfolio_trade.id IN (${subQuery})`)
      .setParameters({
        portfolioId,
        userId,
        tradeIds,
      })
      .execute()
  }
}
