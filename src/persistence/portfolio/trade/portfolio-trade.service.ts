import { Injectable } from '@nestjs/common'
import { DeleteResult, EntityManager } from 'typeorm'
import { PortfolioTradeEntity } from './portfolio-trade.entity'
import { PortfolioTradeCreateModel, PortfolioTradeUpdateModel } from './portfolio-trade.model'
import { PortfolioTradeRepository } from './portfolio-trade.repository'

@Injectable()
export class PortfolioTradeService {
  constructor(
    private repository: PortfolioTradeRepository,
  ) {}

  addOne(dto: PortfolioTradeCreateModel, portfolioId: string, entityManager?: EntityManager): Promise<PortfolioTradeEntity> {
    return this.repository.createOrUpdateOne({
      ...dto,
      portfolio: { id: portfolioId },
    }, entityManager)
  }

  addMany(dtos: PortfolioTradeCreateModel[], portfolioId: string, entityManager?: EntityManager): Promise<PortfolioTradeEntity[]> {
    return this.repository.createOrUpdateMany(dtos.map(dto => ({
      ...dto,
      portfolio: { id: portfolioId },
    })), entityManager)
  }

  async createOrUpdateOne(dto: (PortfolioTradeCreateModel | PortfolioTradeUpdateModel), portfolioId: string, entityManager?: EntityManager): Promise<PortfolioTradeEntity> {
    return this.repository.createOrUpdateOne({
      ...dto,
      portfolio: { id: portfolioId },
    }, entityManager)
  }

  deleteManyByIdsAndPortfolioIdAndUserId(userId: string, portfolioId: string, tradeIds: string[]): Promise<DeleteResult> {
    return this.repository.deleteManyByIdsAndPortfolioIdAndUserId(userId, portfolioId, tradeIds)
  }
} 
