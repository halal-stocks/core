import { Injectable } from '@nestjs/common'
import { DeleteResult, EntityManager } from 'typeorm'
import { TickerType } from '../../consts/report.enum'
import { PortfolioEntity } from './portfolio.entity'
import { UserPortfolioCreateRequestModel, UserPortfolioUpdateRequestModel } from './portfolio.model'
import { PortfolioRepository } from './portfolio.repository'

@Injectable()
export class PortfolioService {
  constructor(
    private repository: PortfolioRepository,
  ) {}

  async addOne(
    model: UserPortfolioCreateRequestModel,
    entityManager?: EntityManager,
  ): Promise<string> {
    const entityLike = {
      user: { id: model.userId },
      company: { id: model.companyId },
    }
    const added = await this.repository.createOrUpdateOne(entityLike, entityManager)
    return added.id
  }

  async countByIdAndUserId(id: string, userId: string): Promise<number> {
    return this.repository.countByIdAndUserId(id, userId)
  }

  async countByTickerAndUserId(ticker: TickerType, userId: string): Promise<number> {
    return this.repository.countByTickerAndUserId(ticker, userId)
  }

  findManyByUserId(userId: string): Promise<Omit<PortfolioEntity, 'user'>[]> {
    return this.repository.findManyByUserId(userId)
  }

  removeManyByIdsAndUserId(ids: string[], userId: string): Promise<DeleteResult> {
    return this.repository.removeManyByIdsAndUserId(ids, userId)
  }

  async updateOneByUserId(
    id: string,
    model: UserPortfolioUpdateRequestModel,
    userId: string,
  ): Promise<void> {
    const {
      companyId,
      ...restModel
    } = model

    const entityLike = {
      company: companyId ? { id: companyId } : undefined,
      ...restModel,
    }

    await this.repository.updateOneByUserId(id, entityLike, userId)
  }
}
