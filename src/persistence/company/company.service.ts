import { Injectable } from '@nestjs/common'
import { DeleteResult, EntityManager } from 'typeorm'
import { ScreenerFiltersRequestDto } from '../../api/screener/screener.dtos'
import { CompanySector } from '../../consts/company-sector'
import { Exchange } from '../../consts/exchange.enum'
import { TickerType } from '../../consts/report.enum'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { SortAndFilters } from '../../shared/generics/search-and-filters.generic'
import { toUpperCase } from '../../utils'
import arrayToObject from '../../utils/object/array-to-object/array-to-object.util'
import { CompanyEntity } from './company.entity'
import {
  CompanyBriefResponseModel,
  CompanyCreateOneModel,
  CompanyForIndicatorSync,
  CompanyShariahFilterResultResponseModel,
  CompanyUpdateOneModel,
  CompanyWithLastShariahFilterResultModel,
  CompanyWithNoJoinsModel,
  GetAllForScreenerResponseModel,
} from './company.model'
import { CompanyRepository } from './company.repository'

@Injectable()
export class CompanyService {
  constructor(
    private repository: CompanyRepository,
  ) {}

  async createOne(dto: CompanyCreateOneModel, entityManager?: EntityManager): Promise<CompanyWithNoJoinsModel> {
    const { sector, exchange, ticker, ...restCompany } = dto
    return this.repository.createOrUpdateOne({
      ...restCompany,
      ticker: toUpperCase(ticker),
      sector: toUpperCase(sector),
      exchange: toUpperCase(exchange),
    }, entityManager)
  }

  async updateOne(dto: CompanyUpdateOneModel, entityManager?: EntityManager): Promise<CompanyWithNoJoinsModel> {
    const { sector, exchange, ticker, ...restCompany } = dto
    return this.repository.createOrUpdateOne({
      ...restCompany,
      ticker: ticker ? toUpperCase(ticker) : undefined,
      sector: sector ? toUpperCase(sector) : undefined,
      exchange: exchange ? toUpperCase(exchange) : undefined,
    }, entityManager)
  }

  async updateMany(dtos: CompanyUpdateOneModel[], entityManager?: EntityManager): Promise<CompanyWithNoJoinsModel[]> {
    return this.repository.createOrUpdateMany(dtos.map(dto => {
      const { sector, exchange, ticker, ...restCompany } = dto

      return {
        ...restCompany,
        ticker: ticker ? toUpperCase(ticker) : undefined,
        sector: sector ? toUpperCase(sector) : undefined,
        exchange: exchange ? toUpperCase(exchange) : undefined,
      }
    }), entityManager)
  }


  async getAllForScreener(isPaid: boolean, sortAndFilters?: SortAndFilters<ScreenerFiltersRequestDto>): Promise<ItemsWithCount<GetAllForScreenerResponseModel>> {
    return this.repository.getAllForScreener(isPaid, sortAndFilters)
  }

  async getExchangeByTicker(ticker: TickerType): Promise<Exchange> {
    const company = await this.repository.findOneOrFail({ ticker }, { select: ['exchange'] })
    return company.exchange
  }

  async getAllTickers(olderThan?: Date): Promise<TickerType[]> {
    const allCompanies = await this.repository.getAllTickers(olderThan)
    return allCompanies.map(it => it.ticker)
  }

  async findManyForIndicatorSyncByTickers(tickers?: TickerType[]): Promise<CompanyForIndicatorSync[]> {
    return this.repository.findManyForIndicatorSync(tickers)
  }

  async findManyByTickers(tickers: TickerType[]): Promise<CompanyEntity[]> {
    return await this.repository.findManyByTickers(tickers)
  }

  async findManyAsDictionaryByTickers(tickers: TickerType[]): Promise<Record<TickerType, CompanyEntity>> {
    const result = await this.repository.findManyByTickers(tickers)
    return arrayToObject(result, 'ticker')
  }

  async findOneByTickerWithLastShariahFilterResult(ticker: TickerType): Promise<CompanyWithLastShariahFilterResultModel | undefined> {
    return this.repository.findOneByTickerWithLastShariahFilterResult(ticker)
  }

  async isExistsByTicker(ticker: TickerType): Promise<boolean> {
    return this.repository.isExistsByTicker(ticker)
  }

  async findOneDetailedByTicker(ticker: TickerType): Promise<CompanyEntity | undefined> {
    return this.repository.findOneDetailedByTicker(ticker)
  }

  async findManyDetailedById(ids: string[]): Promise<CompanyEntity[]> {
    return this.repository.findManyDetailedById(ids)
  }

  async findIdByTicker(tickers: TickerType[]): Promise<Pick<CompanyEntity, 'id' | 'ticker'>[]> {
    return this.repository.findIdsByTickers(tickers)
  }

  async findManyByTickerPartialOrName(tickerOrName: TickerType | string): Promise<CompanyEntity[]> {
    return this.repository.searchManyByTickerOrName(tickerOrName)
  }

  async findManyBySector(sector: CompanySector): Promise<CompanyEntity[]> {
    return this.repository.findManyBySector(sector)
  }

  async getLastShariahFilterResults(tickers: TickerType[]): Promise<CompanyShariahFilterResultResponseModel[]> {
    return this.repository.getLastShariahFilterResults(tickers)
  }

  async getBriefCompaniesBySectorIncludeDailyChangeIndicators(sector: CompanySector, limit: number = 10): Promise<CompanyBriefResponseModel[]> {
    return this.repository.getBriefCompaniesBySectorIncludeDailyChangeIndicators(toUpperCase(sector), limit)
  }

  async getBriefCompaniesByIndexIncludeDailyChangeIndicators(indexCode: string, limit: number = 50): Promise<CompanyBriefResponseModel[]> {
    return this.repository.getBriefCompaniesByIndexIncludeDailyChangeIndicators(indexCode, limit)
  }

  async getCompaniesMissingShariahFilterResultOrIndicatorsOrAnalyzes(): Promise<any> {
    return this.repository.getCompaniesMissingShariahFilterResultOrIndicatorsOrAnalyzes()
  }

  async deleteManyByTickers(tickers: TickerType[]): Promise<DeleteResult> {
    return this.repository.deleteManyByTickers(tickers)
  }
}
