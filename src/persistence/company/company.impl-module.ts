import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CompanyAnalysisResultRepository } from './analysis-result/company-analysis-result.repository'
import { CompanyAnalysisResultService } from './analysis-result/company-analysis-result.service'
import { CompanyMarketIndexRepository } from './company-market-index/company-market-index.repository'
import { CompanyMarketIndexService } from './company-market-index/company-market-index.service'
import { CompanyRepository } from './company.repository'
import { CompanyService } from './company.service'
import { CompanyIndicatorsRepository } from './indicators/company-indicators.repository'
import { CompanyIndicatorsService } from './indicators/company-indicators.service'
import { ShariahFilterResultRepository } from './shariah-filter-result/shariah-filter-result.repository'
import { ShariahFilterResultService } from './shariah-filter-result/shariah-filter-result.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CompanyRepository,
      ShariahFilterResultRepository,
      CompanyIndicatorsRepository,
      CompanyAnalysisResultRepository,
      CompanyMarketIndexRepository,
    ]),
  ],
  providers: [
    CompanyService,
    ShariahFilterResultService,
    CompanyIndicatorsService,
    CompanyAnalysisResultService,
    CompanyMarketIndexService,
  ],
  exports: [
    CompanyImplModule,
    CompanyService,
    ShariahFilterResultService,
    CompanyIndicatorsService,
    CompanyAnalysisResultService,
    CompanyMarketIndexService,
  ],
})
export class CompanyImplModule {}
