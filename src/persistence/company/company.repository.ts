import { DeleteResult, EntityManager, EntityRepository, getConnection, In, SelectQueryBuilder } from 'typeorm'
import { ScreenerFiltersRequestDto } from '../../api/screener/screener.dtos'
import { CompanySector } from '../../consts/company-sector'
import { TickerType } from '../../consts/report.enum'
import { SortByTypes, SortDto } from '../../shared/decorators/sort.decorator'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { SortAndFilters } from '../../shared/generics/search-and-filters.generic'
import { BaseRepository } from '../../shared/repository/base-repository'
import {
  sqlFilterByNumberRange,
  sqlSearchByArrayOfStrings,
  sqlSearchByString,
} from '../../shared/repository/sql-generators'
import { isNullOrUndefined } from '../../utils'
import { CompanyAnalysisResultEntity } from './analysis-result/company-analysis-result.entity'
import { CompanyEntity } from './company.entity'
import {
  CompanyBriefResponseModel,
  CompanyForIndicatorSync,
  CompanyShariahFilterResultResponseModel,
  CompanyWithLastShariahFilterResultModel,
  GetAllForScreenerResponseModel,
} from './company.model'
import { ShariahFilterResultEntity } from './shariah-filter-result/shariah-filter-result.entity'

@EntityRepository(CompanyEntity)
export class CompanyRepository extends BaseRepository<CompanyEntity> {
  getQueryBuilder(entityManager?: EntityManager): SelectQueryBuilder<CompanyEntity> {
    return entityManager
      ? entityManager.createQueryBuilder(CompanyEntity, 'company')
      : this.createQueryBuilder('company')
  }

  async isExistsByTicker(ticker: TickerType): Promise<boolean> {
    const count = await this.createQueryBuilder('company')
      .where('company.ticker = :ticker', { ticker })
      .getCount()

    return count > 0
  }

  async getBriefCompaniesBySectorIncludeDailyChangeIndicators(sector: CompanySector, limit: number): Promise<CompanyBriefResponseModel[]> {
    return this.briefCompanyQueryBuilder()
      .where('company.sector = :sector', { sector })
      .limit(limit)
      .getMany()
  }

  async getBriefCompaniesByIndexIncludeDailyChangeIndicators(indexCode: string, limit: number): Promise<CompanyBriefResponseModel[]> {
    return this.briefCompanyQueryBuilder()
      .orderBy('company.isFree', SortByTypes.desc)
      .leftJoin('company.stockMarketIndexes', 'stockMarketIndexes')
      .where('stockMarketIndexes.code = :indexCode', { indexCode: indexCode.toLocaleUpperCase() })
      .limit(limit)
      .getMany()
  }

  async findOneByTickerWithLastShariahFilterResult(ticker: TickerType): Promise<CompanyWithLastShariahFilterResultModel | undefined> {
    const queryBuilder = this.createQueryBuilder('company')

    const shariahFilterSubQuery = this.getQueryToGetShariahFilters(1)
    queryBuilder.leftJoinAndSelect('company.shariahFilterResults', 'shariahFilterResults', `shariahFilterResults.id IN (${shariahFilterSubQuery})`)

    return queryBuilder.where('company.ticker = :ticker', { ticker })
      .getOne()
  }

  async findOneDetailedByTicker(ticker: TickerType): Promise<CompanyEntity | undefined> {
    const queryBuilder = this.getQueryBuilder()
      .where('company.ticker = :ticker', { ticker })

    queryBuilder.leftJoinAndSelect('company.indicators', 'indicators')

    const analyzesSubQuery = this.getQueryToGetAnalyzes(1)
    queryBuilder.leftJoin('company.analyzes', 'analyzes', `analyzes.id IN (${analyzesSubQuery})`)
      .addSelect([
        'analyzes.beta',
        'analyzes.pbRatio',
        'analyzes.eps',
        'analyzes.peRatio',
        'analyzes.psRatio',
        'analyzes.dividendYield',
      ])

    const shariahFilterSubQuery = this.getQueryToGetShariahFilters(1)
    queryBuilder.leftJoinAndSelect('company.shariahFilterResults', 'shariahFilterResults', `shariahFilterResults.id IN (${shariahFilterSubQuery})`)
    queryBuilder.leftJoinAndSelect('company.stockMarketIndexes', 'stockMarketIndexes')

    return queryBuilder.getOne()
  }

  async findManyDetailedById(ids: string[]): Promise<CompanyEntity[]> {
    if (!ids.length) {
      return []
    }
    const queryBuilder = this.getQueryBuilder()
      .where('company.id IN (:...ids)', { ids })

    queryBuilder.leftJoinAndSelect('company.indicators', 'indicators')

    const analyzesSubQuery = this.getQueryToGetAnalyzes(1)
    queryBuilder.leftJoin('company.analyzes', 'analyzes', `analyzes.id IN (${analyzesSubQuery})`)
      .addSelect([
        'analyzes.beta',
        'analyzes.pbRatio',
        'analyzes.eps',
        'analyzes.peRatio',
        'analyzes.psRatio',
        'analyzes.dividendYield',
      ])

    const shariahFilterSubQuery = this.getQueryToGetShariahFilters(1)
    queryBuilder.leftJoinAndSelect('company.shariahFilterResults', 'shariahFilterResults', `shariahFilterResults.id IN (${shariahFilterSubQuery})`)
    queryBuilder.leftJoinAndSelect('company.stockMarketIndexes', 'stockMarketIndexes')

    return queryBuilder.getMany()
  }

  async findManyByTickers(tickers: TickerType[]): Promise<CompanyEntity[]> {
    return this.getQueryBuilder()
      .andWhere('company.ticker IN (:...tickers)', { tickers })
      .getMany()
  }

  async findIdsByTickers(tickers: TickerType[]): Promise<Pick<CompanyEntity, 'id' | 'ticker'>[]> {
    return this.createQueryBuilder('company')
      .select(['company.id', 'company.ticker'])
      .where('company.ticker IN (:...tickers)', { tickers })
      .getMany()
  }

  async searchManyByTickerOrName(tickerOrName: TickerType | string): Promise<CompanyEntity[]> {
    return this.createQueryBuilder('company')
      .where(sqlSearchByString('company.ticker', tickerOrName))
      .orWhere(sqlSearchByString('company.name', tickerOrName))
      .getMany()
  }

  findManyBySector(sector: CompanySector): Promise<CompanyEntity[]> {
    return this.createQueryBuilder('company')
      .where('company.sector = :sector', { sector })
      .getMany()
  }

  getAllTickers(olderThan?: Date): Promise<Pick<CompanyEntity, 'ticker'>[]> {
    const queryBuilder = this.createQueryBuilder('company')
      .select('company.ticker')

    if (olderThan) {
      const shariahFilterSubQuery = this.getQueryToGetShariahFilters(1)
      queryBuilder.leftJoin('company.shariahFilterResults', 'shariahFilterResults', `shariahFilterResults.id IN (${shariahFilterSubQuery})`)
      queryBuilder.where('shariahFilterResults.checkedAt < :olderThan', { olderThan })
    }

    return queryBuilder.getMany()

  }

  async findManyForIndicatorSync(tickers?: TickerType[]): Promise<CompanyForIndicatorSync[]> {
    if (tickers && !tickers.length) {
      return []
    }
    const queryBuilder = this.createQueryBuilder('company')
      .select([
        'company.id',
        'company.ticker',
      ])
      .innerJoinAndSelect('company.indicators', 'indicators')

    if (tickers) {
      queryBuilder.where('company.ticker IN (:...tickers)', { tickers })
    }
    return queryBuilder.getMany()
  }

  async getLastShariahFilterResults(tickers: TickerType[]): Promise<CompanyShariahFilterResultResponseModel[]> {
    if (!tickers.length) {
      return []
    }

    const shariahFilterSubQuery = this.getQueryToGetShariahFilters(1)

    return this.createQueryBuilder('company')
      .select(['company.id', 'company.ticker'])
      .where('company.ticker IN (:...tickers)', { tickers })
      .leftJoinAndSelect('company.shariahFilterResults', 'shariahFilterResults', `shariahFilterResults.id IN (${shariahFilterSubQuery})`)
      .getMany()
  }


  async getAllForScreener(
    isPaid: boolean,
    sortAndFilters: SortAndFilters<ScreenerFiltersRequestDto> = { sort: {}, filters: {} },
    entityManager?: EntityManager,
  ): Promise<ItemsWithCount<GetAllForScreenerResponseModel>> {
    const perPage = sortAndFilters.sort!.perPage || 10
    const skip = ((sortAndFilters.sort!.page || 1) - 1) * perPage
    const queryBuilder = this.createQueryBuilder('company')
      .take(perPage)
      .skip(skip)

    queryBuilder.leftJoinAndSelect('company.indicators', 'indicators')

    const analyzesSubQuery = this.getQueryToGetAnalyzes(1)
    queryBuilder.leftJoin('company.analyzes', 'analyzes', `analyzes.id IN (${analyzesSubQuery})`)
      .addSelect([
        'analyzes.beta',
        'analyzes.pbRatio',
        'analyzes.eps',
        'analyzes.peRatio',
        'analyzes.psRatio',
        'analyzes.dividendYield',
      ])


    const {
      sort,
      filters,
    } = sortAndFilters

    const sortByQuery = this.getSortQuery(sort!.sortBy)
    if (sortByQuery) {
      queryBuilder.orderBy(sortByQuery, sort!.sortBy![1])
    } else {
      queryBuilder.orderBy(`company.isFree`, SortByTypes.desc)
    }

    if (filters!.ticker?.length) {
      queryBuilder.andWhere(sqlSearchByArrayOfStrings('company.ticker', filters!.ticker))
    }

    if (filters!.peRatioMIn || filters!.peRatioMax) {
      const query = sqlFilterByNumberRange(
        'analyzes.peRatio',
        { from: filters!.peRatioMIn, to: filters!.peRatioMax },
      )
      query && queryBuilder.andWhere(query)
    }

    if (filters!.epsMin || filters!.epsMax) {
      const query = sqlFilterByNumberRange(
        'analyzes.eps',
        { from: filters!.epsMin, to: filters!.epsMax },
      )
      query && queryBuilder.andWhere(query)
    }

    if (filters!.betaMin || filters!.betaMax || filters!.beta) {
      const query = sqlFilterByNumberRange(
        'analyzes.beta',
        { from: filters!.betaMin, to: filters!.betaMax, exact: filters!.beta },
      )
      query && queryBuilder.andWhere(query)
    }

    if (filters!.dividendYieldMin || filters!.dividendYieldMax || filters!.dividendYield) {
      const query = sqlFilterByNumberRange(
        'analyzes.dividendYield',
        { from: filters!.dividendYieldMin, to: filters!.dividendYieldMax, exact: filters!.dividendYield },
      )
      query && queryBuilder.andWhere(query)
    }

    if (filters!.actualStockPriceMin || filters!.actualStockPriceMax) {
      const query = sqlFilterByNumberRange(
        'indicators.actualStockPrice',
        { from: filters!.actualStockPriceMin, to: filters!.actualStockPriceMax },
      )
      query && queryBuilder.andWhere(query)
    }

    if (filters!.actualMarketCapMin || filters!.actualMarketCapMax) {
      const query = sqlFilterByNumberRange(
        'indicators.actualMarketCap',
        { from: filters!.actualMarketCapMin, to: filters!.actualMarketCapMax },
      )
      query && queryBuilder.andWhere(query)
    }

    if (filters!.isHalal && isPaid) {
      const isIncludeNull = filters!.isHalal.some(it => isNullOrUndefined(it))
      queryBuilder.andWhere(`(company.isHalal IN (:...isHalal) ${isIncludeNull ? 'OR company.isHalal IS NULL' : ''})`, { isHalal: filters!.isHalal })
    }

    if (filters!.index) {
      queryBuilder.leftJoin('company.stockMarketIndexes', 'companyStockMarketIndexes')
        .andWhere(
          'companyStockMarketIndexes.code = :index',
          { index: filters!.index },
        )
    }

    if (filters!.sector) {
      queryBuilder.andWhere(
        'company.sector = :sector',
        { sector: filters!.sector },
      )
    }

    const [items, count] = await queryBuilder.getManyAndCount()

    return {
      count,
      items,
    }

  }

  async getCompaniesMissingShariahFilterResultOrIndicatorsOrAnalyzes(): Promise<any> {
    const [items, count] = await this.createQueryBuilder('company')
      .select([
        'company.id',
        'company.ticker',
      ])
      .leftJoin('company.shariahFilterResults', 'shariahFilterResults')
      .addSelect('shariahFilterResults.id')
      .leftJoin('company.indicators', 'indicators')
      .addSelect('indicators.id')
      .leftJoin('company.analyzes', 'analyzes')
      .addSelect('analyzes.id')
      .where('shariahFilterResults.id IS NULL')
      .orWhere('indicators.id IS NULL')
      .orWhere('analyzes.id IS NULL')
      .getManyAndCount()

    return {
      count,
      items,
    }
  }

  deleteManyByTickers(tickers: TickerType[]): Promise<DeleteResult> {
    return this.softDelete({ ticker: In(tickers) })
  }

  private getSortQuery(sortBy: SortDto['sortBy'] | undefined): string | null {
    if (isNullOrUndefined(sortBy)) {
      return null
    }
    switch (sortBy[0]) {
      case 'actualStockPrice':
        return `indicators.actualStockPrice`
      case 'actualMarketCap':
        return `indicators.actualMarketCap`
      case 'peRatio':
      case 'psRatio':
      case 'pbRatio':
      case 'eps':
      case 'beta':
      case 'dividendYield':
        return `analyzes.${sortBy[0]}`
      default:
        return null
    }
  }

  private getQueryToGetShariahFilters(limit: number): string {
    return getConnection().createQueryBuilder()
      .select('shariahFilterSub.id')
      .from(ShariahFilterResultEntity, 'shariahFilterSub')
      .orderBy('shariahFilterSub.checked_at', 'DESC', 'NULLS LAST')
      .where('company.id = company_id')
      .limit(limit)
      .getQuery()
  }

  private getQueryToGetAnalyzes(limit: number): string {
    return getConnection().createQueryBuilder()
      .select('analysisSub.id')
      .from(CompanyAnalysisResultEntity, 'analysisSub')
      .orderBy('analysisSub.createdAt', 'DESC')
      .where('company.id = company_id')
      .limit(limit)
      .getQuery()
  }

  private briefCompanyQueryBuilder(includeDailyChange: boolean = true): SelectQueryBuilder<CompanyEntity> {
    const queryBuilder = this.createQueryBuilder('company')
      .select([
        'company.id',
        'company.name',
        'company.ticker',
        'company.isHalal',
        'company.isFree',
      ])


    if (includeDailyChange) {
      queryBuilder.leftJoinAndSelect('company.indicators', 'indicators')
    }

    return queryBuilder
  }
}
