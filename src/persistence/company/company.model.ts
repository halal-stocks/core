import { DeepPartial } from 'typeorm'
import { TickerType } from '../../consts/report.enum'
import { CompanyAnalysisResultEntity } from './analysis-result/company-analysis-result.entity'
import { CompanyEntity } from './company.entity'
import { CompanyIndicatorsEntity } from './indicators/company-indicators.entity'
import { CompanyIndicatorsResponseModel } from './indicators/company-indicators.model'
import { ShariahFilterResultResponseModel } from './shariah-filter-result/shariah-filter-result.model'

export interface CompanyTinyModel extends Pick<CompanyEntity, 'id' | 'ticker'> {}

export interface CompanyShariahFilterResultResponseModel extends CompanyTinyModel {
  shariahFilterResults: ShariahFilterResultResponseModel[]
}

export interface CompanyShariahFilterLastResultResponseModel extends CompanyTinyModel {
  shariahFilterResult: ShariahFilterResultResponseModel
}

export interface CompanyBriefResponseModel extends Pick<CompanyEntity, 'id' | 'ticker' | 'isHalal' | 'name' | 'isFree'> {
  indicators: Omit<CompanyIndicatorsEntity, 'company'>
}

export interface CompanyWithLastShariahFilterResultModel extends Omit<CompanyEntity, 'indicators' | 'analyzes' | 'stockMarketIndexes'> {}

export interface CompanyWithNoJoinsModel extends Omit<CompanyEntity, 'shariahFilterResults' | 'indicators' | 'analyzes' | 'stockMarketIndexes'> {}

export interface CompanyCreateOneModel extends DeepPartial<CompanyEntity> {
  ticker: TickerType
}

export interface CompanyUpdateOneModel extends Partial<CompanyEntity> {
  id: string
}

export interface CompanyForIndicatorSync extends Pick<CompanyEntity, 'id' | 'ticker'> {
  indicators: CompanyIndicatorsResponseModel
}

export interface GetAllForScreenerResponseModel extends CompanyWithNoJoinsModel {
  analyzes: Pick<CompanyAnalysisResultEntity, 'beta' | 'pbRatio' | 'eps' | 'peRatio' | 'psRatio' | 'dividendYield'>[]
  indicators: CompanyIndicatorsEntity
}
