import { Injectable } from '@nestjs/common'
import { EntityManager } from 'typeorm'
import { ShariahFilterResultEntity } from './shariah-filter-result.entity'
import {
  ShariahFilterResultCreateRequestModel,
  ShariahFilterResultUpdateRequestModel,
} from './shariah-filter-result.model'
import { ShariahFilterResultRepository } from './shariah-filter-result.repository'

@Injectable()
export class ShariahFilterResultService {
  constructor(
    private repository: ShariahFilterResultRepository,
  ) {}

  createOne(
    model: ShariahFilterResultCreateRequestModel,
    companyId: string,
    entityManager?: EntityManager,
  ): Promise<ShariahFilterResultEntity> {
    return this.repository.createOrUpdateOne({
        ...model,
        company: { id: companyId },
      },
      entityManager,
    )
  }

  updateOne(
    model: ShariahFilterResultUpdateRequestModel,
    entityManager?: EntityManager,
  ): Promise<ShariahFilterResultEntity> {
    return this.repository.createOrUpdateOne(model, entityManager)
  }

  updateMany(
    models: ShariahFilterResultUpdateRequestModel[],
    entityManager?: EntityManager,
  ): Promise<ShariahFilterResultEntity[]> {
    return this.repository.createOrUpdateMany(models, entityManager)
  }

  findLastByCompanyIds(companyIds: string[]): Promise<ShariahFilterResultEntity[]> {
    return this.repository.findLastByCompanyIds(companyIds)
  }
}
