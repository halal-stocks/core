import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import {
  CashAndShortTermInvestmentsType,
  DepositToMarketCapRatioType,
  DeptToMarketCapRatioType,
  ForbiddenIncomePerShareType,
  ForbiddenIncomeRatioType,
  InterestIncomeType,
  LongTermDebtType,
  LongTermInvestmentsType,
  MarketCapType,
  OtherCurrentAssetsType,
  OtherCurrentLiabilitiesType,
  RevenueType,
  SharesOutstandingType,
  ShortTermDebtType,
} from '../../../consts/report.enum'
import { NumericColumnTransformer } from '../../../shared/repository/numeric-column.transformer'
import { CompanyEntity } from '../company.entity'

@Entity('shariah_filter_result')
export class ShariahFilterResultEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @CreateDateColumn()
  checkedAt: Date

  @Column({ type: 'timestamp with time zone', nullable: true })
  reportedAt: Date | null

  @Column({ nullable: true, type: 'boolean' })
  isCharterValid: boolean | null

  @Column({ nullable: true, type: 'varchar' })
  linkToCharter: string | null

  @Column({ type: 'varchar', nullable: true })
  charterNotValidReason: string | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  otherCurrentAssets: OtherCurrentAssetsType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  cashAndShortTermInvestments: CashAndShortTermInvestmentsType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  longTermInvestments: LongTermInvestmentsType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  shortTermDebt: ShortTermDebtType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  otherCurrentLiabilities: OtherCurrentLiabilitiesType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  longTermDebt: LongTermDebtType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  marketCapAverage: MarketCapType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  sharesOutstanding: SharesOutstandingType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  interestIncome: InterestIncomeType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  revenue: RevenueType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  depositToMarketCap: DepositToMarketCapRatioType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  deptToMarketCap: DeptToMarketCapRatioType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  forbiddenIncome: ForbiddenIncomeRatioType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  forbiddenIncomePerShare: ForbiddenIncomePerShareType | null

  @ManyToOne(
    () => CompanyEntity,
    company => company.shariahFilterResults,
  )
  @JoinColumn()
  company: CompanyEntity

  @UpdateDateColumn()
  updatedAt: Date | null

  @DeleteDateColumn()
  deletedAt: Date | null
}
