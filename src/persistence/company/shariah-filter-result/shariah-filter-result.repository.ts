import { EntityRepository } from 'typeorm'
import { BaseRepository } from '../../../shared/repository/base-repository'
import { ShariahFilterResultEntity } from './shariah-filter-result.entity'

@EntityRepository(ShariahFilterResultEntity)
export class ShariahFilterResultRepository extends BaseRepository<ShariahFilterResultEntity> {
  findLastByCompanyIds(companyIds: string[]): Promise<ShariahFilterResultEntity[]> {
    return this.createQueryBuilder('shariah-filter')
      .leftJoin('shariah-filter.company', 'company')
      .addSelect('company.id')
      .distinctOn(['company.id'])
      .orderBy('company.id', 'DESC')
      .addOrderBy('shariah-filter.reportedAt', 'DESC')
      .where('company.id IN (:...companyIds)', { companyIds })
      .getMany()
  }
}
