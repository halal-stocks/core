import { ShariahFilterResultEntity } from './shariah-filter-result.entity'

export interface ShariahFilterResultCreateRequestModel
  extends Omit<ShariahFilterResultEntity, 'checkedAt' | 'company' | 'updatedAt' | 'deletedAt' | 'id' | 'isCharterValid' | 'charterNotValidReason' | 'linkToCharter'> {
}

export interface ShariahFilterResultUpdateRequestModel
  extends Partial<Omit<ShariahFilterResultEntity, 'checkedAt' | 'company' | 'id' | 'updatedAt' | 'deletedAt'>> {
  id: string
}

export type ShariahFilterResultResponseModel = Omit<ShariahFilterResultEntity, | 'company'>
