import { MarketCapType, StockPriceType } from '../../../consts/report.enum'
import { CompanyEntity } from '../company.entity'
import { CompanyIndicatorsEntity } from './company-indicators.entity'

export interface CompanyIndicatorsCreateModel
  extends Omit<CompanyIndicatorsEntity, 'id' | 'updatedAt' | 'company'> {}

export interface CompanyIndicatorsCreateManyModel extends CompanyIndicatorsCreateModel {
  companyId: string
}

export interface CompanyIndicatorsUpdateModel extends Partial<CompanyIndicatorsCreateModel> {
  id: string
}

export type CompanyIndicatorsResponseModel = Omit<CompanyIndicatorsEntity, 'company'>

export interface CompanyIndicatorsWithCompanyResponseModel extends CompanyIndicatorsResponseModel {
  company: Pick<CompanyEntity, 'id' | 'ticker'>
}

/** Response from repository*/
export interface CompanyIndicatorsMinMaxValuesRepositoryResponseModel {
  stockPriceMax: string
  stockPriceMin: string
  marketCapMax: string
  marketCapMin: string
}

export interface CompanyIndicatorsMinMaxValuesModel {
  stockPriceMax: StockPriceType
  stockPriceMin: StockPriceType
  marketCapMax: MarketCapType
  marketCapMin: MarketCapType
}
