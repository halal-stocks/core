import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { MarketCapType, StockPriceType } from '../../../consts/report.enum'
import { NumericColumnTransformer } from '../../../shared/repository/numeric-column.transformer'
import { CompanyEntity } from '../company.entity'

@Entity('company_indicators')
export class CompanyIndicatorsEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  actualStockPrice: StockPriceType

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  actualMarketCap: MarketCapType

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  previousDayStockPrice: StockPriceType

  @CreateDateColumn({ name: 'updated_at', nullable: true })
  updatedAt: Date | null

  @OneToOne(() => CompanyEntity, company => company.indicators)
  @JoinColumn()
  company: CompanyEntity
}
