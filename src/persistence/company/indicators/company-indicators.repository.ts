import { EntityManager, EntityRepository } from 'typeorm'
import { TickerType } from '../../../consts/report.enum'
import { BaseRepository } from '../../../shared/repository/base-repository'
import { convertDateToDBFormat } from '../../../utils'
import { CompanyIndicatorsEntity } from './company-indicators.entity'
import {
  CompanyIndicatorsMinMaxValuesRepositoryResponseModel,
  CompanyIndicatorsUpdateModel,
  CompanyIndicatorsWithCompanyResponseModel,
} from './company-indicators.model'

@EntityRepository(CompanyIndicatorsEntity)
export class CompanyIndicatorsRepository extends BaseRepository<CompanyIndicatorsEntity> {
  async findManyByTickers(tickers: TickerType[]): Promise<CompanyIndicatorsWithCompanyResponseModel[]> {
    if (!tickers.length) {
      return []
    }

    return this.createQueryBuilder('indicators')
      .leftJoin('indicators.company', 'company')
      .addSelect([
        'company.id',
        'company.ticker',
      ])
      .where('company.ticker IN (:...tickers)', { tickers })
      .getMany()
  }

  async updateMany(dtos: CompanyIndicatorsUpdateModel[], entityManager?: EntityManager): Promise<void> {
    if (!dtos.length) {
      return
    }
    const currentDate = convertDateToDBFormat(new Date())
    const values = dtos.map(dto => (`(
        '${dto.id}'::uuid,
        ${dto.actualMarketCap},
        ${dto.actualStockPrice},
        ${dto.previousDayStockPrice},
        '${currentDate}'::timestamp
      )`))
      .join(',')

    return this.query(`
      update company_indicators as ci set
          actual_stock_price = val.actual_stock_price,
          actual_market_cap = val.actual_market_cap,
          previous_day_stock_price = val.previous_day_stock_price,
          updated_at = val.updated_at
      from (values ${values}
        ) as val(id, actual_market_cap, actual_stock_price, previous_day_stock_price, updated_at) 
      where val.id = ci.id 
    `)
  }

  findOneByTicker(ticker: TickerType): Promise<CompanyIndicatorsWithCompanyResponseModel | undefined> {
    return this.createQueryBuilder('indicators')
      .leftJoinAndSelect('indicators.company', 'company')
      .addSelect([
        'company.id',
        'company.ticker',
      ])
      .where('company.ticker = :ticker', { ticker })
      .getOne()
  }

  getMinMaxValues(): Promise<CompanyIndicatorsMinMaxValuesRepositoryResponseModel> {
    return this.createQueryBuilder('indicators')
      .select('MAX(indicators.actualStockPrice)', 'stockPriceMax')
      .addSelect('MIN(indicators.actualStockPrice)', 'stockPriceMin')
      .addSelect('MAX(indicators.actualMarketCap)', 'marketCapMax')
      .addSelect('MIN(indicators.actualMarketCap)', 'marketCapMin')
      .getRawOne()
  }
}
