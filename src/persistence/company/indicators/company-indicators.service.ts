import { Injectable } from '@nestjs/common'
import { EntityManager } from 'typeorm'
import { TickerType } from '../../../consts/report.enum'
import { convertAllDataToNumeric } from '../../../shared/repository/numeric-column.transformer'
import { CompanyIndicatorsEntity } from './company-indicators.entity'
import {
  CompanyIndicatorsCreateManyModel,
  CompanyIndicatorsCreateModel,
  CompanyIndicatorsMinMaxValuesModel,
  CompanyIndicatorsUpdateModel,
  CompanyIndicatorsWithCompanyResponseModel,
} from './company-indicators.model'
import { CompanyIndicatorsRepository } from './company-indicators.repository'

@Injectable()
export class CompanyIndicatorsService {
  constructor(
    private repository: CompanyIndicatorsRepository,
  ) {}

  createOne(
    model: CompanyIndicatorsCreateModel,
    companyId: string,
    entityManager?: EntityManager,
  ): Promise<CompanyIndicatorsEntity> {
    return this.repository.createOrUpdateOne({
      ...model,
      company: { id: companyId },
    }, entityManager)
  }

  createMany(models: CompanyIndicatorsCreateManyModel[], entityManager?: EntityManager): Promise<CompanyIndicatorsEntity[]> {
    const toSave = models.map(model => {
      const {
        companyId,
        ...restModel
      } = model

      return {
        ...restModel,
        company: { id: companyId },
      }
    })

    return this.repository.createOrUpdateMany(toSave, entityManager)
  }

  updateMany(models: CompanyIndicatorsUpdateModel[], entityManager?: EntityManager): Promise<void> {
    return this.repository.updateMany(models, entityManager)
  }

  findManyByTickers(tickers: TickerType[]): Promise<CompanyIndicatorsWithCompanyResponseModel[]> {
    return this.repository.findManyByTickers(tickers)
  }

  findOneByTicker(ticker: TickerType): Promise<CompanyIndicatorsWithCompanyResponseModel | undefined> {
    return this.repository.findOneByTicker(ticker)
  }

  async getMinMaxValues(): Promise<CompanyIndicatorsMinMaxValuesModel> {
    const result = await this.repository.getMinMaxValues()
    return convertAllDataToNumeric(result) as CompanyIndicatorsMinMaxValuesModel
  }
}
