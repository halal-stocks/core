import { IsEnum } from 'class-validator'
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { CompanySector } from '../../consts/company-sector'
import { Exchange } from '../../consts/exchange.enum'
import { IsCompanyFreeToUse, IsCompanyHalalType, TickerType } from '../../consts/report.enum'
import { StockMarketIndexEntity } from '../stock-market-index/stock-market-index.entity'
import { CompanyAnalysisResultEntity } from './analysis-result/company-analysis-result.entity'
import { CompanyIndicatorsEntity } from './indicators/company-indicators.entity'
import { ShariahFilterResultEntity } from './shariah-filter-result/shariah-filter-result.entity'

@Entity('company')
export class CompanyEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  name: string

  @Column({ nullable: true, type: 'varchar' })
  description: string | null

  @Column({ nullable: true, type: 'int' })
  fullTimeEmployees: number | null

  @Column({ nullable: true, type: 'varchar' })
  industry: string | null

  @Column({ unique: true, type: 'varchar' })
  ticker: TickerType

  @Column({ type: 'varchar' })
  @IsEnum(CompanySector)
  sector: CompanySector

  @Column({ type: 'enum', enum: Exchange })
  @IsEnum(Exchange)
  exchange: Exchange

  @Column({ type: 'boolean', nullable: true })
  isHalal: IsCompanyHalalType

  @Column({ type: 'boolean' })
  isFree: IsCompanyFreeToUse

  @OneToMany(
    () => ShariahFilterResultEntity,
    shariahFilterResult => shariahFilterResult.company,
  )
  shariahFilterResults: ShariahFilterResultEntity[]

  @OneToOne(() => CompanyIndicatorsEntity, indicators => indicators.company)
  indicators: CompanyIndicatorsEntity

  @OneToMany(() => CompanyAnalysisResultEntity, analysis => analysis.company)
  analyzes: CompanyAnalysisResultEntity[]

  @ManyToMany(
    () => StockMarketIndexEntity,
    stockMarketIndex => stockMarketIndex.companies,
  )
  @JoinTable({
    name: 'company_stock_market_indexes',
    joinColumn: { name: 'company_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'stock_market_index_id', referencedColumnName: 'id' },
  })
  stockMarketIndexes: StockMarketIndexEntity[]

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn({ type: 'timestamp with time zone', nullable: true })
  updatedAt: Date | null

  @DeleteDateColumn({ type: 'timestamp with time zone', nullable: true, select: false })
  deletedAt: Date | null
}
