import { EntityRepository } from 'typeorm'
import { BaseRepository } from '../../../shared/repository/base-repository'
import { CompanyAnalysisResultEntity } from './company-analysis-result.entity'
import { CompanyAnalysisResultMinMaxValuesRepositoryResponse } from './company-analysis-result.model'

@EntityRepository(CompanyAnalysisResultEntity)
export class CompanyAnalysisResultRepository extends BaseRepository<CompanyAnalysisResultEntity> {
  getMinMaxValues(): Promise<CompanyAnalysisResultMinMaxValuesRepositoryResponse> {
    return this.createQueryBuilder('companyAnalysisResult')
      .select('MAX(companyAnalysisResult.peRatio)', 'peRatioMax')
      .addSelect('MIN(companyAnalysisResult.peRatio)', 'peRatioMin')
      .addSelect('MAX(companyAnalysisResult.psRatio)', 'psRatioMax')
      .addSelect('MIN(companyAnalysisResult.psRatio)', 'psRatioMin')
      .addSelect('MAX(companyAnalysisResult.beta)', 'betaMax')
      .addSelect('MIN(companyAnalysisResult.beta)', 'betaMin')
      .addSelect('MAX(companyAnalysisResult.eps)', 'epsMax')
      .addSelect('MIN(companyAnalysisResult.eps)', 'epsMin')
      .addSelect('MAX(companyAnalysisResult.pbRatio)', 'pbRatioMax')
      .addSelect('MIN(companyAnalysisResult.pbRatio)', 'pbRatioMin')
      .addSelect('MAX(companyAnalysisResult.dividendYield)', 'dividendYieldMax')
      .addSelect('MIN(companyAnalysisResult.dividendYield)', 'dividendYieldMin')
      .getRawOne()
  }
}
