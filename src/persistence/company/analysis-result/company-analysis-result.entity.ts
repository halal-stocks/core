import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import {
  BetaType,
  CurrentAssetsMinusLiabilitiesType,
  DividendYieldType,
  EPSType,
  FreeCashFlowType,
  GrahamRatioType,
  NetProfitMarginType,
  PayoutRatioType,
  PBRatioType,
  PEGRatioType,
  PERatioType,
  PSRatioType,
  ROAType,
  ROEType,
} from '../../../consts/report.enum'
import { NumericColumnTransformer } from '../../../shared/repository/numeric-column.transformer'
import { CompanyEntity } from '../company.entity'

@Entity('company_analysis_result')
export class CompanyAnalysisResultEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  currentAssetsMinusLiabilities: CurrentAssetsMinusLiabilitiesType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  beta: BetaType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  pegRatio: PEGRatioType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  pbRatio: PBRatioType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  eps: EPSType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  peRatio: PERatioType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  psRatio: PSRatioType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  grahamRatio: GrahamRatioType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  freeCashFlow: FreeCashFlowType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  payoutRatio: PayoutRatioType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  dividendYield: DividendYieldType | null

  @Column({
    precision: 20,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  netProfitMargin: NetProfitMarginType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  roa: ROAType | null

  @Column({
    precision: 15,
    scale: 5,
    transformer: new NumericColumnTransformer(),
    nullable: true,
    type: 'numeric',
  })
  roe: ROEType | null

  @Column({ type: 'timestamp with time zone', nullable: true })
  reportedAt: Date | null

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn({ type: 'timestamp with time zone', nullable: true })
  updatedAt: Date | null

  @DeleteDateColumn({ type: 'timestamp with time zone', nullable: true })
  deletedAt: Date | null

  @OneToOne(() => CompanyEntity, company => company.analyzes)
  @JoinColumn({ name: 'company_id' })
  company: CompanyEntity
}
