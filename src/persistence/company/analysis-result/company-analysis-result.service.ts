import { Injectable } from '@nestjs/common'
import { EntityManager } from 'typeorm'
import { convertAllDataToNumeric } from '../../../shared/repository/numeric-column.transformer'
import { CompanyAnalysisResultEntity } from './company-analysis-result.entity'
import {
  CompanyAnalysisResultCreateRequest,
  CompanyAnalysisResultMinMaxValuesModel,
} from './company-analysis-result.model'
import { CompanyAnalysisResultRepository } from './company-analysis-result.repository'

@Injectable()
export class CompanyAnalysisResultService {
  constructor(
    private repository: CompanyAnalysisResultRepository,
  ) {}

  createOrUpdateOne(
    model: Partial<CompanyAnalysisResultCreateRequest>,
    companyId: string,
    entityManager?: EntityManager,
  ): Promise<CompanyAnalysisResultEntity> {
    try {
      return this.repository.createOrUpdateOne({
        ...model,
        company: { id: companyId },
      }, entityManager)
    } catch (e) {
      throw e
    }
  }

  async getMinMaxValues(): Promise<CompanyAnalysisResultMinMaxValuesModel> {
    const result = await this.repository.getMinMaxValues()
    return convertAllDataToNumeric(result) as CompanyAnalysisResultMinMaxValuesModel
  }

}
