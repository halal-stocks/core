import {
  BetaType,
  DividendYieldType,
  EPSType,
  PBRatioType,
  PERatioType,
  PSRatioType,
} from '../../../consts/report.enum'
import { CompanyAnalysisResultEntity } from './company-analysis-result.entity'

export interface CompanyAnalysisResultCreateRequest
  extends Omit<CompanyAnalysisResultEntity, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt' | 'company'> {}

export interface CompanyAnalysisResultMinMaxValuesRepositoryResponse {
  peRatioMax: string
  peRatioMin: string
  psRatioMax: string
  psRatioMin: string
  betaMax: string
  betaMin: string
  epsMax: string
  epsMin: string
  pbRatioMax: string
  pbRatioMin: string
  dividendYieldMax: string
  dividendYieldMin: string
}

export interface CompanyAnalysisResultMinMaxValuesModel {
  peRatioMax: PERatioType
  peRatioMin: PERatioType
  psRatioMax: PSRatioType
  psRatioMin: PSRatioType
  betaMax: BetaType
  betaMin: BetaType
  epsMax: EPSType
  epsMin: EPSType
  pbRatioMax: PBRatioType
  pbRatioMin: PBRatioType
  dividendYieldMax: DividendYieldType
  dividendYieldMin: DividendYieldType
}
