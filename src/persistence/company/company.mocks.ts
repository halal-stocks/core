import { dummyBriefCompaniesBySectorIncludeDailyChangeIndicators } from '../../dummy/company/company.dummy'

export const mockedCompanyService = {
  createOne: () => {},
  getAll: () => [],
  getAllTickers: () => [],
  findManyByTickers: () => [],
  findManyAsDictionaryByTickers: () => [],
  isExistsByTicker: () => true,
  findOneByTicker: () => {},
  findIdByTicker: () => {},
  findManyByTickerPartial: () => [],
  findManyBySector: () => [],
  getLastShariahFilterResults: () => {},
  getBriefCompaniesBySectorIncludeDailyChangeIndicators: () => [dummyBriefCompaniesBySectorIncludeDailyChangeIndicators],
}
