import { Column, Entity, OneToOne, PrimaryColumn } from 'typeorm'
import { StockMarketIndexEntity } from '../../stock-market-index/stock-market-index.entity'
import { CompanyEntity } from '../company.entity'

/**
 * Need this entity to batch update relations between company and market index
 */
@Entity('company_stock_market_indexes')
export class CompanyMarketIndexEntity {
  @OneToOne(() => CompanyEntity)
  company: CompanyEntity

  @Column()
  @PrimaryColumn()
  companyId: string

  @OneToOne(() => CompanyEntity)
  marketIndex: StockMarketIndexEntity

  @Column({ name: 'stock_market_index_id' })
  marketIndexId: string
}
