import { DeleteResult, EntityManager, EntityRepository } from 'typeorm'
import { BaseRepository } from '../../../shared/repository/base-repository'
import { CompanyMarketIndexEntity } from './company-market-index.entity'

@EntityRepository(CompanyMarketIndexEntity)
export class CompanyMarketIndexRepository extends BaseRepository<CompanyMarketIndexEntity> {
  deleteAllRelationsByMarketIndexId(marketIndexId: string, entityManager?: EntityManager): Promise<DeleteResult> {
    return entityManager
      ? entityManager.delete(CompanyMarketIndexEntity, { marketIndexId })
      : this.delete({ marketIndexId })
  }

  async createBatch(companyIds: string[], marketIndexId: string, entityManager?: EntityManager): Promise<void> {
    const manager = entityManager ?? this
    await manager.query(`
        INSERT INTO 
            company_stock_market_indexes (company_id, stock_market_index_id)
        VALUES ${companyIds.map(companyId => `('${companyId}', '${marketIndexId}')`).join(',')} 
    `)
  }
}
