import { Injectable } from '@nestjs/common'
import { EntityManager } from 'typeorm'
import { CompanyMarketIndexRepository } from './company-market-index.repository'

@Injectable()
export class CompanyMarketIndexService {
  constructor(
    private repository: CompanyMarketIndexRepository,
  ) {}

  async deleteAllRelationsByMarketIndexId(marketIndexId: string, entityManager?: EntityManager): Promise<void> {
    await this.repository.deleteAllRelationsByMarketIndexId(marketIndexId, entityManager)
  }

  async createMany(companyIds: string[], marketIndexId: string, entityManager?: EntityManager): Promise<void> {
    await this.repository.createBatch(companyIds, marketIndexId, entityManager)
  }
}
