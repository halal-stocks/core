export interface CompanyMarketIndexCreateModel {
  companyId: string
  marketIndexId: string
}
