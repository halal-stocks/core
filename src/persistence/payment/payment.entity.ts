import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { Currency } from '../../consts/currency.enum'
import { PaymentStatus } from '../../domains/payment/payment.interfaces'
import { NumericColumnTransformer } from '../../shared/repository/numeric-column.transformer'
import { UserEntity } from '../user/user.entity'

@Entity('payment')
export class PaymentEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Generated('increment')
  @Column()
  invoiceId: number

  /** Recurrent payment started from this invoice */
  @Column()
  isActiveRecurrentParent: boolean

  /** Recurrent payment started from this invoice */
  @ManyToOne(() => PaymentEntity)
  @JoinColumn()
  parentPayment: PaymentEntity | null

  @Column()
  priceId: string

  @Column()
  status: PaymentStatus

  @Column()
  paymentUrl: string

  @Column()
  currency: Currency

  @Column({
    precision: 10,
    scale: 2,
    transformer: new NumericColumnTransformer(),
    type: 'numeric',
  })
  price: number

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn({ type: 'timestamp with time zone', nullable: true })
  updatedAt: Date | null

  @DeleteDateColumn({ type: 'timestamp with time zone', nullable: true })
  deletedAt: Date | null

  @ManyToOne(() => UserEntity, user => user.payments)
  @JoinColumn()
  user: UserEntity
}
