import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PaymentRepository } from './payment.repository'
import { PaymentService } from './payment.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([PaymentRepository]),
  ],
  providers: [
    PaymentService,
  ],
  exports: [PaymentService, PaymentImplModule],
})
export class PaymentImplModule {}
