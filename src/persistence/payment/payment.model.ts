import { PaymentEntity } from './payment.entity'

export type PaymentResponseModel = Omit<PaymentEntity, 'user'>
export type PaymentWithUserResponseModel = PaymentEntity

export interface PaymentCreateRequestModel extends Pick<PaymentEntity, 'paymentUrl' | 'currency' | 'price' | 'priceId'> {
  userId: string
  isActiveRecurrentParent?: boolean
  parentPaymentId?: string
}

export interface PaymentUpdateRequestModel extends Partial<Pick<PaymentEntity, 'status' | 'paymentUrl'>> {
  id: string
}
