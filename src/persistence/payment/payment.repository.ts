import { EntityRepository, UpdateResult } from 'typeorm'
import { BaseRepository } from '../../shared/repository/base-repository'
import { PaymentEntity } from './payment.entity'
import { PaymentResponseModel } from './payment.model'

@EntityRepository(PaymentEntity)
export class PaymentRepository extends BaseRepository<PaymentEntity> {
  cancelSubscriptionsByUserId(userId: string, exceptId?: string): Promise<UpdateResult> {
    const queryBuilder = this.createQueryBuilder('payment')
      .innerJoin('payment.user', 'user', 'user.id = payment.user_id AND user.id = :userId', { userId })
      .update(PaymentEntity)
      .set({
        isActiveRecurrentParent: false,
      })

    if (exceptId) {
      queryBuilder.where('payment.id != :exceptId', { exceptId })
    }

    return queryBuilder.execute()
  }

  getActiveParentPaymentIdsWithUpcomingPaidTillExpirationDate(paidExpiration: Date): Promise<Pick<PaymentResponseModel, 'id'>[]> {
    return this.createQueryBuilder('payment')
      .select('payment.id', 'id')
      .leftJoin('payment.user', 'user')
      .where('payment.isActiveRecurrentParent IS TRUE')
      .andWhere('user.paidTill <= :paidExpiration', { paidExpiration })
      .getRawMany()
  }
}
