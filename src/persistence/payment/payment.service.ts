import { Injectable } from '@nestjs/common'
import { EntityManager, UpdateResult } from 'typeorm'
import {
  PaymentCreateRequestModel,
  PaymentResponseModel,
  PaymentUpdateRequestModel,
  PaymentWithUserResponseModel,
} from './payment.model'
import { PaymentRepository } from './payment.repository'

@Injectable()
export class PaymentService {
  constructor(
    private repository: PaymentRepository,
  ) {}

  createOne(dto: PaymentCreateRequestModel, entityManager?: EntityManager): Promise<PaymentResponseModel> {
    const { userId, parentPaymentId, ...restDto } = dto
    return this.repository.createOrUpdateOne({
      ...restDto,
      user: { id: userId },
      parentPayment: parentPaymentId ? { id: parentPaymentId } : undefined,
    }, entityManager)
  }

  updateOne(dto: PaymentUpdateRequestModel, entityManager?: EntityManager): Promise<PaymentResponseModel> {
    return this.repository.createOrUpdateOne(dto, entityManager)
  }

  findOneById(id: string): Promise<PaymentResponseModel | undefined> {
    return this.repository.findOne(id)
  }

  findOneWithUserById(id: string): Promise<PaymentWithUserResponseModel | undefined> {
    return this.repository.findOne(id, { relations: ['user'] })
  }

  findOneByInvoiceId(invoiceId: number): Promise<PaymentResponseModel | undefined> {
    return this.repository.findOne({ where: { invoiceId } })
  }

  cancelSubscriptionsByUserId(userId: string, exceptId?: string): Promise<UpdateResult> {
    return this.repository.cancelSubscriptionsByUserId(userId, exceptId)
  }

  async getActiveParentPaymentIdsWithUpcomingPaidTillExpirationDate(paidExpiration: Date): Promise<string[]> {
    const payments = await this.repository.getActiveParentPaymentIdsWithUpcomingPaidTillExpirationDate(paidExpiration)
    return payments.map(payment => payment.id)
  }
} 
