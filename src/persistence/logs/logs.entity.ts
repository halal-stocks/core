import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { UserEntity } from '../user/user.entity'

@Entity('logs')
export class LogsEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ type: 'smallint', nullable: true })
  status: number | null

  @Column({ type: 'json', nullable: true })
  request: string | null

  @Column({ type: 'json', nullable: true })
  response: string | null

  @Column({ type: 'json', nullable: true })
  error: string | null

  @Column()
  createdAt: Date

  @ManyToOne(() => UserEntity, { nullable: true })
  @JoinColumn({ name: 'user_firebase_id' })
  user: UserEntity | null

  @Column({ type: 'varchar', nullable: true })
  userFirebaseId: string | null
}
