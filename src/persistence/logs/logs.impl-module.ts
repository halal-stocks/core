import { Global, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { LogsRepository } from './logs.repository'
import { LogsService } from './logs.service'

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([
      LogsRepository,
    ]),
  ],
  providers: [
    {
      provide: LogsService.name,
      useClass: LogsService,
    },
  ],
  exports: [
    LogsService,
    LogsImplModule,
  ],
})
export class LogsImplModule {}
