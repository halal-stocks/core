import { EntityRepository } from 'typeorm'
import { BaseRepository } from '../../shared/repository/base-repository'
import { LogsEntity } from './logs.entity'

@EntityRepository(LogsEntity)
export class LogsRepository extends BaseRepository<LogsEntity> {

}
