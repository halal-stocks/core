import { Injectable } from '@nestjs/common'
import { LogsEntity } from './logs.entity'
import { LogsRepository } from './logs.repository'

@Injectable()
export class LogsService {
  constructor(
    private repository: LogsRepository,
  ) {}

  async addLog(dto: Partial<LogsEntity>): Promise<void> {
    await this.repository.save(dto)
  }
}
