export enum CompanySector {
  FINANCIAL_SERVICES = 'FINANCIAL_SERVICES',
  TECHNOLOGY = 'TECHNOLOGY',
  BASIC_MATERIALS = 'BASIC_MATERIALS',
  COMMUNICATION_SERVICES = 'COMMUNICATION_SERVICES',
  CONSUMER_CYCLICAL = 'CONSUMER_CYCLICAL',
  CONSUMER_DEFENSIVE = 'CONSUMER_DEFENSIVE',
  ENERGY = 'ENERGY',
  HEALTHCARE = 'HEALTHCARE',
  INDUSTRIALS = 'INDUSTRIALS',
  REAL_ESTATE = 'REAL_ESTATE',
  UTILITIES = 'UTILITIES',
}
