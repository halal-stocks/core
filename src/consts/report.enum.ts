export type OtherCurrentAssetsType = number & { readonly _: unique symbol }
export type CashAndShortTermInvestmentsType = number & { readonly _: unique symbol }
export type LongTermInvestmentsType = number & { readonly _: unique symbol }
export type MarketCapType = number & { readonly _: unique symbol }
export type CashAndCashEquivalentsType = number & { readonly _: unique symbol }
export type ShortTermInvestmentsType = number & { readonly _: unique symbol }
export type NetReceivablesType = number & { readonly _: unique symbol }
export type InventoryType = number & { readonly _: unique symbol }
export type CurrentAssetsType = number & { readonly _: unique symbol }
export type PropertyPlantEquipmentNetType = number & { readonly _: unique symbol }
export type GoodwillType = number & { readonly _: unique symbol }
export type IntangibleAssetsType = number & { readonly _: unique symbol }
export type GoodwillAndIntangibleAssetsType = number & { readonly _: unique symbol }
export type TaxAssetsType = number & { readonly _: unique symbol }
export type OtherNonCurrentAssetsType = number & { readonly _: unique symbol }
export type TotalNonCurrentAssetsType = number & { readonly _: unique symbol }
export type OtherAssetsType = number & { readonly _: unique symbol }
export type TotalAssetsType = number & { readonly _: unique symbol }
export type AccountPayablesType = number & { readonly _: unique symbol }
export type ShortTermDebtType = number & { readonly _: unique symbol }
export type TaxPayablesType = number & { readonly _: unique symbol }
export type DeferredRevenueType = number & { readonly _: unique symbol }
export type OtherCurrentLiabilitiesType = number & { readonly _: unique symbol }
export type TotalCurrentLiabilitiesType = number & { readonly _: unique symbol }
export type LongTermDebtType = number & { readonly _: unique symbol }
export type DeferredRevenueNonCurrentType = number & { readonly _: unique symbol }
export type DeferredTaxLiabilitiesNonCurrentType = number & { readonly _: unique symbol }
export type OtherNonCurrentLiabilitiesType = number & { readonly _: unique symbol }
export type TotalNonCurrentLiabilitiesType = number & { readonly _: unique symbol }
export type OtherLiabilitiesType = number & { readonly _: unique symbol }
export type TotalLiabilitiesType = number & { readonly _: unique symbol }
export type CommonStockType = number & { readonly _: unique symbol }
export type RetainedEarningsType = number & { readonly _: unique symbol }
export type AccumulatedOtherComprehensiveIncomeLossType = number & { readonly _: unique symbol }
export type OthertotalStockholdersEquityType = number & { readonly _: unique symbol }
export type TotalStockholdersEquityType = number & { readonly _: unique symbol }
export type TotalLiabilitiesAndStockholdersEquityType = number & { readonly _: unique symbol }
export type TotalInvestmentsType = number & { readonly _: unique symbol }
export type TotalDebtType = number & { readonly _: unique symbol }
export type NetDebtType = number & { readonly _: unique symbol }
export type InterestExpenseType = number & { readonly _: unique symbol }
export type InterestIncomeType = number & { readonly _: unique symbol }
export type RevenueType = number & { readonly _: unique symbol }
export type SharesOutstandingType = number & { readonly _: unique symbol }
export type DepositToMarketCapRatioType = number & { readonly _: unique symbol }
export type DeptToMarketCapRatioType = number & { readonly _: unique symbol }
export type ForbiddenIncomeRatioType = number & { readonly _: unique symbol }
export type ForbiddenIncomePerShareType = number & { readonly _: unique symbol }
export type TickerType = string & { readonly _: unique symbol }
export type PERatioType = number & { readonly _: unique symbol }
export type PEGRatioType = number & { readonly _: unique symbol }
export type PSRatioType = number & { readonly _: unique symbol }
export type PBRatioType = number & { readonly _: unique symbol }
export type DividendYieldPercentageType = number & { readonly _: unique symbol }
export type DividendYieldType = number & { readonly _: unique symbol }
export type PayoutRatioType = number & { readonly _: unique symbol }
export type GrahamRatioType = number & { readonly _: unique symbol }
export type CurrentAssetsMinusLiabilitiesType = number & { readonly _: unique symbol }
export type FreeCashFlowType = number & { readonly _: unique symbol }
export type NetProfitMarginType = number & { readonly _: unique symbol }
export type ROEType = number & { readonly _: unique symbol }
export type ROAType = number & { readonly _: unique symbol }
export type BetaType = number & { readonly _: unique symbol }
export type CompanyReportDateType = Date & { readonly _: unique symbol }
export type CompanyReportDateAsStringType = string & { readonly _: unique symbol }
export type EPSType = number & { readonly _: unique symbol }
export type StockPriceType = number & { readonly _: unique symbol }
export type CurrentCash = number & { readonly _: unique symbol }
export type IsCompanyHalalType = boolean & { readonly _: unique symbol } | null
export type IsCompanyFreeToUse = boolean & { readonly _: unique symbol } | null
