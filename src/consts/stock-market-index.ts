export enum StockMarketIndex {
  SP500 = 'SP500',
  DOW_JONES = 'DOW_JONES',
  NASDAQ = 'NASDAQ'
}
