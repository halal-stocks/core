export enum PortfolioValueHistoryPeriod {
  TEN_DAYS = 'TEN_DAYS',
  ONE_MONTH = 'ONE_MONTH',
  SIX_MONTH = 'SIX_MONTH',
  TTM = 'TTM',
  FIVE_YEAR = 'FIVE_YEAR',
  ALL = 'ALL',
}
