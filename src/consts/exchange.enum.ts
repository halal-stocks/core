export enum Exchange {
  ETF = 'ETF',
  MUTUAL_FUND = 'MUTUAL_FUND',
  COMMODITY = 'COMMODITY',
  INDEX = 'INDEX',
  CRYPTO = 'CRYPTO',
  FOREX = 'FOREX',
  TSX = 'TSX',
  AMEX = 'AMEX',
  NASDAQ = 'NASDAQ',
  NYSE = 'NYSE',
  EURONEXT = 'EURONEXT',
  MCX = 'MCX',
}

export const ExchangeFullNameToShortName = {
  'NYSE Arca': Exchange.NYSE,
  'New York Stock Exchange': Exchange.NYSE,
  'Nasdaq Global Select': Exchange.NASDAQ,
  'NASDAQ Global Market': Exchange.NASDAQ,
  'NASDAQ Capital Market': Exchange.NASDAQ,
  'BATS Exchange': Exchange.ETF,
}
