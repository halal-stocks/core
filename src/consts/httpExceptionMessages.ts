export const httpExceptionMessages = {
  user: {
    notFoundById: 'User not found by id',
    notFoundByEmail: 'User not found by email',
    notFoundByFirebaseId: 'user not found by firebase id',
    notFoundByPaymentId: 'user not found by payment id',
  },
  indicators: {
    notFoundByTicker: 'Did not found indicators by ticker',
  },
  company: {
    notFoundByTicker: 'Company not found by ticker',
    alreadyExists: 'Company already exists',
    companyDoesNotAnyShariahFilterResult: 'Company does not have any shariah filter results',
  },
  stockMarketIndex: {
    notFoundByCode: 'Stock market index not found by code',
  },
  datahub: {
    sp500UrlNotProvided: 'Api url to get S&P 500 not provided',
  },
  fmp: {
    notFoundQuoteByTicker: 'Did not found any quote by ticker',
    notFoundMarketCapitalizationByTicker: 'Did not found any market capitalization by ticker',
    notFoundKeyMetricsByTicker: 'Did not found any key metrics by ticker',
    notFoundCashFlowByTicker: 'Did not found any cash flow by ticker',
    notFoundIncomeStatementsTicker: 'Did not found any income statements by ticker',
    notFoundBalanceSheetByTicker: 'Did not found any balance sheet by ticker',
    notFoundRatioTTMByTicker: 'Did not found any ratio ttm by ticker',
    notFoundCompanyProfileByTicker: 'Did not found company profile by ticker',
    notFoundCompanyHistoricalStockPriceByTicker: 'Did not found company historical stock price by ticker',
    notFoundCompanyHistoricalMarketCapByTicker: 'Did not found company historical market cap by ticker',
    notFoundCompanyHistoricalBriefPrice: 'Did not found company historical brief price',
  },
  yahoo: {
    notFoundInterestIncome: 'Did not found interest income',
  },
  portfolioValueHistory: {
    notSupportedPeriod: 'Selected period is not supported',
  },
  iexcloud: {
    notFoundCompanyFundamental: 'Did not found company fundamentals',
  },
  payment: {
    signatureIsNotValidOnResultWebhook: 'Signature is not valid on result webhook',
    notFoundByInvoiceId: 'Payment not found by invoice id',
    notFoundById: 'Payment not found by id',
  },
  mailer: {
    notEmailFormat: 'Doesn\'t looks like email',
    alreadySubscribed: 'You are already subscribed',
  },
  wishlist: {
    alreadyInList: 'Company already in user\'s wishlist',
  },
}
