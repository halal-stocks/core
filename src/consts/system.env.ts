export enum NodeEnv {
  production = 'production',
  local = 'local',
  synchronizer = 'synchronizer',
}
