import { PortfolioItemTradeResponseDto } from '../../../api/portfolio/portfolio.dto'
import { calculateTotal } from '../calculate-total/calculate-total.util'
import toFixed from '../to-fixed/to-fixed.util'

export const getAverage = <T extends number>(values: number[]): T => {
  if (!values.length) {
    return 0 as unknown as T
  }
  const average = calculateTotal<T>(values) / values.length
  return toFixed(average) as unknown as T
}

export const getPortfolioTradesAverage = <T extends number>(trades: Pick<PortfolioItemTradeResponseDto, 'quantity' | 'entryStockPrice'>[]): T => {
  const { totalSharesPrice, totalSharesCount } = trades.reduce((acc, cur) => {
    return {
      totalSharesPrice: acc.totalSharesPrice + (cur.entryStockPrice * cur.quantity),
      totalSharesCount: acc.totalSharesCount + cur.quantity,
    }
  }, { totalSharesPrice: 0, totalSharesCount: 0 })

  return ((totalSharesPrice / totalSharesCount) || 0) as T
}
