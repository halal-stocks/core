import toFixed from '../to-fixed/to-fixed.util'

const calculateThePercentageChange = (newNumber: number, originalNumber: number): number => {
  return toFixed((newNumber - originalNumber) / originalNumber * 100)
}

export default calculateThePercentageChange
