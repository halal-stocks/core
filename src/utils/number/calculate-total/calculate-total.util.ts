export const calculateTotal = <T extends number>(values: number[]): T => {
  if (!values.length) {
    return 0 as unknown as T
  }
  return values.reduce((acc, cur) => acc + cur, 0) as unknown as T
}
