import toFixed from '../to-fixed/to-fixed.util'

const calculateNumberFromPercentage = (number: number, percentage: number): number => {
  return toFixed((percentage / 100) * number)
}

export default calculateNumberFromPercentage
