/**
 * Need to convert to unknown before setting as T
 * @param value
 */
import { isNullOrUndefined } from '../../validation/variable-type/variable-type.util'

function stringToNumber<T = number>(value: string): T
function stringToNumber<T = number>(value: string, nullValues: string[]): T | null
function stringToNumber<T = number>(value: string, nullValues?: string[]): T | null {
  if (nullValues && nullValues.includes(value)) {
    return null
  }
  if (isNullOrUndefined(value)) {
    return value
  }
  const result = parseFloat(value.replace(/,/g, '')) as unknown
  return result as T
}

export default stringToNumber
