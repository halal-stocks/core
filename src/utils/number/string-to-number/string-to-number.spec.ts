import stringToNumber from './string-to-number.util'

describe('stringToNumber', () => {
  it('should return integer number', () => {
    expect(stringToNumber('2'))
    .toStrictEqual(2)
  })

  it('should return float number', () => {
    expect(stringToNumber('2.3481'))
    .toStrictEqual(2.3481)
  })
})
