import { isNullOrUndefined } from '../../validation/variable-type/variable-type.util'

const toFixed = <T>(value: T, precision: number = 2): T => {
  if (isNullOrUndefined(value)) {
    return value
  }
  const power = Math.pow(10, precision)
  const normalizedValue = value as unknown as number
  const result = Math.round(normalizedValue * power) / power
  return result as unknown as T
}

export default toFixed
