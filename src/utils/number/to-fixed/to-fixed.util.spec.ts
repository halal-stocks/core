import toFixed from './to-fixed.util'

describe('toFixed', () => {
  it('should return rounded up numbed', () => {
    expect(toFixed(2.3481))
    .toStrictEqual(2.35)
  })


  it('should return rounded down numbed', () => {
    expect(toFixed(2.3411))
    .toStrictEqual(2.34)
  })
})
