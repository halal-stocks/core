import toFixed from '../to-fixed/to-fixed.util'

const calculatePercentage = (from: number, number: number): number => {
  return toFixed(number / from * 100)
}

export default calculatePercentage
