export const ceilNumber = <T>(number: T): T => {
  return Math.ceil(number as unknown as number) as unknown as T
}
