import { isNullOrUndefined } from '../validation/variable-type/variable-type.util'

export const trim = (string: string, delimiter: string): string => {
  let l = 0
  let r = string.length - 1
  while (delimiter.indexOf(string[l]) >= 0 && l < r) l++
  while (delimiter.indexOf(string[r]) >= 0 && r >= l) r--
  return string.substring(l, r + 1)
}

export const removeSubstring = (str: string, subStr: string): string => {
  const regExp = new RegExp(subStr, 'g')
  return str.replace(regExp, '')
}

export function removeNewLines(string: string): string
export function removeNewLines(string: string | null): string
export function removeNewLines(string: string | null): string | null {
  if (isNullOrUndefined(string)) {
    return string
  }
  return string.replace(/[\n\r]/g, '').trim()
}
