export const decodeBase64 = <T>(encodedString: string): T => {
  const decoded = Buffer.from(encodedString, 'base64').toString('utf-8')
  return JSON.parse(JSON.parse(decoded))
}

export const encodeToBase64 = (obj: WithImplicitCoercion<Uint8Array | ReadonlyArray<number> | string>): string => {
  return Buffer.from(obj).toString('base64')
}
