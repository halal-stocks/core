import { isString } from '../validation/variable-type/variable-type.util'
import { toUpperCase } from './string.util'

export const spaceToUnderscore = <T>(str: T, isToUppercase: boolean = false): T => {
  if (!isString(str)) {
    return '' as unknown as T
  }
  const replaced = str.replace(/ /g, '_') as unknown as T
  return isToUppercase ? toUpperCase(replaced) : replaced
}
