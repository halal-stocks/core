import { isNullOrUndefined, isString } from '../validation/variable-type/variable-type.util'

export const replaceIfNulOrUndefined = (string: any, replaceTo = '') => {
  return isNullOrUndefined(string) ? '' : string
}

export const stringToBoolean = (str: string | number | boolean | undefined): boolean => {
  return (str === true || str === 'true' || str === 1)
}

export const capitalizeFirstLetter = (str: string): string => {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

export const lowercaseFirstLetter = (str: string): string => {
  return str.charAt(0).toLowerCase() + str.slice(1)
}

export const toUpperCase = <T>(str: T): T => {
  if (!isString(str)) {
    return '' as unknown as T
  }
  return str.toLocaleUpperCase() as unknown as T
}
