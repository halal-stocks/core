import { BadRequestException } from '@nestjs/common'
import {
  validateManyRecordExistence,
  validateOneRecordExistence,
  validateRecordExistence,
} from './validate-record-existance.util'

describe('validateRecordExistence', () => {
  it('should return positive result with one record', () => {
    expect(validateRecordExistence('1', [{ id: '1' }], ''))
    .toBeUndefined()
  })

  it('should return positive result with many records', () => {
    expect(validateRecordExistence(['1'], [{ id: '1' }], ''))
    .toBeUndefined()
  })

  it('should throw BadRequestException error', (done) => {
    try {
      validateRecordExistence('1', [], '')
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException)
      done()
    }
  })
})

describe('validateOneRecordExistence', () => {
  it('should return positive result', () => {
    expect(validateOneRecordExistence('1', [{ id: '1' }], ''))
    .toBeUndefined()
  })

  it('should throw BadRequestException error', (done) => {
    try {
      validateOneRecordExistence('1', [], '')
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException)
      done()
    }
  })
})

describe('validateManyRecordExistence', () => {
  it('should return positive result', () => {
    expect(validateManyRecordExistence(['1'], [{ id: '1' }], ''))
    .toBeUndefined()
  })

  it('should throw BadRequestException error', (done) => {
    try {
      validateManyRecordExistence(['1'], [], '')
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException)
      done()
    }
  })
})
