import { BadRequestException } from '@nestjs/common'
import { isArray } from '../..'
import arrayToObject from '../../object/array-to-object/array-to-object.util'

export const validateRecordExistence = (initial: string[] | string, fromDB: { id: string }[], errorTemplate: string): void => {
  if (isArray(initial)) {
    return validateManyRecordExistence(initial as string[], fromDB, errorTemplate)
  }
  return validateOneRecordExistence(initial as string, fromDB, errorTemplate)
}

export const validateOneRecordExistence = (id: string, fromDB: { id: string }[], errorTemplate: string): void => {
  if (!!fromDB.length) {
    return
  }
  throw new BadRequestException(`${errorTemplate} ${id}`)
}

export const validateManyRecordExistence = (ids: string[], records: { id: string }[], errorTemplate: string): void => {
  const idsWithoutDuplicates = [...new Set(ids)]
  if (records.length === idsWithoutDuplicates.length) {
    return
  }

  const recordsAsObject = arrayToObject(records)

  const notExists = idsWithoutDuplicates.filter(it => !recordsAsObject[it])

  throw new BadRequestException(`${errorTemplate} ${notExists}`)
}
