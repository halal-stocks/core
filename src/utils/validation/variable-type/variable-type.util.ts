import { isDate as isDateValidator } from 'class-validator'

const isNullOrUndefined = <T>(obj: T | null | undefined): obj is null | undefined => {
  return typeof obj === 'undefined' || obj === null
}

const isUndefined = <T>(obj: T | undefined): obj is undefined => {
  return typeof obj === 'undefined'
}
const isBoolean = (arg: any): arg is boolean => Object.prototype.toString.call(arg) === '[object Boolean]'
const isObject = <T>(arg: any): arg is Record<string, T> => Object.prototype.toString.call(arg) === '[object Object]'
const isArray = <T>(arg: T[] | any): arg is T[] => Object.prototype.toString.call(arg) === '[object Array]'
const isString = (arg: any): arg is string => Object.prototype.toString.call(arg) === '[object String]'
const isFunction = (arg: any): arg is Function => Object.prototype.toString.call(arg) === '[object Function]'
const isNumber = (arg: any): arg is number => !isNaN(arg) && Object.prototype.toString.call(arg) === '[object Number]'
const isDate = (arg: any): arg is Date => isDateValidator(arg)
const isType = <T extends unknown>(item: any, keys: string[]): item is T => {
  return keys.every(key => (item as T)[key] !== undefined)
}
const isFalsy = (arg: any): arg is false => arg === false
const isTruthy = (arg: any): arg is true => arg === true

const isKeyIterable = (arg: any): arg is number | string => isNumber(arg) || isString(arg)
export {
  isUndefined,
  isBoolean,
  isNullOrUndefined,
  isObject,
  isArray,
  isString,
  isNumber,
  isType,
  isDate,
  isFunction,
  isKeyIterable,
  isFalsy,
  isTruthy,
}
