import { isArray, isDate, isFunction, isKeyIterable, isNumber, isObject, isString, isType } from './variable-type.util'

describe('isType', () => {
  it('should return positive result', () => {
    expect(isType(
      {
        foo: '1',
      },
      ['foo'],
      ),
    )
    .toBe(true)
  })

  it('should return negative result', () => {
    expect(
      isType(
        {
          foo: '1',
        },
        ['bar'],
      ),
    )
    .toBe(false)
  })
})

describe('isKeyIterable', () => {
  it('should return positive result to number', () => {
    expect(isKeyIterable(1)).toBe(true)
  })

  it('should return positive result to string', () => {
    expect(isKeyIterable('1')).toBe(true)
  })

  it('should return negative result to any format except string and number', () => {
    expect(isKeyIterable({})).toBe(false)
  })
})

describe('isObject', () => {
  it('should return positive result', async () => {
    expect(isObject({})).toBe(true)
  })

  it('should return negative result', async () => {
    expect(isObject(1)).toBe(false)
  })
})
describe('isArray', () => {
  it('should return positive result', async () => {
    expect(isArray([])).toBe(true)
  })

  it('should return negative result', async () => {
    expect(isArray({})).toBe(false)
  })
})
describe('isString', () => {
  it('should return positive result', async () => {
    expect(isString('')).toBe(true)
  })

  it('should return negative result', async () => {
    expect(isString(1)).toBe(false)
  })
})
describe('isFunction', () => {
  it('should return positive result', async () => {
    expect(isFunction(() => {})).toBe(true)
  })

  it('should return negative result', async () => {
    expect(isFunction({})).toBe(false)
  })
})
describe('isNumber', () => {
  it('should return positive result', async () => {
    expect(isNumber(1)).toBe(true)
  })

  it('should return negative result', async () => {
    expect(isNumber('')).toBe(false)
  })
})
describe('isDate', () => {
  it('should return positive result', async () => {
    expect(isDate(new Date())).toBe(true)
  })

  it('should return negative result', async () => {
    expect(isDate({})).toBe(false)
  })
})

