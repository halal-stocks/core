import { IsCompanyFreeToUse } from '../../consts/report.enum'
import { LockableData } from '../../shared/generics/lockable-data.generic'

export const lockDataUtil = <T>(data: T, isCompanyFree: IsCompanyFreeToUse, isUserPaid: boolean): LockableData<T> => {
  return (isUserPaid || isCompanyFree) ? data : 'locked'
}
