import { add } from 'date-fns'
import { BillingPlanPeriod } from '../../domains/payment/payment.interfaces'

export const getPayedTillDate = (period: BillingPlanPeriod, startDate = new Date()): Date => {
  return add(startDate, {
    months: period === BillingPlanPeriod.monthly ? 1 : 0,
    years: period === BillingPlanPeriod.yearly ? 1 : 0,
  })
}
