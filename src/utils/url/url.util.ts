import * as FormData from 'form-data'

interface GenerateUrlUtl {
  baseUrl: string,
  params?: ParamType
  urlParts?: string[]
}

type ParamType = Record<string, string | number | boolean | undefined | null>


export const generateUrl = (props: GenerateUrlUtl): string => {
  const paramsAsString = props.params && urlParamsToString(props.params)
  const queryPart = paramsAsString && paramsAsString.length
    ? `?${paramsAsString}`
    : ''

  const urlPart = props.urlParts ? `/${props.urlParts.join('/')}` : ''

  return `${props.baseUrl}${urlPart}${queryPart}`
}

export const urlParamsToString = (params: ParamType): string => {
  return Object.entries(params)
    .filter(([key, value]) => key && value)
    .map(([key, value]) => `${key}=${value}`)
    .join('&')
}

export const getFormData = (params?: Record<string, string | boolean | number>): FormData => {
  const form = new FormData()

  if (params) {
    Object.entries(params).forEach(([key, value]) =>
      form.append(key, `${value}`),
    )
  }

  return form
}
