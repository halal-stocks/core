import { BadRequestException } from '@nestjs/common'
import ArrayAsObject from '../../../shared/generics/array-as-object.generic'
import { isArray, isNullOrUndefined } from '../../index'

/**
 * @deprecated Use array-to-map instead
 */
export default function arrayToObject<T>(arr: T[], key?: string): ArrayAsObject<T>
export default function arrayToObject<T>(arr: T[], key: string, arrayValue: true): ArrayAsObject<T[]>

/**
 *
 * @param arr
 * @param key. To get value from child objects - provide path to property adding dot. Ex: product.id
 * @param arrayValue true
 */
export default function arrayToObject<T>(arr: T[], key: string = 'id', arrayValue?: true): ArrayAsObject<T> | ArrayAsObject<T[]> {
  const keyParts = key.split('.')

  if (isNullOrUndefined(arr) || !isArray(arr)) {
    throw new BadRequestException(`Unable to convert array to object`)
  }

  return arr.reduce((acc, cur) => {
    let value
    if (keyParts.length === 1) {
      value = cur[key]
    } else {
      let keyValue = null
      for (const keyPart of keyParts) {
        keyValue = keyValue ? keyValue[keyPart] : cur[keyPart]
      }
      value = keyValue
    }

    if (value) {
      if (arrayValue) {
        acc[value] ? acc[value].push(cur) : acc[value] = [cur]
      } else {
        acc[value] = cur
      }
    }
    return acc
  }, {})
}
