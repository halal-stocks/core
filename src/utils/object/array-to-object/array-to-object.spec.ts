import { BadRequestException } from '@nestjs/common'
import arrayToObject from './array-to-object.util'

const mockedObject = {
  id: 1,
  foo: '1',
  bar: {
    nested: 1,
  },
}

describe('arrayToObject', () => {
  it('should return properly converted object', () => {
    expect(arrayToObject([mockedObject]))
    .toStrictEqual({
      [mockedObject.id]: mockedObject,
    })
  })

  it('should return properly converted array object with nested key property', () => {
    expect(arrayToObject([mockedObject, mockedObject], 'bar.nested', true))
    .toStrictEqual({
      [mockedObject.id]: [mockedObject, mockedObject],
    })
  })

  it('should return empty object', () => {
    expect(arrayToObject([mockedObject, mockedObject], 'nothing.nested', true))
    .toStrictEqual({})
  })

  it('should return properly converted array object ', () => {
    expect(arrayToObject([mockedObject, mockedObject], 'id', true))
    .toStrictEqual({
      [mockedObject.id]: [mockedObject, mockedObject],
    })
  })

  it('should throw BadRequestException error', (done) => {
    try {
      arrayToObject(null!)
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException)
      done()
    }
  })
})
