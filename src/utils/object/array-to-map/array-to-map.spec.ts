import { arrayToMap } from './array-to-map.util'

const mockedObject = {
  id: 1,
  foo: '1',
  bar: {
    nested: 1,
  },
}

describe('arrayToObject', () => {
  it('should return properly converted to map', () => {
    const converted = arrayToMap([mockedObject], 'id')
    expect(converted.has(mockedObject.id))
      .toBeTruthy()
  })

  it('should return properly converted array object with nested key property', () => {
    const converted = arrayToMap([mockedObject, mockedObject], 'id', true)
    expect(converted.get(mockedObject.id))
      .toStrictEqual([mockedObject, mockedObject])
  })
})
