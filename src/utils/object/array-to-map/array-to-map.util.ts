export function arrayToMap<T, K extends keyof T>(arr: T[], key: K): Map<T[K], T>
export function arrayToMap<T, K extends keyof T>(arr: T[], key: K, arrayValue: true): Map<T[K], T[]>
export function arrayToMap<T, K extends keyof T>(arr: T[], key: K, arrayValue?: true): Map<T[K], T | T[]> {
  const map = new Map()
  for (const arrElement of arr) {
    const keyValue = arrElement[key]
    if (arrayValue) {
      map.has(keyValue)
        ? map.set(keyValue, [...map.get(keyValue), arrElement])
        : map.set(keyValue, [arrElement])
    } else {
      map.set(keyValue, arrElement)
    }
  }
  return map
}
