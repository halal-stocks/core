export const reversEnumUtil = (enumObject: Record<string, string>): Record<string, string> => {
  return Object.entries(enumObject).reduce((acc, [key, value]) => {
    return { ...acc, [value]: key }
  }, {})
}
