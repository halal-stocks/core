import { isArray } from '..'

const convertParamToArray = <T>(param: T | T[]): T[] => {
  return isArray(param) ? param : [param]
}

export default convertParamToArray
