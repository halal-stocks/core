import { isNullOrUndefined } from '../validation/variable-type/variable-type.util'

export const replaceIfNullUtil = <T>(input: T | null, replace: any): T => {
  return isNullOrUndefined(input) ? replace as T : input
}
