import { isUndefined } from '../validation/variable-type/variable-type.util'

export const nullIfUndefined = <T>(input: T | undefined): T | null => {
  return isUndefined(input) ? null : input
}
