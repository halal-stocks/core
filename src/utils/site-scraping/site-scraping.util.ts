import { InternalServerErrorException } from '@nestjs/common'
import * as cheerio from 'cheerio'
import { CheerioAPI } from 'cheerio'
import * as puppeteer from 'puppeteer'
import { Browser, Page } from 'puppeteer'

interface GetPageContent {
  url: string
  browser: Browser
  pageBlocks?: PageBlocks,
  onPageLoad?: (page: Page) => void
  onEmptyPageReady?: (page: Page) => void
  beforePageLoad?: (browser: Browser) => void
  className?: string
}

interface PageBlocks {
  blockStylesheet?: boolean
  blockFont?: boolean
  blockImage?: boolean
  noBlocks?: boolean
}

interface GetPage {
  browser: Browser,
  pageBlocks?: PageBlocks,
}

export const getPageContent = async (props: GetPageContent): Promise<CheerioAPI> => {
  if (props.beforePageLoad) {
    await props.beforePageLoad(props.browser)
  }

  const page = await getEmptyPage({ browser: props.browser, pageBlocks: props.pageBlocks })
  if (props.onEmptyPageReady) {
    await props.onEmptyPageReady(page)
  }

  await page.goto(props.url).catch(e => {
    throw new InternalServerErrorException(`Can't navigate to url to the post in ${props.className}`)
  })

  if (props.onPageLoad) {
    await props.onPageLoad(page)
  }
  const pageContent = await page.content()
  const loadedCheerio = cheerio.load(pageContent)
  // await page.close()
  return loadedCheerio
}

export const getBrowser = async (): Promise<Browser> => {
  return puppeteer.launch({
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
    ],
  })
}

export const getEmptyPage = async (props: GetPage): Promise<Page> => {
  const {
    browser,
    pageBlocks,
  } = props

  const {
    blockImage = true,
    blockStylesheet = true,
    blockFont = true,
    noBlocks = false,
  } = pageBlocks || {}

  const page = await browser.newPage()
  page.setDefaultNavigationTimeout(0)
  if (!noBlocks) {
    await page.setRequestInterception(true)
    page.on('request', (req) => {
      if (
        (req.resourceType() == 'stylesheet' && blockStylesheet)
        || (req.resourceType() == 'font' && blockFont)
        || req.resourceType() == 'image' && blockImage) {
        req.abort()
      } else {
        req.continue()
      }
    })
  }
  return page
}

export const scrollToBottom = async (page: Page): Promise<void> => {
  await page.evaluate(async () => {
    await new Promise<void>((resolve, reject) => {
      let totalHeight = 0
      const distance = 100
      const timer = setInterval(() => {
        const scrollHeight = document.body.scrollHeight
        window.scrollBy(0, distance)
        totalHeight += distance

        if (totalHeight >= scrollHeight) {
          clearInterval(timer)
          resolve()
        }
      }, 100)
    })
  })
}

export const takeScreenShot = async (page: Page, path = 'screenshot.png'): Promise<void> => {
  await page.screenshot({
    path,
    fullPage: true,
  })
}
