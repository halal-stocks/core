import { Module } from '@nestjs/common'
import { CompanyImplModule } from '../../persistence/company/company.impl-module'
import { StockMarketIndexImplModule } from '../../persistence/stock-market-index/stock-market-index.impl-module'
import { RpcPublishersModule } from '../../rpc/rpc-publishers.module'
import { FiltersDomainModule } from '../filters/filters.domain-module'
import { ThirdPartyDataSourceDomainModule } from '../third-party-data-source/third-party-data-source.domain-module'
import { CompanyIndicatorsDomain } from './indicators/indicators.domain'
import { CompanyShariahFilterResultDomain } from './shariah-filter-result/shariah-filter-result.domain'
import { StockMarketIndexDomain } from './stock-market-index/stock-market-index.domain'
import { CompanyAnalyzeDomain } from './subdomains/analyze/company-analyze.domain'
import { CompanyMutationDomain } from './subdomains/mutation/company-mutation.domain'
import { CompanyQueryDomain } from './subdomains/query/company-query.domain'
import { CompanySchedulerDomain } from './subdomains/scheduler/company-scheduler.domain'

@Module({
  imports: [
    CompanyImplModule,
    RpcPublishersModule,
    FiltersDomainModule,
    ThirdPartyDataSourceDomainModule,
    StockMarketIndexImplModule,
  ],
  providers: [
    CompanyQueryDomain,
    CompanyMutationDomain,
    CompanyIndicatorsDomain,
    StockMarketIndexDomain,
    CompanyShariahFilterResultDomain,
    CompanyAnalyzeDomain,
    CompanySchedulerDomain,
  ],
  exports: [
    CompanyDomainModule,
    CompanyIndicatorsDomain,
    CompanySchedulerDomain,
    CompanyAnalyzeDomain,
    CompanyQueryDomain,
    CompanyMutationDomain,
    StockMarketIndexDomain,
    CompanyShariahFilterResultDomain,
  ],
})
export class CompanyDomainModule {}
