import { StockPriceType, TickerType } from '../../../../consts/report.enum'
import { CompanyEntity } from '../../../../persistence/company/company.entity'

export interface CompanyCreateResponse extends Pick<CompanyEntity, 'id' | 'name' | 'description' | 'fullTimeEmployees' | 'industry' | 'ticker' | 'sector' | 'exchange' | 'isHalal' | 'createdAt' | 'updatedAt' | 'deletedAt'> {}

export class CompanyCreateRequest {
  ticker: TickerType
  stockPrice?: StockPriceType
  stockMarketIndexId?: string
}
