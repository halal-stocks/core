import { ConflictException, Injectable } from '@nestjs/common'
import { Connection } from 'typeorm'
import { httpExceptionMessages } from '../../../../consts/httpExceptionMessages'
import { TickerType } from '../../../../consts/report.enum'
import { CompanyService } from '../../../../persistence/company/company.service'
import { CompanyPublisher } from '../../../../rpc/company/company.publisher'
import {
  commitTransaction,
  releaseTransaction,
  rollbackTransactionAndThrowError,
  startTransaction,
} from '../../../../shared/repository/transaction'
import { spaceToUnderscore } from '../../../../utils/string/space-to-underscore.util'
import { FmpDomain } from '../../../third-party-data-source/fmp/fmp.domain'
import { SeekingalphaDomain } from '../../../third-party-data-source/seekingalpha/seekingalpha.domain'
import { CompanyQueryDomain } from '../query/company-query.domain'
import { CompanyCreateRequest, CompanyCreateResponse } from './company-mutation.interfaces'

@Injectable()
export class CompanyMutationDomain {
  constructor(
    private fmpDomain: FmpDomain,
    private seekingalphaDomain: SeekingalphaDomain,
    private connection: Connection,
    private companyService: CompanyService,
    private companyQueryDomain: CompanyQueryDomain,
    private companyPublisher: CompanyPublisher,
  ) {}


  async createOne(dto: CompanyCreateRequest): Promise<CompanyCreateResponse> {
    const isExists = await this.companyQueryDomain.findCompanyIdsByTickers(dto.ticker)
    if (isExists) {
      throw new ConflictException(httpExceptionMessages.company.alreadyExists)
    }
    const company = await this._createOne(dto)
    await this.companyPublisher.publishCompanyCreatedMessage(dto.ticker)
    await new Promise((resolve, rejects) => {
      setTimeout(() => {
        resolve('')
      }, 1000)
    })
    return company
  }

  async createMany(dtos: CompanyCreateRequest[]): Promise<void> {
    dtos.forEach(dto => {
      this.companyPublisher.publishCompanyToCreateMessage({
        ticker: dto.ticker,
      })
    })
  }

  test(ticker: TickerType): Promise<any> {
    return this.seekingalphaDomain.getQuarterlyFinancialsParse(ticker)
  }

  /**
   * This function will try to create a company. But wont throw an exception if occurs
   * @param ticker
   */
  async tryToCreateCompany(ticker: TickerType): Promise<void> {
    await this.createOne({ ticker }).catch(e => {})
  }

  async deleteByTicker(tickers: TickerType[]): Promise<void> {
    !!tickers.length && await this.companyService.deleteManyByTickers(tickers)
  }

  private async _createOne(dto: CompanyCreateRequest): Promise<CompanyCreateResponse> {
    const companyProfile = await this.fmpDomain.getCompanyProfile(dto.ticker)
    const { entityManager, queryRunner } = await startTransaction(this.connection)
    try {
      const company = await this.companyService.createOne({
        name: companyProfile.companyName,
        sector: spaceToUnderscore(companyProfile.sector, true),
        exchange: companyProfile.exchangeShortName,
        ticker: dto.ticker,
        description: companyProfile.description,
        fullTimeEmployees: +companyProfile.fullTimeEmployees,
        industry: companyProfile.industry,
        stockMarketIndexes: dto.stockMarketIndexId ? [{ id: dto.stockMarketIndexId }] : undefined,
      }, entityManager)

      await commitTransaction(queryRunner)
      return company
    } catch (err) {
      return await rollbackTransactionAndThrowError(queryRunner, err)
    } finally {
      await releaseTransaction(queryRunner)
    }
  }
}

