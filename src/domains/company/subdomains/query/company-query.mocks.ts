export const mockedCompanyQueryDomain = {
  getAll: () => {},
  throwErrorIfNotExistsByTicker: () => {},
  getOneByTicker: () => {},
  getCompanyIdByTicker: () => {},
  findCompanyIdByTicker: () => {},
  findManyByTickerPartial: () => {},
  findManyBySector: () => {},
}
