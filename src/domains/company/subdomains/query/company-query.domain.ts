import { Injectable, NotFoundException } from '@nestjs/common'
import { format, sub } from 'date-fns'
import { CompanyDetailedResponseDto, CompanyHistoricalPriceResponseDto } from '../../../../api/company/company.dtos'
import { CompanySector } from '../../../../consts/company-sector'
import { httpExceptionMessages } from '../../../../consts/httpExceptionMessages'
import { StockPriceType, TickerType } from '../../../../consts/report.enum'
import { CompanyEntity } from '../../../../persistence/company/company.entity'
import { CompanyBriefResponseModel } from '../../../../persistence/company/company.model'
import { CompanyService } from '../../../../persistence/company/company.service'
import { CompanyPublisher } from '../../../../rpc/company/company.publisher'
import { isArray, isNumber, toUpperCase } from '../../../../utils'
import convertParamToArray from '../../../../utils/array/convert-param-to-array.util'
import { lockDataUtil } from '../../../../utils/data-limitation/lock-data.util'
import calculateThePercentageChange
  from '../../../../utils/number/calculate-the-percentage-change/calculate-the-percentage-change.util'
import toFixed from '../../../../utils/number/to-fixed/to-fixed.util'
import { FmpDomain } from '../../../third-party-data-source/fmp/fmp.domain'
import { CompanyBriefResponse, CompanySearchByTickerResponse } from './company-query.interfaces'

@Injectable()
export class CompanyQueryDomain {
  constructor(
    private companyService: CompanyService,
    private companyPublisher: CompanyPublisher,
    private fmpDomain: FmpDomain,
  ) {}

  async throwErrorIfNotExistsByTicker(ticker: TickerType): Promise<void> {
    const isExists = await this.companyService.isExistsByTicker(toUpperCase(ticker))
    if (!isExists) {
      throw new NotFoundException(httpExceptionMessages.company.notFoundByTicker)
    }
  }

  async getCompanyHistoricalPrice(ticker: TickerType, interval: number = 120): Promise<CompanyHistoricalPriceResponseDto[]> {
    const normalizedInterval = isNumber(interval) && (interval < 120 || interval > 0) ? interval : 120
    const from = format(sub(new Date(), { months: normalizedInterval }), 'yyyy-MM-dd')
    const history = await this.fmpDomain.findCompanyHistoricalBriefPrice(toUpperCase(ticker), from)
    return history.reverse().map(it => ({
      date: it.date,
      stockPrice: it.close as StockPriceType,
    }))
  }

  async getOneDetailedByTicker(ticker: TickerType, isPaid: boolean = false): Promise<CompanyDetailedResponseDto> {
    const company = await this.companyService.findOneDetailedByTicker(toUpperCase(ticker))
    if (!company) {
      throw new NotFoundException(httpExceptionMessages.company.notFoundByTicker)
    }

    return this.normalizeAndCalculateDetailedCompany(company, isPaid)
  }

  async getManyDetailedByIds(ids: string[], isPaid: boolean = false): Promise<CompanyDetailedResponseDto[]> {
    const companies = await this.companyService.findManyDetailedById(ids)
    return companies.map(company => this.normalizeAndCalculateDetailedCompany(company, isPaid))
  }

  async getCompanyIdByTicker(ticker: TickerType): Promise<string> {
    const companyId = await this.findCompanyIdsByTickers(toUpperCase(ticker))
    if (!companyId) {
      throw new NotFoundException(httpExceptionMessages.company.notFoundByTicker)
    }

    return companyId
  }

  async getCompanyIdMapByTickers(tickers: TickerType[]): Promise<Map<TickerType, string>> {
    const companies = await this.companyService.findManyByTickers(tickers)
    return new Map(companies.map(company => [company.ticker, company.id]))
  }

  async findCompanyIdsByTickers(ticker: TickerType): Promise<string | undefined>
  async findCompanyIdsByTickers(ticker: TickerType[]): Promise<string[]>
  async findCompanyIdsByTickers(ticker: TickerType | TickerType[]): Promise<string | undefined | string[]> {
    const tickers = convertParamToArray(ticker)
    const companies = await this.companyService.findIdByTicker(tickers.map(it => toUpperCase(it)))
    if (!isArray(ticker)) {
      return companies?.find(it => it.ticker === ticker.toUpperCase())?.id
    }
    return companies.map(company => company.id)
  }

  async findManyByTickerPartialOrName(tickerOrName: TickerType | string): Promise<CompanySearchByTickerResponse[]> {
    const result = await this.companyService.findManyByTickerPartialOrName(toUpperCase(tickerOrName))

    // Of result is empty then try fo find this company from 3 party services
    if (!result.length) {
      this.companyPublisher.publishNotFoundAnyCompanyByTickerMessage(tickerOrName as TickerType)
    }

    return result
  }

  async findManyBySector(sector: CompanySector): Promise<CompanyEntity[]> {
    return this.companyService.findManyBySector(sector)
  }

  async getBriefCompaniesBySector(sector: CompanySector, isPaid: boolean = false): Promise<CompanyBriefResponse[]> {
    const companies = await this.companyService.getBriefCompaniesBySectorIncludeDailyChangeIndicators(sector)
    return this.prepareBriefCompanyResponse(companies, isPaid)
  }

  async getBriefCompaniesByIndex(indexCode: string, isPaid: boolean = false): Promise<CompanyBriefResponse[]> {
    const companies = await this.companyService.getBriefCompaniesByIndexIncludeDailyChangeIndicators(indexCode)
    return this.prepareBriefCompanyResponse(companies, isPaid)
  }

  async getAllNotCompleteCompanies(): Promise<any[]> {
    return this.companyService.getCompaniesMissingShariahFilterResultOrIndicatorsOrAnalyzes()
  }

  private prepareBriefCompanyResponse(companies: CompanyBriefResponseModel[], isPaid: boolean): CompanyBriefResponse[] {
    return companies.map(company => {
      const { indicators, isHalal, isFree, ...restCompany } = company
      const dailyChangePercentage = calculateThePercentageChange(indicators.actualStockPrice, indicators.previousDayStockPrice)
      return {
        ...restCompany,
        dailyChangePercentage,
        stockPrice: indicators.actualStockPrice,
        isHalal: (isFree || isPaid) ? isHalal : 'locked',
      }
    })
  }

  private normalizeAndCalculateDetailedCompany(company: CompanyEntity, isPaid): CompanyDetailedResponseDto {
    const { indicators, shariahFilterResults, analyzes, isFree, isHalal, ...restCompany } = company

    const dailyChangePercentage = calculateThePercentageChange(indicators.actualStockPrice, indicators.previousDayStockPrice)
    const dailyChange = toFixed(indicators.actualStockPrice - indicators.previousDayStockPrice)

    return {
      id: restCompany.id,
      name: restCompany.name,
      ticker: restCompany.ticker,
      indexes: restCompany.stockMarketIndexes,
      industry: restCompany.industry,
      sector: restCompany.sector,
      employeeNumber: restCompany.fullTimeEmployees,
      description: restCompany.description,
      isHalal: lockDataUtil(isHalal, isFree, isPaid),
      dailyChange,
      dailyChangePercentage,
      indicators,
      analyze: {
        epsRatio: analyzes[0].eps,
        psRatio: analyzes[0].psRatio,
        beta: analyzes[0].beta,
        peRatio: analyzes[0].peRatio,
        pbRatio: analyzes[0].pbRatio,
        dividendYield: analyzes[0].dividendYield,
      },
      shariahFilterResults: lockDataUtil(shariahFilterResults[0], isFree, isPaid),
    }
  }
}
