import { Test } from '@nestjs/testing'
import { CompanySector } from '../../../../consts/company-sector'
import { dummyAppleCompanyIndicators } from '../../../../dummy/company-indicators/company-indicators.dummy'
import { dummyAppleCompany } from '../../../../dummy/company/company.dummy'
import { mockedCompanyService } from '../../../../persistence/company/company.mocks'
import { CompanyService } from '../../../../persistence/company/company.service'
import { mockedCompanyPublisher } from '../../../../rpc/company/company.mocks'
import { CompanyPublisher } from '../../../../rpc/company/company.publisher'
import { FmpDomain } from '../../../third-party-data-source/fmp/fmp.domain'
import { mockedFmpDomain } from '../../../third-party-data-source/fmp/fmp.mocks'
import { CompanyQueryDomain } from './company-query.domain'
import { CompanyBriefResponse } from './company-query.interfaces'

describe('CompanyQueryDomain', () => {
  let companyQueryDomain: CompanyQueryDomain

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        providers: [
          CompanyQueryDomain,
          CompanyService,
          CompanyPublisher,
          FmpDomain,
        ],
      })
      .overrideProvider(CompanyService)
      .useValue(mockedCompanyService)
      .overrideProvider(CompanyPublisher)
      .useValue(mockedCompanyPublisher)
      .overrideProvider(FmpDomain)
      .useValue(mockedFmpDomain)
      .compile()

    companyQueryDomain = moduleRef.get<CompanyQueryDomain>(CompanyQueryDomain)
  })

  describe('getBriefCompaniesBySector', () => {
    it('should return list of companies with daily change', async () => {
      expect(await companyQueryDomain.getBriefCompaniesBySector(CompanySector.FINANCIAL_SERVICES)).toStrictEqual([
        {
          id: dummyAppleCompany.id,
          ticker: dummyAppleCompany.ticker,
          isHalal: dummyAppleCompany.isHalal,
          name: dummyAppleCompany.name,
          dailyChangePercentage: 0.18,
          stockPrice: dummyAppleCompanyIndicators.actualStockPrice,
        },
      ] as CompanyBriefResponse[])
    })
  })
})
