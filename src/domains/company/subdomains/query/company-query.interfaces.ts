import { CompanyIndicatorsResponseDto } from '../../../../api/company/company.dtos'
import { CompanySector } from '../../../../consts/company-sector'
import { Exchange } from '../../../../consts/exchange.enum'
import {
  BetaType,
  DepositToMarketCapRatioType,
  DeptToMarketCapRatioType,
  DividendYieldType,
  EPSType,
  ForbiddenIncomeRatioType,
  IsCompanyHalalType,
  PBRatioType,
  PERatioType,
  PSRatioType,
  StockPriceType,
  TickerType,
} from '../../../../consts/report.enum'
import { LockableData } from '../../../../shared/generics/lockable-data.generic'
import { StockMarketIndexResponse } from '../../stock-market-index/stock-market-index.interfaces'

export interface CompanyBriefResponse extends Pick<CompanyResponse, 'id' | 'ticker' | 'name' | 'dailyChangePercentage' | 'isHalal'> {
  stockPrice: StockPriceType
}


export type CompanySearchByTickerResponse = Pick<CompanyResponse, 'id' | 'ticker' | 'name' | 'exchange'>

export type CompanyTinyResponse = Pick<CompanyResponse, 'id' | 'ticker'>

export interface CompanyResponse {
  id: string
  name: string
  ticker: TickerType
  industry: string | null
  sector: CompanySector
  exchange: Exchange
  employeeNumber: number | null
  description: string | null
  indexes: StockMarketIndexResponse[]
  isHalal: LockableData<IsCompanyHalalType>
  dailyChange: number
  dailyChangePercentage: number
  indicators: Omit<CompanyIndicatorsResponseDto, 'company'>
  analyze: {
    epsRatio: EPSType | null
    psRatio: PSRatioType | null
    beta: BetaType | null
    peRatio: PERatioType | null
    pbRatio: PBRatioType | null
    dividendYield: DividendYieldType | null
  }
  shariahFilterResults: LockableData<{
    depositToMarketCap: DepositToMarketCapRatioType | null
    deptToMarketCap: DeptToMarketCapRatioType | null
    forbiddenIncome: ForbiddenIncomeRatioType | null
    isCharterValid: boolean | null
    linkToCharter: string | null
    charterNotValidReason: string | null
  }>
}
