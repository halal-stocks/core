import { Injectable, NotFoundException } from '@nestjs/common'
import { Connection } from 'typeorm'
import { httpExceptionMessages } from '../../../../consts/httpExceptionMessages'
import { IsCompanyHalalType, TickerType } from '../../../../consts/report.enum'
import { CompanyAnalysisResultService } from '../../../../persistence/company/analysis-result/company-analysis-result.service'
import { CompanyService } from '../../../../persistence/company/company.service'
import { ShariahFilterResultService } from '../../../../persistence/company/shariah-filter-result/shariah-filter-result.service'
import { CompanyPublisher } from '../../../../rpc/company/company.publisher'
import {
  commitTransaction,
  releaseTransaction,
  rollbackTransactionAndThrowError,
  startTransaction,
} from '../../../../shared/repository/transaction'
import { blockProcess } from '../../../../utils/process/block-process.util'
import { FiltersDomain } from '../../../filters/filters.domain'
import { ShariahFilterDomain } from '../../../filters/shariah-filter/shariah-filter.domain'
import { CompanyQueryDomain } from '../query/company-query.domain'

@Injectable()
export class CompanyAnalyzeDomain {
  constructor(
    private shariahFilterResultService: ShariahFilterResultService,
    private companyAnalysisResultService: CompanyAnalysisResultService,
    private shariahFilterDomain: ShariahFilterDomain,
    private companyService: CompanyService,
    private companyQueryDomain: CompanyQueryDomain,
    private filtersDomain: FiltersDomain,
    private connection: Connection,
    private companyPublisher: CompanyPublisher,
  ) {}

  async updateCompanyIsHalalConclusion(ticker: TickerType): Promise<void> {
    const company = await this.companyService.findOneByTickerWithLastShariahFilterResult(ticker)

    if (!company) {
      throw new NotFoundException(httpExceptionMessages.company.notFoundByTicker)
    }

    const shariahFilterResult = company.shariahFilterResults[0]

    if (!shariahFilterResult) {
      throw new NotFoundException(httpExceptionMessages.company.companyDoesNotAnyShariahFilterResult)
    }

    const isIndicatorsValid = this.shariahFilterDomain.getIndicatorsHalalnessConclusion({
      isAllowedDepositToMarketCap: {
        otherCurrentAssets: shariahFilterResult.otherCurrentAssets,
        cashAndShortTermInvestments: shariahFilterResult.cashAndShortTermInvestments,
        longTermInvestments: shariahFilterResult.longTermInvestments,
        marketCap: shariahFilterResult.marketCapAverage,
        depositToMarketCap: shariahFilterResult.depositToMarketCap,
      },
      isAllowedDeptToMarketCap: {
        marketCap: shariahFilterResult.marketCapAverage,
        shortTermDebt: shariahFilterResult.shortTermDebt,
        otherCurrentLiabilities: shariahFilterResult.otherCurrentLiabilities,
        longTermDebt: shariahFilterResult.longTermDebt,
        deptToMarketCap: shariahFilterResult.deptToMarketCap,
      },
      isAllowedForbiddenIncome: {
        interestIncome: shariahFilterResult.interestIncome,
        revenue: shariahFilterResult.revenue,
        forbiddenIncome: shariahFilterResult.forbiddenIncome,
      },
    })

    if (isIndicatorsValid === company.isHalal) {
      /** Nothing to change*/
      return
    }

    if (isIndicatorsValid === false) {
      await this.companyService.updateOne({
        id: company.id,
        isHalal: false as IsCompanyHalalType,
      })
    }

    if (isIndicatorsValid && shariahFilterResult.isCharterValid) {
      await this.companyService.updateOne({
        id: company.id,
        isHalal: true as IsCompanyHalalType,
      })
    }
  }

  async updateFullAnalysis(ticker: TickerType): Promise<void> {
    const { entityManager, queryRunner } = await startTransaction(this.connection)
    let finishedWithError = false
    const startTime = Date.now()
    try {
      const companyId = await this.companyQueryDomain.getCompanyIdByTicker(ticker)
      const fullAnalysis = await this.filtersDomain.passThroughAllFilters(ticker)
      await this.shariahFilterResultService.createOne(
        {
          ...fullAnalysis.shariahFilter,
          reportedAt: fullAnalysis.shariahFilter.reportDate,
        },
        companyId,
        entityManager,
      )

      await this.companyAnalysisResultService.createOrUpdateOne(
        {
          ...fullAnalysis.valueAnalysis,
          ...fullAnalysis.growthAnalysis,
          reportedAt: fullAnalysis.valueAnalysis.reportDate,
        },
        companyId,
        entityManager,
      )

      await commitTransaction(queryRunner)
    } catch (err) {
      finishedWithError = true
      return await rollbackTransactionAndThrowError(queryRunner, err)
    } finally {
      await releaseTransaction(queryRunner)
      const endTime = Date.now()
      const neededDifference = 1000 * 60
      const actualDifference = endTime - startTime
      if (actualDifference < neededDifference) {
        await blockProcess(neededDifference - actualDifference)
      }
      if (!finishedWithError) {
        console.log('Full analysis successfully finished for ticker: ', ticker, new Date())
        await this.companyPublisher.publishCompanyShariahFilterResultsUpdatedMessage(ticker)
      }
    }
  }

  async updateShariahFilterResults(ticker: TickerType): Promise<void> {
    const { entityManager, queryRunner } = await startTransaction(this.connection)

    try {
      const companyId = await this.companyQueryDomain.getCompanyIdByTicker(ticker)
      const shariahFilter = await this.filtersDomain.passThroughShariahFilter(ticker)
      await this.shariahFilterResultService.createOne(
        {
          ...shariahFilter,
          reportedAt: shariahFilter.reportDate,
        },
        companyId,
        entityManager,
      )

      await commitTransaction(queryRunner)
    } catch (err) {
      return await rollbackTransactionAndThrowError(queryRunner, err)
    } finally {
      await releaseTransaction(queryRunner)
      await this.companyPublisher.publishCompanyShariahFilterResultsUpdatedMessage(ticker)
    }
  }

}
