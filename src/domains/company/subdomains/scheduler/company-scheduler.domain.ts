import { Injectable } from '@nestjs/common'
import { sub } from 'date-fns'
import { StockMarketIndex } from '../../../../consts/stock-market-index'
import { CompanyService } from '../../../../persistence/company/company.service'
import { CompanyPublisher } from '../../../../rpc/company/company.publisher'
import { FmpDomain } from '../../../third-party-data-source/fmp/fmp.domain'
import { StockMarketIndexDomain } from '../../stock-market-index/stock-market-index.domain'

@Injectable()
export class CompanySchedulerDomain {
  constructor(
    private companyService: CompanyService,
    private companyPublisher: CompanyPublisher,
    private stockMarketIndexDomain: StockMarketIndexDomain,
    private fmpDomain: FmpDomain,
  ) {}

  async scheduleUpdateShariahFilterResultToAllCompanies(): Promise<void> {
    const allTickers = await this.companyService.getAllTickers()
    const allPromises = allTickers.map((ticker, i) =>
      this.companyPublisher.publishUpdateShariahFilterResultMessage(ticker),
    )
    await Promise.all(allPromises)
  }

  async scheduleUpdateFullAnalysisToAllCompanies(): Promise<void> {
    const olderThan = sub(new Date(), { days: 10 })
    const allTickers = await this.companyService.getAllTickers(olderThan)
    const allPromises = allTickers.map((ticker, i) =>
      this.companyPublisher.publishUpdateFullAnalysisMessage(ticker),
    )
    await Promise.all(allPromises)
  }

  async scheduleUpdateIsHalalConclusionToAll(): Promise<void> {
    const allTickers = await this.companyService.getAllTickers()
    const allPromises = allTickers.map((ticker, i) =>
      this.companyPublisher.publishCompanyShariahFilterResultsUpdatedMessage(ticker),
    )
    await Promise.all(allPromises)
  }

  async scheduleCreationCompaniesFromSP500(): Promise<void> {
    const companiesFromSP500 = await this.stockMarketIndexDomain.getSP500FromDataSource()
    const index = await this.stockMarketIndexDomain.getOneByCode(StockMarketIndex.SP500)
    const allPromises = companiesFromSP500.map((it, i) =>
      this.companyPublisher.publishCompanyToCreateMessage({
        ticker: it.ticker,
        stockMarketIndexId: index.id,
      }),
    )
    await Promise.all(allPromises)
  }

  async scheduleCreationAllCompanies(): Promise<any> {
    // const neededSectors = ['Consumer Cyclical', 'Energy', 'Technology', 'Industrials', 'Financial Services', 'Basic Materials', 'Communication Services', 'Consumer Defensive', 'Healthcare', 'Real Estate', 'Utilities', 'Industrial Goods', 'Financial', 'Services', 'Conglomerates']
    const neededSectors = ['Industrials', 'Financial Services', 'Basic Materials', 'Communication Services', 'Consumer Defensive', 'Healthcare', 'Real Estate', 'Utilities', 'Industrial Goods', 'Financial', 'Services', 'Conglomerates']
    const createAllSectors = neededSectors.map(async it => {
      const allCompanies = await this.fmpDomain.getAllCompanies(it)
      const toCreate = allCompanies.map((it, i) =>
        this.companyPublisher.publishCompanyToCreateMessage({
          ticker: it.symbol,
        }),
      )
      await Promise.all(toCreate)
    })
    await Promise.all(createAllSectors)
  }

}
