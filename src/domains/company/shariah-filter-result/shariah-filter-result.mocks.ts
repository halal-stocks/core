import {
  dummyAppleCompanyShariahFilterLastResults,
  dummyRioCompanyShariahFilterLastResults,
} from '../../../dummy/company/company.dummy'

export const mockedShariahFilterResultDomain = {
  getLastResultsAsDictionaryWithTickerKey: () => ({
    [dummyAppleCompanyShariahFilterLastResults.ticker]: dummyAppleCompanyShariahFilterLastResults,
    [dummyRioCompanyShariahFilterLastResults.ticker]: dummyRioCompanyShariahFilterLastResults,
  }),
}

