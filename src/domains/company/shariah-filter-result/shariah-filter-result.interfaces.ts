import {
  CashAndShortTermInvestmentsType,
  DepositToMarketCapRatioType,
  DeptToMarketCapRatioType,
  ForbiddenIncomePerShareType,
  ForbiddenIncomeRatioType,
  InterestIncomeType,
  LongTermDebtType,
  LongTermInvestmentsType,
  MarketCapType,
  OtherCurrentAssetsType,
  OtherCurrentLiabilitiesType,
  RevenueType,
  SharesOutstandingType,
  ShortTermDebtType,
} from '../../../consts/report.enum'
import { CompanyTinyResponse } from '../subdomains/query/company-query.interfaces'

export interface CompanyShariahFilterLastResultResponse extends CompanyTinyResponse {
  shariahFilterResult: CompanyShariahFilterResult
}


export interface CompanyShariahFilterResult {
  id: string
  checkedAt: Date
  reportedAt: Date | null
  isCharterValid: boolean | null
  linkToCharter: string | null
  charterNotValidReason: string | null
  otherCurrentAssets: OtherCurrentAssetsType | null
  cashAndShortTermInvestments: CashAndShortTermInvestmentsType | null
  longTermInvestments: LongTermInvestmentsType | null
  shortTermDebt: ShortTermDebtType | null
  otherCurrentLiabilities: OtherCurrentLiabilitiesType | null
  longTermDebt: LongTermDebtType | null
  marketCapAverage: MarketCapType | null
  sharesOutstanding: SharesOutstandingType | null
  interestIncome: InterestIncomeType | null
  revenue: RevenueType | null
  depositToMarketCap: DepositToMarketCapRatioType | null
  deptToMarketCap: DeptToMarketCapRatioType | null
  forbiddenIncome: ForbiddenIncomeRatioType | null
  forbiddenIncomePerShare: ForbiddenIncomePerShareType | null
  updatedAt: Date | null
  deletedAt: Date | null
}
