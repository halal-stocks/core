import { Injectable } from '@nestjs/common'
import { TickerType } from '../../../consts/report.enum'
import { CompanyShariahFilterLastResultResponseModel } from '../../../persistence/company/company.model'
import { CompanyService } from '../../../persistence/company/company.service'
import { ShariahFilterResultService } from '../../../persistence/company/shariah-filter-result/shariah-filter-result.service'
import { CompanyPublisher } from '../../../rpc/company/company.publisher'
import ArrayAsObject from '../../../shared/generics/array-as-object.generic'
import convertParamToArray from '../../../utils/array/convert-param-to-array.util'
import arrayToObject from '../../../utils/object/array-to-object/array-to-object.util'
import { CompanyQueryDomain } from '../subdomains/query/company-query.domain'
import { CompanyShariahFilterLastResultResponse } from './shariah-filter-result.interfaces'

@Injectable()
export class CompanyShariahFilterResultDomain {
  constructor(
    private companyService: CompanyService,
    private shariahFilterResultService: ShariahFilterResultService,
    private companyQueryDomain: CompanyQueryDomain,
    private companyPublisher: CompanyPublisher,
  ) {}

  async getLastResultsAsDictionaryWithTickerKey(ticker: TickerType | TickerType[]): Promise<ArrayAsObject<CompanyShariahFilterLastResultResponse>> {
    const results = await this.companyService.getLastShariahFilterResults(convertParamToArray(ticker))

    const normalized: CompanyShariahFilterLastResultResponseModel[] = results.map(company => {
        const { shariahFilterResults, ...restCompany } = company
        return {
          ...restCompany,
          shariahFilterResult: shariahFilterResults[0],
        }
      },
    )
    return arrayToObject(normalized, 'ticker')
  }

  async setCharterAsValid(ticker: TickerType | TickerType[]): Promise<void> {
    const companies = await this.companyQueryDomain.findCompanyIdsByTickers(convertParamToArray(ticker))
    const lastFilterResults = await this.shariahFilterResultService.findLastByCompanyIds(Object.values(companies))
    await this.shariahFilterResultService.updateMany(lastFilterResults.map(it => ({
      id: it.id,
      isCharterValid: true,
    })))

    await Promise.all(Object.keys(companies)
      .map(it => this.companyPublisher.publishCompanyShariahFilterResultsUpdatedMessage(it as TickerType)))
  }
}
