import { Injectable, NotFoundException } from '@nestjs/common'
import { Connection } from 'typeorm'
import { httpExceptionMessages } from '../../../consts/httpExceptionMessages'
import { StockMarketIndex } from '../../../consts/stock-market-index'
import { CompanyMarketIndexService } from '../../../persistence/company/company-market-index/company-market-index.service'
import { StockMarketIndexService } from '../../../persistence/stock-market-index/stock-market-index.service'
import {
  commitTransaction,
  releaseTransaction,
  rollbackTransactionAndThrowError,
  startTransaction,
} from '../../../shared/repository/transaction'
import { DatahubDomain } from '../../third-party-data-source/datahub/datahub.domain'
import { FmpDomain } from '../../third-party-data-source/fmp/fmp.domain'
import { FmpMarketIndex } from '../../third-party-data-source/fmp/fmp.interfaces'
import { CompanyQueryDomain } from '../subdomains/query/company-query.domain'
import {
  StockMarketIndexCompanyResponse,
  StockMarketIndexCreateRequest,
  StockMarketIndexResponse,
} from './stock-market-index.interfaces'

@Injectable()
export class StockMarketIndexDomain {
  constructor(
    private datahubDomain: DatahubDomain,
    private fmpDomain: FmpDomain,
    private stockMarketIndexService: StockMarketIndexService,
    private connection: Connection,
    private companyMarketIndexService: CompanyMarketIndexService,
    private companyQueryDomain: CompanyQueryDomain,
  ) {}

  async getSP500FromDataSource(): Promise<StockMarketIndexCompanyResponse[]> {
    return this.getSP500FromFmp()
  }

  async updateAllIndexes(): Promise<void> {
    const marketIndexCodes = Object.values(StockMarketIndex)
    for (const marketIndexCode of marketIndexCodes) {
      const marketIndex = await this.getOneByCode(marketIndexCode)
      const marketIndexCompanyList = await this.getIndexListFromDataSourceByCode(marketIndexCode)
      const companyIds = await this.companyQueryDomain.findCompanyIdsByTickers(marketIndexCompanyList.map(company => company.ticker))
      const { entityManager, queryRunner } = await startTransaction(this.connection)
      try {
        await this.companyMarketIndexService.deleteAllRelationsByMarketIndexId(marketIndex.id, entityManager)
        await this.companyMarketIndexService.createMany(companyIds, marketIndex.id, entityManager)
        await commitTransaction(queryRunner)
      } catch (e) {
        await rollbackTransactionAndThrowError(queryRunner, e)
      } finally {
        await releaseTransaction(queryRunner)
      }
    }
  }

  createOne(dto: StockMarketIndexCreateRequest): Promise<StockMarketIndexResponse> {
    return this.stockMarketIndexService.createOne(dto)
  }

  findOneByCode(code: StockMarketIndex): Promise<StockMarketIndexResponse | undefined> {
    return this.stockMarketIndexService.findOneByCode(code)
  }

  async getOneByCode(code: StockMarketIndex): Promise<StockMarketIndexResponse> {
    const index = await this.findOneByCode(code)
    if (!index) {
      throw new NotFoundException(`${httpExceptionMessages.stockMarketIndex.notFoundByCode}: ${code}`)
    }
    return index
  }

  async getIndexListFromDataSourceByCode(code: StockMarketIndex): Promise<StockMarketIndexCompanyResponse[]> {
    switch (code) {
      case StockMarketIndex.SP500:
        return this.getSP500FromDataSource()
      case StockMarketIndex.NASDAQ:
        return this.getNasdaqFromFmp()
      case StockMarketIndex.DOW_JONES:
        return this.getDowJonesFromFmp()
    }
  }

  private async getSP500FromFmp(): Promise<StockMarketIndexCompanyResponse[]> {
    const result = await this.fmpDomain.getSP500MarketIndex()
    return this.normalizeMarketIndexResponse(result)
  }

  private async getNasdaqFromFmp(): Promise<StockMarketIndexCompanyResponse[]> {
    const result = await this.fmpDomain.getNasdaqMarketIndex()
    return this.normalizeMarketIndexResponse(result)
  }

  private async getDowJonesFromFmp(): Promise<StockMarketIndexCompanyResponse[]> {
    const result = await this.fmpDomain.getDowJonesMarketIndex()
    return this.normalizeMarketIndexResponse(result)

  }

  private async normalizeMarketIndexResponse(list: FmpMarketIndex[]): Promise<StockMarketIndexCompanyResponse[]> {
    return list.map(it => ({
      name: it.name,
      ticker: it.symbol,
      sector: it.sector,
    }))
  }
}
