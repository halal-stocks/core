import { TickerType } from '../../../consts/report.enum'
import { StockMarketIndex } from '../../../consts/stock-market-index'

export interface StockMarketIndexCompanyResponse {
  name: string
  sector: string
  ticker: TickerType
}

export interface StockMarketIndexCreateRequest {
  code: StockMarketIndex
  name: string
}

export interface StockMarketIndexResponse {
  id: string
  code: StockMarketIndex
  name: string
}
