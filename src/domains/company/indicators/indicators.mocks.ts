import {
  dummyAppleCompanyIndicators,
  dummyRioCompanyIndicators,
} from '../../../dummy/company-indicators/company-indicators.dummy'

export const mockedCompanyIndicatorsDomain = {
  setNew: () => {},
  scheduleUpdatingAllCompaniesIndicators: () => {},
  updateIndicators: () => {},
  findManyByTickers: () => [dummyAppleCompanyIndicators, dummyRioCompanyIndicators],
}
