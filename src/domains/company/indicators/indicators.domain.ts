import { Injectable, NotFoundException } from '@nestjs/common'
import { httpExceptionMessages } from '../../../consts/httpExceptionMessages'
import { TickerType } from '../../../consts/report.enum'
import { CompanyForIndicatorSync } from '../../../persistence/company/company.model'
import { CompanyService } from '../../../persistence/company/company.service'
import { CompanyIndicatorsUpdateModel } from '../../../persistence/company/indicators/company-indicators.model'
import { CompanyIndicatorsService } from '../../../persistence/company/indicators/company-indicators.service'
import { CompanyPublisher } from '../../../rpc/company/company.publisher'
import { chunkArray } from '../../../utils/array/chunk-array.util'
import convertParamToArray from '../../../utils/array/convert-param-to-array.util'
import { FmpDomain } from '../../third-party-data-source/fmp/fmp.domain'
import { CompanyQuoteResponseDto } from '../../third-party-data-source/fmp/fmp.interfaces'
import { CompanyQueryDomain } from '../subdomains/query/company-query.domain'
import { IndicatorsResponse } from './indicators.interfaces'

@Injectable()
export class CompanyIndicatorsDomain {
  constructor(
    private companyIndicatorsService: CompanyIndicatorsService,
    private companyService: CompanyService,
    private companyQueryDomain: CompanyQueryDomain,
    private fmpDomain: FmpDomain,
    private companyPublisher: CompanyPublisher,
  ) {}

  async scheduleUpdateAllCompaniesIndicators(): Promise<void> {
    const companies = await this.companyService.getAllTickers()
    const chunkedCompanies = chunkArray(companies, 100)
    chunkedCompanies.forEach(it => this.companyPublisher.publishUpdateIndicatorsMessage(it))
  }

  async updateIndicators(ticker: TickerType[] | TickerType): Promise<void> {
    const tickers = convertParamToArray(ticker)
    const companies = await this.companyService.findManyForIndicatorSyncByTickers(tickers)
    await this.updateIndicatorsForCompanies(companies)
  }

  async updateIndicatorsToAllCompanies(): Promise<void> {
    const companies = await this.companyService.findManyForIndicatorSyncByTickers()
    const chunkedCompanies = chunkArray(companies, 300)
    for (const chunkedCompaniesItem of chunkedCompanies) {
      await this.updateIndicatorsForCompanies(chunkedCompaniesItem)
    }
  }

  async updateIndicatorsForCompanies(companies: CompanyForIndicatorSync[]): Promise<void> {
    const companyQuotes = await this.fmpDomain.getManyCompaniesQuoteAsMap(companies.map(it => it.ticker))
    const toSave: CompanyIndicatorsUpdateModel[] = []
    for (const company of companies) {
      const currentCompanyQuote: CompanyQuoteResponseDto | undefined = companyQuotes.get(company.ticker)
      if (!currentCompanyQuote) {
        console.error(httpExceptionMessages.indicators.notFoundByTicker, company.ticker)
        continue
      }
      toSave.push({
        id: company.indicators.id,
        actualStockPrice: currentCompanyQuote?.price ?? company.indicators.actualStockPrice,
        actualMarketCap: currentCompanyQuote?.marketCap ?? company.indicators.actualMarketCap,
        previousDayStockPrice: currentCompanyQuote?.previousClose ?? company.indicators.previousDayStockPrice,
      })
    }


    await this.companyIndicatorsService.updateMany(toSave)
  }

  async addInitialIndicators(ticker: TickerType): Promise<void> {
    const companyId = await this.companyQueryDomain.getCompanyIdByTicker(ticker)
    const {
      price,
      previousClose,
      marketCap,
    } = await this.fmpDomain.getCompanyQuote(ticker)

    await this.companyIndicatorsService.createOne({
      actualStockPrice: price,
      previousDayStockPrice: previousClose,
      actualMarketCap: marketCap,
    }, companyId)
  }

  async findManyByTickers(tickers: TickerType[]): Promise<IndicatorsResponse[]> {
    return this.companyIndicatorsService.findManyByTickers(tickers)
  }

  async getOneByTicker(ticker: TickerType): Promise<IndicatorsResponse> {
    const indicators = await this.companyIndicatorsService.findOneByTicker(ticker)
    if (!indicators) {
      throw new NotFoundException(`${httpExceptionMessages.indicators.notFoundByTicker}. Ticker: ${ticker}`)
    }
    return indicators
  }
}


