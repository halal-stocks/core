import { MarketCapType, StockPriceType, TickerType } from '../../../consts/report.enum'

export interface IndicatorsSetNewRequest {
  stockPrice: StockPriceType,
  marketCap: MarketCapType
}

export interface IndicatorsResponse {
  id: string
  actualStockPrice: StockPriceType
  actualMarketCap: MarketCapType
  previousDayStockPrice: StockPriceType
  updatedAt: Date | null
  company: {
    id: string
    ticker: TickerType
  }
}
