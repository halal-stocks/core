import { Module } from '@nestjs/common'
import { WishlistImplModule } from '../../persistence/wishlist/wishlist.impl-module'
import { CompanyDomainModule } from '../company/company.domain-module'
import { WishlistDomain } from './wishlist.domain'

@Module({
  imports: [
    WishlistImplModule,
    CompanyDomainModule,
  ],
  providers: [WishlistDomain],
  exports: [WishlistDomain, WishlistDomainModule],
})
export class WishlistDomainModule {

}
