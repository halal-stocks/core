import { BadRequestException, Injectable } from '@nestjs/common'
import { httpExceptionMessages } from '../../consts/httpExceptionMessages'
import { TickerType } from '../../consts/report.enum'
import { WishlistService } from '../../persistence/wishlist/wishlist.service'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { CompanyQueryDomain } from '../company/subdomains/query/company-query.domain'
import { WishlistItemResponse } from './wishlist.interfaces'

@Injectable()
export class WishlistDomain {
  constructor(
    private wishlistService: WishlistService,
    private companyQueryDomain: CompanyQueryDomain,
  ) {}

  async getManyByUserId(userId: string): Promise<ItemsWithCount<WishlistItemResponse>> {
    const { items, count } = await this.wishlistService.findCompanyIdsByUserId(userId)
    const companyIdMap = new Map(items.map(item => [item.company.id, item.id]))
    const detailed = await this.companyQueryDomain.getManyDetailedByIds(items.map(item => item.company.id))

    return {
      items: detailed.map(company => ({
        id: companyIdMap.get(company.id)!,
        company,
      })),
      count,
    }
  }

  async deleteFromUser(ids: string[], userId: string): Promise<void> {
    await this.wishlistService.deleteByIdsAndUserId(ids, userId)
  }

  async addOneToUser(ticker: TickerType, userId: string): Promise<ItemsWithCount<WishlistItemResponse>> {
    const isExists = await this.isAlreadyInUsersWishlistByTicker(ticker, userId)

    if (isExists) {
      throw new BadRequestException(httpExceptionMessages.wishlist.alreadyInList)
    }

    const companyId = await this.companyQueryDomain.getCompanyIdByTicker(ticker)

    await this.wishlistService.addByIdAndUserId(companyId, userId)
    return this.getManyByUserId(userId)
  }

  async isAlreadyInUsersWishlistByTicker(ticker: TickerType, userId: string): Promise<boolean> {
    return !!(await this.wishlistService.findByUserIdAndTicker(ticker, userId))
  }
}

