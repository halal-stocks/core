import { CompanyDetailedResponseDto } from '../../api/company/company.dtos'

export interface WishlistItemResponse {
  id: string
  company: CompanyDetailedResponseDto
}
