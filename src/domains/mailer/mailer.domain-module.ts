import { Module } from '@nestjs/common'
import { MailchimpDomainModule } from '../mailchimp/mailchimp.domain-module'
import { MailerDomain } from './mailer.domain'

@Module({
  imports: [
    MailchimpDomainModule,
  ],
  providers: [MailerDomain],
  exports: [MailerDomain, MailerDomainModule],
})
export class MailerDomainModule {

}
