import { BadRequestException, Injectable } from '@nestjs/common'
import { isEmail } from 'class-validator'
import { httpExceptionMessages } from '../../consts/httpExceptionMessages'
import { MailchimpDomain } from '../mailchimp/mailchimp.domain'
import { MailchimpAudienceTag, MailchimpSubscriberStatus } from '../mailchimp/mailchimp.interfaces'

@Injectable()
export class MailerDomain {
  constructor(
    private mailchimpDomain: MailchimpDomain,
  ) {}

  async addSubscriber(email: string): Promise<void> {
    if (!isEmail(email)) {
      throw new BadRequestException(httpExceptionMessages.mailer.notEmailFormat)
    }
    const audienceId = await this.getAudienceId()
    await this.throwErrorIfAlreadySubscribed(email, audienceId)
    await this.mailchimpDomain.addSubscriber(audienceId, {
      email_address: email,
      status: MailchimpSubscriberStatus.subscribed,
      tags: [MailchimpAudienceTag['10stocks']],
    })
  }

  async isAlreadySubscribed(email: string, listId: string): Promise<boolean> {
    const allMembers = await this.mailchimpDomain.getAllMembers(listId)
    return allMembers.some(member => member.email_address === email)
  }

  async throwErrorIfAlreadySubscribed(email: string, listId: string): Promise<void> {
    const isSubscribed = await this.isAlreadySubscribed(email, listId)
    if (isSubscribed) {
      throw new BadRequestException(httpExceptionMessages.mailer.alreadySubscribed)
    }
  }

  async getAudienceId(): Promise<string> {
    const allAudience = await this.mailchimpDomain.getAllAudiences()
    return allAudience[0].id
  }
} 
