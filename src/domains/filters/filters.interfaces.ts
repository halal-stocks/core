import {
  GrowthAnalysisGetResultResponse,
  GrowthAnalysisResultRequest,
} from './growth-analysis/growth-analysis.interfaces'
import {
  ShariahFilterNeededIndicatorsToPassFilterAndFilterResponse,
  ShariahFilterNeededIndicatorsToPassFilterResponse,
} from './shariah-filter/shariah-filter.interfaces'
import { ValueAnalysisResultRequest, ValueAnalysisResultResponse } from './value-analysis/value-analysis.interfaces'

export interface FilterGetAllNeededIndicatorsResponse {
  toShariahFilter: ShariahFilterNeededIndicatorsToPassFilterResponse
  toValueAnalysis: ValueAnalysisResultRequest
  toGrowthAnalysis: GrowthAnalysisResultRequest
}

export interface FilterShariahFilterAndFullAnalysisResponse {
  shariahFilter: ShariahFilterNeededIndicatorsToPassFilterAndFilterResponse
  valueAnalysis: ValueAnalysisResultResponse
  growthAnalysis: GrowthAnalysisGetResultResponse
}
