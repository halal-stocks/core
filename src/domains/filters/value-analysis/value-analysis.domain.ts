import { Injectable } from '@nestjs/common'
import { CompanyReportDateType, TickerType } from '../../../consts/report.enum'
import toFixed from '../../../utils/number/to-fixed/to-fixed.util'
import { FmpDomain } from '../../third-party-data-source/fmp/fmp.domain'
import { ValueAnalysisCalculateDomain } from './calculate/value-analysis.calculate.domain'
import {
  ValueAnalysisGetNeededIndicatorsResponse,
  ValueAnalysisResultRequest,
  ValueAnalysisResultResponse,
} from './value-analysis.interfaces'

@Injectable()
export class ValueAnalysisDomain {
  constructor(
    private valueFilterCalculateDomain: ValueAnalysisCalculateDomain,
    private fmpDomain: FmpDomain,
  ) {}

  async analiseCompany(ticker: TickerType): Promise<ValueAnalysisResultResponse> {
    const indicators = await this.getNeededIndicators(ticker)
    return this.getAnalysisResult(indicators)
  }

  async getNeededIndicators(ticker: TickerType): Promise<ValueAnalysisGetNeededIndicatorsResponse> {
    const {
      totalCurrentAssets,
      totalLiabilities,
      date,
    } = await this.fmpDomain.getLastBalanceSheetStatement(ticker)

    const reportDate = new Date(date) as CompanyReportDateType

    const {
      peRatioTTM,
      dividendYieldTTM,
      payoutRatioTTM,
      priceToSalesRatioTTM,
      pbRatioTTM,
    } = await this.fmpDomain.getKeyMetrics(ticker)

    const {
      freeCashFlow,
    } = await this.fmpDomain.getLastCashFlowStatements(ticker)

    const {
      eps,
    } = await this.fmpDomain.getCompanyQuote(ticker)

    return {
      reportDate,
      totalCurrentAssets,
      totalLiabilities,
      freeCashFlow,
      peRatio: peRatioTTM,
      dividendYield: dividendYieldTTM,
      payoutRatio: payoutRatioTTM,
      psRatio: priceToSalesRatioTTM,
      pbRatio: pbRatioTTM,
      eps,
    }

  }

  getAnalysisResult(dto: ValueAnalysisResultRequest): ValueAnalysisResultResponse {
    const {
      totalCurrentAssets,
      totalLiabilities,
      freeCashFlow,
      peRatio,
      dividendYield,
      payoutRatio,
      psRatio,
      pbRatio,
      reportDate,
      eps,
    } = dto

    const grahamRatio = this.valueFilterCalculateDomain.getGrahamRatio({
      peRatio,
      pbRatio,
    })

    const currentAssetsMinusLiabilities = this.valueFilterCalculateDomain.getCurrentAssetsMinusLiabilities({
      totalCurrentAssets,
      totalLiabilities,
    })

    return {
      reportDate,
      currentAssetsMinusLiabilities: toFixed(currentAssetsMinusLiabilities, 5),
      peRatio: toFixed(peRatio, 5),
      pbRatio: toFixed(pbRatio, 5),
      grahamRatio: toFixed(grahamRatio, 5),
      dividendYield: toFixed(dividendYield, 5),
      freeCashFlow: toFixed(freeCashFlow, 5),
      payoutRatio: toFixed((payoutRatio), 5),
      psRatio: toFixed(psRatio, 5),
      eps: toFixed(eps, 5),
    }
  }
}
