import {
  CompanyReportDateType,
  CurrentAssetsMinusLiabilitiesType,
  CurrentAssetsType,
  DividendYieldType,
  EPSType,
  FreeCashFlowType,
  GrahamRatioType,
  PayoutRatioType,
  PBRatioType,
  PERatioType,
  PSRatioType,
  TotalLiabilitiesType,
} from '../../../consts/report.enum'

export interface ValueAnalysisResultRequest extends ValueAnalysisGetNeededIndicatorsResponse {}

export interface ValueAnalysisGetNeededIndicatorsResponse {
  reportDate: CompanyReportDateType | null
  totalCurrentAssets: CurrentAssetsType | null
  totalLiabilities: TotalLiabilitiesType | null
  dividendYield: DividendYieldType | null
  payoutRatio: PayoutRatioType | null
  peRatio: PERatioType | null
  psRatio: PSRatioType | null
  pbRatio: PBRatioType | null
  freeCashFlow: FreeCashFlowType | null
  eps: EPSType | null
}

export interface ValueAnalysisResultResponse {
  reportDate: CompanyReportDateType | null
  currentAssetsMinusLiabilities: CurrentAssetsMinusLiabilitiesType | null
  grahamRatio: GrahamRatioType | null
  dividendYield: DividendYieldType | null
  payoutRatio: PayoutRatioType | null
  peRatio: PERatioType | null
  psRatio: PSRatioType | null
  pbRatio: PBRatioType | null
  freeCashFlow: FreeCashFlowType | null
  eps: EPSType | null
}
