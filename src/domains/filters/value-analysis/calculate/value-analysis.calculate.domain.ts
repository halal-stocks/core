import { Injectable } from '@nestjs/common'
import { CurrentAssetsMinusLiabilitiesType, GrahamRatioType } from '../../../../consts/report.enum'
import { isNullOrUndefined } from '../../../../utils'
import toFixed from '../../../../utils/number/to-fixed/to-fixed.util'
import {
  CompanyValueFilterCalculateGrahamRatio,
  CompanyValueFilterCalculateTotalAssetsMinusLiabilities,
} from './value-analysis.calculate.interfaces'

@Injectable()
export class ValueAnalysisCalculateDomain {
  getCurrentAssetsMinusLiabilities(
    dto: CompanyValueFilterCalculateTotalAssetsMinusLiabilities,
  ): CurrentAssetsMinusLiabilitiesType | null {
    const {
      totalCurrentAssets,
      totalLiabilities,
    } = dto

    if (isNullOrUndefined(totalCurrentAssets) || isNullOrUndefined(totalLiabilities)) {
      return null
    }

    return (totalCurrentAssets - totalLiabilities) as CurrentAssetsMinusLiabilitiesType
  }

  getGrahamRatio(dto: CompanyValueFilterCalculateGrahamRatio): GrahamRatioType | null {
    const {
      peRatio,
      pbRatio,
    } = dto

    if (isNullOrUndefined(peRatio) || isNullOrUndefined(pbRatio)) {
      return null
    }

    const calculateGrahamRatio = (peRatio * pbRatio) as GrahamRatioType

    return toFixed(calculateGrahamRatio)
  }
}
