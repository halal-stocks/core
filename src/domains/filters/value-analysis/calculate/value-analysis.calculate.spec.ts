import { Test } from '@nestjs/testing'
import {
  dummyCurrentAssetsMinusLiabilitiesType,
  dummyGrahamRatio,
  dummyPBRatio,
  dummyPERatio,
  dummyTotalCurrentAssets,
  dummyTotalLiabilities,
} from '../../../../dummy/report-values/report-values.dummy'
import { ValueAnalysisCalculateDomain } from './value-analysis.calculate.domain'

describe('ValueAnalysisCalculateDomain', () => {
  let companyValueAnalysisCalculateDomain: ValueAnalysisCalculateDomain

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [],
      providers: [ValueAnalysisCalculateDomain],
    }).compile()

    companyValueAnalysisCalculateDomain = moduleRef.get<ValueAnalysisCalculateDomain>(ValueAnalysisCalculateDomain)
  })

  describe('getCurrentAssetsMinusLiabilities', () => {
    it('should return proper result', () => {
      expect(companyValueAnalysisCalculateDomain.getCurrentAssetsMinusLiabilities({
        totalCurrentAssets: dummyTotalCurrentAssets,
        totalLiabilities: dummyTotalLiabilities,
      }))
      .toStrictEqual(dummyCurrentAssetsMinusLiabilitiesType)
    })
  })

  describe('getGrahamRatio', () => {
    it('should return proper result', async () => {
      expect(companyValueAnalysisCalculateDomain.getGrahamRatio({
        peRatio: dummyPERatio,
        pbRatio: dummyPBRatio,
      }))
      .toStrictEqual(dummyGrahamRatio)
    })
  })
})
