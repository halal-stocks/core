import { CurrentAssetsType, PBRatioType, PERatioType, TotalLiabilitiesType } from '../../../../consts/report.enum'

export interface CompanyValueFilterCalculateTotalAssetsMinusLiabilities {
  totalCurrentAssets: CurrentAssetsType | null
  totalLiabilities: TotalLiabilitiesType | null
}

export interface CompanyValueFilterCalculateGrahamRatio {
  peRatio: PERatioType | null
  pbRatio: PBRatioType | null
}
