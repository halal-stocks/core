import {
  CashAndShortTermInvestmentsType,
  CompanyReportDateType,
  DepositToMarketCapRatioType,
  DeptToMarketCapRatioType,
  ForbiddenIncomePerShareType,
  ForbiddenIncomeRatioType,
  InterestIncomeType,
  LongTermDebtType,
  LongTermInvestmentsType,
  MarketCapType,
  OtherCurrentAssetsType,
  OtherCurrentLiabilitiesType,
  RevenueType,
  SharesOutstandingType,
  ShortTermDebtType,
} from '../../../consts/report.enum'

export interface ShariahFilterDepositToMarketCapRequest {
  otherCurrentAssets: OtherCurrentAssetsType | null
  cashAndShortTermInvestments: CashAndShortTermInvestmentsType | null
  longTermInvestments: LongTermInvestmentsType | null
  marketCap: MarketCapType | null
}

export interface ShariahFilterDeptToMarketCapRequest {
  marketCap: MarketCapType | null
  shortTermDebt: ShortTermDebtType | null
  otherCurrentLiabilities: OtherCurrentLiabilitiesType | null
  longTermDebt: LongTermDebtType | null
}

export interface ShariahFilterForbiddenIncomeRequest {
  interestIncome: InterestIncomeType | null
  revenue: RevenueType | null
}

export interface ShariahFilterForbiddenIncomePerShare {
  interestIncome: InterestIncomeType | null
  sharesOutstanding: SharesOutstandingType | null
}

export interface ShariahFilterNeededIndicatorsToPassFilterResponse {
  reportDate: CompanyReportDateType | null
  otherCurrentAssets: OtherCurrentAssetsType | null
  cashAndShortTermInvestments: CashAndShortTermInvestmentsType | null
  longTermInvestments: LongTermInvestmentsType | null
  marketCapAverage: MarketCapType | null
  shortTermDebt: ShortTermDebtType | null
  otherCurrentLiabilities: OtherCurrentLiabilitiesType | null
  longTermDebt: LongTermDebtType | null
  sharesOutstanding: SharesOutstandingType | null
  interestIncome: InterestIncomeType | null
  revenue: RevenueType | null
}

export interface ShariahFilterPassThroughFilterResponse {
  reportDate: CompanyReportDateType | null
  depositToMarketCap: DepositToMarketCapRatioType | null
  deptToMarketCap: DeptToMarketCapRatioType | null
  forbiddenIncome: ForbiddenIncomeRatioType | null
  forbiddenIncomePerShare: ForbiddenIncomePerShareType | null
}

export type ShariahFilterNeededIndicatorsToPassFilterAndFilterResponse =
  ShariahFilterNeededIndicatorsToPassFilterResponse
  & ShariahFilterPassThroughFilterResponse

export interface ShariahFilterIsCompanyPassShariahFilterRequest {
  isAllowedDepositToMarketCap: ShariahFilterIsAllowedDepositToMarketCapRequest
  isAllowedDeptToMarketCap: ShariahFilterIsAllowedDeptToMarketCaoRequest
  isAllowedForbiddenIncome: ShariahFilterIsAllowedForbiddenIncome
}

export interface ShariahFilterIsAllowedDepositToMarketCapRequest extends ShariahFilterDepositToMarketCapRequest {
  depositToMarketCap: DepositToMarketCapRatioType | null
}

export interface ShariahFilterIsAllowedDeptToMarketCaoRequest extends ShariahFilterDeptToMarketCapRequest {
  deptToMarketCap: DeptToMarketCapRatioType | null
}

export interface ShariahFilterIsAllowedForbiddenIncome extends ShariahFilterForbiddenIncomeRequest {
  forbiddenIncome: ForbiddenIncomeRatioType | null
}
