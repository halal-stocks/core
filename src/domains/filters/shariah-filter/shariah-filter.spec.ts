import { ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import {
  CashAndShortTermInvestmentsType,
  DepositToMarketCapRatioType,
  DeptToMarketCapRatioType,
  ForbiddenIncomeRatioType,
  InterestIncomeType,
  LongTermDebtType,
  LongTermInvestmentsType,
  MarketCapType,
  OtherCurrentAssetsType,
  OtherCurrentLiabilitiesType,
  RevenueType,
  ShortTermDebtType,
} from '../../../consts/report.enum'
import {
  dummyCashAndShortTermInvestments,
  dummyInterestIncome,
  dummyLongTermDebt,
  dummyLongTermInvestments,
  dummyMarketCap,
  dummyOtherCurrentAssets,
  dummyOtherCurrentLiabilities,
  dummyReportDate,
  dummySharesOutstanding,
  dummyShortTermDebt,
  dummyTicker,
  dummyTotalRevenue,
} from '../../../dummy/report-values/report-values.dummy'
import { FmpDomain } from '../../third-party-data-source/fmp/fmp.domain'
import { mockedFmpDomain } from '../../third-party-data-source/fmp/fmp.mocks'
import { YahooDomain } from '../../third-party-data-source/yahoo/yahoo.domain'
import { mockedYahooDomain } from '../../third-party-data-source/yahoo/yahoo.mocks'
import { ShariahFilterCalculateDomain } from './calculate/shariah-filter.calculate.domain'
import { mockedShariahFilterCalculateDomain } from './calculate/shariah-filter.calculate.mocks'
import { ShariahFilterDomain } from './shariah-filter.domain'
import { ShariahFilterNeededIndicatorsToPassFilterResponse } from './shariah-filter.interfaces'

describe('ShariahFilterDomain', () => {
  let shariahFilterDomain: ShariahFilterDomain
  let configService: ConfigService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        imports: [],
        providers: [
          ConfigService,
          ShariahFilterDomain,
          FmpDomain,
          ShariahFilterCalculateDomain,
          YahooDomain,
        ],
      })
      .overrideProvider(FmpDomain)
      .useValue(mockedFmpDomain)
      .overrideProvider(ShariahFilterCalculateDomain)
      .useValue(mockedShariahFilterCalculateDomain)
      .overrideProvider(YahooDomain)
      .useValue(mockedYahooDomain)
      .compile()

    configService = moduleRef.get<ConfigService>(ConfigService)
    shariahFilterDomain = moduleRef.get<ShariahFilterDomain>(ShariahFilterDomain)
  })

  describe('getMultipliersToPassThroughFilters', () => {
    it('should return proper result', async () => {
      expect(await shariahFilterDomain.getDataToPassThroughFilters(dummyTicker))
        .toStrictEqual({
          reportDate: dummyReportDate,
          otherCurrentAssets: dummyOtherCurrentAssets,
          cashAndShortTermInvestments: dummyCashAndShortTermInvestments,
          longTermInvestments: dummyLongTermInvestments,
          shortTermDebt: dummyShortTermDebt,
          otherCurrentLiabilities: dummyOtherCurrentLiabilities,
          longTermDebt: dummyLongTermDebt,
          marketCapAverage: dummyMarketCap,
          sharesOutstanding: dummySharesOutstanding,
          interestIncome: dummyInterestIncome,
          revenue: dummyTotalRevenue,
        } as ShariahFilterNeededIndicatorsToPassFilterResponse)
    })
  })

  describe('checkIsAllowedDepositToMarketCap', () => {
    it('should be falsy', () => {
      jest.spyOn(configService, 'get').mockImplementation(() => 15)
      expect(shariahFilterDomain.checkIsAllowedDepositToMarketCap({
        depositToMarketCap: 20 as DepositToMarketCapRatioType,
        otherCurrentAssets: 0 as OtherCurrentAssetsType,
        cashAndShortTermInvestments: 0 as CashAndShortTermInvestmentsType,
        longTermInvestments: 0 as LongTermInvestmentsType,
        marketCap: 0 as MarketCapType,
      }))
        .toBeFalsy()
    })

    it('should be truthy', () => {
      jest.spyOn(configService, 'get').mockImplementation(() => 30)
      expect(shariahFilterDomain.checkIsAllowedDepositToMarketCap({
        depositToMarketCap: 20 as DepositToMarketCapRatioType,
        otherCurrentAssets: 0 as OtherCurrentAssetsType,
        cashAndShortTermInvestments: 0 as CashAndShortTermInvestmentsType,
        longTermInvestments: 0 as LongTermInvestmentsType,
        marketCap: 0 as MarketCapType,
      }))
        .toBeTruthy()
    })
  })

  describe('checkIsAllowedDeptToMarketCap', () => {
    it('should be falsy', () => {
      jest.spyOn(configService, 'get').mockImplementation(() => 15)
      expect(shariahFilterDomain.checkIsAllowedDeptToMarketCap({
        deptToMarketCap: 20 as DeptToMarketCapRatioType,
        marketCap: 0 as MarketCapType,
        shortTermDebt: 0 as ShortTermDebtType,
        otherCurrentLiabilities: 0 as OtherCurrentLiabilitiesType,
        longTermDebt: 0 as LongTermDebtType,
      }))
        .toBeFalsy()
    })

    it('should be truthy', () => {
      jest.spyOn(configService, 'get').mockImplementation(() => 30)
      expect(shariahFilterDomain.checkIsAllowedDeptToMarketCap({
        deptToMarketCap: 20 as DeptToMarketCapRatioType,
        marketCap: 0 as MarketCapType,
        shortTermDebt: 0 as ShortTermDebtType,
        otherCurrentLiabilities: 0 as OtherCurrentLiabilitiesType,
        longTermDebt: 0 as LongTermDebtType,
      }))
        .toBeTruthy()
    })
  })

  describe('checkIsAllowedForbiddenIncome', () => {
    it('should be falsy', () => {
      jest.spyOn(configService, 'get').mockImplementation(() => 15)
      expect(shariahFilterDomain.checkIsAllowedForbiddenIncome({
        forbiddenIncome: 20 as ForbiddenIncomeRatioType,
        interestIncome: 0 as InterestIncomeType,
        revenue: 0 as RevenueType,
      }))
        .toBeFalsy()
    })

    it('should be truthy', () => {
      jest.spyOn(configService, 'get').mockImplementation(() => 30)
      expect(shariahFilterDomain.checkIsAllowedForbiddenIncome({
        forbiddenIncome: 20 as ForbiddenIncomeRatioType,
        interestIncome: 0 as InterestIncomeType,
        revenue: 0 as RevenueType,
      }))
        .toBeTruthy()
    })
  })

  describe('getHalalnessConclusion', () => {
    it('should be falsy because of not valid deposit to market cap', () => {
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedDepositToMarketCap').mockImplementation(() => false)
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedDeptToMarketCap').mockImplementation(() => true)
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedForbiddenIncome').mockImplementation(() => true)

      expect(shariahFilterDomain.getIndicatorsHalalnessConclusion({
        isAllowedDepositToMarketCap: {
          depositToMarketCap: 20 as DepositToMarketCapRatioType,
          otherCurrentAssets: 0 as OtherCurrentAssetsType,
          cashAndShortTermInvestments: 0 as CashAndShortTermInvestmentsType,
          longTermInvestments: 0 as LongTermInvestmentsType,
          marketCap: 0 as MarketCapType,
        },
        isAllowedDeptToMarketCap: {
          deptToMarketCap: 20 as DeptToMarketCapRatioType,
          marketCap: 0 as MarketCapType,
          shortTermDebt: 0 as ShortTermDebtType,
          otherCurrentLiabilities: 0 as OtherCurrentLiabilitiesType,
          longTermDebt: 0 as LongTermDebtType,
        },
        isAllowedForbiddenIncome: {
          forbiddenIncome: 20 as ForbiddenIncomeRatioType,
          interestIncome: 0 as InterestIncomeType,
          revenue: 0 as RevenueType,
        },
      })).toBeFalsy()
    })

    it('should be falsy because of not valid dept to market cap', () => {
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedDepositToMarketCap').mockImplementation(() => true)
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedDeptToMarketCap').mockImplementation(() => false)
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedForbiddenIncome').mockImplementation(() => true)

      expect(shariahFilterDomain.getIndicatorsHalalnessConclusion({
        isAllowedDepositToMarketCap: {
          depositToMarketCap: 20 as DepositToMarketCapRatioType,
          otherCurrentAssets: 0 as OtherCurrentAssetsType,
          cashAndShortTermInvestments: 0 as CashAndShortTermInvestmentsType,
          longTermInvestments: 0 as LongTermInvestmentsType,
          marketCap: 0 as MarketCapType,
        },
        isAllowedDeptToMarketCap: {
          deptToMarketCap: 20 as DeptToMarketCapRatioType,
          marketCap: 0 as MarketCapType,
          shortTermDebt: 0 as ShortTermDebtType,
          otherCurrentLiabilities: 0 as OtherCurrentLiabilitiesType,
          longTermDebt: 0 as LongTermDebtType,
        },
        isAllowedForbiddenIncome: {
          forbiddenIncome: 20 as ForbiddenIncomeRatioType,
          interestIncome: 0 as InterestIncomeType,
          revenue: 0 as RevenueType,
        },
      })).toBeFalsy()
    })

    it('should be falsy because of not valid forbidden income', () => {
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedDepositToMarketCap').mockImplementation(() => true)
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedDeptToMarketCap').mockImplementation(() => true)
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedForbiddenIncome').mockImplementation(() => false)

      expect(shariahFilterDomain.getIndicatorsHalalnessConclusion({
        isAllowedDepositToMarketCap: {
          depositToMarketCap: 20 as DepositToMarketCapRatioType,
          otherCurrentAssets: 0 as OtherCurrentAssetsType,
          cashAndShortTermInvestments: 0 as CashAndShortTermInvestmentsType,
          longTermInvestments: 0 as LongTermInvestmentsType,
          marketCap: 0 as MarketCapType,
        },
        isAllowedDeptToMarketCap: {
          deptToMarketCap: 20 as DeptToMarketCapRatioType,
          marketCap: 0 as MarketCapType,
          shortTermDebt: 0 as ShortTermDebtType,
          otherCurrentLiabilities: 0 as OtherCurrentLiabilitiesType,
          longTermDebt: 0 as LongTermDebtType,
        },
        isAllowedForbiddenIncome: {
          forbiddenIncome: 20 as ForbiddenIncomeRatioType,
          interestIncome: 0 as InterestIncomeType,
          revenue: 0 as RevenueType,
        },
      })).toBeFalsy()
    })

    it('should be truthy', () => {
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedDepositToMarketCap').mockImplementation(() => true)
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedDeptToMarketCap').mockImplementation(() => true)
      jest.spyOn(shariahFilterDomain, 'checkIsAllowedForbiddenIncome').mockImplementation(() => true)

      expect(shariahFilterDomain.getIndicatorsHalalnessConclusion({
        isAllowedDepositToMarketCap: {
          depositToMarketCap: 20 as DepositToMarketCapRatioType,
          otherCurrentAssets: 0 as OtherCurrentAssetsType,
          cashAndShortTermInvestments: 0 as CashAndShortTermInvestmentsType,
          longTermInvestments: 0 as LongTermInvestmentsType,
          marketCap: 0 as MarketCapType,
        },
        isAllowedDeptToMarketCap: {
          deptToMarketCap: 20 as DeptToMarketCapRatioType,
          marketCap: 0 as MarketCapType,
          shortTermDebt: 0 as ShortTermDebtType,
          otherCurrentLiabilities: 0 as OtherCurrentLiabilitiesType,
          longTermDebt: 0 as LongTermDebtType,
        },
        isAllowedForbiddenIncome: {
          forbiddenIncome: 20 as ForbiddenIncomeRatioType,
          interestIncome: 0 as InterestIncomeType,
          revenue: 0 as RevenueType,
        },
      })).toBeTruthy()
    })
  })
})
