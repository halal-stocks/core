import { Injectable } from '@nestjs/common'
import {
  DepositToMarketCapRatioType,
  DeptToMarketCapRatioType,
  ForbiddenIncomePerShareType,
  ForbiddenIncomeRatioType,
} from '../../../../consts/report.enum'
import { isNullOrUndefined, replaceIfNullUtil } from '../../../../utils'
import toFixed from '../../../../utils/number/to-fixed/to-fixed.util'
import {
  ShariahFilterDepositToMarketCapRequest,
  ShariahFilterDeptToMarketCapRequest,
  ShariahFilterForbiddenIncomePerShare,
  ShariahFilterForbiddenIncomeRequest,
} from '../shariah-filter.interfaces'

@Injectable()
export class ShariahFilterCalculateDomain {
  depositToMarketCap(dto: ShariahFilterDepositToMarketCapRequest): DepositToMarketCapRatioType | null {
    const otherCurrentAssets = replaceIfNullUtil(dto.otherCurrentAssets, 0)
    const cashAndShortTermInvestments = replaceIfNullUtil(dto.cashAndShortTermInvestments, 0)
    const longTermInvestments = replaceIfNullUtil(dto.longTermInvestments, 0)
    const marketCap = dto.marketCap

    if (isNullOrUndefined(marketCap)) {
      return null
    }

    if (marketCap === 0) {
      return 0 as DepositToMarketCapRatioType
    }

    const result = (otherCurrentAssets + cashAndShortTermInvestments + longTermInvestments) / marketCap
    return toFixed(result) as DepositToMarketCapRatioType
  }

  deptToMarketCap(dto: ShariahFilterDeptToMarketCapRequest): DeptToMarketCapRatioType | null {
    const longTermDebt = replaceIfNullUtil(dto.longTermDebt, 0)
    const shortTermDebt = replaceIfNullUtil(dto.shortTermDebt, 0)
    const otherCurrentLiabilities = replaceIfNullUtil(dto.otherCurrentLiabilities, 0)
    const marketCap = dto.marketCap

    if (isNullOrUndefined(marketCap)) {
      return null
    }

    if (marketCap === 0) {
      return 0 as DeptToMarketCapRatioType
    }

    const result = (longTermDebt + shortTermDebt + otherCurrentLiabilities) / marketCap
    return toFixed(result) as DeptToMarketCapRatioType
  }

  forbiddenIncome(dto: ShariahFilterForbiddenIncomeRequest): ForbiddenIncomeRatioType | null {
    const {
      interestIncome,
      revenue,
    } = dto

    if (isNullOrUndefined(interestIncome) || isNullOrUndefined(revenue)) {
      return null
    }

    if (revenue === 0) {
      return 0 as ForbiddenIncomeRatioType
    }

    const normalizedInterestIncome = Math.abs(interestIncome)

    const result = normalizedInterestIncome / revenue
    return toFixed(result) as ForbiddenIncomeRatioType
  }

  forbiddenIncomePerShare(dto: ShariahFilterForbiddenIncomePerShare): ForbiddenIncomePerShareType | null {
    const {
      interestIncome,
      sharesOutstanding,
    } = dto

    if (isNullOrUndefined(interestIncome) || isNullOrUndefined(sharesOutstanding)) {
      return null
    }

    const normalizedInterestIncome = Math.abs(interestIncome)

    const result = normalizedInterestIncome / sharesOutstanding
    return toFixed(result) as ForbiddenIncomePerShareType
  }
}
