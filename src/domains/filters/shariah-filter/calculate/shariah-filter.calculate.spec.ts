import { Test } from '@nestjs/testing'
import {
  dummyCashAndShortTermInvestments,
  dummyInterestIncome,
  dummyLongTermDebt,
  dummyLongTermInvestments,
  dummyMarketCap,
  dummyOtherCurrentAssets,
  dummyOtherCurrentLiabilities,
  dummySharesOutstanding,
  dummyShortTermDebt,
  dummyTotalRevenue,
} from '../../../../dummy/report-values/report-values.dummy'
import { ShariahFilterCalculateDomain } from './shariah-filter.calculate.domain'

describe('ShariahFilterCalculateDomain', () => {
  let shariahFilterCalculateDomain: ShariahFilterCalculateDomain

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [],
      providers: [ShariahFilterCalculateDomain],
    }).compile()

    shariahFilterCalculateDomain = moduleRef.get<ShariahFilterCalculateDomain>(ShariahFilterCalculateDomain)
  })

  describe('depositToMarketCap', () => {
    it('should calculate with proper data', () => {
      expect(shariahFilterCalculateDomain.depositToMarketCap({
        otherCurrentAssets: dummyOtherCurrentAssets,
        cashAndShortTermInvestments: dummyCashAndShortTermInvestments,
        longTermInvestments: dummyLongTermInvestments,
        marketCap: dummyMarketCap,
      }))
      .toStrictEqual(2.22)
    })
  })

  describe('deptToMarketCap', () => {
    it('should calculate with proper data', () => {
      expect(shariahFilterCalculateDomain.deptToMarketCap({
        longTermDebt: dummyLongTermDebt,
        shortTermDebt: dummyShortTermDebt,
        otherCurrentLiabilities: dummyOtherCurrentLiabilities,
        marketCap: dummyMarketCap,
      }))
        .toStrictEqual(3.13)
    })

    it('should calculate with proper data', () => {
      expect(shariahFilterCalculateDomain.deptToMarketCap({
        longTermDebt: null,
        shortTermDebt: dummyShortTermDebt,
        otherCurrentLiabilities: dummyOtherCurrentLiabilities,
        marketCap: dummyMarketCap,
      }))
        .toStrictEqual(2.7)
    })
    it('should calculate with proper data', () => {
      expect(shariahFilterCalculateDomain.deptToMarketCap({
        longTermDebt: dummyLongTermDebt,
        shortTermDebt: null,
        otherCurrentLiabilities: dummyOtherCurrentLiabilities,
        marketCap: dummyMarketCap,
      }))
        .toStrictEqual(1.19)
    })
    it('should calculate with proper data', () => {
      expect(shariahFilterCalculateDomain.deptToMarketCap({
        longTermDebt: dummyLongTermDebt,
        shortTermDebt: dummyShortTermDebt,
        otherCurrentLiabilities: null,
        marketCap: dummyMarketCap,
      }))
        .toStrictEqual(2.37)
    })
    it('should calculate with proper data', () => {
      expect(shariahFilterCalculateDomain.deptToMarketCap({
        longTermDebt: dummyLongTermDebt,
        shortTermDebt: dummyShortTermDebt,
        otherCurrentLiabilities: dummyOtherCurrentLiabilities,
        marketCap: null,
      }))
        .toBeNull()
    })
    it('should calculate with proper data', () => {
      expect(shariahFilterCalculateDomain.deptToMarketCap({
        longTermDebt: null,
        shortTermDebt: null,
        otherCurrentLiabilities: null,
        marketCap: dummyMarketCap,
      }))
        .toStrictEqual(0)
    })
  })

  describe('forbiddenIncome', () => {
    it('should calculate with proper data', async () => {
      expect(shariahFilterCalculateDomain.forbiddenIncome({
        interestIncome: dummyInterestIncome,
        revenue: dummyTotalRevenue,
      }))
      .toStrictEqual(0.14)
    })

    it('should calculate with null interestIncome', async () => {
      expect(shariahFilterCalculateDomain.forbiddenIncome({
        interestIncome: null,
        revenue: dummyTotalRevenue,
      }))
        .toBeNull()
    })
  })

  describe('forbiddenIncomePerShare', () => {
    it('should calculate with proper data', async () => {
      expect(shariahFilterCalculateDomain.forbiddenIncomePerShare({
        interestIncome: dummyInterestIncome,
        sharesOutstanding: dummySharesOutstanding,
      }))
      .toStrictEqual(1.26)
    })

    it('should calculate with null interestIncome', async () => {
      expect(shariahFilterCalculateDomain.forbiddenIncomePerShare({
        interestIncome: null,
        sharesOutstanding: dummySharesOutstanding,
      }))
        .toBeNull()
    })
  })
})
