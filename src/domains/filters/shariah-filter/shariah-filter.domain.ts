import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { CompanyReportDateType, IsCompanyHalalType, TickerType } from '../../../consts/report.enum'
import { isFalsy, isNullOrUndefined, isNumber } from '../../../utils'
import toFixed from '../../../utils/number/to-fixed/to-fixed.util'
import { FmpDomain } from '../../third-party-data-source/fmp/fmp.domain'
import { YahooDomain } from '../../third-party-data-source/yahoo/yahoo.domain'
import { ShariahFilterCalculateDomain } from './calculate/shariah-filter.calculate.domain'
import {
  ShariahFilterIsAllowedDepositToMarketCapRequest,
  ShariahFilterIsAllowedDeptToMarketCaoRequest,
  ShariahFilterIsAllowedForbiddenIncome,
  ShariahFilterIsCompanyPassShariahFilterRequest,
  ShariahFilterNeededIndicatorsToPassFilterAndFilterResponse,
  ShariahFilterNeededIndicatorsToPassFilterResponse,
  ShariahFilterPassThroughFilterResponse,
} from './shariah-filter.interfaces'

@Injectable()
export class ShariahFilterDomain {
  constructor(
    private configService: ConfigService,
    private fmpDomain: FmpDomain,
    private yahooDomain: YahooDomain,
    private shariahFilterCalculateDomain: ShariahFilterCalculateDomain,
  ) {}

  async checkCompanyForHalalness(ticker: TickerType): Promise<ShariahFilterNeededIndicatorsToPassFilterAndFilterResponse> {
    const data = await this.getDataToPassThroughFilters(ticker)
    const result = this.passThroughFilter(data)
    return {
      ...data,
      ...result,
    }
  }

  getIndicatorsHalalnessConclusion(dto: ShariahFilterIsCompanyPassShariahFilterRequest): IsCompanyHalalType {
    const isAllowedDepositToMarketCap = this.checkIsAllowedDepositToMarketCap(dto.isAllowedDepositToMarketCap)
    const isAllowedDeptToMarketCap = this.checkIsAllowedDeptToMarketCap(dto.isAllowedDeptToMarketCap)
    const isAllowedForbiddenIncome = this.checkIsAllowedForbiddenIncome(dto.isAllowedForbiddenIncome)
    if (isFalsy(isAllowedDepositToMarketCap) || isFalsy(isAllowedDeptToMarketCap) || isFalsy(isAllowedForbiddenIncome)) {
      return false as IsCompanyHalalType
    }

    if (isNullOrUndefined(isAllowedDepositToMarketCap) || isNullOrUndefined(isAllowedDeptToMarketCap) || isNullOrUndefined(isAllowedForbiddenIncome)) {
      return null
    }

    return true as IsCompanyHalalType
  }

  checkIsAllowedDepositToMarketCap(dto: ShariahFilterIsAllowedDepositToMarketCapRequest): boolean | null {
    if (isNullOrUndefined(dto.depositToMarketCap)) {
      return null
    }

    const allowedRatio = this.configService.get<number>('SHARIAH_ALLOWED_DEPOSIT_TO_MARKET_CAP_RATIO')

    if (!allowedRatio) {
      throw new InternalServerErrorException(
        '[ShariahFilterDomain] Allowed deposit to market cap ratio is not found',
      )
    }

    if (dto.depositToMarketCap >= allowedRatio) {
      return false
    }

    /** Return true only if ratio is valid and all data exists */
    return isNumber(dto.cashAndShortTermInvestments)
      && isNumber(dto.longTermInvestments)
      && isNumber(dto.otherCurrentAssets)
  }

  checkIsAllowedDeptToMarketCap(dto: ShariahFilterIsAllowedDeptToMarketCaoRequest): boolean | null {
    if (isNullOrUndefined(dto.deptToMarketCap)) {
      return null
    }

    const allowedRatio = this.configService.get('SHARIAH_ALLOWED_DEPT_TO_MARKET_CAP_RATIO')

    if (!allowedRatio) {
      throw new InternalServerErrorException(
        '[ShariahFilterDomain] Allowed dept to market cap ratio is not found',
      )
    }

    if (dto.deptToMarketCap >= allowedRatio) {
      return false
    }

    return isNumber(dto.longTermDebt)
      && isNumber(dto.shortTermDebt)
      && isNumber(dto.otherCurrentLiabilities)
  }

  checkIsAllowedForbiddenIncome(dto: ShariahFilterIsAllowedForbiddenIncome): boolean | null {
    if (isNullOrUndefined(dto.forbiddenIncome)) {
      return null
    }

    const allowedRatio = this.configService.get('SHARIAH_ALLOWED_FORBIDDEN_INCOME_RATIO')

    if (!allowedRatio) {
      throw new InternalServerErrorException(
        '[ShariahFilterDomain] Allowed forbidden income ratio is not found',
      )
    }

    return dto.forbiddenIncome < allowedRatio
  }

  async getDataToPassThroughFilters(ticker: TickerType): Promise<ShariahFilterNeededIndicatorsToPassFilterResponse> {
    const {
      date,
      otherCurrentAssets,
      cashAndShortTermInvestments,
      longTermInvestments,
      shortTermDebt,
      otherCurrentLiabilities,
      longTermDebt,
    } = await this.fmpDomain.getLastBalanceSheetStatement(ticker)

    const reportDate = new Date(date) as CompanyReportDateType

    const {
      marketCap,
      sharesOutstanding,
    } = await this.fmpDomain.getCompanyQuote(ticker)

    const {
      revenue,
    } = await this.fmpDomain.getLastIncomeStatements(ticker).catch(e => {
      console.error(`getLastQuarterlyIncomeStatement => ${e}`)
      throw new BadRequestException()
    })

    const interestIncome = await this.yahooDomain.getQuarterlyInterestIncome(ticker)

    return {
      reportDate,
      otherCurrentAssets,
      cashAndShortTermInvestments,
      longTermInvestments,
      shortTermDebt,
      otherCurrentLiabilities,
      longTermDebt,
      marketCapAverage: marketCap,
      sharesOutstanding,
      interestIncome,
      revenue,
    }
  }

  passThroughFilter(dto: ShariahFilterNeededIndicatorsToPassFilterResponse): ShariahFilterPassThroughFilterResponse {
    const {
      reportDate,
      otherCurrentAssets,
      cashAndShortTermInvestments,
      longTermInvestments,
      shortTermDebt,
      otherCurrentLiabilities,
      longTermDebt,
      marketCapAverage,
      sharesOutstanding,
      interestIncome,
      revenue,
    } = dto

    const depositToMarketCap = this.shariahFilterCalculateDomain.depositToMarketCap({
      marketCap: marketCapAverage,
      otherCurrentAssets,
      cashAndShortTermInvestments,
      longTermInvestments,
    })

    const deptToMarketCap = this.shariahFilterCalculateDomain.deptToMarketCap({
      shortTermDebt,
      otherCurrentLiabilities,
      longTermDebt,
      marketCap: marketCapAverage,
    })

    const forbiddenIncome = this.shariahFilterCalculateDomain.forbiddenIncome({
      interestIncome,
      revenue,
    })

    const forbiddenIncomePerShare = this.shariahFilterCalculateDomain.forbiddenIncomePerShare({
      interestIncome,
      sharesOutstanding,
    })

    return {
      reportDate,
      depositToMarketCap: toFixed(depositToMarketCap, 5),
      deptToMarketCap: toFixed(deptToMarketCap, 5),
      forbiddenIncome: toFixed(forbiddenIncome, 5),
      forbiddenIncomePerShare: toFixed(forbiddenIncomePerShare, 5),
    }
  }


}
