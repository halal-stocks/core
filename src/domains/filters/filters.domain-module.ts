import { Module } from '@nestjs/common'
import { CompanyImplModule } from '../../persistence/company/company.impl-module'
import { ThirdPartyDataSourceDomainModule } from '../third-party-data-source/third-party-data-source.domain-module'
import { FilterDataAggregatorDomain } from './data-aggregator/filter-data-aggregator.domain'
import { FilterRussianDataAggregatorDomain } from './data-aggregator/filter-russian-data-aggregator.domain'
import { FilterUSADataAggregatorDomain } from './data-aggregator/filter-usa-data-aggregator.domain'
import { FiltersDomain } from './filters.domain'
import { GrowthAnalysisDomain } from './growth-analysis/growth-analysis.domain'
import { ShariahFilterCalculateDomain } from './shariah-filter/calculate/shariah-filter.calculate.domain'
import { ShariahFilterDomain } from './shariah-filter/shariah-filter.domain'
import { ValueAnalysisCalculateDomain } from './value-analysis/calculate/value-analysis.calculate.domain'
import { ValueAnalysisDomain } from './value-analysis/value-analysis.domain'

@Module({
  imports: [
    ThirdPartyDataSourceDomainModule,
    CompanyImplModule,
  ],
  providers: [
    ValueAnalysisDomain,
    ValueAnalysisCalculateDomain,
    ShariahFilterDomain,
    ShariahFilterCalculateDomain,
    GrowthAnalysisDomain,
    FiltersDomain,
    FilterDataAggregatorDomain,
    FilterRussianDataAggregatorDomain,
    FilterUSADataAggregatorDomain,
  ],
  exports: [
    FiltersDomainModule,
    FiltersDomain,
    ShariahFilterDomain,
  ],
})
export class FiltersDomainModule {}
