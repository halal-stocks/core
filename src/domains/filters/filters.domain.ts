import { Injectable } from '@nestjs/common'
import { TickerType } from '../../consts/report.enum'
import { CompanyService } from '../../persistence/company/company.service'
import { FilterDataAggregatorDomain } from './data-aggregator/filter-data-aggregator.domain'
import { FilterShariahFilterAndFullAnalysisResponse } from './filters.interfaces'
import { GrowthAnalysisDomain } from './growth-analysis/growth-analysis.domain'
import { GrowthAnalysisGetResultResponse } from './growth-analysis/growth-analysis.interfaces'
import { ShariahFilterDomain } from './shariah-filter/shariah-filter.domain'
import { ShariahFilterNeededIndicatorsToPassFilterAndFilterResponse } from './shariah-filter/shariah-filter.interfaces'
import { ValueAnalysisDomain } from './value-analysis/value-analysis.domain'
import { ValueAnalysisResultResponse } from './value-analysis/value-analysis.interfaces'

@Injectable()
export class FiltersDomain {
  constructor(
    private shariahFilterDomain: ShariahFilterDomain,
    private valueAnalysisDomain: ValueAnalysisDomain,
    private companyGrowthDomain: GrowthAnalysisDomain,
    private filterDataAggregatorDomain: FilterDataAggregatorDomain,
    private companyService: CompanyService,
  ) {}

  async passThroughShariahFilter(ticker: TickerType): Promise<ShariahFilterNeededIndicatorsToPassFilterAndFilterResponse> {
    return this.shariahFilterDomain.checkCompanyForHalalness(ticker)
  }

  async passThroughCompanyValueAnalysis(ticker: TickerType): Promise<ValueAnalysisResultResponse> {
    return this.valueAnalysisDomain.analiseCompany(ticker)
  }

  async passThroughCompanyGrowthFilter(ticker: TickerType): Promise<GrowthAnalysisGetResultResponse> {
    return this.companyGrowthDomain.analiseCompany(ticker)
  }

  async passThroughAllFilters(ticker: TickerType): Promise<FilterShariahFilterAndFullAnalysisResponse> {
    const exchange = await this.companyService.getExchangeByTicker(ticker)
    const allNeededIndicators = await this.filterDataAggregatorDomain
      .getDataAggregatorByExchange(exchange)
      .getAllNeededIndicators(ticker)

    const shariahFilterResult = this.shariahFilterDomain.passThroughFilter(allNeededIndicators.toShariahFilter)

    const valueAnalysisResult = this.valueAnalysisDomain.getAnalysisResult(allNeededIndicators.toValueAnalysis)

    const growthAnalysisResult = this.companyGrowthDomain.getAnaliseResult(allNeededIndicators.toGrowthAnalysis)

    return {
      shariahFilter: {
        ...allNeededIndicators.toShariahFilter,
        ...shariahFilterResult,
      },
      valueAnalysis: valueAnalysisResult,
      growthAnalysis: growthAnalysisResult,
    }
  }

  // Trash
  async testNew(ticker: TickerType): Promise<any> {
    return this.filterDataAggregatorDomain.testNew(ticker)
  }
}
