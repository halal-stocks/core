import { Injectable } from '@nestjs/common'
import { TickerType } from '../../../consts/report.enum'
import toFixed from '../../../utils/number/to-fixed/to-fixed.util'
import { FmpDomain } from '../../third-party-data-source/fmp/fmp.domain'
import {
  GrowthAnalysisGetNeededIndicatorsResponse,
  GrowthAnalysisGetResultResponse,
  GrowthAnalysisResultRequest,
} from './growth-analysis.interfaces'

@Injectable()
export class GrowthAnalysisDomain {
  constructor(
    private fmpDomain: FmpDomain,
  ) {}

  async analiseCompany(ticker: TickerType): Promise<GrowthAnalysisGetResultResponse> {
    const indicators = await this.getNeededIndicators(ticker)
    return this.getAnaliseResult(indicators)
  }

  async getNeededIndicators(ticker: TickerType): Promise<GrowthAnalysisGetNeededIndicatorsResponse> {
    const {
      pegRatioTTM,
      netProfitMarginTTM,
      returnOnAssetsTTM,
      returnOnEquityTTM,
    } = await this.fmpDomain.getRatioTTM(ticker)

    const {
      beta,
    } = await this.fmpDomain.getCompanyProfile(ticker)

    return {
      beta,
      pegRatio: pegRatioTTM,
      netProfitMargin: netProfitMarginTTM,
      returnOnAssets: returnOnAssetsTTM,
      returnOnEquity: returnOnEquityTTM,
    }
  }

  getAnaliseResult(dto: GrowthAnalysisResultRequest): GrowthAnalysisGetResultResponse {
    return {
      beta: toFixed(dto.beta, 5),
      pegRatio: toFixed(dto.pegRatio, 5),
      netProfitMargin: toFixed(dto.netProfitMargin, 5),
      roa: toFixed(dto.returnOnAssets, 5),
      roe: toFixed(dto.returnOnEquity, 5),
    }
  }
}
