import { BetaType, NetProfitMarginType, PEGRatioType, ROAType, ROEType } from '../../../consts/report.enum'

export interface GrowthAnalysisGetNeededIndicatorsResponse {
  pegRatio: PEGRatioType
  netProfitMargin: NetProfitMarginType
  returnOnAssets: ROAType
  returnOnEquity: ROEType
  beta: BetaType
}

export interface GrowthAnalysisResultRequest {
  pegRatio: PEGRatioType | null
  netProfitMargin: NetProfitMarginType | null
  returnOnAssets: ROAType | null
  returnOnEquity: ROEType | null
  beta: BetaType | null
}

export interface GrowthAnalysisGetResultResponse {
  pegRatio: PEGRatioType | null
  netProfitMargin: NetProfitMarginType | null
  roa: ROAType | null
  roe: ROEType | null
  beta: BetaType | null
}
