import { BadRequestException, Injectable } from '@nestjs/common'
import { CompanyReportDateType, TickerType } from '../../../consts/report.enum'
import { FmpDomain } from '../../third-party-data-source/fmp/fmp.domain'
import { SeekingalphaDomain } from '../../third-party-data-source/seekingalpha/seekingalpha.domain'
import { YahooDomain } from '../../third-party-data-source/yahoo/yahoo.domain'
import { FilterGetAllNeededIndicatorsResponse } from '../filters.interfaces'
import { FilterDataAggregatorInterface } from './filter-data-aggregator.interface'

@Injectable()
export class FilterUSADataAggregatorDomain implements FilterDataAggregatorInterface {
  constructor(
    private fmpDomain: FmpDomain,
    private seekingalphaDomain: SeekingalphaDomain,
    private yahooDomain: YahooDomain,
  ) {}

  async getAllNeededIndicators(ticker: TickerType): Promise<FilterGetAllNeededIndicatorsResponse> {
    const {
      date = null,
      otherCurrentAssets = null,
      cashAndShortTermInvestments = null,
      longTermInvestments = null,
      shortTermDebt = null,
      otherCurrentLiabilities = null,
      longTermDebt = null,

      totalCurrentAssets = null,
      totalLiabilities = null,
    } = await this.fmpDomain.getLastBalanceSheetStatement(ticker).catch(e => {
      console.error(`getLastBalanceSheetStatement => ${e}`)
      throw new BadRequestException()
    })

    const reportDate = date && new Date(date) as CompanyReportDateType

    const {
      sharesOutstanding = null,
      pe = null,
      eps = null,
    } = await this.fmpDomain.getCompanyQuote(ticker).catch(e => {
      console.error(`getCompanyQuote => ${e}`)
      throw new BadRequestException()
    })

    const {
      dividendYieldTTM = null,
      payoutRatioTTM = null,
      priceToSalesRatioTTM = null,
      pbRatioTTM = null,
    } = await this.fmpDomain.getKeyMetrics(ticker).catch(e => {
      console.error(`getKeyMetrics => ${e}`)
      throw new BadRequestException()
    })

    const {
      freeCashFlow = null,
    } = await this.fmpDomain.getLastCashFlowStatements(ticker).catch(e => {
      console.error(`getLastCashFlowStatements => ${e}`)
      throw new BadRequestException()
    })

    const {
      pegRatioTTM = null,
      netProfitMarginTTM = null,
      returnOnAssetsTTM = null,
      returnOnEquityTTM = null,
    } = await this.fmpDomain.getRatioTTM(ticker).catch(e => {
      console.error(`getRatioTTM => ${e}`)
      throw new BadRequestException()
    })

    const {
      beta = null,
    } = await this.fmpDomain.getCompanyProfile(ticker).catch(e => {
      console.error(`getCompanyProfile => ${e}`)
      throw new BadRequestException()
    })

    const { revenue, interestIncome } = await this.seekingalphaDomain.getQuarterlyFinancialsParse(ticker)

    const marketCapAverage = await this.fmpDomain.averageMarketCapForLast3YearsOrActual(ticker)

    return {
      toShariahFilter: {
        reportDate,
        otherCurrentAssets,
        cashAndShortTermInvestments,
        longTermInvestments,
        shortTermDebt,
        otherCurrentLiabilities,
        longTermDebt,
        marketCapAverage,
        sharesOutstanding,
        interestIncome,
        revenue,
      },
      toValueAnalysis: {
        reportDate,
        totalCurrentAssets,
        totalLiabilities,
        freeCashFlow,
        peRatio: pe,
        dividendYield: dividendYieldTTM,
        payoutRatio: payoutRatioTTM,
        psRatio: priceToSalesRatioTTM,
        pbRatio: pbRatioTTM,
        eps,
      },
      toGrowthAnalysis: {
        beta,
        pegRatio: pegRatioTTM,
        netProfitMargin: netProfitMarginTTM,
        returnOnAssets: returnOnAssetsTTM,
        returnOnEquity: returnOnEquityTTM,
      },
    }
  }

  // Trash
  async testNew(ticker: TickerType): Promise<any> {
    const sp500 = await this.fmpDomain.getSP500MarketIndex()
    const i = 1
    for (const fmpSP500 of sp500) {
      const interest = await this.yahooDomain.getQuarterlyInterestIncome(fmpSP500.symbol)
      console.log(`${i})  ${fmpSP500.symbol} interest => ${interest}`)
      setTimeout(() => {}, 500)
    }
  }
}
