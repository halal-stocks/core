import { Injectable } from '@nestjs/common'
import { Exchange } from '../../../consts/exchange.enum'
import { TickerType } from '../../../consts/report.enum'
import { FilterRussianDataAggregatorDomain } from './filter-russian-data-aggregator.domain'
import { FilterUSADataAggregatorDomain } from './filter-usa-data-aggregator.domain'

@Injectable()
export class FilterDataAggregatorDomain {
  constructor(
    private filterUSADataAggregatorDomain: FilterUSADataAggregatorDomain,
    private filterRussianDataAggregatorDomain: FilterRussianDataAggregatorDomain,
  ) {}

  getDataAggregatorByExchange(exchange: Exchange) {
    switch (exchange) {
      case Exchange.ETF:
      case Exchange.MUTUAL_FUND:
      case Exchange.COMMODITY:
      case Exchange.INDEX:
      case Exchange.CRYPTO:
      case Exchange.FOREX:
      case Exchange.TSX:
      case Exchange.AMEX:
      case Exchange.NASDAQ:
      case Exchange.NYSE:
      case Exchange.EURONEXT: {
        return this.filterUSADataAggregatorDomain
      }
      case Exchange.MCX: {
        return this.filterRussianDataAggregatorDomain
      }
    }
  }

  testNew(ticker: TickerType) {
    return this.filterRussianDataAggregatorDomain.testNew(ticker)
  }
}
