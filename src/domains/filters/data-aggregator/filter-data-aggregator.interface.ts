import { TickerType } from '../../../consts/report.enum'
import { FilterGetAllNeededIndicatorsResponse } from '../filters.interfaces'

export interface FilterDataAggregatorInterface {
  getAllNeededIndicators(ticker: TickerType): Promise<FilterGetAllNeededIndicatorsResponse>

  testNew?(ticker: TickerType): Promise<any>
}
