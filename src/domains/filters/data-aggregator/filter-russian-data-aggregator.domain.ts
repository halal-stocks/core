import { Injectable } from '@nestjs/common'
import { TickerType } from '../../../consts/report.enum'
import { getBrowser, getPageContent } from '../../../utils/site-scraping/site-scraping.util'
import { FilterGetAllNeededIndicatorsResponse } from '../filters.interfaces'
import { FilterDataAggregatorInterface } from './filter-data-aggregator.interface'

@Injectable()
export class FilterRussianDataAggregatorDomain implements FilterDataAggregatorInterface {
  async getAllNeededIndicators(ticker: TickerType): Promise<FilterGetAllNeededIndicatorsResponse> {
    const browser = await getBrowser()
    const $ = await getPageContent({
      browser,
      url: `https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=13684&report_type=BAL&period_type=Interim`,
      pageBlocks: {
        noBlocks: true,
      },
      onPageLoad: async page => {
        await page.waitForTimeout(10000)
      },
    })

    const toShariahFilter = {
      cashAndShortTermInvestments: $('.genTbl.reportTbl span:contains("Cash and Short Term Investments")')
        .parent()
        .next('td')
        .html() as any,
      totalLiabilities: $('.genTbl.reportTbl span:contains("Total Current Liabilities")')
        .parent()
        .next('td')
        .html() as any,
      otherCurrentLiabilities: $('.genTbl.reportTbl .child.last span:contains("Other Liabilities, Total")')
        .parent()
        .next('td')
        .html() as any,
      sharesOutstanding: $('.genTbl.reportTbl span.bold:contains("Total Common Shares Outstanding")')
        .parent()
        .next('td')
        .html() as any,
      longTermDebt: $('.genTbl.reportTbl span:contains("Total Long Term Debt")').parent().next('td').html() as any,

      otherCurrentAssets: 0 as any,
      longTermInvestments: $('.genTbl.reportTbl tr.child span:contains("Long Term Investments")')
        .parent()
        .next('td')
        .html() as any,
      marketCapAverage: 0 as any,
      shortTermDebt: 0 as any,
      interestIncome: 0 as any,
      revenue: 0 as any,
      totalCurrentAssets: 0 as any,
      dividendYield: 0 as any,
      payoutRatio: 0 as any,
      peRatio: 0 as any,
      psRatio: 0 as any,
      pbRatio: 0 as any,
      freeCashFlow: 0 as any,
      eps: 0 as any,
      pegRatio: 0 as any,
      netProfitMargin: 0 as any,
      returnOnAssets: 0 as any,
      returnOnEquity: 0 as any,
      beta: 0 as any,
    }

    return {} as FilterGetAllNeededIndicatorsResponse
  }

  async testNew(ticker: TickerType): Promise<any> {
    await this.getAllNeededIndicators(ticker)
  }
}
