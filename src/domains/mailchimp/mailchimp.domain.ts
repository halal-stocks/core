import * as mailchimp from '@mailchimp/mailchimp_marketing'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import {
  MailchimpAddSubscriberRequest,
  MailchimpAudienceResponse,
  MailchimpMemberResponse,
} from './mailchimp.interfaces'


@Injectable()
export class MailchimpDomain {
  private mailchimp = mailchimp

  constructor(
    private configService: ConfigService,
  ) {
    this.mailchimp.setConfig({
      apiKey: this.configService.get('MAILCHIMP_API_KEY'),
      server: this.configService.get('MAILCHIMP_SERVER_PREFIX'),
    })
  }

  async addSubscriber(listId: string, member: MailchimpAddSubscriberRequest): Promise<void> {
    await this.mailchimp.lists.addListMember(listId, member)
  }

  async getAllMembers(listId: string): Promise<MailchimpMemberResponse[]> {
    const response: { members: MailchimpMemberResponse[] } = await this.mailchimp.lists.getListMembersInfo(listId)
    return response.members
  }

  async getAllAudiences(): Promise<MailchimpAudienceResponse[]> {
    const response: { lists: MailchimpAudienceResponse[] } = await this.mailchimp.lists.getAllLists()
    return response.lists
  }
} 
