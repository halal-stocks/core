import { Module } from '@nestjs/common'
import { MailchimpDomain } from './mailchimp.domain'

@Module({
  imports: [],
  providers: [MailchimpDomain],
  exports: [MailchimpDomain, MailchimpDomainModule],
})
export class MailchimpDomainModule {

}
