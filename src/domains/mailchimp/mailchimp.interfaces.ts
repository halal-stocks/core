export enum MailchimpSubscriberStatus {
  subscribed = 'subscribed',
  unsubscribed = 'unsubscribed',
  cleaned = 'cleaned',
  pending = 'pending',
  transactional = 'transactional',
}

export interface MailchimpAddSubscriberRequest {
  email_address: string
  status: MailchimpSubscriberStatus
  tags: string[]
}

// It has much more fields. Typed only needed
export interface MailchimpAudienceResponse {
  id: string,
  web_id: number,
  name: string,
}

// It has much more fields. Typed only needed
export interface MailchimpMemberResponse {
  id: string
  email_address: string
  status: MailchimpSubscriberStatus,
  tags: string
}

export enum MailchimpAudienceTag {
  '10stocks' = '10stocks',
  WelcomeH = 'WelcomeH',
}
