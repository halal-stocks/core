import {
  BetaType,
  DividendYieldType,
  EPSType,
  MarketCapType,
  PBRatioType,
  PERatioType,
  PSRatioType,
  StockPriceType,
} from '../../consts/report.enum'
import { MinMaxValue } from '../../shared/generics/min-max.generic'

export class ScreenerFilterOptionsResponse {
  stockPrice: MinMaxValue<StockPriceType>
  marketCap: MinMaxValue<MarketCapType>
  peRatio: MinMaxValue<PERatioType>
  psRatio: MinMaxValue<PSRatioType>
  beta: MinMaxValue<BetaType>
  eps: MinMaxValue<EPSType>
  pbRatio: MinMaxValue<PBRatioType>
  dividendYield: MinMaxValue<DividendYieldType>
}
