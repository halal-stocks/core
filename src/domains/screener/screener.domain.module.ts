import { Module } from '@nestjs/common'
import { CompanyImplModule } from '../../persistence/company/company.impl-module'
import { ThirdPartyDataSourceDomainModule } from '../third-party-data-source/third-party-data-source.domain-module'
import { ScreenerDomain } from './screener.domain'

@Module({
  imports: [
    CompanyImplModule,
    ThirdPartyDataSourceDomainModule,
  ],
  providers: [ScreenerDomain],
  exports: [ScreenerDomainModule, ScreenerDomain],
})
export class ScreenerDomainModule {}
