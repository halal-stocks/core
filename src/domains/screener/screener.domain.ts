import { Injectable } from '@nestjs/common'
import { ScreenerFiltersRequestDto, ScreenerResponseDto } from '../../api/screener/screener.dtos'
import { IsCompanyHalalType } from '../../consts/report.enum'
import { CompanyAnalysisResultService } from '../../persistence/company/analysis-result/company-analysis-result.service'
import { CompanyService } from '../../persistence/company/company.service'
import { CompanyIndicatorsService } from '../../persistence/company/indicators/company-indicators.service'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { LockableData } from '../../shared/generics/lockable-data.generic'
import { SortAndFilters } from '../../shared/generics/search-and-filters.generic'
import { ceilNumber } from '../../utils/number/round/rount-number.util'
import { YahooDomain } from '../third-party-data-source/yahoo/yahoo.domain'
import { ScreenerFilterOptionsResponse } from './screener.interfaces'

@Injectable()
export class ScreenerDomain {
  constructor(
    private companyIndicatorsService: CompanyIndicatorsService,
    private companyAnalysisResultService: CompanyAnalysisResultService,
    private companyService: CompanyService,
    private yahooDomain: YahooDomain,
  ) {}


  async getScreenerFilterOptions(): Promise<ScreenerFilterOptionsResponse> {
    const indicatorsMinMaxValues = await this.companyIndicatorsService.getMinMaxValues()
    const analysisResultMinMaxValues = await this.companyAnalysisResultService.getMinMaxValues()

    return {
      stockPrice: {
        min: ceilNumber(indicatorsMinMaxValues.stockPriceMin),
        max: ceilNumber(indicatorsMinMaxValues.stockPriceMax),
      },
      marketCap: {
        min: ceilNumber(indicatorsMinMaxValues.marketCapMin),
        max: ceilNumber(indicatorsMinMaxValues.marketCapMax),
      },
      peRatio: {
        min: ceilNumber(analysisResultMinMaxValues.peRatioMin),
        max: ceilNumber(analysisResultMinMaxValues.peRatioMax),
      },
      psRatio: {
        min: ceilNumber(analysisResultMinMaxValues.psRatioMin),
        max: ceilNumber(analysisResultMinMaxValues.psRatioMax),
      },
      beta: {
        min: ceilNumber(analysisResultMinMaxValues.betaMin),
        max: ceilNumber(analysisResultMinMaxValues.betaMax),
      },
      eps: {
        min: ceilNumber(analysisResultMinMaxValues.epsMin),
        max: ceilNumber(analysisResultMinMaxValues.epsMax),
      },
      pbRatio: {
        min: ceilNumber(analysisResultMinMaxValues.pbRatioMin),
        max: ceilNumber(analysisResultMinMaxValues.pbRatioMax),
      },
      dividendYield: {
        min: ceilNumber(analysisResultMinMaxValues.dividendYieldMin),
        max: ceilNumber(analysisResultMinMaxValues.dividendYieldMax),
      },
    }
  }

  async getAllForScreener(isPaid: boolean = false, sortAndFilters?: SortAndFilters<ScreenerFiltersRequestDto>): Promise<ItemsWithCount<ScreenerResponseDto>> {
    const { count, items } = await this.companyService.getAllForScreener(isPaid, sortAndFilters)
    const normalizedItems = items.map(({ analyzes, isFree, isHalal, ...restCompany }) => ({
      ...restCompany,
      analysis: analyzes.length ? analyzes[0] : {},
      isHalal: (isPaid || isFree) ? isHalal : 'locked' as LockableData<IsCompanyHalalType>,
    }))
    return {
      items: normalizedItems,
      count,
    }
  }

  async getRussiansStocks(): Promise<any> {
    return this.yahooDomain.getRussianStocks()
  }
}
