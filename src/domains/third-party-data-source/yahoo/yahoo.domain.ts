import { HttpService, Injectable, InternalServerErrorException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { lastValueFrom } from 'rxjs'
import { InterestIncomeType, TickerType } from '../../../consts/report.enum'
import { isArray, isType } from '../../../utils'
import {
  YahooMakeRequest,
  YahooQuarterlyInterestIncomeNonOperating,
  YahooQuarterlyInterestIncomeResponse,
  YahooResponse,
  YahooScreenerItemResponse,
  YahooScreenerResponse,
} from './yahoo.interfaces'

@Injectable()
export class YahooDomain {
  constructor(
    private configService: ConfigService,
    private httpService: HttpService,
  ) {}

  async getQuarterlyInterestIncome(ticker: TickerType): Promise<InterestIncomeType | null> {
    const params = {
      type: 'quarterlyInterestIncomeNonOperating,quarterlyInterestIncome',
      period1: '493590046',
      period2: Math.floor(Date.now() / 1000),
    }

    const endpoint = 'ws/fundamentals-timeseries/v1/finance/timeseries'

    const response = await this.makeRequest<YahooResponse>({ ticker, params, endpoint })
    const result = response.timeseries.result[0]
    const interestIncome = isType<YahooQuarterlyInterestIncomeResponse>(result, ['quarterlyInterestIncome'])
      ? result.quarterlyInterestIncome
      : (result as YahooQuarterlyInterestIncomeNonOperating).quarterlyInterestIncomeNonOperating

    const report = interestIncome && interestIncome[interestIncome.length - 1]
    if (!report) {
      return null
    }
    return report.reportedValue.raw
  }

  async getRussianStocks(page = 1): Promise<YahooScreenerItemResponse[]> {
    const perPage = 200
    const offset = (page - 1) * perPage

    const cookie = 'APID=VA46eab3fa-b451-11ea-b3e5-06fd66a034ec; PRF=t%3DDNLM.L%252BDNLMY%252BAAPL%252BSOFI; F=d=LqafoTY9vNzb831I8yydW.cJQfmYVe6gnlXiIi795aYstig4c5ON; PH=l=en-US; Y=v=1&n=4ms4fjkrv0g8t&l=h8a11g9uu5nl1xduupinjoxslgxdc0pwlbmnjojn/o&p=n2vvvkg00000000&r=184&intl=us; A1=d=AQABBMKoZGECEJAD68zJZ8AE1sRmcyxl3sUFEgEABAJdeWFcYtwyPzIB_eMBAAcIwqhkYSxl3sUIDwMbeK8YPSeK5LxqWi5_jQkBBwoBkA&S=AQAAAsN30Epu7pUkQiBqzeC4UG8; A3=d=AQABBMKoZGECEJAD68zJZ8AE1sRmcyxl3sUFEgEABAJdeWFcYtwyPzIB_eMBAAcIwqhkYSxl3sUIDwMbeK8YPSeK5LxqWi5_jQkBBwoBkA&S=AQAAAsN30Epu7pUkQiBqzeC4UG8; A1S=d=AQABBMKoZGECEJAD68zJZ8AE1sRmcyxl3sUFEgEABAJdeWFcYtwyPzIB_eMBAAcIwqhkYSxl3sUIDwMbeK8YPSeK5LxqWi5_jQkBBwoBkA&S=AQAAAsN30Epu7pUkQiBqzeC4UG8&j=WORLD; GUC=AQEABAJheV1iXEIgFwRG; ucs=tr=1636010142000; OTH=v=1&d=eyJraWQiOiIwMTY0MGY5MDNhMjRlMWMxZjA5N2ViZGEyZDA5YjE5NmM5ZGUzZWQ5IiwiYWxnIjoiUlMyNTYifQ.eyJjdSI6eyJndWlkIjoiUVhBSVdYT0VLQ1RTVkROSUlFMzY0N05HU00iLCJwZXJzaXN0ZW50Ijp0cnVlLCJzaWQiOiJWcDJTeGRkRzFwZTcifX0.um7ZhtLr2OxJ4uh9iX2BEGGV14HCx-H-818UUyEtEj683PGRFS_4v7kwfWYu9hc5tCeh4onwtW-9pMvQBQJC8iuFXsE5rFWV8UViaUCSrSoZwPir13qpMPEkecgJjqfhbYbWaW-bmioGZ1z9iWOZi-Sm5Y2FLKcAFD6bIeeIMs4; T=af=JnRzPTE2MzU5MjM3NDMmcHM9eWRXNF9NZW1GS0pJUkN4NENnRGZ1Zy0t&d=bnMBeWFob28BZwFRWEFJV1hPRUtDVFNWRE5JSUUzNjQ3TkdTTQFhYwFBRUpoMzlqUQFhbAFkYXZyYW5zYXRAZ21haWwuY29tAXNjAWRlc2t0b3Bfd2ViAWZzATl6eDVSNGxoZUF5RwF6egFHeUFlaEJnb0gBYQFRQUUBbGF0AUd5QWVoQgFudQEw&sk=DAAb0njhG5PS3v&kt=EAAI1RVuxZI96sCB8_oKS_xwQ--~I&ku=FAAbJy.F9j672hgD.hvdGq.nfeYOF423DL7I6Ntbq34.zcZrP3Ozcwgj4d8AJVetL.5JzVX3V6Fwy76ftu6mjRX9ky60xeRnTzHaZLTH.VKlX2K4HCWQ2SpW2eVqWUJeI4KLrWL2egN.0XEgBVcQTjI_PndwMwAFfalc5tUQODhCS8-~E; B=cbnj55hgm9a62&b=4&d=QVZaOiBtYFlAUwUn45Td&s=ms&i=Axt4rxg9J4rkvGpaLn.N; APIDTS=1635923874; cmp=t=1636008536&j=0'
    const endpoint = 'v1/finance/screener'
    const crumb = 'BxCwD%2FZ5NVF'
    const body = {
      'size': perPage,
      'offset': offset,
      'sortField': 'intradaymarketcap',
      'sortType': 'DESC',
      'quoteType': 'EQUITY',
      'topOperator': 'AND',
      'query': {
        'operator': 'AND',
        'operands': [
          {
            'operator': 'or',
            'operands': [
              {
                'operator': 'EQ',
                'operands': [
                  'region',
                  'ru',
                ],
              },
            ],
          },
        ],
      },
    }

    const response = await this.makeRequest<YahooScreenerResponse>({
      headers: {
        cookie,
      },
      endpoint,
      method: 'POST',
      params: {
        crumb,
      },
      body,
    })

    if (response.finance.error) {
      throw new InternalServerErrorException(`Yahoo getRussianStocks error => ${response.finance.error}`)
    }

    const quotes = response.finance.result[0].quotes

    if (!isArray(quotes)) {
      throw new InternalServerErrorException(
        `Yahoo getRussianStocks. Quote is not an array => ${quotes}`)
    }

    if (!isType(quotes[0], ['symbol', 'marketCap'])) {
      throw new InternalServerErrorException(
        `Yahoo getRussianStocks. Quote is not in right format => ${quotes[0]}`)
    }

    const normalizedQuotes = this.normalizeQuotes(quotes)

    if (response.finance.result[0].start + perPage >= response.finance.result[0].total) {
      return normalizedQuotes
    }

    const nextPage = await this.getRussianStocks(page + 1)
    return [...normalizedQuotes, ...nextPage]
  }

  private normalizeQuotes(quotes: YahooScreenerItemResponse[]): any[] {
    return quotes.map((quote) => ({
      ticker: quote.symbol,
      marketCap: quote.marketCap,
      stockPrice: quote.regularMarketPrice,
      currency: quote.financialCurrency,
      dividendYield: quote.trailingAnnualDividendYield,
    }))
  }

  private async makeRequest<T>(props: YahooMakeRequest): Promise<T> {
    const {
      ticker,
      params = {},
      endpoint,
      method = 'GET',
      body,
      headers,
    } = props

    const apiPath = this.configService.get<string>('YAHOO_FINANCE_API')
    const tickerString = ticker ? `${ticker}/` : ''
    const requestUrl = `${apiPath}/${endpoint}${tickerString}`
    const response = await this.httpService.request({
      url: requestUrl,
      method,
      data: body,
      headers,
      params: {
        ...params,
      },
    })

    const data = (await lastValueFrom(response)).data

    if (data['Error Message']) {
      throw data['Error Message']
    }
    return data
  }
}
