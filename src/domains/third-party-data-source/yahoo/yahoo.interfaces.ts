import { Currency } from '../../../consts/currency.enum'
import { Exchange } from '../../../consts/exchange.enum'
import {
  CompanyReportDateAsStringType,
  DividendYieldType,
  InterestIncomeType,
  MarketCapType,
  StockPriceType,
  TickerType,
} from '../../../consts/report.enum'

export interface YahooQuarterlyInterestIncomeResponse {
  meta: {
    symbol: [TickerType]
    type: ['quarterlyInterestIncome']
  },
  timestamp?: number[],
  quarterlyInterestIncome?: {
    dataId: number,
    asOfDate: CompanyReportDateAsStringType,
    periodType: string,
    currencyCode: Currency,
    reportedValue: {
      raw: InterestIncomeType,
      fmt: string
    }
  }[]
}

export interface YahooQuarterlyInterestIncomeNonOperating {
  meta: {
    symbol: [TickerType]
    type: ['quarterlyInterestIncomeNonOperating']
  },
  timestamp?: number[],
  quarterlyInterestIncomeNonOperating?: {
    dataId: number,
    asOfDate: CompanyReportDateAsStringType,
    periodType: string,
    currencyCode: Currency,
    reportedValue: {
      raw: InterestIncomeType,
      fmt: string
    }
  }[]
}

export interface YahooResponse {
  timeseries: {
    result: [YahooQuarterlyInterestIncomeNonOperating | YahooQuarterlyInterestIncomeResponse],
    error: null
  }
}

export interface YahooMakeRequest {
  endpoint: string
  ticker?: TickerType
  method?: 'POST' | 'GET'
  headers?: Record<string, string | number>
  body?: Record<string, any>
  params?: Record<string, number | string | boolean>
}

export interface YahooScreenerResponse {
  finance: {
    result: [{
      start: number,
      count: number,
      total: number,
      quotes: YahooScreenerItemResponse[]
    }],
    error: string | null,
  }
}

export interface YahooScreenerItemResponse {
  language: 'en-US',
  region: 'US',
  quoteType: 'EQUITY',
  quoteSourceName: 'Free Realtime Quote',
  triggerable: false,
  currency: 'RUB',
  regularMarketChangePercent: 0,
  esgPopulated: false,
  tradeable: false,
  firstTradeDateMilliseconds: 1267597800000,
  priceHint: 4,
  regularMarketChange: 0,
  regularMarketTime: 1636107709,
  regularMarketPrice: StockPriceType,
  regularMarketDayHigh: StockPriceType,
  regularMarketDayRange: string,
  regularMarketDayLow: StockPriceType,
  regularMarketVolume: 550000,
  regularMarketPreviousClose: StockPriceType,
  bid: StockPriceType,
  ask: StockPriceType,
  bidSize: 10000,
  askSize: 10000,
  exchange: Exchange.MCX,
  market: 'ru_market',
  messageBoardId: 'finmb_25237120',
  fullExchangeName: 'MCX',
  longName: 'public-stock company Tomsk distribution company',
  financialCurrency: Currency,
  regularMarketOpen: 0.382,
  averageDailyVolume3Month: 357077,
  averageDailyVolume10Day: 744287,
  fiftyTwoWeekLowChange: 0.036000013,
  fiftyTwoWeekLowChangePercent: 0.107142895,
  fiftyTwoWeekRange: '0.336 - 0.45',
  fiftyTwoWeekHighChange: -0.07799998,
  fiftyTwoWeekHighChangePercent: -0.17333329,
  fiftyTwoWeekLow: 0.336,
  fiftyTwoWeekHigh: 0.45,
  trailingAnnualDividendRate: 0.009,
  trailingAnnualDividendYield: DividendYieldType,
  marketState: 'REGULAR',
  epsTrailingTwelveMonths: -0.09,
  sharesOutstanding: 3819320064,
  bookValue: 0.852,
  fiftyDayAverage: 0.38471428,
  fiftyDayAverageChange: -0.012714267,
  fiftyDayAverageChangePercent: -0.033048596,
  twoHundredDayAverage: 0.3718042,
  twoHundredDayAverageChange: 0.00019580126,
  twoHundredDayAverageChangePercent: 0.0005266246,
  marketCap: MarketCapType,
  priceToBook: 0.43661973,
  sourceInterval: 15,
  exchangeDataDelayedBy: 0,
  exchangeTimezoneName: 'Europe/Moscow',
  exchangeTimezoneShortName: 'MSK',
  gmtOffSetMilliseconds: number,
  prevName: string,
  nameChangeDate: string,
  shortName: string,
  symbol: TickerType
}
