import { dummyInterestIncome } from '../../../dummy/report-values/report-values.dummy'

export const mockedYahooDomain = {
  getQuarterlyInterestIncome: () => dummyInterestIncome,
}
