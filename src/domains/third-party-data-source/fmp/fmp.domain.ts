import { BadRequestException, HttpService, Injectable, NotFoundException } from '@nestjs/common'
import { HttpException } from '@nestjs/common/exceptions/http.exception'
import { ConfigService } from '@nestjs/config'
import { format } from 'date-fns'
import { lastValueFrom } from 'rxjs'
import { httpExceptionMessages } from '../../../consts/httpExceptionMessages'
import { MarketCapType, TickerType } from '../../../consts/report.enum'
import { isArray, isNullOrUndefined, toUpperCase } from '../../../utils'
import { getAverage } from '../../../utils/number/avarage/calculate-avarage.util'
import { arrayToMap } from '../../../utils/object/array-to-map/array-to-map.util'
import {
  CompanyQuoteResponseDto,
  FmpBalanceSheetStatementResponse,
  FmpCashFlowStatement,
  FmpCompanyKeyMetricsResponse,
  FmpCompanyProfileResponseDto,
  FmpHistoricalBriefPrice,
  FmpHistoricalMarketCap,
  FmpHistoricalPrice,
  FmpHistoricalPriceItem,
  FmpMakeRequest,
  FmpMakeRequestByTicker,
  FmpMarketIndex,
  FmpPeriodRequest,
  FmpRationTTMResponse,
  FmpScreenerResponse,
  IncomeStatementsResponseDto,
  MarketCapitalizationResponseDto,
} from './fmp.interfaces'

@Injectable()
export class FmpDomain {
  constructor(
    private configService: ConfigService,
    private httpService: HttpService,
  ) {
  }

  async getBalanceSheetStatement(
    ticker: TickerType,
    period: FmpPeriodRequest = FmpPeriodRequest.QUARTER,
  ): Promise<FmpBalanceSheetStatementResponse[]> {
    const service = 'balance-sheet-statement'
    return this.makeRequestByTicker<FmpBalanceSheetStatementResponse[]>({ service, ticker, period })
      .catch(this.handleError('getBalanceSheetStatement'))
  }

  async getLastBalanceSheetStatement(
    ticker: TickerType,
    period: FmpPeriodRequest = FmpPeriodRequest.QUARTER,
  ): Promise<FmpBalanceSheetStatementResponse> {
    const statements = await this.getBalanceSheetStatement(ticker, period)
      .catch(this.handleError('getLastBalanceSheetStatement'))
    if (!isArray(statements)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundBalanceSheetByTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements[0] || {}
  }

  async getIncomeStatements(
    ticker: TickerType,
    period: FmpPeriodRequest = FmpPeriodRequest.QUARTER,
  ): Promise<IncomeStatementsResponseDto[]> {
    const service = 'income-statement'
    return this.makeRequestByTicker<IncomeStatementsResponseDto[]>({ service, ticker, period })
      .catch(this.handleError('getIncomeStatements'))
  }

  async getLastIncomeStatements(
    ticker: TickerType,
    period: FmpPeriodRequest = FmpPeriodRequest.QUARTER,
  ): Promise<IncomeStatementsResponseDto> {
    const statements = await this.getIncomeStatements(ticker, period)
      .catch(this.handleError('getLastIncomeStatements'))
    if (!isArray(statements)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundIncomeStatementsTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements[0] || {}
  }

  async getCashFlowStatements(
    ticker: TickerType,
    period: FmpPeriodRequest = FmpPeriodRequest.QUARTER,
  ): Promise<FmpCashFlowStatement[]> {
    const service = 'cash-flow-statement'
    return this.makeRequestByTicker<FmpCashFlowStatement[]>({ service, ticker, period })
      .catch(this.handleError('getCashFlowStatements'))
  }

  async getLastCashFlowStatements(
    ticker: TickerType,
    period: FmpPeriodRequest = FmpPeriodRequest.QUARTER,
  ): Promise<FmpCashFlowStatement> {
    const statements = await this.getCashFlowStatements(ticker, period)
      .catch(this.handleError('getLastCashFlowStatements'))

    if (!isArray(statements)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundCashFlowByTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements[0] || {}
  }

  async getCompanyProfile(ticker: TickerType): Promise<FmpCompanyProfileResponseDto> {
    const service = 'profile'
    const statements = await this.makeRequestByTicker<FmpCompanyProfileResponseDto[]>({ service, ticker })
      .catch(this.handleError('getCompanyProfile'))
    if (!isArray(statements)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundCompanyProfileByTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements[0] || {}
  }

  async findCompanyHistoricalBriefPrice(ticker: TickerType, from: string, to?: string): Promise<FmpHistoricalBriefPrice['historical']> {
    const service = 'historical-price-full'
    const toDate = isNullOrUndefined(to) ? format(new Date(), 'yyyy-MM-dd') : to
    const statements = await this.makeRequestByTicker<FmpHistoricalBriefPrice>({ service, ticker, params: { serietype: 'line', from, to: toDate } })
      .catch(this.handleError('findCompanyHistoricalBriefPrice'))
    if (!statements?.symbol) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundCompanyHistoricalBriefPrice}: ${ticker}. Statements: ${statements}`)
    }
    return statements.historical
  }

  async getRatioTTM(ticker: TickerType): Promise<FmpRationTTMResponse> {
    const service = 'ratios-ttm'
    const statements = await this.makeRequestByTicker<FmpRationTTMResponse[]>({ service, ticker })
      .catch(this.handleError('getRatioTTM'))
    if (!isArray(statements)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundRatioTTMByTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements[0] || {}
  }

  async getCompanyQuote(ticker: TickerType): Promise<CompanyQuoteResponseDto> {
    const service = 'quote'
    const statements = await this.makeRequestByTicker<CompanyQuoteResponseDto[]>({ service, ticker })
      .catch(this.handleError('getCompanyQuote'))

    if (!isArray(statements)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundMarketCapitalizationByTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements[0] || {}
  }

  async getHistoricalPrice(ticker: TickerType, takeFrom?: Date): Promise<FmpHistoricalPriceItem[]> {
    const service = 'historical-price-full'
    const params = {}
    if (takeFrom) {
      params['from'] = format(takeFrom, 'yyyy-MM-dd')
    }

    const statement = await this.makeRequestByTicker<FmpHistoricalPrice>({
        service,
        ticker,
        params,
      })
      .catch(this.handleError('getHistoricalPrice'))

    if (!statement.symbol) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundCompanyHistoricalStockPriceByTicker}: ${ticker}. Statements: ${statement}`)
    }
    return statement.historical
  }

  async getHistoricalMarketCap(ticker: TickerType, limit: number): Promise<FmpHistoricalMarketCap[]> {
    const service = 'historical-market-capitalization'
    const statements = await this.makeRequestByTicker<FmpHistoricalMarketCap[]>({
        service,
        ticker,
        params: {
          limit,
        },
      })
      .catch(this.handleError('getHistoricalMarketCap'))

    if (!(isArray(statements) && statements.length)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundCompanyHistoricalMarketCapByTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements
  }

  async averageMarketCapForLast3YearsOrActual(ticker: TickerType): Promise<MarketCapType> {
    const limit = 800 // approximately returns for last 3 years
    const historicalMarketCap = await this.getHistoricalMarketCap(ticker, limit).catch(e => {
      return []
    })
    const averageMarketCap = getAverage<MarketCapType>(historicalMarketCap.map(it => it.marketCap))
    if (averageMarketCap > 0) {
      return averageMarketCap
    }
    const { marketCap } = await this.getCompanyQuote(ticker)
    return marketCap
  }

  async getManyCompaniesQuoteAsMap(tickers: TickerType[]): Promise<Map<TickerType, CompanyQuoteResponseDto>> {
    const service = 'quote'
    const tickersAsStr = tickers.join(',') as TickerType
    const statements = await this.makeRequestByTicker<CompanyQuoteResponseDto[]>({ service, ticker: tickersAsStr })
      .catch(this.handleError('getManyCompaniesQuoteAsDictionary'))
    return arrayToMap(statements, 'symbol')
  }

  async getMarketCapitalization(ticker: TickerType): Promise<MarketCapitalizationResponseDto> {
    const service = 'market-capitalization'
    const statements = await this.makeRequestByTicker<MarketCapitalizationResponseDto[]>({ service, ticker })
      .catch(this.handleError('getMarketCapitalization'))
    if (!isArray(statements)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundMarketCapitalizationByTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements[0]
  }

  async getKeyMetrics(ticker): Promise<FmpCompanyKeyMetricsResponse> {
    const service = 'key-metrics'
    const statements = await this.makeRequestByTicker<FmpCompanyKeyMetricsResponse[]>({ service, ticker })
      .catch(this.handleError('getKeyMetrics'))
    if (!isArray(statements)) {
      throw new NotFoundException(`${httpExceptionMessages.fmp.notFoundKeyMetricsByTicker}: ${ticker}. Statements: ${statements}`)
    }
    return statements[0] || {}
  }

  async getSP500MarketIndex(): Promise<FmpMarketIndex[]> {
    const service = 'sp500_constituent'
    return this.makeRequest<FmpMarketIndex[]>({ service })
      .catch(this.handleError('getSP500MarketIndex'))
  }

  async getNasdaqMarketIndex(): Promise<FmpMarketIndex[]> {
    const service = 'nasdaq_constituent'
    return this.makeRequest<FmpMarketIndex[]>({ service })
      .catch(this.handleError('getNasdaqMarketIndex'))
  }

  async getDowJonesMarketIndex(): Promise<FmpMarketIndex[]> {
    const service = 'dowjones_constituent'
    return this.makeRequest<FmpMarketIndex[]>({ service })
      .catch(this.handleError('getDowJonesMarketIndex'))
  }

  async getAllCompanies(sector: string): Promise<FmpScreenerResponse[]> {
    // const service = 'stock/list'
    const service = 'stock-screener'
    const params = {
      exchange: 'nyse,nasdaq',
      priceMoreThan: 1,
      isEtf: false,
      isActivelyTrading: true,
      marketCapMoreThan: 1000000,
      sector,
    }
    return this.makeRequest<FmpScreenerResponse[]>({ service, params })
      .catch(this.handleError('getAllCompanies'))
  }

  private handleError = (method: string) => (e: string | HttpException): never => {
    // Remove previous method marker if HttpException instance
    const errorMessage = e instanceof HttpException ? e.message.split(']: ')[1] : e
    throw new BadRequestException(`[FmpDomain ${method}]: ${errorMessage}`)
  }

  private makeRequestByTicker<T>(props: FmpMakeRequestByTicker): Promise<T> {
    return this.makeRequest<T>(props)
  }

  private async makeRequest<T>({ service, ticker, period = FmpPeriodRequest.QUARTER, params = {} }: FmpMakeRequest): Promise<T> {
    const apiPath = this.configService.get<string>('FMP_API_PATH')
    const apiVersion = this.configService.get<string>('FMP_API_VERSION')
    const apiKey = this.configService.get<string>('FMP_API_KEY')

    let requestUrl = `${apiPath}/${apiVersion}/${service}`
    if (ticker) {
      requestUrl = `${requestUrl}/${toUpperCase(ticker)}`
    }

    const response = await this.httpService.get(requestUrl, {
      params: {
        period,
        apikey: apiKey,
        ...params,
      },
    })

    const data = (await lastValueFrom(response)).data

    if (data['Error Message']) {
      throw data['Error Message']
    }
    return data
  }
}
