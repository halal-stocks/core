import { CompanySector } from '../../../consts/company-sector'
import { Exchange } from '../../../consts/exchange.enum'
import {
  AccountPayablesType,
  AccumulatedOtherComprehensiveIncomeLossType,
  BetaType,
  CashAndCashEquivalentsType,
  CashAndShortTermInvestmentsType,
  CommonStockType,
  CompanyReportDateAsStringType,
  CurrentAssetsType,
  DeferredRevenueNonCurrentType,
  DeferredRevenueType,
  DeferredTaxLiabilitiesNonCurrentType,
  DividendYieldPercentageType,
  DividendYieldType,
  EPSType,
  FreeCashFlowType,
  GoodwillAndIntangibleAssetsType,
  GoodwillType,
  IntangibleAssetsType,
  InventoryType,
  LongTermDebtType,
  LongTermInvestmentsType,
  MarketCapType,
  NetDebtType,
  NetProfitMarginType,
  NetReceivablesType,
  OtherAssetsType,
  OtherCurrentAssetsType,
  OtherCurrentLiabilitiesType,
  OtherLiabilitiesType,
  OtherNonCurrentAssetsType,
  OtherNonCurrentLiabilitiesType,
  OthertotalStockholdersEquityType,
  PayoutRatioType,
  PBRatioType,
  PEGRatioType,
  PERatioType,
  PropertyPlantEquipmentNetType,
  PSRatioType,
  RetainedEarningsType,
  RevenueType,
  ROAType,
  ROEType,
  SharesOutstandingType,
  ShortTermDebtType,
  ShortTermInvestmentsType,
  StockPriceType,
  TaxAssetsType,
  TaxPayablesType,
  TickerType,
  TotalAssetsType,
  TotalCurrentLiabilitiesType,
  TotalDebtType,
  TotalInvestmentsType,
  TotalLiabilitiesAndStockholdersEquityType,
  TotalLiabilitiesType,
  TotalNonCurrentAssetsType,
  TotalNonCurrentLiabilitiesType,
  TotalStockholdersEquityType,
} from '../../../consts/report.enum'

export enum FmpPeriodRequest {
  QUARTER = 'quarter',
  ANNUAL = 'annual'
}

export enum ReportPeriodResponse {
  fullYear = 'FY'
}

export interface FmpBalanceSheetStatementResponse {
  acceptedDate: string,
  accountPayables: AccountPayablesType,
  accumulatedOtherComprehensiveIncomeLoss: AccumulatedOtherComprehensiveIncomeLossType,
  date: CompanyReportDateAsStringType,
  symbol: TickerType
  fillingDate: string,
  period: string,
  link: string,
  finalLink: string
  cashAndShortTermInvestments: CashAndShortTermInvestmentsType,
  otherCurrentAssets: OtherCurrentAssetsType,
  longTermInvestments: LongTermInvestmentsType,
  cashAndCashEquivalents: CashAndCashEquivalentsType,
  shortTermInvestments: ShortTermInvestmentsType,
  netReceivables: NetReceivablesType,
  inventory: InventoryType,
  totalCurrentAssets: CurrentAssetsType,
  propertyPlantEquipmentNet: PropertyPlantEquipmentNetType,
  goodwill: GoodwillType,
  intangibleAssets: IntangibleAssetsType,
  goodwillAndIntangibleAssets: GoodwillAndIntangibleAssetsType,
  taxAssets: TaxAssetsType,
  otherNonCurrentAssets: OtherNonCurrentAssetsType,
  totalNonCurrentAssets: TotalNonCurrentAssetsType,
  otherAssets: OtherAssetsType,
  totalAssets: TotalAssetsType,

  // Текущая часть долгосрочных кредитов и займов/Капитализируемая аренда
  shortTermDebt: ShortTermDebtType,
  taxPayables: TaxPayablesType,
  deferredRevenue: DeferredRevenueType,
  // Прочие краткосрочные обязательства, всего
  otherCurrentLiabilities: OtherCurrentLiabilitiesType,
  totalCurrentLiabilities: TotalCurrentLiabilitiesType,
  // Долгосрочные кредиты и займы
  longTermDebt: LongTermDebtType,
  deferredRevenueNonCurrent: DeferredRevenueNonCurrentType,
  deferredTaxLiabilitiesNonCurrent: DeferredTaxLiabilitiesNonCurrentType,
  otherNonCurrentLiabilities: OtherNonCurrentLiabilitiesType,
  totalNonCurrentLiabilities: TotalNonCurrentLiabilitiesType,
  otherLiabilities: OtherLiabilitiesType,
  totalLiabilities: TotalLiabilitiesType,
  commonStock: CommonStockType,
  retainedEarnings: RetainedEarningsType,

  othertotalStockholdersEquity: OthertotalStockholdersEquityType,
  totalStockholdersEquity: TotalStockholdersEquityType,
  totalLiabilitiesAndStockholdersEquity: TotalLiabilitiesAndStockholdersEquityType,
  totalInvestments: TotalInvestmentsType,
  totalDebt: TotalDebtType,
  netDebt: NetDebtType,
}

export interface FmpCompanyProfileResponseDto {
  symbol: TickerType
  price: number,
  beta: BetaType,
  volAvg: number,
  mktCap: MarketCapType,
  lastDiv: number,
  range: string,
  changes: number,
  companyName: string,
  currency: string,
  cik: string,
  isin: string,
  cusip: string,
  exchange: string,
  exchangeShortName: Exchange,
  industry: string,
  website: string,
  description: string,
  ceo: string,
  sector: CompanySector,
  country: string,
  fullTimeEmployees: string,
  phone: string,
  address: string,
  city: string,
  state: string,
  zip: string,
  dcfDiff: number,
  dcf: number,
  image: string,
  ipoDate: string,
  defaultImage: boolean
}

export interface MarketCapitalizationResponseDto {
  symbol: TickerType
  date: string,
  marketCap: MarketCapType
}

export interface CompanyQuoteResponseDto {
  symbol: TickerType
  name: string
  exchange: Exchange,
  earningsAnnouncement: string,
  price: StockPriceType
  changesPercentage: number
  change: number
  dayLow: StockPriceType
  dayHigh: StockPriceType
  yearHigh: number
  yearLow: number
  marketCap: MarketCapType
  priceAvg50: StockPriceType
  priceAvg200: StockPriceType
  volume: number
  avgVolume: number
  open: number
  previousClose: StockPriceType
  eps: EPSType
  pe: PERatioType
  sharesOutstanding: SharesOutstandingType
  timestamp: number
}

export interface IncomeStatementsResponseDto {
  date: string
  symbol: TickerType
  fillingDate: string
  acceptedDate: string
  period: string
  link: string
  finalLink: string
  revenue: RevenueType
  costOfRevenue: number
  grossProfit: number
  grossProfitRatio: number
  researchAndDevelopmentExpenses: number
  generalAndAdministrativeExpenses: number
  sellingAndMarketingExpenses: number
  otherExpenses: number
  operatingExpenses: number
  costAndExpenses: number
  interestExpense: number
  depreciationAndAmortization: number
  ebitda: number
  ebitdaratio: number
  operatingIncome: number
  operatingIncomeRatio: number
  totalOtherIncomeExpensesNet: number
  incomeBeforeTax: number
  incomeBeforeTaxRatio: number
  incomeTaxExpense: number
  netIncome: number
  netIncomeRatio: number
  eps: number
  epsdiluted: number
  weightedAverageShsOut: number
  weightedAverageShsOutDil: number
}

export interface FmpCompanyKeyMetricsResponse {
  revenuePerShareTTM: number
  netIncomePerShareTTM: number
  operatingCashFlowPerShareTTM: number
  freeCashFlowPerShareTTM: number
  cashPerShareTTM: number
  bookValuePerShareTTM: number
  tangibleBookValuePerShareTTM: number
  shareholdersEquityPerShareTTM: number
  interestDebtPerShareTTM: number
  marketCapTTM: number
  enterpriseValueTTM: number
  peRatioTTM: PERatioType
  priceToSalesRatioTTM: PSRatioType
  pocfratioTTM: number
  pfcfRatioTTM: number
  pbRatioTTM: PBRatioType
  ptbRatioTTM: number
  evToSalesTTM: number
  enterpriseValueOverEBITDATTM: number
  evToOperatingCashFlowTTM: number
  evToFreeCashFlowTTM: number
  earningsYieldTTM: number
  freeCashFlowYieldTTM: number
  debtToEquityTTM: number
  debtToAssetsTTM: number
  netDebtToEBITDATTM: number
  currentRatioTTM: number
  interestCoverageTTM: number
  incomeQualityTTM: number
  dividendYieldTTM: DividendYieldType
  dividendYieldPercentageTTM: DividendYieldPercentageType
  payoutRatioTTM: PayoutRatioType
  researchAndDevelopementToRevenueTTM: number
  intangiblesToTotalAssetsTTM: number
  capexToOperatingCashFlowTTM: number
  capexToRevenueTTM: number
  capexToDepreciationTTM: number
  stockBasedCompensationToRevenueTTM: number
  grahamNumberTTM: number
  roicTTM: number
  returnOnTangibleAssetsTTM: number
  grahamNetNetTTM: number
  workingCapitalTTM: number
  netCurrentAssetValueTTM: number
  averageReceivablesTTM: number
  averagePayablesTTM: number
  averageInventoryTTM: number
  daysSalesOutstandingTTM: number
  daysPayablesOutstandingTTM: number
  daysOfInventoryOnHandTTM: number
  receivablesTurnoverTTM: number
  payablesTurnoverTTM: number
  inventoryTurnoverTTM: number
  roeTTM: ROEType
  capexPerShareTTM: number
  salesGeneralAndAdministrativeToRevenueTTM: null
  tangibleAssetValueTTM: null
  investedCapitalTTM: null
}

export interface FmpCashFlowStatement {
  date: Date
  symbol: TickerType
  fillingDate: Date
  acceptedDate: Date
  period: ReportPeriodResponse
  link: string
  finalLink: string
  netIncome: number
  depreciationAndAmortization: number
  deferredIncomeTax: number
  stockBasedCompensation: number
  changeInWorkingCapital: number
  accountsReceivables: number
  inventory: number
  accountsPayables: number
  otherWorkingCapital: number
  otherNonCashItems: number
  netCashProvidedByOperatingActivities: number
  investmentsInPropertyPlantAndEquipment: number
  acquisitionsNet: number
  purchasesOfInvestments: number
  salesMaturitiesOfInvestments: number
  otherInvestingActivites: number
  netCashUsedForInvestingActivites: number
  debtRepayment: number
  commonStockIssued: number
  commonStockRepurchased: number
  dividendsPaid: number
  otherFinancingActivites: number
  netCashUsedProvidedByFinancingActivities: number
  effectOfForexChangesOnCash: number
  netChangeInCash: number
  cashAtEndOfPeriod: number
  cashAtBeginningOfPeriod: number
  operatingCashFlow: number
  capitalExpenditure: number
  freeCashFlow: FreeCashFlowType
}

export interface FmpRationTTMResponse {
  dividendYielTTM: number
  dividendYielPercentageTTM: number
  peRatioTTM: PERatioType
  pegRatioTTM: PEGRatioType
  payoutRatioTTM: number
  currentRatioTTM: number
  quickRatioTTM: number
  cashRatioTTM: number
  daysOfSalesOutstandingTTM: number
  daysOfInventoryOutstandingTTM: number
  operatingCycleTTM: number
  daysOfPayablesOutstandingTTM: number
  cashConversionCycleTTM: number
  grossProfitMarginTTM: number
  operatingProfitMarginTTM: number
  pretaxProfitMarginTTM: number
  netProfitMarginTTM: NetProfitMarginType
  effectiveTaxRateTTM: number
  returnOnAssetsTTM: ROAType
  returnOnEquityTTM: ROEType
  returnOnCapitalEmployedTTM: number
  netIncomePerEBTTTM: number
  ebtPerEbitTTM: number
  ebitPerRevenueTTM: number
  debtRatioTTM: number
  debtEquityRatioTTM: number
  longTermDebtToCapitalizationTTM: number
  totalDebtToCapitalizationTTM: number
  interestCoverageTTM: number
  cashFlowToDebtRatioTTM: number
  companyEquityMultiplierTTM: number
  receivablesTurnoverTTM: number
  payablesTurnoverTTM: number
  inventoryTurnoverTTM: number
  fixedAssetTurnoverTTM: number
  assetTurnoverTTM: number
  operatingCashFlowPerShareTTM: number
  freeCashFlowPerShareTTM: number
  cashPerShareTTM: number
  operatingCashFlowSalesRatioTTM: number
  freeCashFlowOperatingCashFlowRatioTTM: number
  cashFlowCoverageRatiosTTM: number
  shortTermCoverageRatiosTTM: number
  capitalExpenditureCoverageRatioTTM: number
  dividendPaidAndCapexCoverageRatioTTM: number
  priceBookValueRatioTTM: number
  priceToBookRatioTTM: number
  priceToSalesRatioTTM: number
  priceEarningsRatioTTM: number
  priceToFreeCashFlowsRatioTTM: number
  priceToOperatingCashFlowsRatioTTM: number
  priceCashFlowRatioTTM: number
  priceEarningsToGrowthRatioTTM: number
  priceSalesRatioTTM: number
  dividendYieldTTM: number
  enterpriseValueMultipleTTM: number
  priceFairValueTTM: number
}

export interface FmpMarketIndex {
  symbol: TickerType
  name: string
  sector: string
  subSector: string
  headQuarter: string
  dateFirstAdded: string
  cik: string
  founded: string
}

export interface FmpHistoricalPriceItem {
  date: string
  open: StockPriceType
  high: StockPriceType
  low: StockPriceType
  close: StockPriceType
  volume: number
  unadjustedVolume: number
  change: number
  changePercent: number
  vwap: number
  label: string
  changeOverTime: number
}

export interface FmpHistoricalPrice {
  symbol: TickerType,
  historical: FmpHistoricalPriceItem[]
}

export interface FmpMakeRequest {
  service: string,
  ticker?: TickerType,
  period?: FmpPeriodRequest
  params?: Record<string, string | number | boolean>
}

export interface FmpMakeRequestByTicker extends Omit<FmpMakeRequest, 'ticker'> {
  ticker: TickerType,
}

export interface FmpHistoricalMarketCap {
  symbol: TickerType
  date: string
  marketCap: MarketCapType
}

export interface FmpHistoricalBriefPrice {
  symbol: TickerType,
  historical: {
    date: string,
    close: number
  }[]
}

export interface FmpGetAllCompaniesResponse {
  symbol: TickerType,
  name: string,
  price: StockPriceType,
  exchange: Exchange
}

export interface FmpScreenerResponse {
  symbol: TickerType,
  companyName: string,
  marketCap: MarketCapType,
  sector: CompanySector,
  industry: string,
  beta: BetaType,
  price: StockPriceType,
  lastAnnualDividend: DividendYieldType,
  volume: number,
  exchange: string,
  exchangeShortName: Exchange,
  country: string,
  isEtf: boolean,
  isActivelyTrading: boolean
}
