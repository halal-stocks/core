import {
  dummyCashAndShortTermInvestments,
  dummyLongTermDebt,
  dummyLongTermInvestments,
  dummyMarketCap,
  dummyOtherCurrentAssets,
  dummyOtherCurrentLiabilities,
  dummyReportDate,
  dummySharesOutstanding,
  dummyShortTermDebt,
  dummyTotalRevenue,
} from '../../../dummy/report-values/report-values.dummy'

export const mockedBalanceStatement = {
  date: dummyReportDate,
  otherCurrentAssets: dummyOtherCurrentAssets,
  cashAndShortTermInvestments: dummyCashAndShortTermInvestments,
  longTermInvestments: dummyLongTermInvestments,
  shortTermDebt: dummyShortTermDebt,
  otherCurrentLiabilities: dummyOtherCurrentLiabilities,
  longTermDebt: dummyLongTermDebt,
}

export const mockedCompanyQuote = {
  marketCap: dummyMarketCap,
  sharesOutstanding: dummySharesOutstanding,
}

export const mockedFmpDomain = {
  getBalanceSheetStatement: () => [mockedBalanceStatement],
  getLastBalanceSheetStatement: async () => mockedBalanceStatement,
  getIncomeStatements: () => {},
  getLastIncomeStatements: async () => ({ revenue: dummyTotalRevenue }),
  getGeneralInfo: () => {},
  getCompanyQuote: () => mockedCompanyQuote,
  getLastCompanyQuote: () => mockedCompanyQuote,
  getMarketCapitalization: () => {},
  getKeyMetrics: () => {},
}
