import { HttpService, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { lastValueFrom } from 'rxjs'
import { TickerType } from '../../../consts/report.enum'
import {
  AlphavantageIncomeStatementsApiResponse,
  AlphavantageIncomeStatementsItemResponse,
  AlphavantageIncomeStatementsResponse,
} from './alphavantage.interfaces'

@Injectable()
export class AlphavantageDomain {
  constructor(
    private configService: ConfigService,
    private httpService: HttpService,
  ) {
  }

  async getBalanceSheetStatements(ticker: TickerType): Promise<any> {
    const service = 'BALANCE_SHEET'
    return this.makeRequest<AlphavantageIncomeStatementsApiResponse>(service, ticker)
  }

  async getLastQuarterlyIncomeStatement(ticker: TickerType): Promise<AlphavantageIncomeStatementsItemResponse> {
    const statement = await this.getIncomeStatement(ticker)
    if (!statement) {
      throw new NotFoundException(
        `httpExceptionMessages.company.notFoundByTicker : ${ticker}`,
      )
    }
    if (!statement.quarterlyReports) {
      console.log(statement)
    }
    return statement.quarterlyReports[0]
  }

  async getIncomeStatement(ticker: TickerType): Promise<AlphavantageIncomeStatementsResponse> {
    const service = 'INCOME_STATEMENT'
    const result = await this.makeRequest<AlphavantageIncomeStatementsApiResponse>(service, ticker)
    return AlphavantageIncomeStatementsResponse.of(result)
  }

  private async makeRequest<T>(
    service: string,
    ticker: TickerType,
  ): Promise<T> {
    const apiPath = this.configService.get<string>('ALPHAVANTAGE_API_PATH')
    const apiKey = this.configService.get<string>('ALPHAVANTAGE_API_KEY')
    if (!(apiKey && apiPath)) {
      throw new InternalServerErrorException(
        'Credential to alphavantage are not provided',
      )
    }

    const response = await this.httpService.get(apiPath, {
      params: {
        apikey: apiKey,
        function: service,
        symbol: ticker,
      },
    })

    return (await lastValueFrom(response)).data
  }

}
