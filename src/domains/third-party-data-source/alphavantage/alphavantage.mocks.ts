import { dummyInterestIncome, dummyTotalRevenue } from '../../../dummy/report-values/report-values.dummy'

export const mockedQuarterlyIncomeStatement = {
  interestIncome: dummyInterestIncome,
  totalRevenue: dummyTotalRevenue,
}

export const mockedAlphavantageDomain = {
  getBalanceSheetStatements: () => {},
  getLastQuarterlyIncomeStatement: () => mockedQuarterlyIncomeStatement,
  getIncomeStatement: () => {},
}
