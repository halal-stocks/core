/**
 * Response from Alphavantage API
 */
import { Currency } from '../../../consts/currency.enum'
import { InterestExpenseType, InterestIncomeType, RevenueType } from '../../../consts/report.enum'
import { isArray } from '../../../utils'
import stringToNumber from '../../../utils/number/string-to-number/string-to-number.util'

/**
 * Report response from Alphavantage API
 */
export interface AlphavantageIncomeStatementsApiResponse {
  symbol: string
  annualReports: AlphavantageIncomeStatementsApiItemResponse[]
  quarterlyReports: AlphavantageIncomeStatementsApiItemResponse[]
}

/**
 * Report item from Alphavantage API
 */
export interface AlphavantageIncomeStatementsApiItemResponse {
  fiscalDateEnding: string
  reportedCurrency: Currency
  totalRevenue: string
  totalOperatingExpense: string
  costOfRevenue: string
  grossProfit: string
  ebit: string
  netIncome: string
  researchAndDevelopment: string
  effectOfAccountingCharges: string
  incomeBeforeTax: string
  minorityInterest: string
  sellingGeneralAdministrative: string
  otherNonOperatingIncome: string
  operatingIncome: string
  otherOperatingExpense: string
  interestExpense: string
  taxProvision: string
  interestIncome: string
  netInterestIncome: string
  extraordinaryItems: string
  nonRecurring: string
  otherItems: string
  incomeTaxExpense: string
  totalOtherIncomeExpense: string
  discontinuedOperations: string
  netIncomeFromContinuingOperations: string
  netIncomeApplicableToCommonShares: string
  preferredStockAndOtherAdjustments: string
}

/**
 * Normalized report
 */
export class AlphavantageIncomeStatementsResponse {
  symbol: string
  annualReports: AlphavantageIncomeStatementsItemResponse[]
  quarterlyReports: AlphavantageIncomeStatementsItemResponse[]

  constructor(dto: AlphavantageIncomeStatementsApiResponse) {
    this.symbol = dto.symbol
    this.annualReports = AlphavantageIncomeStatementsItemResponse.of(dto.annualReports)
    this.quarterlyReports = AlphavantageIncomeStatementsItemResponse.of(dto.quarterlyReports)
  }


  static of(dto: AlphavantageIncomeStatementsApiResponse): AlphavantageIncomeStatementsResponse
  static of(dto: AlphavantageIncomeStatementsApiResponse[]): AlphavantageIncomeStatementsResponse[]
  static of(
    model: AlphavantageIncomeStatementsApiResponse | AlphavantageIncomeStatementsApiResponse[],
  ): AlphavantageIncomeStatementsResponse | AlphavantageIncomeStatementsResponse[] {
    return isArray(model)
      ? model.map((it) => it && new AlphavantageIncomeStatementsResponse(it))
      : model && new AlphavantageIncomeStatementsResponse(model)
  }
}

/**
 * Normalize report item
 */
export class AlphavantageIncomeStatementsItemResponse {
  fiscalDateEnding: string
  reportedCurrency: Currency
  effectOfAccountingCharges: string
  nonRecurring: string
  otherItems: string
  preferredStockAndOtherAdjustments: string
  totalRevenue: RevenueType
  totalOperatingExpense: number
  costOfRevenue: number
  grossProfit: number
  ebit: number
  netIncome: number
  researchAndDevelopment: number
  incomeBeforeTax: number
  sellingGeneralAdministrative: number
  otherNonOperatingIncome: number
  operatingIncome: number
  interestExpense: InterestExpenseType
  taxProvision: number
  interestIncome: InterestIncomeType
  netInterestIncome: number
  incomeTaxExpense: number
  totalOtherIncomeExpense: number
  netIncomeFromContinuingOperations: number
  netIncomeApplicableToCommonShares: number
  minorityInterest: number | null
  otherOperatingExpense: number | null
  discontinuedOperations: number | null
  extraordinaryItems: number | null

  constructor(dto: AlphavantageIncomeStatementsApiItemResponse) {
    this.fiscalDateEnding = dto.fiscalDateEnding
    this.reportedCurrency = dto.reportedCurrency
    this.effectOfAccountingCharges = dto.effectOfAccountingCharges
    this.nonRecurring = dto.nonRecurring
    this.otherItems = dto.otherItems
    this.preferredStockAndOtherAdjustments = dto.preferredStockAndOtherAdjustments
    this.totalRevenue = stringToNumber(dto.totalRevenue)
    this.totalOperatingExpense = stringToNumber(dto.totalOperatingExpense)
    this.costOfRevenue = stringToNumber(dto.costOfRevenue)
    this.grossProfit = stringToNumber(dto.grossProfit)
    this.ebit = stringToNumber(dto.ebit)
    this.netIncome = stringToNumber(dto.netIncome)
    this.researchAndDevelopment = stringToNumber(dto.researchAndDevelopment)
    this.incomeBeforeTax = stringToNumber(dto.incomeBeforeTax)
    this.minorityInterest = stringToNumber(dto.minorityInterest)
    this.sellingGeneralAdministrative = stringToNumber(dto.sellingGeneralAdministrative)
    this.otherNonOperatingIncome = stringToNumber(dto.otherNonOperatingIncome)
    this.operatingIncome = stringToNumber(dto.operatingIncome)
    this.otherOperatingExpense = stringToNumber(dto.otherOperatingExpense)
    this.interestExpense = stringToNumber(dto.interestExpense)
    this.taxProvision = stringToNumber(dto.taxProvision)
    this.interestIncome = stringToNumber(dto.interestIncome)
    this.netInterestIncome = stringToNumber(dto.netInterestIncome)
    this.extraordinaryItems = stringToNumber(dto.extraordinaryItems)
    this.incomeTaxExpense = stringToNumber(dto.incomeTaxExpense)
    this.totalOtherIncomeExpense = stringToNumber(dto.totalOtherIncomeExpense)
    this.discontinuedOperations = stringToNumber(dto.discontinuedOperations)
    this.netIncomeFromContinuingOperations = stringToNumber(dto.netIncomeFromContinuingOperations)
    this.netIncomeApplicableToCommonShares = stringToNumber(dto.netIncomeApplicableToCommonShares)
  }

  static of(dto: AlphavantageIncomeStatementsApiItemResponse): AlphavantageIncomeStatementsItemResponse
  static of(dto: AlphavantageIncomeStatementsApiItemResponse[]): AlphavantageIncomeStatementsItemResponse[]
  static of(
    dto: AlphavantageIncomeStatementsApiItemResponse | AlphavantageIncomeStatementsApiItemResponse[],
  ): AlphavantageIncomeStatementsItemResponse | AlphavantageIncomeStatementsItemResponse[] {
    return isArray(dto)
      ? dto.map((it) => it && new AlphavantageIncomeStatementsItemResponse(it))
      : dto && new AlphavantageIncomeStatementsItemResponse(dto)
  }

}
