import { HttpModule, Module } from '@nestjs/common'
import { AlphavantageDomain } from './alphavantage/alphavantage.domain'
import { DatahubDomain } from './datahub/datahub.domain'
import { FmpDomain } from './fmp/fmp.domain'
import { IexcloudDomain } from './iexcloud/iexcloud.domain'
import { SeekingalphaDomain } from './seekingalpha/seekingalpha.domain'
import { YahooDomain } from './yahoo/yahoo.domain'

@Module({
  imports: [
    HttpModule,
  ],
  providers: [
    FmpDomain,
    AlphavantageDomain,
    DatahubDomain,
    YahooDomain,
    IexcloudDomain,
    SeekingalphaDomain,
  ],
  exports: [
    FmpDomain,
    AlphavantageDomain,
    ThirdPartyDataSourceDomainModule,
    DatahubDomain,
    YahooDomain,
    IexcloudDomain,
    SeekingalphaDomain,
  ],
})
export class ThirdPartyDataSourceDomainModule {}
