import { HttpService, Injectable, InternalServerErrorException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { lastValueFrom } from 'rxjs'
import { httpExceptionMessages } from '../../../consts/httpExceptionMessages'
import { DatahubSP500Response } from './datahub.interfaces'

@Injectable()
export class DatahubDomain {
  constructor(
    private configService: ConfigService,
    private httpService: HttpService,
  ) {}

  getSP500(): Promise<DatahubSP500Response[]> {
    const url = this.configService.get<string>('DATAHUB_SP500_API_PATH')
    if (!url) {
      throw new InternalServerErrorException(httpExceptionMessages.datahub.sp500UrlNotProvided)
    }
    return this.makeRequest<DatahubSP500Response[]>(url)
  }

  private async makeRequest<T>(
    url: string,
  ): Promise<T> {

    const response = await this.httpService.get(url)
    return (await lastValueFrom(response)).data
  }
}
