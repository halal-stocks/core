import { TickerType } from '../../../consts/report.enum'

export interface DatahubSP500Response {
  Name: string
  Sector: string
  Symbol: TickerType
}
