import { Currency } from '../../../consts/currency.enum'
import {
  CommonStockType,
  CompanyReportDateAsStringType,
  CurrentAssetsType,
  CurrentCash,
  LongTermDebtType,
  LongTermInvestmentsType,
  OtherCurrentAssetsType,
  OtherCurrentLiabilitiesType,
  RevenueType,
  SharesOutstandingType,
  TickerType,
} from '../../../consts/report.enum'

export enum IexcloudPeriodRequest {
  QUARTER = 'quarterly',
  ANNUAL = 'annual'
}


export interface IexcloudBalanceSheetResponse {
  capitalSurplus: null
  commonStock: CommonStockType
  currentAssets: CurrentAssetsType
  currentCash: CurrentCash
  currentLongTermDebt: LongTermDebtType
  accountsPayable: number
  fiscalQuarter: number
  fiscalYear: number
  goodwill: number
  intangibleAssets: number
  inventory: number
  longTermDebt: LongTermDebtType
  longTermInvestments: LongTermInvestmentsType
  minorityInterest: number
  netTangibleAssets: number
  otherAssets: number
  otherCurrentAssets: OtherCurrentAssetsType
  otherCurrentLiabilities: OtherCurrentLiabilitiesType
  otherLiabilities: number
  propertyPlantEquipment: number
  receivables: number
  retainedEarnings: number
  shareholderEquity: number
  shortTermInvestments: number
  totalAssets: number
  totalCurrentLiabilities: number
  totalLiabilities: number
  treasuryStock: number
  date: number
  updated: number
  currency: Currency
  filingType: string
  fiscalDate: string
  reportDate: CompanyReportDateAsStringType
  symbol: TickerType
  id: string
  key: TickerType
  subkey: string
}

export interface IexcloudBalanceSheetApiResponse extends IexcloudCommonResponse {
  balancesheet: IexcloudBalanceSheetResponse[]
}

export interface IexcloudCommonResponse {
  symbol: TickerType
}

export interface IexcloudRequest {
  service: string
  params?: Record<string, string | number>
}


export interface IexcloudFundamentalsResponse {
  accountsPayable: number
  accountsPayableTurnover: number
  accountsReceivable: number
  accountsReceivableTurnover: number
  assetsCurrentCash: number
  assetsCurrentCashRestricted: number
  assetsCurrentDeferredCompensation: number
  assetsCurrentDeferredTax: number
  assetsCurrentDiscontinuedOperations: number
  assetsCurrentInvestments: number
  assetsCurrentLeasesOperating: number
  assetsCurrentLoansNet: number
  assetsCurrentOther: number
  assetsCurrentSeparateAccounts: number
  assetsCurrentUnadjusted: number
  assetsFixed: number
  assetsFixedDeferredCompensation: number
  assetsFixedDeferredTax: number
  assetsFixedDiscontinuedOperations: number
  assetsFixedLeasesOperating: number
  assetsFixedOperatingDiscontinuedOperations: number
  assetsFixedOperatingSubsidiaryUnconsolidated: number
  assetsFixedOreo: number
  assetsFixedOther: number
  assetsFixedUnconsolidated: number
  assetsUnadjusted: number
  capex: number
  capexAcquisition: number
  capexMaintenance: number
  cashConversionCycle: number
  cashFlowFinancing: number
  cashFlowInvesting: number
  cashFlowOperating: number
  cashFlowShareRepurchase: number
  cashLongTerm: number
  cashOperating: number
  cashPaidForIncomeTaxes: number
  cashPaidForInterest: number
  cashRestricted: number
  chargeAfterTax: number
  chargeAfterTaxDiscontinuedOperations: number
  chargesAfterTaxOther: number
  creditLossProvision: number
  daysInAccountsPayable: number
  daysInInventory: number
  daysInRevenueDeferred: number
  daysRevenueOutstanding: number
  debtFinancial: number
  debtShortTerm: number
  depreciationAndAmortizationAccumulated: number
  depreciationAndAmortizationCashFlow: number
  dividendsPreferred: number
  dividendsPreferredRedeemableMandatorily: number
  earningsRetained: number
  ebitReported: number
  ebitdaReported: number
  equityShareholder: number
  equityShareholderOther: number
  equityShareholderOtherDeferredCompensation: number
  equityShareholderOtherEquity: number
  equityShareholderOtherMezzanine: number
  expenses: number
  expensesAcquisitionMerger: number
  expensesCompensation: number
  expensesDepreciationAndAmortization: number
  expensesDerivative: number
  expensesDiscontinuedOperations: number
  expensesDiscontinuedOperationsReits: number
  expensesEnergy: number
  expensesForeignCurrency: number
  expensesInterest: number
  expensesInterestFinancials: number
  expensesInterestMinority: number
  expensesLegalRegulatoryInsurance: number
  expensesNonOperatingCompanyDefinedOther: number
  expensesNonOperatingOther: number
  expensesNonOperatingSubsidiaryUnconsolidated: number
  expensesNonRecurringOther: number
  expensesOperating: number
  expensesOperatingOther: number
  expensesOperatingSubsidiaryUnconsolidated: number
  expensesOreo: number
  expensesOreoReits: number
  expensesOtherFinancing: number
  expensesRestructuring: number
  expensesSga: number
  expensesStockCompensation: number
  expensesWriteDown: number
  ffo: number
  fiscalQuarter: number
  fiscalYear: number
  goodwillAmortizationCashFlow: number
  goodwillAmortizationIncomeStatement: number
  goodwillAndIntangiblesNetOther: number
  goodwillNet: number
  incomeFromOperations: number
  incomeNet: number
  incomeNetPerRevenue: number
  incomeNetPerWabso: number
  incomeNetPerWabsoSplitAdjusted: number
  incomeNetPerWabsoSplitAdjustedYoyDeltaPercent: number
  incomeNetPerWadso: number
  incomeNetPerWadsoSplitAdjusted: number
  incomeNetPerWadsoSplitAdjustedYoyDeltaPercent: number
  incomeNetPreTax: number
  incomeNetYoyDelta: number
  incomeOperating: number
  incomeOperatingDiscontinuedOperations: number
  incomeOperatingOther: number
  incomeOperatingSubsidiaryUnconsolidated: number
  incomeOperatingSubsidiaryUnconsolidatedAfterTax: number
  incomeTax: number
  incomeTaxCurrent: number
  incomeTaxDeferred: number
  incomeTaxDiscontinuedOperations: number
  incomeTaxOther: number
  incomeTaxRate: number
  interestMinority: number
  inventory: number
  inventoryTurnover: number
  liabilities: number
  liabilitiesCurrent: number
  liabilitiesNonCurrentAndInterestMinorityTotal: number
  liabilitiesNonCurrentDebt: number
  liabilitiesNonCurrentDeferredCompensation: number
  liabilitiesNonCurrentDeferredTax: number
  liabilitiesNonCurrentDiscontinuedOperations: number
  liabilitiesNonCurrentLeasesOperating: number
  liabilitiesNonCurrentLongTerm: number
  liabilitiesNonCurrentOperatingDiscontinuedOperations: number
  liabilitiesNonCurrentOther: number
  nibclDeferredCompensation: number
  nibclDeferredTax: number
  nibclDiscontinuedOperations: number
  nibclLeasesOperating: number
  nibclOther: number
  nibclRestructuring: number
  nibclRevenueDeferred: number
  nibclRevenueDeferredTurnover: number
  nibclSeparateAccounts: number
  oci: number
  ppAndENet: number
  pricePerEarnings: number
  pricePerEarningsPerRevenueYoyDeltaPercent: number
  profitGross: number
  profitGrossPerRevenue: number
  researchAndDevelopmentExpense: number
  reserves: number
  reservesInventory: number
  reservesLifo: number
  reservesLoanLoss: number
  revenue: RevenueType
  revenueCostOther: number
  revenueIncomeInterest: number
  revenueOther: number
  revenueSubsidiaryUnconsolidated: number
  salesCost: number
  sharesIssued: SharesOutstandingType
  sharesOutstandingPeDateBs: number
  sharesTreasury: number
  stockCommon: number
  stockPreferred: number
  stockPreferredEquity: number
  stockPreferredMezzanine: number
  stockTreasury: number
  wabso: number
  wabsoSplitAdjusted: number
  wadso: number
  wadsoSplitAdjusted: number
  date: number
  updated: number
  asOfDate: string
  cik: string
  dataGenerationDate: string
  figi: string
  filingDate: string
  filingType: string
  periodEndDate: string
  reportDate: CompanyReportDateAsStringType
  symbol: TickerType
  id: string
  key: TickerType
  subkey: string
}
