import { HttpService, Injectable, NotFoundException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { lastValueFrom } from 'rxjs'
import { httpExceptionMessages } from '../../../consts/httpExceptionMessages'
import { TickerType } from '../../../consts/report.enum'
import {
  IexcloudBalanceSheetApiResponse,
  IexcloudBalanceSheetResponse,
  IexcloudFundamentalsResponse,
  IexcloudPeriodRequest,
  IexcloudRequest,
} from './iexcloud.interfaces'

@Injectable()
export class IexcloudDomain {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  async getLastBalanceSheetStatement(ticker: TickerType): Promise<IexcloudBalanceSheetResponse> {
    const service = `stock/${ticker}/balance-sheet`
    const params = {
      period: IexcloudPeriodRequest.QUARTER,
    }

    const response = await this.makeRequest<IexcloudBalanceSheetApiResponse>({
      service,
      params,
    })

    return response.balancesheet[0]
  }

  async getCompanyFundamentals(ticker): Promise<IexcloudFundamentalsResponse> {
    const service = `time-series/fundamentals/${ticker}/${IexcloudPeriodRequest.QUARTER}`

    const response = await this.makeRequest<IexcloudFundamentalsResponse[]>({
      service,
    })

    if (!response && response[0]) {
      throw new NotFoundException(`${httpExceptionMessages.iexcloud.notFoundCompanyFundamental}: ${ticker}. Statements: getCompanyFundamentals`)
    }

    return response[0]
  }

  private async makeRequest<T>({ service, params = {} }: IexcloudRequest): Promise<T> {
    const apiPath = this.configService.get<string>('IEXCLOUD_API_URL')
    const apiVersion = this.configService.get<string>('IEXCLOUD_API_VERSION')
    const apiKey = this.configService.get<string>('IEXCLOUD_API_TOKEN')

    let requestUrl = `${apiPath}/${apiVersion}/${service}`

    const response = await this.httpService.get(requestUrl, {
      params: {
        ...params,
        token: apiKey,
      },
    })

    const data = (await lastValueFrom(response)).data

    if (data['Error Message']) {
      throw data['Error Message']
    }
    return data
  }

}
