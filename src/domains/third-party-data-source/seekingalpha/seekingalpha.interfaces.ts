import { InterestIncomeType, RevenueType, TickerType } from '../../../consts/report.enum'

export interface SeekingalphaMakeRequest {
  ticker: TickerType
  params?: Record<string, number | string | boolean>
  service: string
}


export interface SeekingAlphaValueHeaderResponse {
  name: string
  value: SeekingAlphaHeaderValue
  sectionGroup: string
  class: string
  headerClass: string
  chartId: string
  itemId: string
}

export interface SeekingAlphaValueResponse {
  name: string
  value: string
  raw_value: string
  format: string
  yoy_value: string
  class: string
  revenue_percent: string
}

export type SeekingAlphaItemResponse = [header: SeekingAlphaValueHeaderResponse, ...value: SeekingAlphaValueResponse[]]

export enum SeekingAlphaHeaderValue {
  revenues = 'Revenues',
  otherRevenues = 'Other Revenues',
  totalRevenues = 'Total Revenues',
  'Cost Of Revenues' = 'Cost Of Revenues',
  'Gross Profit' = 'Gross Profit',
  'Selling General & Admin Expenses' = 'Selling General & Admin Expenses',
  'R&D Expenses' = 'R&D Expenses',
  'Total Operating Expenses' = 'Total Operating Expenses',
  'Operating Income' = 'Operating Income',
  'Interest Expense' = 'Interest Expense',
  interestAndInvestmentIncome = 'Interest And Investment Income',
  'Net Interest Expenses' = 'Net Interest Expenses',
  'Currency Exchange Gains (Loss)' = 'Currency Exchange Gains (Loss)',
  'Other Non Operating Income (Expenses)' = 'Other Non Operating Income (Expenses)',
  'EBT, Excl. Unusual Items' = 'EBT, Excl. Unusual Items',
  'EBT, Incl. Unusual Items' = 'EBT, Incl. Unusual Items',
  'Income Tax Expense' = 'Income Tax Expense',
  'Earnings From Continuing Operations' = 'Earnings From Continuing Operations',
  'Net Income to Company' = 'Net Income to Company',
  'Net Income' = 'Net Income',
  'NI to Common Incl Extra Items' = 'NI to Common Incl Extra Items',
  'NI to Common Excl. Extra Items' = 'NI to Common Excl. Extra Items',
  'Revenue Per Share' = 'Revenue Per Share',
  'Basic EPS' = 'Basic EPS',
  'Basic EPS - Continuing Ops' = 'Basic EPS - Continuing Ops',
  'Basic Weighted Average Shares Outst.' = 'Basic Weighted Average Shares Outst.',
  'Diluted EPS' = 'Diluted EPS',
  'Diluted EPS - Continuing Ops' = 'Diluted EPS - Continuing Ops',
  'Diluted Weighted Average Shares Outst.' = 'Diluted Weighted Average Shares Outst.',
  'Normalized Basic EPS' = 'Normalized Basic EPS',
  'Normalized Diluted EPS' = 'Normalized Diluted EPS',
  'Dividend Per Share' = 'Dividend Per Share',
  'Payout Ratio' = 'Payout Ratio',
  'EBITDA' = 'EBITDA',
  'EBITA' = 'EBITA',
  'EBIT' = 'EBIT',
  'Effective Tax Rate' = 'Effective Tax Rate',
  'Normalized Net Income' = 'Normalized Net Income',
  'Interest on Long-Term Debt' = 'Interest on Long-Term Debt',
  'R&D Expense From Footnotes' = 'R&D Expense From Footnotes',
  'Foreign Earnings' = 'Foreign Earnings',
  'Foreign Sales' = 'Foreign Sales',
  'Total Shares Purchased - Quarter' = 'Total Shares Purchased - Quarter',
  'Average Price paid per Share' = 'Average Price paid per Share',
}

type SeekingAlphaQuarterlyFinancialsForLastMonthGenericResponse = { [key in keyof typeof SeekingAlphaHeaderValue]: number }

export interface SeekingAlphaQuarterlyFinancialsForLastMonthResponse extends SeekingAlphaQuarterlyFinancialsForLastMonthGenericResponse {
  revenues: RevenueType
  interestAndInvestmentIncome: InterestIncomeType
}
