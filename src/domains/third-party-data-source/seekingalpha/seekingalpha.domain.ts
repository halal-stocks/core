import { BadRequestException, HttpService, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { CheerioAPI } from 'cheerio'
import { lastValueFrom } from 'rxjs'
import * as UserAgent from 'user-agents'
import { InterestIncomeType, RevenueType, TickerType } from '../../../consts/report.enum'
import { toUpperCase } from '../../../utils'
import stringToNumber from '../../../utils/number/string-to-number/string-to-number.util'
import { reversEnumUtil } from '../../../utils/object/revers-enum/revers-enum.util'
import { getBrowser, getPageContent } from '../../../utils/site-scraping/site-scraping.util'
import { generateUrl } from '../../../utils/url/url.util'
import {
  SeekingAlphaHeaderValue,
  SeekingAlphaItemResponse,
  SeekingalphaMakeRequest,
  SeekingAlphaQuarterlyFinancialsForLastMonthResponse,
} from './seekingalpha.interfaces'

@Injectable()
export class SeekingalphaDomain {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  async getQuarterlyFinancials(ticker: TickerType): Promise<SeekingAlphaItemResponse[][]> {
    const params = {
      period_type: 'quarterly',
      statement_type: 'income-statement',
      order_type: 'latest_left',
      is_pro: false,
    }

    const service = 'financials-data'

    const response = await this.makeRequest<{ data: SeekingAlphaItemResponse[][] }>({ ticker, params, service })
    return response.data
  }

  async getQuarterlyFinancialsForLastMonth(ticket: TickerType): Promise<SeekingAlphaQuarterlyFinancialsForLastMonthResponse> {
    const report = await this.getQuarterlyFinancials(ticket)
    const reversedPropertyValueEnum = reversEnumUtil(SeekingAlphaHeaderValue)
    const normalizedReport = {}
    for (const reportSections of report) {
      for (const reportElement of reportSections) {
        const normalizedPropertyKey = reversedPropertyValueEnum[reportElement[0].value]
        normalizedReport[normalizedPropertyKey] = stringToNumber(reportElement[1].raw_value ?? '-', ['-'])
      }
    }
    return normalizedReport as SeekingAlphaQuarterlyFinancialsForLastMonthResponse
  }

  async getQuarterlyFinancialsParse(ticker: TickerType): Promise<{ revenue: RevenueType | null, interestIncome: InterestIncomeType | null }> {
    const browser = await getBrowser()
    try {
      const $ = await getPageContent({
        browser,
        url: `https://seekingalpha.com/symbol/${toUpperCase(ticker.replace(/-/g, '.'))}/income-statement#figure_type=quarterly#order_type=latest_left`,
        pageBlocks: {
          noBlocks: true,
        },
        onEmptyPageReady: async (page) => {
          const userAgent = new UserAgent()
          await page.setExtraHTTPHeaders({
            'x-requested-with': 'XMLHttpRequest',
            'user-agent': userAgent.toString(),
            'Accept-Encoding': 'gzip, deflate, br',
          })
        },
        onPageLoad: async page => {
          await page.waitForTimeout(10000)
        },
      })
      if (!$('#financial-data').length) {
        throw new BadRequestException('SeekingalphaDomain. Page not fount')
      }
      const interestIncome = this.getInterestIncomeFromPageContent($)
      const revenueTableHeader = $('[chart-id="Total Revenues"]').next()
      return {
        revenue: revenueTableHeader.attr('data-raw-val') ? stringToNumber<RevenueType>(revenueTableHeader.attr('data-raw-val')!) : null,
        interestIncome: interestIncome,
      }
    } finally {
      await browser.close()
    }
  }

  private getInterestIncomeFromPageContent($: CheerioAPI): InterestIncomeType | null {
    const interestIncomeTableHeader = $('[chart-id="Interest And Investment Income"]').next()
    if (interestIncomeTableHeader.length) {
      return stringToNumber(interestIncomeTableHeader.attr('data-raw-val')!) ?? null
    }
    const interestAndDividendIncomeTableHeader = $('[chart-id="Total Interest and Dividend Income"]').next()
    if (interestAndDividendIncomeTableHeader.length) {
      return stringToNumber(interestAndDividendIncomeTableHeader.attr('data-raw-val')!) ?? null
    }
    return null
  }

  private async makeRequest<T>({ ticker, params = {}, service }: SeekingalphaMakeRequest): Promise<T> {
    const apiPath = this.configService.get<string>('SEEKINGALPHA_API_URL')
    const requestUrl = generateUrl({
      baseUrl: apiPath!,
      urlParts: ['symbol', ticker, service],
      params,
    })

    const response = await this.httpService.get<T>(requestUrl, {
      headers: {
        cookie: this.configService.get<string>('SEEKINGALPHA_COOKIE'),
        'x-requested-with': 'XMLHttpRequest',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36',
        'Accept-Encoding': 'gzip, deflate, br',
      },
    })

    const responseAwait = (await lastValueFrom(response).catch(e => {
      console.log(e)
    }))
    return responseAwait!.data
  }

} 
