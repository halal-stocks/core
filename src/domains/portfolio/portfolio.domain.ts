import { BadRequestException, ForbiddenException, Injectable } from '@nestjs/common'
import { sub } from 'date-fns'
import { httpExceptionMessages } from '../../consts/httpExceptionMessages'
import { PortfolioValueHistoryPeriod } from '../../consts/portfolio-value-history-period.enum'
import { TickerType } from '../../consts/report.enum'
import { PortfolioService } from '../../persistence/portfolio/portfolio.service'
import { PortfolioTradeService } from '../../persistence/portfolio/trade/portfolio-trade.service'
import { PortfolioValueHistoryService } from '../../persistence/portfolio/value-history/portfolio-value-history.service'
import { CurrentUser } from '../../shared/decorators/current-user.decorator'
import { lockDataUtil } from '../../utils/data-limitation/lock-data.util'
import arrayToObject from '../../utils/object/array-to-object/array-to-object.util'
import { CompanyIndicatorsDomain } from '../company/indicators/indicators.domain'
import { CompanyShariahFilterResultDomain } from '../company/shariah-filter-result/shariah-filter-result.domain'
import { CompanyQueryDomain } from '../company/subdomains/query/company-query.domain'
import { PortfolioCalculateDomain } from './calculate/portfolio-calculate.domain'
import { PortfolioCalculateResultResponse } from './calculate/portfolio-calculate.interface'
import {
  PortfolioAddCompanyRequest,
  PortfolioTradeUpdateManyRequest,
  PortfolioValueHistoryResponse,
} from './portfolio.interfaces'

@Injectable()
export class PortfolioDomain {
  constructor(
    private companyDomain: CompanyQueryDomain,
    private portfolioService: PortfolioService,
    private portfolioValueHistoryService: PortfolioValueHistoryService,
    private companyIndicatorsDomain: CompanyIndicatorsDomain,
    private companyShariahFilterResultDomain: CompanyShariahFilterResultDomain,
    private portfolioCalculateDomain: PortfolioCalculateDomain,
    private portfolioTradeService: PortfolioTradeService,
  ) {}

  async addCompany(userId: string, dto: PortfolioAddCompanyRequest): Promise<void> {
    const isExists = await this.isExistByTickerAndUserId(dto.ticker, userId)

    if (isExists) {
      return
    }

    const companyId = await this.companyDomain.getCompanyIdByTicker(dto.ticker)
    const portfolioItemId = await this.portfolioService.addOne({ userId, companyId })

    if (dto.trades && dto.trades.length) {
      await this.portfolioTradeService.addMany(dto.trades, portfolioItemId)
    }
  }

  async getMyPortfolio(user: CurrentUser): Promise<PortfolioCalculateResultResponse> {
    const portfolio = await this.portfolioService.findManyByUserId(user.id)
    const companyTickers = portfolio.map(it => it.company.ticker)
    const indicators = await this.companyIndicatorsDomain.findManyByTickers(companyTickers)
    const indicatorsAsObject = arrayToObject(indicators, 'company.ticker')
    const companiesWithLastShariahFilterResults = await this.companyShariahFilterResultDomain.getLastResultsAsDictionaryWithTickerKey(companyTickers)

    const { items, ...restPortfolio } = this.portfolioCalculateDomain.calculatePortfolio(
      portfolio,
      indicatorsAsObject,
      companiesWithLastShariahFilterResults,
    )

    return {
      ...restPortfolio,
      items: items.map(({ sadakaQuarterly, isHalal, ...restItems }) => ({
        isHalal: lockDataUtil(isHalal, restItems.company.isFree, user.isPaid),
        sadakaQuarterly: lockDataUtil(sadakaQuarterly, restItems.company.isFree, user.isPaid),
        ...restItems,
      })),
    }
  }

  async removeManyFromMyPortfolio(ids: string[], userId: string): Promise<void> {
    await this.portfolioService.removeManyByIdsAndUserId(ids, userId)
  }

  async updateTrade(dto: PortfolioTradeUpdateManyRequest): Promise<any> {
    await this.throwErrorIfUserDoesntHaveAccessToPortfolioItem(dto.id, dto.userId)
    await this.portfolioTradeService.createOrUpdateOne(dto.trade, dto.id)
  }

  async throwErrorIfUserDoesntHaveAccessToPortfolioItem(id: string, userId: string): Promise<void> {
    const isExists = await this.isExistByIdAndUserId(id, userId)
    if (!isExists) {
      throw new ForbiddenException()
    }
  }

  async isExistByIdAndUserId(id: string, userId: string): Promise<boolean> {
    return (await this.portfolioService.countByIdAndUserId(id, userId)) > 0
  }

  async isExistByTickerAndUserId(ticker: TickerType, userId: string): Promise<boolean> {
    return (await this.portfolioService.countByTickerAndUserId(ticker, userId)) > 0
  }

  getMyHistoryByPeriod(userId: string, period: PortfolioValueHistoryPeriod = PortfolioValueHistoryPeriod.ONE_MONTH): Promise<PortfolioValueHistoryResponse[]> {
    const limit = this.getHistoryLimit(period)
    return this.portfolioValueHistoryService.findByUserId(userId, limit)
  }

  async removeManyTradesByTradeIdAndUserIdAndPortfolioId(userId: string, portfolioId: string, tradeIds: string[]): Promise<void> {
    await this.portfolioTradeService.deleteManyByIdsAndPortfolioIdAndUserId(userId, portfolioId, tradeIds)
  }

  private getHistoryLimit(period: PortfolioValueHistoryPeriod): Date | undefined {
    switch (period) {
      case PortfolioValueHistoryPeriod.TEN_DAYS:
        return sub(new Date(), { days: 10 })
      case PortfolioValueHistoryPeriod.SIX_MONTH:
        return sub(new Date(), { months: 6 })
      case PortfolioValueHistoryPeriod.TTM:
        return sub(new Date(), { years: 1 })
      case PortfolioValueHistoryPeriod.FIVE_YEAR:
        return sub(new Date(), { years: 5 })
      case PortfolioValueHistoryPeriod.ALL:
        return
      default:
        throw new BadRequestException(`${httpExceptionMessages.portfolioValueHistory.notSupportedPeriod}: ${period}`)
    }
  }

}
