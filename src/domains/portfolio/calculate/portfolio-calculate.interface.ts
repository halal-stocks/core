import { IsCompanyFreeToUse, IsCompanyHalalType, StockPriceType, TickerType } from '../../../consts/report.enum'
import { LockableData } from '../../../shared/generics/lockable-data.generic'
import { PortfolioTradeResponse } from '../portfolio.interfaces'

export interface PortfolioCalculateItemResponse {
  totalQuantity: number
  averageEntryPrice: StockPriceType
  lastPrice: StockPriceType
  dayGainPercentage: number
  dayGain: number
  totalGainPercentage: number
  totalGain: number
  marketValue: StockPriceType
  isHalal: LockableData<IsCompanyHalalType>
  sadakaQuarterly: LockableData<number | null>
  portfolioSharePercentage: number
  trades: PortfolioTradeResponse[]
}


export interface PortfolioCalculateItemsResponse extends PortfolioCalculateItemResponse {
  company: {
    id: string
    ticker: TickerType,
    name: string
    isFree: IsCompanyFreeToUse
  }
  id: string
}

export interface PortfolioCalculateTotalsResponse {
  totalValue: number
  totalGain: number
  totalDailyGain: number
}

export interface PortfolioCalculateResultResponse extends PortfolioCalculateTotalsResponse {
  items: PortfolioCalculateItemsResponse[]
}
