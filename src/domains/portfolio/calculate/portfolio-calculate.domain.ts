import { Injectable } from '@nestjs/common'
import { ForbiddenIncomePerShareType, StockPriceType } from '../../../consts/report.enum'
import { CompanyShariahFilterLastResultResponseModel } from '../../../persistence/company/company.model'
import { PortfolioEntity } from '../../../persistence/portfolio/portfolio.entity'
import ArrayAsObject from '../../../shared/generics/array-as-object.generic'
import { getPortfolioTradesAverage } from '../../../utils/number/avarage/calculate-avarage.util'
import calculatePercentage from '../../../utils/number/calculate-percentage/calculate-percentage.util'
import calculateThePercentageChange
  from '../../../utils/number/calculate-the-percentage-change/calculate-the-percentage-change.util'
import { calculateTotal } from '../../../utils/number/calculate-total/calculate-total.util'
import toFixed from '../../../utils/number/to-fixed/to-fixed.util'
import { IndicatorsResponse } from '../../company/indicators/indicators.interfaces'
import {
  PortfolioCalculateItemResponse,
  PortfolioCalculateItemsResponse,
  PortfolioCalculateResultResponse,
  PortfolioCalculateTotalsResponse,
} from './portfolio-calculate.interface'

@Injectable()
export class PortfolioCalculateDomain {
  calculatePortfolio(
    portfolio: Omit<PortfolioEntity, 'user'>[],
    indicators: ArrayAsObject<IndicatorsResponse>,
    companyWithLastShariahFilterResults: ArrayAsObject<CompanyShariahFilterLastResultResponseModel>,
  ): PortfolioCalculateResultResponse {

    const calculatedItemsExceptSharePercentage = this.calculateManyItems(
      portfolio,
      indicators,
      companyWithLastShariahFilterResults,
    )

    const calculatedTotals = this.calculateTotal(calculatedItemsExceptSharePercentage)
    const itemsWithAdjustedSharePercentage = this.adjustSharePercentage(
      calculatedItemsExceptSharePercentage,
      calculatedTotals.totalValue,
    )

    return {
      ...calculatedTotals,
      items: itemsWithAdjustedSharePercentage,
    }
  }

  private calculateTotal(items: Omit<PortfolioCalculateItemsResponse, 'portfolioSharePercentage'>[]): PortfolioCalculateTotalsResponse {
    return items.reduce((acc, cur) => ({
      totalValue: toFixed(acc.totalValue + (cur.marketValue ?? 0)),
      totalGain: toFixed(acc.totalGain + (cur.totalGain ?? 0)),
      totalDailyGain: toFixed(acc.totalDailyGain + (cur.dayGain ?? 0)),
    }), {
      totalValue: 0,
      totalGain: 0,
      totalDailyGain: 0,
    })
  }


  private calculateManyItems(
    portfolio: Omit<PortfolioEntity, 'user'>[],
    indicators: ArrayAsObject<IndicatorsResponse>,
    companyWithLastShariahFilterResults: ArrayAsObject<CompanyShariahFilterLastResultResponseModel>,
  ): Omit<PortfolioCalculateItemsResponse, 'portfolioSharePercentage'>[] {
    return portfolio.reduce((acc, cur) => {
      const companyIndicators = indicators[cur.company.ticker]
      const forbiddenIncomePerShare = companyWithLastShariahFilterResults[cur.company.ticker].shariahFilterResult.forbiddenIncomePerShare
      const portfolioItem = this.calculateOneItem(
        cur,
        forbiddenIncomePerShare,
        companyIndicators,
      )
      return [
        ...acc,
        {

          company: {
            id: cur.company.id,
            ticker: cur.company.ticker,
            name: cur.company.name,
            isFree: cur.company.isFree,
          },
          ...portfolioItem,
          id: cur.id,
        },
      ]
    }, [] as Omit<PortfolioCalculateItemsResponse, 'portfolioSharePercentage'>[])
  }

  private calculateOneItem(
    portfolioItem: Omit<PortfolioEntity, 'user'>,
    forbiddenIncomePerShare: ForbiddenIncomePerShareType | null,
    indicators: IndicatorsResponse,
  ): Omit<PortfolioCalculateItemResponse, 'portfolioSharePercentage'> {
    const totalQuantity = calculateTotal(portfolioItem.trades.map(it => it.quantity))
    const sadakaQuarterly = forbiddenIncomePerShare ? toFixed(forbiddenIncomePerShare * totalQuantity) : null
    const marketValue = toFixed(indicators.actualStockPrice * totalQuantity)
    const averageEntryPrice = getPortfolioTradesAverage<StockPriceType>(portfolioItem.trades)

    return {
      totalQuantity,
      averageEntryPrice,
      lastPrice: indicators.actualStockPrice,
      dayGainPercentage: totalQuantity > 0 ? calculateThePercentageChange(indicators.actualStockPrice, indicators.previousDayStockPrice) : 0,
      dayGain: toFixed(marketValue - (indicators.previousDayStockPrice * totalQuantity)),
      totalGainPercentage: totalQuantity > 0 ? calculateThePercentageChange(indicators.actualStockPrice, averageEntryPrice) : 0,
      totalGain: toFixed(marketValue - (averageEntryPrice * totalQuantity)),
      marketValue: marketValue as StockPriceType,
      isHalal: portfolioItem.company.isHalal,
      sadakaQuarterly,
      trades: portfolioItem.trades.map(trade => ({
        ...trade,
        marketValue: toFixed(trade.quantity * indicators.actualStockPrice) as StockPriceType,
        dayGain: toFixed((trade.quantity * indicators.actualStockPrice ?? 0) - (trade.quantity * indicators.previousDayStockPrice ?? 0)),
        totalGain: toFixed((trade.quantity * indicators.actualStockPrice ?? 0) - (trade.quantity * trade.entryStockPrice)),
      })),
    }
  }

  private adjustSharePercentage(
    items: Omit<PortfolioCalculateItemsResponse, 'portfolioSharePercentage'>[],
    totalValue: number,
  ): PortfolioCalculateItemsResponse[] {
    return items.map(portfolioItem => ({
      ...portfolioItem,
      portfolioSharePercentage: calculatePercentage(totalValue, portfolioItem.marketValue),
    }))
  }
}
