import { Test } from '@nestjs/testing'
import { TickerType } from '../../consts/report.enum'
import { dummyUser } from '../../dummy/user/user.dummy'
import { mockedPortfolioService } from '../../persistence/portfolio/portfolio.mocks'
import { PortfolioService } from '../../persistence/portfolio/portfolio.service'
import { mockedPortfolioTradeService } from '../../persistence/portfolio/trade/portfolio-trade.mocks'
import { PortfolioTradeService } from '../../persistence/portfolio/trade/portfolio-trade.service'
import { mockedPortfolioValueHistoryService } from '../../persistence/portfolio/value-history/portfolio-value-history.mocks'
import { PortfolioValueHistoryService } from '../../persistence/portfolio/value-history/portfolio-value-history.service'
import { CompanyIndicatorsDomain } from '../company/indicators/indicators.domain'
import { mockedCompanyIndicatorsDomain } from '../company/indicators/indicators.mocks'
import { CompanyShariahFilterResultDomain } from '../company/shariah-filter-result/shariah-filter-result.domain'
import { mockedShariahFilterResultDomain } from '../company/shariah-filter-result/shariah-filter-result.mocks'
import { CompanyQueryDomain } from '../company/subdomains/query/company-query.domain'
import { mockedCompanyQueryDomain } from '../company/subdomains/query/company-query.mocks'
import { PortfolioCalculateDomain } from './calculate/portfolio-calculate.domain'
import { PortfolioCalculateResultResponse } from './calculate/portfolio-calculate.interface'
import { PortfolioDomain } from './portfolio.domain'

describe('PortfolioDomain', () => {
  let portfolioDomain: PortfolioDomain

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        providers: [
          PortfolioDomain,
          CompanyQueryDomain,
          PortfolioService,
          PortfolioTradeService,
          PortfolioValueHistoryService,
          CompanyIndicatorsDomain,
          CompanyShariahFilterResultDomain,
          PortfolioCalculateDomain,
        ],
      })
      .overrideProvider(CompanyQueryDomain)
      .useValue(mockedCompanyQueryDomain)
      .overrideProvider(PortfolioService)
      .useValue(mockedPortfolioService)
      .overrideProvider(PortfolioValueHistoryService)
      .useValue(mockedPortfolioValueHistoryService)
      .overrideProvider(CompanyIndicatorsDomain)
      .useValue(mockedCompanyIndicatorsDomain)
      .overrideProvider(CompanyShariahFilterResultDomain)
      .useValue(mockedShariahFilterResultDomain)
      .overrideProvider(PortfolioTradeService)
      .useValue(mockedPortfolioTradeService)
      .compile()

    portfolioDomain = moduleRef.get<PortfolioDomain>(PortfolioDomain)
  })

  describe('getMyPortfolio', () => {
    it('should return full portfolio result', async () => {
      expect(await portfolioDomain.getMyPortfolio(dummyUser)).toStrictEqual(
        {
          items: [
            {
              company: {
                id: '1',
                name: 'Apple Inc',
                ticker: 'AAPL' as TickerType,
                isFree: true,
              },
              id: '1',
              dayGain: 2.4,
              averageEntryPrice: 120.4,
              totalQuantity: 10,
              dayGainPercentage: 0.18,
              isHalal: true,
              lastPrice: 135.37,
              portfolioSharePercentage: 86.2,
              sadakaQuarterly: 1.5,
              totalGain: 149.7,
              totalGainPercentage: 12.43,
              marketValue: 1353.7,
              trades: [{
                dayGain: 2.4,
                totalGain: 149.7,
                marketValue: 1353.7,
                id: '1',
                entryStockPrice: 120.4,
                quantity: 10,
                tradedAt: null,
              }],
            },
            {
              company: {
                id: '2',
                name: 'ProPetro',
                ticker: 'RIO' as TickerType,
                isFree: false,
              },
              id: '1',
              averageEntryPrice: 10.4,
              dayGain: 2.76,
              dayGainPercentage: 1.29,
              isHalal: 'locked',
              lastPrice: 9.42,
              portfolioSharePercentage: 13.8,
              sadakaQuarterly: 'locked',
              totalGain: -22.54,
              totalGainPercentage: -9.42,
              marketValue: 216.66,
              totalQuantity: 23,
              trades: [{
                dayGain: 2.76,
                totalGain: -22.54,
                id: '2',
                entryStockPrice: 10.4,
                marketValue: 216.66,
                quantity: 23,
                tradedAt: null,
              }],
            },
          ],
          totalDailyGain: 5.16,
          totalGain: 127.16,
          totalValue: 1570.36,
        } as PortfolioCalculateResultResponse)
    })
  })
})


