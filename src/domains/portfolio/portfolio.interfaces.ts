import { StockPriceType, TickerType } from '../../consts/report.enum'
import { PortfolioValueHistoryEntity } from '../../persistence/portfolio/value-history/portfolio-value-history.entity'

export interface PortfolioAddCompanyRequest {
  ticker: TickerType
  trades: PortfolioTradeCreateRequest[]
}


export interface PortfolioValueHistoryResponse extends Omit<PortfolioValueHistoryEntity, 'user'> {}

export interface PortfolioTradeCreateRequest {
  entryStockPrice: StockPriceType
  quantity: number
  tradedAt?: Date
}

export interface PortfolioTradeUpdateOneRequest extends Partial<PortfolioTradeCreateRequest> {
  id: string
}

export interface PortfolioTradeUpdateManyRequest {
  id: string
  userId: string
  trade: any
}

export interface PortfolioTradeResponse {
  id: string
  dayGain: number
  totalGain: number
  marketValue: StockPriceType
  entryStockPrice: number
  quantity: number
  tradedAt: Date | null
}
