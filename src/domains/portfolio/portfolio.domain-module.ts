import { Module } from '@nestjs/common'
import { PortfolioPersistenceModule } from '../../persistence/portfolio/portfolio.persistence-module'
import { CompanyDomainModule } from '../company/company.domain-module'
import { PortfolioCalculateDomain } from './calculate/portfolio-calculate.domain'
import { PortfolioDomain } from './portfolio.domain'

@Module({
  imports: [
    PortfolioPersistenceModule,
    CompanyDomainModule,
  ],
  providers: [
    PortfolioDomain,
    PortfolioCalculateDomain,
  ],
  exports: [
    PortfolioDomain,
    PortfolioCalculateDomain,
    PortfolioDomainModule,
  ],
})
export class PortfolioDomainModule {}
