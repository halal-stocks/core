import { Module } from '@nestjs/common'
import { UserImplModule } from '../../persistence/user/user.impl-module'
import { CompanyDomainModule } from '../company/company.domain-module'
import { StripeDomainModule } from '../stripe/stripe.domain-module'
import { UserDomain } from './user.domain'

@Module({
  imports: [
    CompanyDomainModule,
    UserImplModule,
    StripeDomainModule,
  ],
  providers: [
    UserDomain,
  ],
  exports: [
    UserDomainModule,
    UserDomain,
  ],
})
export class UserDomainModule {}
