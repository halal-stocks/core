import { Injectable, NotFoundException } from '@nestjs/common'
import admin from 'firebase-admin'
import { auth } from 'firebase-admin/lib/auth'
import { UserPaymentInfoResponseDto } from '../../api/user/user.dtos'
import { httpExceptionMessages } from '../../consts/httpExceptionMessages'
import { UserResponseModel, UserUpdateRequestModel } from '../../persistence/user/user.model'
import { UserService } from '../../persistence/user/user.service'
import { isNullOrUndefined, isString } from '../../utils'
import { getPayedTillDate } from '../../utils/date/payed-till-date.util'
import { arrayToMap } from '../../utils/object/array-to-map/array-to-map.util'
import { BillingPlanPeriod } from '../payment/payment.interfaces'
import { StripeDomain } from '../stripe/stripe.domain'
import { UserCreateRequest, UserResponse } from './user.interfaces'

@Injectable()
export class UserDomain {
  constructor(
    private stripeDomain: StripeDomain,
    private userService: UserService,
  ) {}

  getAll(): Promise<UserResponse[]> {
    return this.userService.getAll()
  }

  async recursivelyGetAllUsersFromFirebase(pageToken?: string): Promise<auth.ListUsersResult['users']> {
    const firebaseAuth = admin.app().auth()
    const userRecord = await firebaseAuth.listUsers(1000, pageToken)
    const nextPage = userRecord.pageToken ? await this.recursivelyGetAllUsersFromFirebase(userRecord.pageToken) : []
    return [...userRecord.users, ...nextPage]
  }

  async getAllUsersFromFirebase(): Promise<auth.ListUsersResult['users']> {
    return this.recursivelyGetAllUsersFromFirebase()
  }

  async syncWithFirebase(): Promise<void> {
    const firebaseUsers = await this.getAllUsersFromFirebase()
    const existingUsers = await this.getAllAsDictionaryWithFirebaseIdKeyIncludeDeleted()
    await this.createOrUpdateUsersFromFirebase(firebaseUsers, existingUsers)
    await this.deleteUsersNotExistingInFirebase(firebaseUsers, existingUsers)
  }

  async getAllAsDictionaryWithFirebaseIdKeyIncludeDeleted(): Promise<Map<string, UserResponse>> {
    const users = await this.userService.getAll(true)
    return arrayToMap(users, 'firebaseId')
  }

  async getOneByFirebaseId(firebaseId: string): Promise<UserResponse> {
    const user = await this.findOneByFirebaseId(firebaseId)
    if (!user) {
      throw new NotFoundException(httpExceptionMessages.user.notFoundByFirebaseId)
    }
    return user
  }

  async createOrUpdateUsersFromFirebase(firebaseUsers: auth.ListUsersResult['users'], existingUsers: Map<string, UserResponse>) {
    const toCreate = firebaseUsers.map(it => ({
      id: existingUsers.get(it.uid)?.id,
      firebaseId: it.uid,
      email: it.email,
    }))

    await this.userService.createOrUpdateMany(toCreate)
  }

  async deleteUsersNotExistingInFirebase(firebaseUsers: auth.ListUsersResult['users'], existingUsers: Map<string, UserResponse>): Promise<void> {
    const firebaseUsersAsObject = arrayToMap(firebaseUsers, 'uid')
    const toDelete = Object.values(existingUsers)
      .filter(it => !firebaseUsersAsObject.has(it.firebaseId))
      .map(it => it.id)
    toDelete.length && await this.userService.deleteMany(toDelete)
  }

  findOneByFirebaseId(firebaseId: string): Promise<UserResponse | undefined> {
    return this.userService.findOneByFirebaseId(firebaseId)
  }

  async createOne(dto: UserCreateRequest): Promise<UserResponseModel> {
    return this.userService.createOne(dto)
  }

  async updateOne(dto: UserUpdateRequestModel): Promise<UserResponseModel> {
    return this.userService.updateOne(dto)
  }

  async deleteOneById(id: string): Promise<void> {
    await this.userService.deleteOneById(id)
  }

  async getOneUserByPaymentId(paymentId: string): Promise<UserResponse> {
    const user = await this.findOneUserByPaymentId(paymentId)
    if (!user) {
      throw new NotFoundException(httpExceptionMessages.user.notFoundByPaymentId)
    }
    return user
  }

  async findOneUserByPaymentId(paymentId: string): Promise<UserResponse | undefined> {
    return this.userService.findOneUserByPaymentId(paymentId)
  }

  async setPaidTillDateByIdForOneYear(firebaseId: string): Promise<void> {
    const user = await this.getOneByFirebaseId(firebaseId)
    return this.updatePaidTillDate(user.id, BillingPlanPeriod.yearly)
  }

  async updatePaidTillDate(user: string | UserResponse, period: BillingPlanPeriod): Promise<void> {
    const userObject = isString(user) ? await this.getOneById(user) : user
    const startDate = isNullOrUndefined(userObject.paidTill) || userObject.paidTill < new Date()
      ? new Date()
      : userObject.paidTill
    const paidTillDate = getPayedTillDate(period, startDate)
    await this.userService.updateOne({ id: userObject.id, paidTill: paidTillDate })
  }

  async getOneWithPaidPayments(id: string): Promise<UserPaymentInfoResponseDto> {
    const response = await this.userService.findOneWithPaidPayments(id)
    if (!response) {
      throw new NotFoundException(httpExceptionMessages.user.notFoundById)
    }
    const { payments, ...restResponse } = response
    return {
      ...restResponse,
      history: payments,
    }
  }

  async getOneById(id: string): Promise<UserResponse> {
    const user = await this.userService.findOneById(id)
    if (!user) {
      throw new NotFoundException(httpExceptionMessages.user.notFoundById)
    }
    return user
  }
}
