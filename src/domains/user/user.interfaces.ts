export interface UserCreateRequest {
  email: string
  firebaseId: string
}


export interface UserResponse {
  id: string
  isNotifyOnCompanyHalalnessChange: boolean
  firebaseId: string
  email: string | null
  paidTill: Date | null
  createdAt: Date
  updatedAt: Date | null
  deletedAt: Date | null
}

