import { Global, HttpModule, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { UserDomainModule } from '../user/user.domain-module'
import { JwtStrategy } from './jwt.strategy'

@Global()
@Module({
  imports: [
    HttpModule,
    ConfigModule,
    UserDomainModule,
  ],
  controllers: [],
  providers: [JwtStrategy],
  exports: [AuthModule],
})
export class AuthModule {
}
