import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import * as firebase from 'firebase-admin'
import { app } from 'firebase-admin/lib/firebase-namespace-api'
import { ExtractJwt, Strategy } from 'passport-firebase-jwt'
import * as firebaseConfig from '../../../halal-invest.firebase-config.json'
import { CurrentUser } from '../../shared/decorators/current-user.decorator'
import { UserDomain } from '../user/user.domain'

@Injectable()
export class JwtStrategy extends PassportStrategy(
  Strategy,
  'firebase-auth',
) {
  private defaultApp: app.App

  constructor(
    private userDomain: UserDomain,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    })
    const firebaseParams = getFirebaseParams()
    this.defaultApp = firebase.initializeApp({
      credential: firebase.credential.cert(firebaseParams),
    })
  }

  async validate(token: string): Promise<CurrentUser> {
    try {
      const firebaseUser = await this.defaultApp
        .auth()
        .verifyIdToken(token, true)

      const user = await this.userDomain.getOneByFirebaseId(firebaseUser.sub)

      return {
        id: user.id,
        fullName: firebaseUser.name,
        firebaseId: user.firebaseId,
        email: user.email,
        isPaid: user.paidTill ? user.paidTill > new Date() : false,
      }
    } catch (err) {
      throw new UnauthorizedException(err?.message)
    }
  }
}

const getFirebaseParams = () => {
  return {
    type: firebaseConfig.type,
    projectId: firebaseConfig.project_id,
    privateKeyId: firebaseConfig.private_key_id,
    privateKey: firebaseConfig.private_key,
    clientEmail: firebaseConfig.client_email,
    clientId: firebaseConfig.client_id,
    authUri: firebaseConfig.auth_uri,
    tokenUri: firebaseConfig.token_uri,
    authProviderX509CertUrl: firebaseConfig.auth_provider_x509_cert_url,
    clientC509CertUrl: firebaseConfig.client_x509_cert_url,
  }
}
