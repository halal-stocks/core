import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { add } from 'date-fns'
import { Connection } from 'typeorm'
import {
  PaymentCreateSubscriptionRequestDto,
  PaymentCreatSubscriptionResponseDto,
  PaymentRobokassaResultRequestDto,
  RobokassaSuccessRequestDto,
} from '../../api/payment/payment.dtos'
import { httpExceptionMessages } from '../../consts/httpExceptionMessages'
import { PaymentService } from '../../persistence/payment/payment.service'
import { PaymentPublisher } from '../../rpc/payment/payment.publisher'
import { CurrentUser } from '../../shared/decorators/current-user.decorator'
import {
  commitTransaction,
  releaseTransaction,
  rollbackTransactionAndThrowError,
  startTransaction,
} from '../../shared/repository/transaction'
import { decodeBase64 } from '../../utils/string/base64.util'
import { UserDomain } from '../user/user.domain'
import { BillingPlan, PaymentStatus } from './payment.interfaces'
import { PaymentQueryDomain } from './subdomains/query/payment-query.domain'
import { RobokassaDomain } from './subdomains/robokassa/robokassa.domain'

@Injectable()
export class PaymentDomain {
  constructor(
    private userDomain: UserDomain,
    private configService: ConfigService,
    private paymentService: PaymentService,
    private connection: Connection,
    private paymentPublisher: PaymentPublisher,
    private paymentQueryDomain: PaymentQueryDomain,
    private robokassaDomain: RobokassaDomain,
  ) {}

  async resultWebhook(dto: PaymentRobokassaResultRequestDto): Promise<string> {
    const isSignatureValid = this.robokassaDomain.verifySignatureOnResult(dto)

    if (!isSignatureValid) {
      throw new BadRequestException(httpExceptionMessages.portfolioValueHistory)
    }

    const payment = await this.paymentQueryDomain.getOneByInvoiceId(dto.InvId)
    const billingPlan = this.getPlanByPriceId(payment.priceId)

    await this.paymentService.updateOne({ id: payment.id, status: PaymentStatus.SUCCEEDED })
    await this.userDomain.updatePaidTillDate(dto.Shp_user, billingPlan.period)
    await this.paymentPublisher.publishPaymentSucceededMessage(payment.id)

    return `OK${dto.InvId}`
  }

  async scheduleUpcomingWithdraw(): Promise<void> {
    const paidExpiration = add(new Date(), { days: 3 })
    const paymentIds = await this.paymentService.getActiveParentPaymentIdsWithUpcomingPaidTillExpirationDate(paidExpiration)
    await Promise.all(paymentIds.map(paymentId => this.paymentPublisher.publishWithdrawNeededMessage(paymentId)))
  }

  async successWebhook(dto: RobokassaSuccessRequestDto): Promise<string> {
    return this.configService.get('CLIENT_URL')!
  }

  async failWebhook(dto: RobokassaSuccessRequestDto): Promise<string> {
    return this.configService.get('CLIENT_URL')!
  }

  async createSubscription(user: CurrentUser, dto: PaymentCreateSubscriptionRequestDto): Promise<PaymentCreatSubscriptionResponseDto> {
    const { entityManager, queryRunner } = await startTransaction(this.connection)
    try {
      const billingPlan = this.getPlanByPriceId(dto.priceId)

      const payment = await this.paymentService.createOne({
        userId: user.id,
        paymentUrl: '', // We need an invoice id to create payment url
        priceId: billingPlan.priceId,
        price: billingPlan.price,
        currency: billingPlan.currency,
        isActiveRecurrentParent: true,
      }, entityManager)

      const paymentUrl = this.robokassaDomain.getSubscriptionStarterPaymentUrl({
        user,
        billingPlan,
        language: dto.language,
        invoiceId: payment.invoiceId,
      })

      await commitTransaction(queryRunner)

      await this.paymentService.updateOne({ id: payment.id, paymentUrl }, entityManager)

      return {
        paymentUrl,
      }
    } catch (err) {
      return await rollbackTransactionAndThrowError(queryRunner, err)
    } finally {
      await releaseTransaction(queryRunner)
    }
  }

  getPlanByPriceId(priceId: string): BillingPlan {
    const billingPlansBase64 = this.configService.get<string>('BILLING_PLANS')
    if (!billingPlansBase64) {
      throw new InternalServerErrorException('Billing plans are not exists')
    }

    const billingPlans = decodeBase64<BillingPlan[]>(billingPlansBase64)

    const billingPlan = billingPlans.find(it => it.priceId === priceId)

    if (!billingPlan) {
      throw new BadRequestException('Billing plan by price id did not found')
    }

    return billingPlan
  }

  async cancelSubscriptionByUserId(userId: string): Promise<void> {
    await this.paymentService.cancelSubscriptionsByUserId(userId)
  }

  async withdrawSubscription(id: string): Promise<any> {
    const payment = await this.paymentQueryDomain.getOneWithUserById(id)
    const billingPlan = this.getPlanByPriceId(payment.priceId)
    const { entityManager, queryRunner } = await startTransaction(this.connection)
    try {

      const newPayment = await this.paymentService.createOne({
        userId: payment.user.id,
        paymentUrl: '', // We need an invoice id to create payment url
        priceId: billingPlan.priceId,
        price: payment.price,
        currency: payment.currency,
        parentPaymentId: payment.id,
      }, entityManager)

      const paymentUrl = await this.robokassaDomain.withdrewUpcomingPayment({
        billingPlan,
        invoiceId: newPayment.invoiceId,
        parentPayment: {
          invoiceId: payment.invoiceId,
          price: payment.price,
          currency: payment.currency,
        },
        user: payment.user,
      })

      await commitTransaction(queryRunner)

      await this.paymentService.updateOne({ id: newPayment.id, paymentUrl }, entityManager)
    } catch (err) {
      return await rollbackTransactionAndThrowError(queryRunner, err)
    } finally {
      await releaseTransaction(queryRunner)
    }
  }

  async cancelOtherSubscriptionStarters(paymentId: string): Promise<void> {
    const user = await this.userDomain.getOneUserByPaymentId(paymentId)
    await this.paymentService.cancelSubscriptionsByUserId(user.id, paymentId)
  }
}
