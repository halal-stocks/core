import { HttpService, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { createHash } from 'crypto'
import { lastValueFrom } from 'rxjs'
import { PaymentRobokassaResultRequestDto } from '../../../../api/payment/payment.dtos'
import { Currency } from '../../../../consts/currency.enum'
import { generateUrl, getFormData } from '../../../../utils/url/url.util'
import {
  GetPaymentUrlRequest,
  PaymentGetRecurringPaymentUrlRequest,
  PaymentGetSubscriptionStartingPaymentUrlRequest,
} from '../../payment.interfaces'
import { RobokassaConfig, RobokassaGetSignatureRequest } from './robokassa.interfaces'

@Injectable()
export class RobokassaDomain {
  constructor(
    private configService: ConfigService,
    private httpService: HttpService,
  ) {}

  verifySignatureOnResult(dto: PaymentRobokassaResultRequestDto): boolean {
    const merchantSecondPassword = this.configService.get('ROBOKASSA_SECOND_PASSWORD')

    const signatureElements = [
      dto.OutSum,
      dto.InvId,
      merchantSecondPassword,
      `Shp_user=${dto.Shp_user}`,
    ]

    const signature = createHash('sha256')
      .update(signatureElements.join(':'))
      .digest('hex').toLocaleUpperCase()

    return signature === dto.SignatureValue
  }

  getSubscriptionStarterPaymentUrl(props: PaymentGetSubscriptionStartingPaymentUrlRequest): string {
    const apiService = 'Index.aspx'
    const { billingPlan, user, language, invoiceId } = props

    return this.getPaymentUrl({
      service: apiService,
      user,
      language,
      payment: {
        invoiceId,
        price: billingPlan.price,
        description: billingPlan.description,
        currency: billingPlan.currency,
      },
    })
  }

  async withdrewUpcomingPayment(props: PaymentGetRecurringPaymentUrlRequest): Promise<string> {
    const { merchantLogin, merchantFirstPassword, baseUrl } = this.getRobokassaConfig()

    const apiService = 'Recurring'

    const signature = this.getSignature({
      merchantLogin,
      merchantFirstPassword,
      invoiceId: props.invoiceId,
      price: props.parentPayment.price,
      currency: props.parentPayment.currency,
      userId: props.user.id,
    })

    const params = {
      MerchantLogin: merchantLogin,
      InvoiceId: props.invoiceId,
      PreviousInvoiceID: props.parentPayment.invoiceId,
      Description: props.billingPlan.description,
      SignatureValue: signature,
      OutSum: props.parentPayment.price,
      Shp_user: props.user.id,
    }

    const form = getFormData(params)
    const response = this.httpService.post(
      `${baseUrl}/${apiService}`,
      form,
      {
        headers: form.getHeaders(),
      },
    )

    await lastValueFrom(response)

    return generateUrl({
      baseUrl,
      urlParts: [apiService],
      params,
    })
  }

  getPaymentUrl(props: GetPaymentUrlRequest): string {
    const { merchantLogin, merchantFirstPassword, baseUrl, isProductionMode } = this.getRobokassaConfig()

    const signature = this.getSignature({
      merchantLogin,
      merchantFirstPassword,
      price: props.payment.price,
      invoiceId: props.payment.invoiceId,
      currency: props.payment.currency,
      userId: props.user.id,
    })

    const params = {
      MerchantLogin: merchantLogin,
      InvId: props.payment.invoiceId,
      Description: props.payment.description,
      SignatureValue: signature,
      OutSum: props.payment.price,
      IsTest: !isProductionMode ? 1 : undefined,
      culture: props.language.toLocaleLowerCase(),
      Recurring: true,
      Shp_user: props.user.id,
    }

    return generateUrl({
      baseUrl,
      urlParts: [props.service],
      params,
    })
  }

  getRobokassaConfig(): RobokassaConfig {
    const merchantLogin = this.configService.get('ROBOKASSA_MERCHANT_LOGIN')
    const merchantFirstPassword = this.configService.get('ROBOKASSA_FIRST_PASSWORD')
    const baseUrl = this.configService.get('ROBOKASSA_API_URL')
    const isProductionMode = this.configService.get('ROBOKASSA_IS_PRODUCTION_MODE', false)

    return {
      merchantLogin,
      merchantFirstPassword,
      baseUrl,
      isProductionMode,
    }
  }

  getSignature(props: RobokassaGetSignatureRequest): string {
    const signatureElements = [
      props.merchantLogin,
      props.price,
      props.invoiceId,
    ]
    if (props.currency !== Currency.RUB) {
      signatureElements.push(props.currency)
    }

    // Custom params (Shp_) should be alphabetically ordered
    signatureElements.push(props.merchantFirstPassword, `Shp_user=${props.userId}`)

    return createHash('sha256')
      .update(signatureElements.join(':'))
      .digest('hex')
  }
}
