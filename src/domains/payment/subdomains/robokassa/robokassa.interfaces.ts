import { Currency } from '../../../../consts/currency.enum'

export interface RobokassaGetSignatureRequest {
  merchantLogin: string
  merchantFirstPassword: string
  price: number
  invoiceId: number
  currency: Currency
  userId: string
}

export interface RobokassaConfig {
  merchantLogin: string
  merchantFirstPassword: string
  baseUrl: string
  isProductionMode: string
}
