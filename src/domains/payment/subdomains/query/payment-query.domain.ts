import { Injectable, NotFoundException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { EntityManager } from 'typeorm'
import { httpExceptionMessages } from '../../../../consts/httpExceptionMessages'
import { PaymentService } from '../../../../persistence/payment/payment.service'
import { UserDomain } from '../../../user/user.domain'
import { PaymentResponse, PaymentWithUserResponse } from '../../payment.interfaces'

@Injectable()
export class PaymentQueryDomain {
  constructor(
    private userDomain: UserDomain,
    private configService: ConfigService,
    private paymentService: PaymentService,
  ) {}

  async getOneById(id: string, entityManager?: EntityManager): Promise<PaymentResponse> {
    const invoice = await this.findOneById(id)
    if (!invoice) {
      throw new NotFoundException(`${httpExceptionMessages.payment.notFoundById}: ${id}`)
    }

    return invoice
  }

  findOneById(id: string): Promise<PaymentResponse | undefined> {
    return this.paymentService.findOneById(id)
  }

  async getOneWithUserById(id: string, entityManager?: EntityManager): Promise<PaymentWithUserResponse> {
    const invoice = await this.findOneWithUserById(id)
    if (!invoice) {
      throw new NotFoundException(`${httpExceptionMessages.payment.notFoundById}: ${id}`)
    }

    return invoice
  }

  findOneWithUserById(id: string): Promise<PaymentWithUserResponse | undefined> {
    return this.paymentService.findOneWithUserById(id)
  }

  async getOneByInvoiceId(invoiceId: number): Promise<PaymentResponse> {
    const invoice = await this.findOneByInvoiceId(invoiceId)
    if (!invoice) {
      throw new NotFoundException(`${httpExceptionMessages.payment.notFoundByInvoiceId}: ${invoiceId}`)
    }

    return invoice
  }

  findOneByInvoiceId(invoiceId: number): Promise<PaymentResponse | undefined> {
    return this.paymentService.findOneByInvoiceId(invoiceId)
  }

}
