import { HttpModule, Module } from '@nestjs/common'
import { PaymentImplModule } from '../../persistence/payment/payment.impl-module'
import { RpcPublishersModule } from '../../rpc/rpc-publishers.module'
import { StripeDomainModule } from '../stripe/stripe.domain-module'
import { UserDomainModule } from '../user/user.domain-module'
import { PaymentDomain } from './payment.domain'
import { PaymentQueryDomain } from './subdomains/query/payment-query.domain'
import { RobokassaDomain } from './subdomains/robokassa/robokassa.domain'

@Module({
  imports: [
    HttpModule,
    UserDomainModule,
    StripeDomainModule,
    PaymentImplModule,
    RpcPublishersModule,
  ],
  providers: [
    PaymentDomain,
    PaymentQueryDomain,
    RobokassaDomain,
  ],
  exports: [
    PaymentDomain,
    PaymentDomainModule,
    PaymentQueryDomain,
  ],
})
export class PaymentDomainModule {}
