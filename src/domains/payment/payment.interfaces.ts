import { Currency } from '../../consts/currency.enum'
import { Language } from '../../consts/language'
import { CurrentUser } from '../../shared/decorators/current-user.decorator'
import { UserResponse } from '../user/user.interfaces'

export enum PaymentStatus {
  PENDING = 'PENDING',
  SUCCEEDED = 'SUCCEEDED',
  FAILED = 'FAILED',
}

export interface BillingPlan {
  price: number
  priceId: string
  currency: Currency
  period: BillingPlanPeriod
  description: string
  language: Language
}


export enum BillingPlanPeriod {
  monthly = 'MONTHLY',
  yearly = 'YEARLY'
}

export interface PaymentGetSubscriptionStartingPaymentUrlRequest {
  user: Pick<CurrentUser, 'id' | 'email'>,
  billingPlan: BillingPlan,
  language: Language,
  invoiceId: number
}

export interface PaymentGetRecurringPaymentUrlRequest extends Omit<PaymentGetSubscriptionStartingPaymentUrlRequest, 'language'> {
  parentPayment: Pick<PaymentResponse, 'invoiceId' | 'price' | 'currency'>
}

export interface GetPaymentUrlRequest {
  service: string
  payment: {
    invoiceId: number
    price: number
    currency: Currency
    description: string
  }
  language: Language
  user: Pick<CurrentUser, 'id' | 'email'>,
}

export interface PaymentResponse {
  id: string
  invoiceId: number
  status: PaymentStatus
  paymentUrl: string
  currency: Currency
  priceId: string
  price: number
  createdAt: Date
  updatedAt: Date | null
  deletedAt: Date | null
}

export interface PaymentWithUserResponse extends PaymentResponse {
  user: UserResponse
}
