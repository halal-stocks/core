export interface StripeCreateCustomer {
  email: string
}

export interface StripeCreateSubscriptionRequest {
  priceId: string,
  customerId: string
}

export interface StripeCreateSubscriptionResponse {
  subscriptionId: string
  clientSecret: string
}
