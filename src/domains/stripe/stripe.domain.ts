import { BadRequestException, HttpService, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { InjectStripe } from 'nestjs-stripe'
import { Stripe } from 'stripe'
import {
  StripeCreateCustomer,
  StripeCreateSubscriptionRequest,
  StripeCreateSubscriptionResponse,
} from './stripe.interfaces'

@Injectable()
export class StripeDomain {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    @InjectStripe() private stripe: Stripe,
  ) {}

  async createSubscription(dto: StripeCreateSubscriptionRequest): Promise<StripeCreateSubscriptionResponse> {
    try {
      // Create the subscription. Note we're expanding the Subscription's
      // latest invoice and that invoice's payment_intent
      // so we can pass it to the front end to confirm the payment
      const subscription = await this.stripe.subscriptions.create({
        customer: dto.customerId,
        items: [{
          price: dto.priceId,
        }],
        payment_behavior: 'default_incomplete',
        expand: ['latest_invoice.payment_intent'],
        metadata: {
          userId: 'auth0_user_id',
        },
      })

      return {
        subscriptionId: subscription.id,
        clientSecret: ((subscription.latest_invoice! as Stripe.Invoice).payment_intent as Stripe.PaymentIntent).client_secret!,
      }
    } catch (error) {
      throw new BadRequestException(error.message)
    }
  }

  async webhook(body: any, stripeSignature: string): Promise<void> {
    const event = await this.getEvent(body, stripeSignature)
    // Extract the object from the event.
    const dataObject = event.data.object

    switch (event.type) {
      case 'invoice.paid':
        // Used to provision services after the trial has ended.
        // The status of the invoice will show up as paid. Store the status in your
        // database to reference when a user accesses your service to avoid hitting rate limits.
        break
      case 'invoice.payment_failed':
        // If the payment fails or the customer does not have a valid payment method,
        //  an invoice.payment_failed event is sent, the subscription becomes past_due.
        // Use this webhook to notify your user that their payment has
        // failed and to retrieve new card details.
        break
      case 'customer.subscription.deleted':
        if (event.request != null) {
          // handle a subscription cancelled by your request
          // from above.
        } else {
          // handle subscription cancelled automatically based
          // upon your subscription settings.
        }
        break
      default:
      // Unexpected event type
    }
  }

  async getEvent(body: any, stripeSignature: string): Promise<any> {
    const stripeWebhookSecret = this.configService.get('STRIPE_WEBHOOK_SECRET')

    try {
      return this.stripe.webhooks.constructEvent(
        body,
        stripeSignature,
        stripeWebhookSecret,
      )
    } catch (err) {
      console.log('Webhook signature verification failed. => ', err)
      throw new BadRequestException()
    }
  }

  async createCustomer(dto: StripeCreateCustomer): Promise<{ customer: string }> {
    return {
      customer: 'cus_JaJ8UvjyVYeK9c',
    }
  }
}
