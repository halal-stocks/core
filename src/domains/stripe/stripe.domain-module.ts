import { HttpModule, Module } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { StripeModule } from 'nestjs-stripe'
import { StripeDomain } from './stripe.domain'

@Module({
  imports: [
    HttpModule,
    StripeModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        apiKey: configService.get('STRIPE_SECRET_KEY')!,
        apiVersion: '2020-08-27',
      }),
    }),
  ],
  providers: [StripeDomain],
  exports: [StripeDomainModule, StripeDomain],
})
export class StripeDomainModule {}
