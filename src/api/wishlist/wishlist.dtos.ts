import { ApiProperty } from '@nestjs/swagger'
import { CompanyDetailedResponseDto } from '../company/company.dtos'

export class WishlistItemResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  company: CompanyDetailedResponseDto
}
