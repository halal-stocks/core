import { Controller, Delete, Get, Param, ParseArrayPipe, Post } from '@nestjs/common'
import { ApiProperty, ApiTags } from '@nestjs/swagger'
import { TickerType } from '../../consts/report.enum'
import { WishlistDomain } from '../../domains/wishlist/wishlist.domain'
import { CurrentUser } from '../../shared/decorators/current-user.decorator'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { Auth } from '../../shared/guards/auth/auth-decorators.guard'
import { WishlistItemResponseDto } from './wishlist.dtos'

@Controller('wishlist')
@ApiTags('wishlist')
export class WishlistController {
  constructor(
    private wishlistDomain: WishlistDomain,
  ) {}

  @Get()
  @ApiProperty()
  @Auth()
  getMyWishlist(
    @CurrentUser() user: CurrentUser,
  ): Promise<ItemsWithCount<WishlistItemResponseDto>> {
    return this.wishlistDomain.getManyByUserId(user.id)
  }

  @Delete(':ids')
  @ApiProperty()
  @Auth()
  deleteFromMyWishlist(
    @CurrentUser() user: CurrentUser,
    @Param('ids', new ParseArrayPipe()) ids: string[],
  ): Promise<void> {
    return this.wishlistDomain.deleteFromUser(ids, user.id)
  }

  @Post(':ticker')
  @ApiProperty()
  @Auth()
  addOneToMyWishlist(
    @CurrentUser() user: CurrentUser,
    @Param('ticker') ticker: TickerType,
  ): Promise<ItemsWithCount<WishlistItemResponseDto>> {
    return this.wishlistDomain.addOneToUser(ticker, user.id)
  }
}
