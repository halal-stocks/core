import { Controller, Param, Post } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { MailerDomain } from '../../domains/mailer/mailer.domain'

@Controller('mailer')
@ApiTags('mailer')
export class MailerController {
  constructor(
    private mailerDomain: MailerDomain,
  ) {}

  @Post('subscribe/:email')
  addSubscriber(
    @Param('email') email: string,
  ): Promise<void> {
    return this.mailerDomain.addSubscriber(email)
  }
}
