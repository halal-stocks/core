import { ApiProperty } from '@nestjs/swagger'
import { IsEmail, IsString } from 'class-validator'
import { PaymentResponseDto } from '../payment/payment.dtos'

export class UserCreateRequestDto {
  @ApiProperty()
  @IsString()
  firebaseId: string

  @ApiProperty()
  @IsEmail()
  email: string
}

export class UserPaymentInfoResponseDto {
  paidTill: Date | null
  history: PaymentResponseDto[]
}


export class UserResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  isNotifyOnCompanyHalalnessChange: boolean

  @ApiProperty()
  firebaseId: string

  @ApiProperty()
  email: string | null

  @ApiProperty()
  paidTill: Date | null

  @ApiProperty()
  createdAt: Date

  @ApiProperty()
  updatedAt: Date | null

  @ApiProperty()
  deletedAt: Date | null
}
