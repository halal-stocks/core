import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common'
import { ApiProperty, ApiTags } from '@nestjs/swagger'
import { UserDomain } from '../../domains/user/user.domain'
import { CurrentUser } from '../../shared/decorators/current-user.decorator'
import { Auth } from '../../shared/guards/auth/auth-decorators.guard'
import { UserCreateRequestDto, UserPaymentInfoResponseDto, UserResponseDto } from './user.dtos'

@Controller('user')
@ApiTags('user')
export class UserController {
  constructor(
    private userDomain: UserDomain,
  ) {}

  @Get()
  @Auth()
  getAll(): Promise<UserResponseDto[]> {
    return this.userDomain.getAll()
  }

  @Post()
  async createOne(
    @Body() dto: UserCreateRequestDto,
  ): Promise<void> {
    await this.userDomain.createOne(dto)
  }

  @Post('set-paid-till/:firebaseUserId')
  @Auth()
  setRole(
    @Param('firebaseUserId') firebaseUserId: string,
  ): Promise<void> {
    return this.userDomain.setPaidTillDateByIdForOneYear(firebaseUserId)
  }

  @Get('sync-with-firebase')
  @ApiProperty()
  @Auth()
  syncWithFirebase(): Promise<void> {
    return this.userDomain.syncWithFirebase()
  }

  @Delete()
  @Auth()
  deleteMe(
    @CurrentUser() user: CurrentUser,
  ): Promise<void> {
    return this.userDomain.deleteOneById(user.id)
  }

  @Get('info')
  @Auth()
  getMyPaymentHistory(
    @CurrentUser() user: CurrentUser,
  ): Promise<UserPaymentInfoResponseDto> {
    return this.userDomain.getOneWithPaidPayments(user.id)
  }
}
