import { Module } from '@nestjs/common'
import { CompanyDomainModule } from '../domains/company/company.domain-module'
import { FiltersDomainModule } from '../domains/filters/filters.domain-module'
import { MailerDomainModule } from '../domains/mailer/mailer.domain-module'
import { PaymentDomainModule } from '../domains/payment/payment.domain-module'
import { PortfolioDomainModule } from '../domains/portfolio/portfolio.domain-module'
import { ScreenerDomainModule } from '../domains/screener/screener.domain.module'
import { UserDomainModule } from '../domains/user/user.domain-module'
import { WishlistDomainModule } from '../domains/wishlist/wishlist.domain-module'
import { CompanyController } from './company/company.controller'
import { AnalysisController } from './filters/analysis.controller'
import { MailerController } from './mailer/mailer.controller'
import { PaymentController } from './payment/payment.controller'
import { PortfolioController } from './portfolio/portfolio.controller'
import { ScreenerController } from './screener/screener.controller'
import { ShariahFilterController } from './shariah-filter/shariah-filter.controller'
import { StockMarketIndexController } from './stock-market-index/stock-market-index.controller'
import { UserController } from './user/user.controller'
import { WishlistController } from './wishlist/wishlist.controller'

@Module({
  imports: [
    FiltersDomainModule,
    CompanyDomainModule,
    UserDomainModule,
    ScreenerDomainModule,
    PaymentDomainModule,
    PortfolioDomainModule,
    MailerDomainModule,
    WishlistDomainModule,
  ],
  controllers: [
    StockMarketIndexController,
    AnalysisController,
    CompanyController,
    PortfolioController,
    ScreenerController,
    ShariahFilterController,
    PaymentController,
    UserController,
    MailerController,
    WishlistController,
  ],
})
export class ApiModule {}
