import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import { IsDefined, IsNumber, IsOptional, IsString, IsUUID, ValidateIf, ValidateNested } from 'class-validator'
import { IsCompanyHalalType, StockPriceType, TickerType } from '../../consts/report.enum'
import { LockableData } from '../../shared/generics/lockable-data.generic'
import { isNullOrUndefined } from '../../utils'
import { CompanyTinyResponseDto } from '../company/company.dtos'

export class PortfolioAddTradeRequestDto {
  @IsNumber()
  @IsDefined()
  @ApiProperty()
  entryStockPrice: StockPriceType

  @IsNumber()
  @IsDefined()
  @ApiProperty()
  quantity: number

  @IsOptional()
  @ApiProperty()
  tradedAt?: Date
}

export class PortfolioAddCompanyRequestDto {
  @IsString()
  @IsDefined()
  @ApiProperty()
  ticker: TickerType

  @ApiProperty()
  @ValidateNested()
  @Type(() => PortfolioAddTradeRequestDto)
  trades: PortfolioAddTradeRequestDto[]
}

export class PortfolioValueHistory {
  @ApiProperty()
  id: string

  @ApiProperty()
  value: number

  @ApiProperty()
  snapshottedAt: Date
}

export class PortfolioCreateOrUpdateTrade {
  @ApiProperty()
  @IsUUID()
  @IsOptional()
  id?: string

  @ValidateIf(o => isNullOrUndefined(o.id), { message: 'entryStockPrice required for new trade' })
  @IsNumber()
  @ApiProperty()
  entryStockPrice?: StockPriceType

  @ValidateIf(o => isNullOrUndefined(o.id), { message: 'quantity required for new trade' })
  @IsNumber()
  @ApiPropertyOptional()
  quantity?: number

  @IsOptional()
  @ApiPropertyOptional()
  tradedAt?: Date
}

export class PortfolioItemTradeResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  entryStockPrice: number

  @ApiProperty()
  dayGain: number

  @ApiProperty()
  totalGain: number

  @ApiProperty()
  quantity: number

  @ApiProperty()
  tradedAt: Date | null
}

export class PortfolioItemResponseDto {
  @ApiProperty()
  company: CompanyTinyResponseDto

  @ApiProperty({ type: [PortfolioItemTradeResponseDto] })
  trades: PortfolioItemTradeResponseDto[]

  @ApiProperty()
  totalQuantity: number

  @ApiProperty()
  averageEntryPrice: StockPriceType

  @ApiProperty()
  lastPrice: StockPriceType

  @ApiProperty()
  marketValue: StockPriceType

  @ApiProperty()
  dayGainPercentage: number

  @ApiProperty()
  dayGain: number

  @ApiProperty()
  totalGainPercentage: number

  @ApiProperty()
  totalGain: number

  @ApiProperty()
  isHalal: LockableData<IsCompanyHalalType>

  @ApiProperty()
  sadakaQuarterly: LockableData<number | null>

  @ApiProperty()
  portfolioSharePercentage: number
}


export class PortfolioResponseDto {
  @ApiProperty()
  totalValue: number

  @ApiProperty()
  totalGain: number

  @ApiProperty()
  totalDailyGain: number

  @ApiProperty({ type: [PortfolioItemResponseDto] })
  items: PortfolioItemResponseDto[]
}
