import { Body, Controller, Delete, Get, Param, ParseArrayPipe, Post, Put, Query } from '@nestjs/common'
import { ApiOkResponse, ApiProperty, ApiTags } from '@nestjs/swagger'
import { PortfolioValueHistoryPeriod } from '../../consts/portfolio-value-history-period.enum'
import { PortfolioDomain } from '../../domains/portfolio/portfolio.domain'
import { CurrentUser } from '../../shared/decorators/current-user.decorator'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { Auth } from '../../shared/guards/auth/auth-decorators.guard'
import {
  PortfolioAddCompanyRequestDto,
  PortfolioCreateOrUpdateTrade,
  PortfolioResponseDto,
  PortfolioValueHistory,
} from './portfolio.dto'

@Controller('portfolio')
@ApiTags('portfolio')
export class PortfolioController {
  constructor(
    private portfolioDomain: PortfolioDomain,
  ) {}

  @Get()
  @ApiProperty()
  @Auth()
  @ApiOkResponse({ type: PortfolioResponseDto })
  getMyPortfolio(
    @CurrentUser() user: CurrentUser,
  ): Promise<PortfolioResponseDto> {
    return this.portfolioDomain.getMyPortfolio(user)
  }

  @Get('my-value-history')
  @Auth()
  @ApiProperty()
  async getMyValueHistory(
    @CurrentUser() user: CurrentUser,
    @Query('period') period: PortfolioValueHistoryPeriod,
  ): Promise<ItemsWithCount<PortfolioValueHistory>> {
    const result = await this.portfolioDomain.getMyHistoryByPeriod(user.id, period)
    return {
      count: result.length,
      items: result,
    }
  }

  @Post()
  @ApiOkResponse({ description: 'Company added' })
  @Auth()
  addToMyPortfolio(
    @CurrentUser() user: CurrentUser,
    @Body() dto: PortfolioAddCompanyRequestDto,
  ): Promise<void> {
    return this.portfolioDomain.addCompany(user.id, dto)
  }

  @Put(':id/trade')
  @ApiProperty()
  @Auth()
  createOrUpdateTrade(
    @Param('id') id: string,
    @CurrentUser() user: CurrentUser,
    @Body() dto: PortfolioCreateOrUpdateTrade,
  ): Promise<void> {
    return this.portfolioDomain.updateTrade({
      id,
      userId: user.id,
      trade: dto,
    })
  }

  @Delete(':ids')
  @ApiProperty()
  @Auth()
  async removeFromMyPortfolio(
    @CurrentUser() user: CurrentUser,
    @Param('ids', new ParseArrayPipe()) ids: string[],
  ): Promise<any> {
    return this.portfolioDomain.removeManyFromMyPortfolio(ids, user.id)
  }

  @Delete(':id/trades/:tradeIds')
  @ApiProperty()
  @Auth()
  async removeManyMyTrades(
    @CurrentUser() user: CurrentUser,
    @Param('id') id: string,
    @Param('tradeIds', new ParseArrayPipe()) tradeIds: string[],
  ): Promise<any> {
    return this.portfolioDomain.removeManyTradesByTradeIdAndUserIdAndPortfolioId(user.id, id, tradeIds)
  }
}
