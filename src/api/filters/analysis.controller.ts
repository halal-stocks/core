import { Controller, Get, Param } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { TickerType } from '../../consts/report.enum'
import { FiltersDomain } from '../../domains/filters/filters.domain'
import { FilterShariahFilterAndFullAnalysisResponse } from '../../domains/filters/filters.interfaces'
import { GrowthAnalysisGetResultResponse } from '../../domains/filters/growth-analysis/growth-analysis.interfaces'
import { ShariahFilterPassThroughFilterResponse } from '../../domains/filters/shariah-filter/shariah-filter.interfaces'
import { ValueAnalysisResultResponse } from '../../domains/filters/value-analysis/value-analysis.interfaces'

@Controller('analysis')
@ApiTags('Analysis')
export class AnalysisController {
  constructor(
    private filtersDomain: FiltersDomain,
  ) {}

  @Get('shariah/:ticker')
  passThroughShariahFilter(@Param('ticker') ticker: TickerType): Promise<ShariahFilterPassThroughFilterResponse> {
    return this.filtersDomain.passThroughShariahFilter(ticker)
  }

  @Get('value/:ticker')
  passThroughCompanyValueFilter(@Param('ticker') ticker: TickerType): Promise<ValueAnalysisResultResponse> {
    return this.filtersDomain.passThroughCompanyValueAnalysis(ticker)
  }

  @Get('growth/:ticker')
  passThroughCompanyGrowthFilter(@Param('ticker') ticker: TickerType): Promise<GrowthAnalysisGetResultResponse> {
    return this.filtersDomain.passThroughCompanyGrowthFilter(ticker)
  }

  @Get('all/:ticker')
  passThroughAllFilters(@Param('ticker') ticker: TickerType): Promise<FilterShariahFilterAndFullAnalysisResponse> {
    return this.filtersDomain.passThroughAllFilters(ticker)
  }

  @Get('test/:ticker')
  testNew(@Param('ticker') ticker: TickerType): Promise<any> {
    return this.filtersDomain.testNew(ticker)
  }

}

