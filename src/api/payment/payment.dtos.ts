import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Transform } from 'class-transformer'
import { IsDefined, IsEnum, IsNumber, IsOptional, IsString, IsUUID } from 'class-validator'
import { Currency } from '../../consts/currency.enum'
import { Language } from '../../consts/language'
import { PaymentStatus } from '../../domains/payment/payment.interfaces'
import stringToNumber from '../../utils/number/string-to-number/string-to-number.util'

export class PaymentCreateSubscriptionRequestDto {
  @ApiProperty()
  @IsUUID()
  @IsDefined()
  priceId: string

  @ApiPropertyOptional({ enum: Language })
  @IsEnum(Language)
  @IsOptional()
  language: Language
}

export class PaymentCreatSubscriptionResponseDto {
  @ApiProperty()
  paymentUrl: string
}


export class PaymentRobokassaResultRequestDto {
  @ApiProperty()
  @Transform(({ value }) => stringToNumber(value))
  @IsNumber()
  out_summ: number

  @ApiProperty()
  @Transform(({ value }) => stringToNumber(value))
  @IsNumber()
  OutSum: number

  @ApiProperty()
  @Transform(({ value }) => stringToNumber(value))
  @IsNumber()
  inv_id: number

  @ApiProperty()
  @Transform(({ value }) => stringToNumber(value))
  @IsNumber()
  InvId: number

  @ApiProperty()
  @IsString()
  crc: string

  @ApiProperty()
  @IsString()
  SignatureValue: string

  @ApiProperty()
  @IsString()
  PaymentMethod: string

  @ApiProperty()
  @IsString()
  IncSum: string

  @ApiProperty()
  @IsString()
  IncCurrLabel: string

  @ApiProperty()
  @IsString()
  IsTest: string

  @ApiProperty()
  @IsString()
  EMail: string

  @ApiProperty()
  @Transform(({ value }) => stringToNumber(value))
  @IsNumber()
  Fee: number

  @ApiProperty()
  @IsString()
  Shp_user: string
}

export class RobokassaSuccessRequestDto {
  @Transform(({ value }) => stringToNumber(value))
  @IsNumber()
  @ApiProperty()
  OutSum: number

  @Transform(({ value }) => stringToNumber(value))
  @IsNumber()
  @ApiProperty()
  InvId: number

  @ApiProperty()
  @IsString()
  Shp_user: string

  @ApiProperty()
  @IsString()
  SignatureValue: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  IsTest?: '1'

  @ApiProperty()
  @IsString()
  Culture: Language
}

export class PaymentSuccessOrFailWebhookResponseDto {
  @ApiProperty()
  url: string
}

export class PaymentResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  invoiceId: number

  @ApiProperty()
  status: PaymentStatus

  @ApiProperty()
  paymentUrl: string

  @ApiProperty()
  currency: Currency

  @ApiProperty()
  priceId: string

  @ApiProperty()
  price: number

  @ApiProperty()
  createdAt: Date

  @ApiProperty()
  updatedAt: Date | null

  @ApiProperty()
  deletedAt: Date | null
}
