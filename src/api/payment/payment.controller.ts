import { Body, Controller, Post, Redirect, Req } from '@nestjs/common'
import { ApiOkResponse, ApiProperty } from '@nestjs/swagger'
import { PaymentDomain } from '../../domains/payment/payment.domain'
import { CurrentUser } from '../../shared/decorators/current-user.decorator'
import { Auth } from '../../shared/guards/auth/auth-decorators.guard'
import {
  PaymentCreateSubscriptionRequestDto,
  PaymentCreatSubscriptionResponseDto,
  PaymentRobokassaResultRequestDto,
  PaymentSuccessOrFailWebhookResponseDto,
  RobokassaSuccessRequestDto,
} from './payment.dtos'

@Controller('payment')
export class PaymentController {
  constructor(
    private paymentDomain: PaymentDomain,
  ) {}

  @Post('webhook/result')
  @ApiProperty()
  resultWebhook(
    @Body() dto: PaymentRobokassaResultRequestDto,
  ): Promise<string> {
    return this.paymentDomain.resultWebhook(dto)
  }

  @Post('webhook/fail')
  @ApiProperty()
  @Redirect()
  async failWebhook(
    @Body() dto: any,
    @Req() req,
  ): Promise<PaymentSuccessOrFailWebhookResponseDto> {
    const url = await this.paymentDomain.failWebhook(dto)
    return { url }
  }

  @Post('webhook/success')
  @ApiProperty()
  @ApiOkResponse({ type: PaymentSuccessOrFailWebhookResponseDto })
  @Redirect()
  async successWebhook(
    @Body() dto: RobokassaSuccessRequestDto,
  ): Promise<PaymentSuccessOrFailWebhookResponseDto> {
    const url = await this.paymentDomain.successWebhook(dto)
    return { url }
  }

  @Post('create-subscription')
  @ApiProperty()
  @Auth()
  createSubscription(
    @Body() dto: PaymentCreateSubscriptionRequestDto,
    @CurrentUser() user: CurrentUser,
  ): Promise<PaymentCreatSubscriptionResponseDto> {
    return this.paymentDomain.createSubscription(user, dto)
  }

  @Post('cancel-subscription')
  @ApiProperty()
  @Auth()
  cancelMySubscription(
    @CurrentUser() user: CurrentUser,
  ): Promise<void> {
    return this.paymentDomain.cancelSubscriptionByUserId(user.id)
  }
}
