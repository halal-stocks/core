import { ApiProperty } from '@nestjs/swagger'
import { CompanySector } from '../../consts/company-sector'
import { Exchange } from '../../consts/exchange.enum'
import {
  BetaType,
  DividendYieldType,
  EPSType,
  IsCompanyHalalType,
  MarketCapType,
  PBRatioType,
  PERatioType,
  PSRatioType,
  StockPriceType,
  TickerType,
} from '../../consts/report.enum'
import { StockMarketIndex } from '../../consts/stock-market-index'
import { LockableData } from '../../shared/generics/lockable-data.generic'
import { MinMaxValue } from '../../shared/generics/min-max.generic'

export class ScreenerAnalyzesResponseDto {
  @ApiProperty()
  beta: BetaType

  @ApiProperty()
  pbRatio: PBRatioType

  @ApiProperty()
  eps: EPSType

  @ApiProperty()
  peRatio: PERatioType

  @ApiProperty()
  psRatio: PSRatioType

  @ApiProperty()
  dividendYield: DividendYieldType
}

export class ScreenerIndicatorsResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  actualStockPrice: StockPriceType

  @ApiProperty()
  actualMarketCap: MarketCapType

  @ApiProperty()
  previousDayStockPrice: StockPriceType

  @ApiProperty()
  updatedAt: Date | null
}

export class ScreenerResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  ticker: TickerType

  @ApiProperty()
  name: string

  @ApiProperty()
  description: string | null

  @ApiProperty()
  fullTimeEmployees: number | null

  @ApiProperty()
  industry: string | null

  @ApiProperty()
  sector: CompanySector

  @ApiProperty()
  exchange: Exchange

  @ApiProperty()
  isHalal: LockableData<IsCompanyHalalType>

  @ApiProperty({ type: ScreenerIndicatorsResponseDto })
  indicators: ScreenerIndicatorsResponseDto

  @ApiProperty({ type: ScreenerAnalyzesResponseDto })
  analysis: ScreenerAnalyzesResponseDto | {}

  @ApiProperty()
  createdAt: Date

  @ApiProperty()
  updatedAt: Date | null

  @ApiProperty()
  deletedAt: Date | null
}

export class ScreenerFilterOptionsResponseDto {
  stockPrice: MinMaxValue<StockPriceType>
  marketCap: MinMaxValue<MarketCapType>
  peRatio: MinMaxValue<PERatioType>
  psRatio: MinMaxValue<PSRatioType>
  beta: MinMaxValue<BetaType>
  eps: MinMaxValue<EPSType>
  pbRatio: MinMaxValue<PBRatioType>
  dividendYield: MinMaxValue<DividendYieldType>
}

export interface ScreenerFiltersRequestDto {
  peRatioMIn?: PERatioType
  peRatioMax?: PERatioType
  epsMin?: EPSType
  epsMax?: EPSType
  betaMin?: BetaType
  betaMax?: BetaType
  beta?: BetaType
  dividendYieldMin?: DividendYieldType
  dividendYieldMax?: DividendYieldType
  dividendYield?: DividendYieldType
  actualStockPriceMin?: StockPriceType
  actualStockPriceMax?: StockPriceType
  actualMarketCapMin?: MarketCapType
  actualMarketCapMax?: MarketCapType
  isHalal?: IsCompanyHalalType[]
  index?: StockMarketIndex
  sector?: CompanySector
  ticker?: TickerType[]
}

export const screenerAllowedFieldsForSorting = [
  'actualStockPrice',
  'actualMarketCap',
  'peRatio',
  'psRatio',
  'pbRatio',
  'eps',
  'beta',
  'dividendYield',
]
