import { Controller, Get } from '@nestjs/common'
import { ApiProperty, ApiQuery, ApiTags } from '@nestjs/swagger'
import { CompanySector } from '../../consts/company-sector'
import { StockMarketIndex } from '../../consts/stock-market-index'
import { ScreenerDomain } from '../../domains/screener/screener.domain'
import { CurrentUser, CurrentUserOptional } from '../../shared/decorators/current-user.decorator'
import { Filter } from '../../shared/decorators/filter/filter.decorator'
import { QueryValueType } from '../../shared/decorators/filter/filter.interfaces'
import { Sort, SortDto } from '../../shared/decorators/sort.decorator'
import { ItemsWithCount } from '../../shared/generics/items-with-count'
import { Auth } from '../../shared/guards/auth/auth-decorators.guard'
import {
  screenerAllowedFieldsForSorting,
  ScreenerFilterOptionsResponseDto,
  ScreenerFiltersRequestDto,
  ScreenerResponseDto,
} from './screener.dtos'

@Controller('screener')
@ApiTags('screener')
export class ScreenerController {
  constructor(
    private screenerDomain: ScreenerDomain,
  ) {}

  @Get()
  @ApiQuery({ name: 'minPERatio', required: false, type: Number })
  @ApiQuery({ name: 'maxPERatio', required: false, type: Number })
  @ApiQuery({ name: 'minEPS', required: false, type: Number })
  @ApiQuery({ name: 'maxEPS', required: false, type: Number })
  @ApiQuery({ name: 'minBeta', required: false, type: Number })
  @ApiQuery({ name: 'maxBeta', required: false, type: Number })
  @ApiQuery({ name: 'beta', required: false, type: Number })
  @ApiQuery({ name: 'minDividendYield', required: false, type: Number })
  @ApiQuery({ name: 'maxDividendYield', required: false, type: Number })
  @ApiQuery({ name: 'dividendYield', required: false, type: Number })
  @ApiQuery({ name: 'minPrice', required: false, type: Number })
  @ApiQuery({ name: 'maxPrice', required: false, type: Number })
  @ApiQuery({ name: 'isHalal', required: false, type: Boolean })
  @ApiQuery({ name: 'index', required: false, type: () => StockMarketIndex })
  @ApiQuery({ name: 'sector', required: false, type: () => CompanySector })
  @ApiProperty()
  @Auth({ isRequired: false })
  async getAllForScreener(
    @Sort({ allowedFieldsForSorting: screenerAllowedFieldsForSorting }) sort: SortDto,
    @Filter({
      actualMarketCapMin: QueryValueType.number,
      actualMarketCapMax: QueryValueType.number,
      peRatioMin: QueryValueType.number,
      peRatioMax: QueryValueType.number,
      epsMin: QueryValueType.number,
      epsMax: QueryValueType.number,
      betaMin: QueryValueType.number,
      betaMax: QueryValueType.number,
      dividendYieldMin: QueryValueType.number,
      dividendYieldMax: QueryValueType.number,
      actualStockPriceMin: QueryValueType.number,
      actualStockPriceMax: QueryValueType.number,
      isHalal: { type: QueryValueType.boolean, isArray: true, isNullable: true },
      index: { type: QueryValueType.enum, enum: StockMarketIndex },
      sector: { type: QueryValueType.enum, enum: CompanySector },
      ticker: { type: QueryValueType.string, isArray: true },
    }) filters: ScreenerFiltersRequestDto,
    @CurrentUser({ required: false }) user?: CurrentUserOptional,
  ): Promise<ItemsWithCount<ScreenerResponseDto>> {
    return this.screenerDomain.getAllForScreener(
      user?.isPaid,
      {
        sort,
        filters,
      },
    )
  }

  @Get('filter-options')
  @ApiProperty()
  getScreenerFilterOptions(): Promise<ScreenerFilterOptionsResponseDto> {
    return this.screenerDomain.getScreenerFilterOptions()
  }

  @Get('russian-stocks')
  @ApiProperty()
  getRussiansStocks(): Promise<any> {
    return this.screenerDomain.getRussiansStocks()
  }
}
