import { Body, Controller, Post } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { CompanyShariahFilterResultDomain } from '../../domains/company/shariah-filter-result/shariah-filter-result.domain'
import { ShariahFilterSetCharterAsValidDto } from './shariah-filter.dto'

@Controller('shariah-filter')
export class ShariahFilterController {
  constructor(
    private shariahFilterResultDomain: CompanyShariahFilterResultDomain,
  ) {}

  @Post('set-charter-as-valid')
  @ApiProperty()
  setCharterAsValid(@Body() dto: ShariahFilterSetCharterAsValidDto): Promise<void> {
    return this.shariahFilterResultDomain.setCharterAsValid(dto.tickers)
  }
}
