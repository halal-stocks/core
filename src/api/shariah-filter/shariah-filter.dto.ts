import { ApiProperty } from '@nestjs/swagger'
import { IsArray, IsDefined, IsString } from 'class-validator'
import { TickerType } from '../../consts/report.enum'

export class ShariahFilterSetCharterAsValidDto {
  @ApiProperty()
  @IsDefined()
  @IsString({ each: true })
  @IsArray()
  tickers: TickerType[]
}
