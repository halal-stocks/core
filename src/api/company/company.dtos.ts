import { ApiProperty } from '@nestjs/swagger'
import { IsDefined, IsString } from 'class-validator'
import { CompanySector } from '../../consts/company-sector'
import { Exchange } from '../../consts/exchange.enum'
import {
  BetaType,
  DepositToMarketCapRatioType,
  DeptToMarketCapRatioType,
  DividendYieldType,
  EPSType,
  ForbiddenIncomeRatioType,
  IsCompanyFreeToUse,
  IsCompanyHalalType,
  MarketCapType,
  PBRatioType,
  PERatioType,
  PSRatioType,
  StockPriceType,
  TickerType,
} from '../../consts/report.enum'
import { StockMarketIndexResponse } from '../../domains/company/stock-market-index/stock-market-index.interfaces'
import { LockableData } from '../../shared/generics/lockable-data.generic'

export class CompanyCreateRequestDto {
  @IsString()
  @IsDefined()
  @ApiProperty()
  ticker: TickerType
}

export class CompanySearchByTickerResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  ticker: TickerType

  @ApiProperty()
  name: string

  @ApiProperty({ enum: Exchange })
  exchange: Exchange
}

export class CompanyBriefResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  ticker: TickerType

  @ApiProperty()
  name: string

  @ApiProperty()
  stockPrice: StockPriceType

  @ApiProperty()
  dailyChangePercentage: number

  @ApiProperty({ nullable: true })
  isHalal: LockableData<IsCompanyHalalType>
}

export class CompanyDetailedResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  ticker: TickerType

  @ApiProperty()
  industry: string | null

  @ApiProperty()
  sector: CompanySector

  @ApiProperty()
  employeeNumber: number | null

  @ApiProperty()
  description: string | null

  @ApiProperty()
  indexes: StockMarketIndexResponse[]

  @ApiProperty()
  isHalal: LockableData<IsCompanyHalalType>

  @ApiProperty()
  dailyChange: number

  @ApiProperty()
  dailyChangePercentage: number

  @ApiProperty()
  indicators: Omit<CompanyIndicatorsResponseDto, 'company'>

  analyze: {
    epsRatio: EPSType | null
    psRatio: PSRatioType | null
    beta: BetaType | null
    peRatio: PERatioType | null
    pbRatio: PBRatioType | null
    dividendYield: DividendYieldType | null
  }
  shariahFilterResults: LockableData<{
    depositToMarketCap: DepositToMarketCapRatioType | null
    deptToMarketCap: DeptToMarketCapRatioType | null
    forbiddenIncome: ForbiddenIncomeRatioType | null
    isCharterValid: boolean | null
    linkToCharter: string | null
    charterNotValidReason: string | null
  }>
}

export class CompanyHistoricalPriceResponseDto {
  @ApiProperty()
  date: string

  @ApiProperty()
  stockPrice: StockPriceType
}

export class CompanyIndicatorsResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  actualStockPrice: StockPriceType

  @ApiProperty()
  actualMarketCap: MarketCapType

  @ApiProperty()
  previousDayStockPrice: StockPriceType

  @ApiProperty()
  updatedAt: Date | null

  @ApiProperty()
  company: {
    id: string
    ticker: TickerType
  }
}

export class CompanyTinyResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  ticker: TickerType

  @ApiProperty()
  name: string

  @ApiProperty()
  isFree: IsCompanyFreeToUse
}
