import { Body, Controller, Delete, Get, Param, Post, Query } from '@nestjs/common'
import { ApiProperty, ApiTags } from '@nestjs/swagger'
import { CompanySector } from '../../consts/company-sector'
import { TickerType } from '../../consts/report.enum'
import { CompanyIndicatorsDomain } from '../../domains/company/indicators/indicators.domain'
import { CompanyMutationDomain } from '../../domains/company/subdomains/mutation/company-mutation.domain'
import { CompanyQueryDomain } from '../../domains/company/subdomains/query/company-query.domain'
import { CompanySchedulerDomain } from '../../domains/company/subdomains/scheduler/company-scheduler.domain'
import { CurrentUser, CurrentUserOptional } from '../../shared/decorators/current-user.decorator'
import { Auth } from '../../shared/guards/auth/auth-decorators.guard'
import { ParseOptionalIntPipe } from '../../shared/pipes/parseOptionalInt.pipe'
import {
  CompanyBriefResponseDto,
  CompanyCreateRequestDto,
  CompanyDetailedResponseDto,
  CompanyHistoricalPriceResponseDto,
  CompanySearchByTickerResponseDto,
} from './company.dtos'

@Controller('company')
@ApiTags('company')
export class CompanyController {
  constructor(
    private companyIndicatorsDomain: CompanyIndicatorsDomain,
    private companyQueryDomain: CompanyQueryDomain,
    private companyMutationDomain: CompanyMutationDomain,
    private companySchedulerDomain: CompanySchedulerDomain,
  ) {}

  @Get('test/:ticker')
  test(
    @Param('ticker') ticker: TickerType,
  ): Promise<any> {
    return this.companyMutationDomain.test(ticker)
  }

  @Post()
  @ApiProperty()
  createOne(@Body() dto: CompanyCreateRequestDto): Promise<any> {
    return this.companyMutationDomain.createOne(dto)
  }

  @Post('many')
  @ApiProperty()
  createMany(@Body() dtos: CompanyCreateRequestDto[]): Promise<void> {
    return this.companyMutationDomain.createMany(dtos)
  }

  @Get('brief-companies-by-sector/:sector')
  @ApiProperty()
  @Auth({ isRequired: false })
  getBriefCompaniesBySector(
    @Param('sector') sector: CompanySector,
    @CurrentUser({ required: false }) user: CurrentUserOptional,
  ): Promise<CompanyBriefResponseDto[]> {
    return this.companyQueryDomain.getBriefCompaniesBySector(sector, user?.isPaid)
  }

  @Get('brief-companies-by-index/index-code/:indexCode')
  @ApiProperty()
  @Auth({ isRequired: false })
  getBriefCompaniesByIndex(
    @Param('indexCode') indexCode: string,
    @CurrentUser({ required: false }) user: CurrentUserOptional,
  ): Promise<CompanyBriefResponseDto[]> {
    return this.companyQueryDomain.getBriefCompaniesByIndex(indexCode, user?.isPaid)
  }

  @Get('search-by-ticker-or-name/:tickerOrName')
  @ApiProperty()
  findManyByTickerPartialOrName(@Param('tickerOrName') tickerOrName: TickerType | string): Promise<CompanySearchByTickerResponseDto[]> {
    return this.companyQueryDomain.findManyByTickerPartialOrName(tickerOrName)
  }

  @Post('create-all-from-sp500')
  @ApiProperty()
  scheduleCreationCompaniesFromSP500(): Promise<void> {
    return this.companySchedulerDomain.scheduleCreationCompaniesFromSP500()
  }

  @Post('create-all')
  @ApiProperty()
  scheduleCreationAllCompanies(): Promise<void> {
    return this.companySchedulerDomain.scheduleCreationAllCompanies()
  }

  @Get('indicators/:ticker')
  @ApiProperty()
  getCompanyIndicators(@Param('ticker') ticker: TickerType): Promise<any> {
    return this.companyIndicatorsDomain.getOneByTicker(ticker)
  }

  @Post('update-full-analysis-to-all')
  @ApiProperty()
  scheduleUpdateFullAnalysisToAllCompanies(): Promise<void> {
    return this.companySchedulerDomain.scheduleUpdateFullAnalysisToAllCompanies()
  }

  @Post('update-is-halal-conclusion-to-all')
  @ApiProperty()
  scheduleUpdateIsHalalConclusionToAll(): Promise<void> {
    return this.companySchedulerDomain.scheduleUpdateIsHalalConclusionToAll()
  }

  @Post('update-shariah-filter-result-to-all')
  @ApiProperty()
  scheduleUpdateShariahFilterResultToAllCompanies(): Promise<void> {
    return this.companySchedulerDomain.scheduleUpdateShariahFilterResultToAllCompanies()
  }

  @Get('historical-price-brief/:ticker')
  @ApiProperty()
  getCompanyHistoricalPrice(
    @Param('ticker') ticker: TickerType,
    @Query('interval', new ParseOptionalIntPipe()) interval: number,
  ): Promise<CompanyHistoricalPriceResponseDto[]> {
    return this.companyQueryDomain.getCompanyHistoricalPrice(ticker, interval)
  }

  @Get('get-all-not-complete-companies')
  @ApiProperty()
  getAllNotCompleteCompanies(): Promise<any> {
    return this.companyQueryDomain.getAllNotCompleteCompanies()
  }

  @Get(':ticker')
  @ApiProperty()
  @Auth({ isRequired: false })
  getOneDetailedByTicker(
    @Param('ticker') ticker: TickerType,
    @CurrentUser({ required: false }) user: CurrentUserOptional,
  ): Promise<CompanyDetailedResponseDto> {
    return this.companyQueryDomain.getOneDetailedByTicker(ticker, user?.isPaid)
  }

  @Post('add-initial-indicators/:ticker')
  @ApiProperty()
  addIndicatorsToCompany(@Param('ticker') ticker: TickerType): Promise<void> {
    return this.companyIndicatorsDomain.addInitialIndicators(ticker)
  }

  @Delete(':ticker')
  @ApiProperty()
  deleteByTicker(@Param('ticker') ticker: TickerType): Promise<void> {
    return this.companyMutationDomain.deleteByTicker(ticker.split(',') as TickerType[])
  }
}


