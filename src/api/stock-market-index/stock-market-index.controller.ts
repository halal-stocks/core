import { Body, Controller, Get, Param, Post } from '@nestjs/common'
import { ApiProperty, ApiTags } from '@nestjs/swagger'
import { StockMarketIndex } from '../../consts/stock-market-index'
import { StockMarketIndexDomain } from '../../domains/company/stock-market-index/stock-market-index.domain'
import { toUpperCase } from '../../utils'
import {
  StockMarketIndexCompaniesFromSP500ResponseDto,
  StockMarketIndexCreateRequestDto,
  StockMarketIndexResponseDto,
} from './stock-market-index.dto'

@Controller('stock-market-index')
@ApiTags('stock-market-index')
export class StockMarketIndexController {
  constructor(
    private stockMarketIndexDomain: StockMarketIndexDomain,
  ) {}

  @Get(':code')
  @ApiProperty()
  getIndexListFromDataSourceByCode(@Param('code') code: StockMarketIndex): Promise<StockMarketIndexCompaniesFromSP500ResponseDto[]> {
    return this.stockMarketIndexDomain.getIndexListFromDataSourceByCode(toUpperCase<StockMarketIndex>(code))
  }

  @Post()
  @ApiProperty()
  createOne(@Body() dto: StockMarketIndexCreateRequestDto): Promise<StockMarketIndexResponseDto> {
    return this.stockMarketIndexDomain.createOne(dto)
  }

  @Post('update-all-indexes')
  @ApiProperty()
  updateAllIndexes(): Promise<void> {
    return this.stockMarketIndexDomain.updateAllIndexes()
  }
}
