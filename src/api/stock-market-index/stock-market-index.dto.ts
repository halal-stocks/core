import { ApiProperty } from '@nestjs/swagger'
import { Transform, TransformFnParams } from 'class-transformer'
import { IsEnum, IsString } from 'class-validator'
import { TickerType } from '../../consts/report.enum'
import { StockMarketIndex } from '../../consts/stock-market-index'

export class StockMarketIndexCompaniesFromSP500ResponseDto {
  @ApiProperty()
  name: string

  @ApiProperty()
  sector: string

  @ApiProperty()
  ticker: TickerType
}

export class StockMarketIndexCreateRequestDto {
  @ApiProperty({ enum: StockMarketIndex })
  @IsEnum(StockMarketIndex)
  @Transform((params: TransformFnParams) => params.value.toLocaleUpperCase())
  code: StockMarketIndex

  @ApiProperty()
  @IsString()
  name: string
}

export class StockMarketIndexResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty({ enum: StockMarketIndex })
  code: StockMarketIndex

  @ApiProperty()
  name: string
}
