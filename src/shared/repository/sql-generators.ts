import { convertDateToDBFormat } from '../../utils'
import { DateRange, NumberRange } from '../decorators/filter/filter.interfaces'

export const sqlSearchByTwoColumns = (firstColumnName: string, lastColumnName, value: string) => {
  return `(CONCAT_WS(\' \', ${firstColumnName}, ${lastColumnName}) ILIKE '%${value}%')
          OR CONCAT_WS(\' \', ${lastColumnName}, ${firstColumnName}) ILIKE '%${value}%'
          `
}

export const sqlSearchByString = (columnName: string, value: string) => {
  return `${columnName} ILIKE '%${value}%'`
}

export const sqlSearchByArrayOfStrings = (columnName: string, values: string[]) => {
  return `${columnName} ILIKE ANY (ARRAY[${values.map(it => (`'%${it}%'`))}])`
}

export const sqlFilterByDateRange = (columnName: string, date: DateRange): string | undefined => {
  const { from, to } = date
  const convertedFrom = from && convertDateToDBFormat(from)
  const convertedTo = to && convertDateToDBFormat(to)
  if (convertedFrom && convertedTo) {
    return `${columnName} BETWEEN '${convertedFrom}' AND '${convertedTo}'`
  }
  if (convertedFrom) {
    return `${columnName} >= '${convertedFrom}'`
  }
  if (convertedTo) {
    return `${columnName} <= '${convertedTo}'`
  }
  return undefined
}

export const sqlFilterByNumberRange = (columnName: string, date: NumberRange): string | undefined => {
  const { from, to, exact } = date
  if (exact) {
    return `${columnName} = ${exact}`
  }
  if (from && to) {
    return `${columnName} BETWEEN ${from} AND ${to}`
  }
  if (from) {
    return `${columnName} >= ${from}`
  }
  if (to) {
    return `${columnName} <= ${to}`
  }
  return undefined
}

export const sqlFilterByLastSequenceDays = (columnName: string, days: number = 30): string => {
  return `${columnName} >= current_timestamp - interval \'${days} day\'`
}

export const sqlFilterBySpecificDays = (columnName: string, days: number[], limit?: Date | string): string => {
  const byDaysFilter = days.map(it => `extract(day from ${columnName}) = ${it}`).join(' or ')
  return limit ? `(${byDaysFilter}) AND ${columnName} >= '${limit}'` : byDaysFilter
}
