import { SortDto } from '../decorators/sort.decorator'

export class SortAndFilters<F> {
  sort?: SortDto
  filters?: F
}
