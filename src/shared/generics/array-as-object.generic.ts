type ArrayAsObject<T> = {
  [key: string]: T
}

export default ArrayAsObject
