export type LockableData<T> = T | 'locked'
