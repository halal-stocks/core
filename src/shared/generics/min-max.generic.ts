export interface MinMaxValue<T = number> {
  min: T
  max: T
}
