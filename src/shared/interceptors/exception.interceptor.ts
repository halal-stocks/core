import { CallHandler, ExecutionContext, HttpException, Injectable, NestInterceptor } from '@nestjs/common'
import jwt_decode, { JwtPayload } from 'jwt-decode'
import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { LogsService } from '../../persistence/logs/logs.service'
import { isNullOrUndefined } from '../../utils'

@Injectable()
export class ExceptionInterceptor implements NestInterceptor {
  constructor(
    private logsService: LogsService,
  ) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(
        catchError(error => {
            this.logError(context, error)
            return throwError(error)
          },
        ),
      )
  }

  async logError(context: ExecutionContext, error: HttpException | string): Promise<void> {
    const request = context.switchToHttp().getRequest()
    const requestLogs = {
      path: request.originalUrl,
      body: request.body,
      query: request.query,
    }

    await this.logsService.addLog({
      request: JSON.stringify(requestLogs),
      error: error instanceof HttpException ? error.stack : JSON.stringify(error),
      status: error instanceof HttpException ? error.getStatus() : 500,
      userFirebaseId: this.getFirebaseUserId(request.headers.authorization),
    })
  }

  getFirebaseUserId(bearerToken: string | undefined | null): string | null {
    if (isNullOrUndefined(bearerToken)) {
      return null
    }
    const decoded = jwt_decode<JwtPayload>(bearerToken.replace('Bearer ', ''))
    return decoded.sub ?? null
  }
}
