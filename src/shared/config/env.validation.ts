import { plainToClass, Transform } from 'class-transformer'
import { IsEnum, IsNumber, IsOptional, IsString, validateSync } from 'class-validator'
import { NodeEnv } from '../../consts/system.env'
import { stringToBoolean } from '../../utils'
import stringToNumber from '../../utils/number/string-to-number/string-to-number.util'

class EnvironmentVariables {
  @IsEnum(NodeEnv)
  @IsOptional()
  NODE_ENV: NodeEnv

  @IsNumber()
  @Transform(({ value }) => stringToNumber(value))
  PG_PORT: number

  @IsString()
  PG_HOST: string

  @IsString()
  PG_USER: string

  @IsString()
  PG_PASSWORD: string

  @IsString()
  PG_DATABASE: string

  @IsString()
  AUTH_API_KEY: string

  @IsString()
  RABBITMQ_URI: string

  @IsString()
  FMP_API_KEY: string

  @IsString()
  FMP_API_PATH: string

  @IsString()
  FMP_API_VERSION: string

  @IsNumber()
  @Transform(({ value }) => stringToNumber(value))
  SHARIAH_ALLOWED_DEPOSIT_TO_MARKET_CAP_RATIO: number

  @IsNumber()
  @Transform(({ value }) => stringToNumber(value))
  SHARIAH_ALLOWED_DEPT_TO_MARKET_CAP_RATIO: number

  @IsNumber()
  @Transform(({ value }) => stringToNumber(value))
  SHARIAH_ALLOWED_FORBIDDEN_INCOME_RATIO: number

  @IsString()
  ROBOKASSA_FIRST_PASSWORD: string

  @IsString()
  ROBOKASSA_SECOND_PASSWORD: string

  @IsString()
  ROBOKASSA_MERCHANT_LOGIN: string

  @IsString()
  ROBOKASSA_API_URL: string

  @Transform(({ value }) => stringToBoolean(value))
  ROBOKASSA_IS_PRODUCTION_MODE: boolean

  @IsString()
  BILLING_PLANS: string

  @IsString()
  CLIENT_URL: string
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToClass(
    EnvironmentVariables,
    config,
  )

  const errors = validateSync(validatedConfig, { skipMissingProperties: false })

  if (errors.length > 0) {
    throw new Error(errors.toString())
  }
  return validatedConfig
}
