import { BadRequestException, InternalServerErrorException } from '@nestjs/common'
import { isArray, isObject, isString, isType, removeNewLines, trim } from '../../../utils'
import { BooleanValue, EnumValue, QueryValueType, StringValue } from './filter.interfaces'

export const filterStrategy = {
  [QueryValueType.number]: (query: any, key: string, value?: any) => prepareDataForNumberFormat(query, key),
  [QueryValueType.string]: (query: any, key: string, value?: StringValue) => prepareDataForStringFormat(query, key, value),
  [QueryValueType.arrayOfNumbers]: (query: any, key: string, value?: any) => prepareDataForArrayOfNumberFormat(query, key),
  [QueryValueType.arrayOfStrings]: (query: any, key: string, value?: any) => prepareDataForArrayOfStringFormat(query, key),
  [QueryValueType.boolean]: (query: any, key: string, value?: BooleanValue) => prepareDataForBooleanFormat(query, key, value),
  [QueryValueType.date]: (query: any, key: string, value?: any) => prepareDataForDateFormat(query, key),
  [QueryValueType.dateRange]: (query: any, key: string, value?: any) => prepareDataForDateRangeFormat(query, key),
  [QueryValueType.enum]: (query: any, key: string, value?: QueryValueType | EnumValue) => prepareDataForEnumFormat(query, key, value),
  [QueryValueType.existence]: (query: any, key: string, value?: any) => prepareDataForExistenceFormat(query, key),
}

const prepareDataForNumberFormat = (query: string, key: string): number => {
  try {
    return parseInt(query, 10)
  } catch (e) {
    throw new BadRequestException(`Wrong type of filter ${key}. Must be ${QueryValueType.number}`)
  }
}

const prepareDataForArrayOfNumberFormat = (query: any, key: string): number[] => {
  try {
    const value = isArray(query) ? query : query.split(',')
    return value.map(it => parseInt(it, 10))
  } catch (e) {
    throw new BadRequestException(`Wrong type of filter ${key}. Must be ${QueryValueType.arrayOfNumbers}`)
  }
}

const prepareDataForArrayOfStringFormat = (query: any, key: string) => {
  if (!isArray(query)) {
    throw new BadRequestException(`Wrong type of filter ${key}. Must be ${QueryValueType.arrayOfStrings}`)
  }
  return query
}

const prepareDataForBooleanFormat = (query: any, key: string, value?: BooleanValue) => {
  if (value?.isArray && !isArray(query)) {
    throw new BadRequestException(`Validation failed (array of boolean string is expected for '${key}' field)`)
  }

  if (!value?.isArray && isArray(query)) {
    throw new BadRequestException(`Validation failed (Boolean string is expected for '${key}' field)`)
  }

  return value?.isArray ? query.map(it => parseStringBoolean(key, it, value?.isNullable)) : parseStringBoolean(key, query, value?.isNullable)
}

const parseStringBoolean = (key: string, query: string | boolean | null, isNullable?: boolean): boolean | null => {
  if (query === true || query === 'true') {
    return true
  }
  if (query === false || query === 'false') {
    return false
  }

  if ((query === null || query === 'null') && isNullable) {
    return null
  }

  throw new BadRequestException(`Validation failed (boolean string is expected for '${key}' field). Got ${query}`)
}

const prepareDataForDateFormat = (query: any, key: string) => {
  try {
    return new Date(query)
  } catch (e) {
    throw new BadRequestException(`Validation failed (date format is expected for '${key}' field)`)
  }
}

const prepareDataForStringFormat = (query: any, key: string, value?: StringValue) => {
  if (!isString(query)) {
    throw new BadRequestException(`Validation failed (string format is expected for '${key}' field)`)
  }

  if (value && value.isArray) {
    // Do not pass em[ty string
    return query.split(',').filter(it => it).map(it => removeNewLines(it))
  }

  return query
}

const prepareDataForDateRangeFormat = (query: any, key: string) => {
  try {
    const { from, to } = isObject(query) ? query : JSON.parse(query)
    return {
      from: from ? new Date(from) : undefined,
      to: to ? new Date(to) : undefined,
    }
  } catch (e) {
    throw new BadRequestException(`Validation failed (date range format is expected for '${key}' field)`)
  }
}

const prepareDataForEnumFormat = (query: string, key: string, value?: QueryValueType | EnumValue) => {
  if (!value) {
    throw new InternalServerErrorException('No enum object for enum filter')
  }

  if (!isType<EnumValue>(value, ['enum'])) {
    throw new InternalServerErrorException('No enum object for enum filter')
  }

  if (value.isArray) {
    return isArray(query)
      ? query.map(it => enumFormatNormalizer(it, key, value))
      : query.split(',').map(it => enumFormatNormalizer(trim(it, ' '), key, value))
  }

  return enumFormatNormalizer(query, key, value)
}

const enumFormatNormalizer = (query: any, key: string, value: EnumValue) => {
  if (!Object.values(value.enum).some(it => it.toUpperCase() === query.toUpperCase())) {
    throw new BadRequestException(`Validation failed (Not supported value for '${key}' field)`)
  }
  return query.toUpperCase()
}

const prepareDataForExistenceFormat = (query: any, key: string) => {
  const [field, isExists] = query[key].split(':')
  switch (isExists) {
    case 'true':
      return true
    case 'false':
      return false
    default:
      throw new BadRequestException(`Validation failed (Not supported value for '${key}' field). Should be exist:true or exist:true`)
  }
}
