export enum QueryValueType {
  number = 'NUMBER',
  string = 'STRING',
  arrayOfNumbers = 'ARRAY_OF_NUMBERS',
  arrayOfStrings = 'ARRAY_OF_STRINGS',
  boolean = 'BOOLEAN',
  date = 'DATE',
  dateRange = 'DATE_RANGE',
  enum = 'ENUM',
  existence = 'EXISTENCE',
}

export interface DataType {
  [key: string]: QueryValueType | EnumValue | BooleanValue | StringValue
}

export interface BooleanValue {
  type: QueryValueType.boolean
  isArray?: boolean
  isNullable?: boolean
}

export interface StringValue {
  type: QueryValueType.string
  isArray?: boolean
  isNullable?: boolean
}

export interface EnumValue {
  type: QueryValueType.enum,
  isArray?: boolean
  enum: Record<string, string>
}

export type QueryObject = EnumValue | BooleanValue | StringValue

export interface QueryValue {
  [key: string]: any
}

export class DateRange {
  from?: Date | undefined
  to?: Date | undefined
}

export class NumberRange {
  from?: number | undefined
  to?: number | undefined
  exact?: number | undefined
}
