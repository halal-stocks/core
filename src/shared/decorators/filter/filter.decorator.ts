import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { isNullOrUndefined, isObject } from '../../../utils'
import { DataType, QueryObject, QueryValue, QueryValueType } from './filter.interfaces'
import { filterStrategy } from './filter.strategy'

const getProperValue = (data: DataType, query: QueryValue) => {
  return Object.entries(data)
    .reduce((acc, [key, value]) => {
      if (!isNullOrUndefined(query[key])) {
        const normalizedType: QueryValueType = isObject<QueryObject>(value) ? value.type as QueryValueType : value as QueryValueType
        acc[key] = filterStrategy[normalizedType](query[key], key, value)
      }
      return acc
    }, {})
}

export const Filter = createParamDecorator((data: DataType, ctx: ExecutionContext): QueryValue => {
  const request = ctx.switchToHttp().getRequest()
  return getProperValue(data, request.query)
})
