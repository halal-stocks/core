import { BadRequestException, createParamDecorator, ExecutionContext } from '@nestjs/common'

interface SortRequestDto {
  page?: string
  perPage?: string
  sortBy?: string
}

export interface SortData {
  allowedFieldsForSorting: string[]
}

export enum SortByTypes {
  asc = 'ASC',
  desc = 'DESC'
}

export interface SortDto {
  page?: number
  perPage?: number
  sortBy?: [string, SortByTypes]
  currentLanguage?: any
}

export const Sort = createParamDecorator((data: SortData, ctx: ExecutionContext): SortDto => {
  const req = ctx.switchToHttp().getRequest()

  let result: SortDto = {}
  const query = req.query as SortRequestDto

  if (query.page) {
    const page = parseInt(query.page, 10)
    result.page = page > 0 ? page : 1
  }
  if (query.perPage) {
    const perPage = parseInt(query.perPage, 10)
    result.perPage = perPage > 0 ? perPage : 0
  }

  if (query.sortBy && data.allowedFieldsForSorting) {
    const [key, value] = query.sortBy.split(':')
    const valueAsUpperCase = value.toLocaleUpperCase()
    if (!(valueAsUpperCase === SortByTypes.asc || valueAsUpperCase === SortByTypes.desc)) {
      throw new BadRequestException(`'${value}' command is not allowed for sorting`)
    }
    if (data.allowedFieldsForSorting.indexOf(key) > -1) {
      result.sortBy = [key, valueAsUpperCase] as [string, SortByTypes]
    }
  }

  return result
})
