import { SetMetadata } from '@nestjs/common'

export enum PermissionScope {
  ALL = 'ALL',
  MY = 'MY'
}

export interface PermissionDecorator {
  code: string
  description: string,
  scope?: PermissionScope
}

export const PermissionArray = (permissions: PermissionDecorator[]) => SetMetadata('permission', permissions)
export const Permission = (...permissions: PermissionDecorator[]) => SetMetadata('permission', permissions)
