import { createParamDecorator, ExecutionContext, UnauthorizedException } from '@nestjs/common'

export interface CurrentUser {
  id: string
  fullName: string
  firebaseId: string
  email: string | null,
  isPaid: boolean
}

export type CurrentUserOptional = CurrentUser | undefined

export interface CurrentUserOptions {
  required?: boolean
}

export const CurrentUser: (options?: CurrentUserOptions) => ParameterDecorator = createParamDecorator((options: CurrentUserOptions = {}, ctx: ExecutionContext) => {
  const { required = true } = options || {}
  const req = ctx.switchToHttp().getRequest()
  const user: CurrentUserOptional = req.user
  if (required && !user) {
    throw new UnauthorizedException()
  }
  return user
})
