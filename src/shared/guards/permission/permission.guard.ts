import { CanActivate, ExecutionContext, Inject, Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'

@Injectable()
export class PermissionGuard implements CanActivate {
  @Inject('Reflector') reflector: Reflector

  async canActivate(context: ExecutionContext): Promise<boolean> {
    return true
    // const permissions = this.reflector.get<PermissionDecorator[] | undefined>('permission', context.getHandler())
    // if (!(permissions && permissions.length > 0)) {
    //   return true
    // }
    // const request = context.switchToHttp().getRequest()
    // const currentUser: CurrentUser = request.user
    // if (!currentUser) {
    //   return true
    // }
    // const userPermissions = await this.authService.getUserPermissions(currentUser.id)
    // const foundPermission = permissions.find(permission => userPermissions.some(it => permission.code === it.code))
    // if (!foundPermission) {
    //   return false
    // }
    // request.user.permissionScope = foundPermission.scope
    // return true
  }
}
