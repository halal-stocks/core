import { applyDecorators, UseGuards } from '@nestjs/common'
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger'
import { PermissionArray, PermissionDecorator } from '../../decorators/permission.decorator'
import { AuthSupportingApiKeyGuard } from '../auth-supporting-api-key/auth-supporting-api-key.guard'
import { PermissionGuard } from '../permission/permission.guard'

interface AuthProps {
  isRequired?: boolean,
  permissions?: PermissionDecorator[]
}

export function Auth(props?: AuthProps) {
  const { isRequired = true, permissions = [] } = props || {}

  return applyDecorators(
    PermissionArray(permissions),
    UseGuards(AuthSupportingApiKeyGuard(isRequired), PermissionGuard),
    ApiBearerAuth(),
    ApiUnauthorizedResponse({ description: 'Unauthorized"' }),
  )
}
