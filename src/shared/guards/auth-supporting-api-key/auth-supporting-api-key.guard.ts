import { ExecutionContext, Inject, mixin } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { AuthGuard } from '@nestjs/passport'

export const AuthSupportingApiKeyGuard = (isRequired = true) => {
  class AuthSupportingApiKeyGuardMixin extends AuthGuard('firebase-auth') {
    @Inject(ConfigService) private configService: ConfigService

    async canActivate(context: ExecutionContext): Promise<boolean> {
      try {
        await super.canActivate(context)
        return true
      } catch (e) {
        if (!isRequired) {
          return true
        }
        const request = context.switchToHttp().getRequest()
        const apyKeyFromHeader = request.headers['x-api-key']
        const apyKeyFromEnv = this.configService.get('AUTH_API_KEY')

        if (!(apyKeyFromEnv && apyKeyFromHeader)) {
          return false
        }

        return apyKeyFromEnv === apyKeyFromHeader
      }
    }
  }

  return mixin(AuthSupportingApiKeyGuardMixin)
}
