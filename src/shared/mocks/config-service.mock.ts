export const ConfigServiceMock = {
  get: <T>(value: T) => (key: string) => value,
}
