import { ArgumentMetadata, BadRequestException, Injectable } from '@nestjs/common'
import { isArray } from '../../utils'

@Injectable()
export class ParseIntArrayPipe {
  transform(value: any, metadata: ArgumentMetadata) {
    if (!value) {
      throw new BadRequestException(`Validation failed for query: ${metadata.data} (array of numeric string is expected)`)
    }
    try {
      const parsed = isArray(value) ? value : value.split(',')
      return parsed.map(it => parseInt(it, 10))
    } catch (e) {
      throw new BadRequestException(`Validation failed for query: ${metadata.data} (array of numeric string is expected)`)
    }
  }
}
