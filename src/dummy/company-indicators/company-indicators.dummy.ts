import { MarketCapType, StockPriceType } from '../../consts/report.enum'

export const dummyAppleCompanyIndicators = {
  id: '1',
  actualStockPrice: 135.37 as StockPriceType,
  actualMarketCap: 10_000_000_000 as MarketCapType,
  previousDayStockPrice: 135.13 as StockPriceType,
  previousDayMarketCap: 10_000_000_000 as MarketCapType,
  updatedAt: null,
  company: {
    id: '1',
    ticker: 'AAPL',
  },
}

export const dummyRioCompanyIndicators = {
  id: '1',
  actualStockPrice: 9.42 as StockPriceType,
  actualMarketCap: 10_000_000_000 as MarketCapType,
  previousDayStockPrice: 9.3 as StockPriceType,
  previousDayMarketCap: 10_000_000_000 as MarketCapType,
  updatedAt: null,
  company: {
    id: '2',
    ticker: 'RIO',
  },
}

