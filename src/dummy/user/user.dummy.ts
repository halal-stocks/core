import { CurrentUser } from '../../shared/decorators/current-user.decorator'

export const dummyUserId = '37550312-808d-4d83-ae10-ecf61b741324'
export const dummyUser: CurrentUser = {
  id: dummyUserId,
  firebaseId: 'firebaseId',
  email: 'email',
  isPaid: false,
  fullName: 'fullName',
}

