import { CompanySector } from '../../consts/company-sector'
import { Exchange } from '../../consts/exchange.enum'
import { IsCompanyFreeToUse, IsCompanyHalalType, TickerType } from '../../consts/report.enum'
import { CompanyEntity } from '../../persistence/company/company.entity'
import {
  CompanyBriefResponseModel,
  CompanyShariahFilterLastResultResponseModel,
  CompanyShariahFilterResultResponseModel,
} from '../../persistence/company/company.model'
import { dummyAppleCompanyIndicators } from '../company-indicators/company-indicators.dummy'
import {
  dummyAppleShariahFilterResult,
  dummyRioShariahFilterResult,
} from '../shariah-filter-result/shariah-filter-result.dummy'

export const dummyAppleCompany: Omit<CompanyEntity, 'shariahFilterResults' | 'indicators' | 'stockMarketIndexes' | 'deletedAt' | 'analyzes'> = {
  id: '1',
  name: 'Apple Inc',
  description: '',
  fullTimeEmployees: 1,
  industry: 'Consumer Electronics',
  ticker: 'AAPL' as TickerType,
  sector: CompanySector.TECHNOLOGY,
  exchange: Exchange.NASDAQ,
  isHalal: true as IsCompanyHalalType,
  isFree: true as IsCompanyFreeToUse,
  createdAt: new Date(),
  updatedAt: null,
}

export const dummyRioCompany: Omit<CompanyEntity, 'shariahFilterResults' | 'indicators' | 'stockMarketIndexes' | 'deletedAt' | 'analyzes'> = {
  id: '2',
  name: 'ProPetro',
  description: '',
  fullTimeEmployees: 1,
  industry: 'Consumer Electronics',
  ticker: 'RIO' as TickerType,
  isFree: false as IsCompanyFreeToUse,
  sector: CompanySector.FINANCIAL_SERVICES,
  exchange: Exchange.NASDAQ,
  isHalal: false as IsCompanyHalalType,
  createdAt: new Date(),
  updatedAt: null,
}

export const dummyAppleCompanyShariahFilterResults: CompanyShariahFilterResultResponseModel = {
  id: dummyAppleCompany.id,
  ticker: dummyAppleCompany.ticker,
  shariahFilterResults: [dummyAppleShariahFilterResult],
}

export const dummyAppleCompanyShariahFilterLastResults: CompanyShariahFilterLastResultResponseModel = {
  id: dummyAppleCompany.id,
  ticker: dummyAppleCompany.ticker,
  shariahFilterResult: dummyAppleShariahFilterResult,
}

export const dummyRioCompanyShariahFilterResults: CompanyShariahFilterResultResponseModel = {
  id: dummyRioCompany.id,
  ticker: dummyRioCompany.ticker,
  shariahFilterResults: [dummyRioShariahFilterResult],
}

export const dummyRioCompanyShariahFilterLastResults: CompanyShariahFilterLastResultResponseModel = {
  id: dummyRioCompany.id,
  ticker: dummyRioCompany.ticker,
  shariahFilterResult: dummyRioShariahFilterResult,
}

export const dummyBriefCompaniesBySectorIncludeDailyChangeIndicators: CompanyBriefResponseModel = {
  id: dummyAppleCompany.id,
  ticker: dummyAppleCompany.ticker,
  isHalal: dummyAppleCompany.isHalal,
  isFree: dummyAppleCompany.isFree,
  name: dummyAppleCompany.name,
  indicators: dummyAppleCompanyIndicators,
}
