import {
  CashAndShortTermInvestmentsType,
  CompanyReportDateAsStringType,
  CompanyReportDateType,
  CurrentAssetsMinusLiabilitiesType,
  CurrentAssetsType,
  GrahamRatioType,
  InterestIncomeType,
  LongTermDebtType,
  LongTermInvestmentsType,
  MarketCapType,
  OtherCurrentAssetsType,
  OtherCurrentLiabilitiesType,
  PBRatioType,
  PERatioType,
  RevenueType,
  SharesOutstandingType,
  ShortTermDebtType,
  TickerType,
  TotalLiabilitiesType,
} from '../../consts/report.enum'

export const dummyOtherCurrentAssets = 54326000000 as OtherCurrentAssetsType
export const dummyCashAndShortTermInvestments = 320000000 as CashAndShortTermInvestmentsType
export const dummyLongTermInvestments = 31947000000 as LongTermInvestmentsType
export const dummyMarketCap = 39054000000 as MarketCapType
export const dummyLongTermDebt = 16844000000 as LongTermDebtType
export const dummyShortTermDebt = 75842000000 as ShortTermDebtType
export const dummyOtherCurrentLiabilities = 29558000000 as OtherCurrentLiabilitiesType
export const dummyInterestIncome = 5837000000 as InterestIncomeType
export const dummyTotalRevenue = 42459000000 as RevenueType
export const dummySharesOutstanding = 4630000000 as SharesOutstandingType
export const dummyTicker = 'ABC' as TickerType
export const dummyReportDate = new Date() as CompanyReportDateType
export const dummyReportDateAsString = '2021-05-21' as CompanyReportDateAsStringType
export const dummyTotalCurrentAssets = 54326000000 as CurrentAssetsType
export const dummyTotalLiabilities = 320000000 as TotalLiabilitiesType
export const dummyCurrentAssetsMinusLiabilitiesType = 54006000000 as CurrentAssetsMinusLiabilitiesType
export const dummyPERatio = 5.7 as PERatioType
export const dummyPBRatio = 10.3 as PBRatioType
export const dummyGrahamRatio = 58.71 as GrahamRatioType
