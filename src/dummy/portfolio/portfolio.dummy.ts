import { StockPriceType } from '../../consts/report.enum'
import { dummyAppleCompany, dummyRioCompany } from '../company/company.dummy'
import { dummyPortfolioTradeAppleCompany, dummyPortfolioTradeRioCompany } from './trade/portfolio-trade.dummy'

export const dummyPortfolioList = [
  {
    id: '1',
    boughtStockPrice: 120.4 as StockPriceType,
    quantity: 10,
    createdAt: new Date(),
    updatedAt: null,
    company: dummyAppleCompany,
    trades: [
      dummyPortfolioTradeAppleCompany,
    ],
  },

  {
    id: '1',
    boughtStockPrice: 10.4 as StockPriceType,
    quantity: 23,
    createdAt: new Date(),
    updatedAt: null,
    company: dummyRioCompany,
    trades: [
      dummyPortfolioTradeRioCompany,
    ],
  },
]
