export const dummyPortfolioTradeAppleCompany = {
  id: '1',
  entryStockPrice: 120.4,
  quantity: 10,
  tradedAt: null,
}

export const dummyPortfolioTradeRioCompany = {
  id: '2',
  entryStockPrice: 10.4,
  quantity: 23,
  tradedAt: null,
}
